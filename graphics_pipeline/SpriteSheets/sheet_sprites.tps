<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>5</int>
        <key>texturePackerVersion</key>
        <string>6.0.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>godot3-spritesheet</string>
        <key>textureFileName</key>
        <filename>../../game/assets/graphics/sheet_sprites.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrQualityLevel</key>
        <uint>3</uint>
        <key>astcQualityLevel</key>
        <uint>2</uint>
        <key>basisUniversalQualityLevel</key>
        <uint>2</uint>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1QualityLevel</key>
        <uint>70</uint>
        <key>etc2QualityLevel</key>
        <uint>70</uint>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../game/assets/graphics/sheet_sprites.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">sprites/environment/cloud_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/environment/debri_originals1.png</key>
            <key type="filename">sprites/environment/debri_originals2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,24,16,48</rect>
                <key>scale9Paddings</key>
                <rect>8,24,16,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/environment/explosion_particles_original.png</key>
            <key type="filename">sprites/screen_enemies/floater_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,8,128,16</rect>
                <key>scale9Paddings</key>
                <rect>64,8,128,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/environment/explosions_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>168,24,336,48</rect>
                <key>scale9Paddings</key>
                <rect>168,24,336,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/environment/killed_pickup_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>128,40,256,80</rect>
                <key>scale9Paddings</key>
                <rect>128,40,256,80</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/environment/room_arrow_original.png</key>
            <key type="filename">sprites/player/bullet_spinning_container_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/environment/shooting_star_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,8,64,16</rect>
                <key>scale9Paddings</key>
                <rect>32,8,64,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/environment/spawn_ship_original1.png</key>
            <key type="filename">sprites/environment/spawn_ship_original2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,39,60,78</rect>
                <key>scale9Paddings</key>
                <rect>30,39,60,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/moving/bounce_triangle_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>44,16,88,32</rect>
                <key>scale9Paddings</key>
                <rect>44,16,88,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/moving/rex2_caterpillar_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>60,14,120,28</rect>
                <key>scale9Paddings</key>
                <rect>60,14,120,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/moving/rex2_drip_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,8,168,16</rect>
                <key>scale9Paddings</key>
                <rect>84,8,168,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/moving/rex2_spore_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,16,96,32</rect>
                <key>scale9Paddings</key>
                <rect>48,16,96,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/player/defence_bubble_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,24,48,48</rect>
                <key>scale9Paddings</key>
                <rect>24,24,48,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/player/rex_anims_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>80,32,160,64</rect>
                <key>scale9Paddings</key>
                <rect>80,32,160,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/player/rex_mini_launcher_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,8,24,16</rect>
                <key>scale9Paddings</key>
                <rect>12,8,24,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/projectiles/bullets_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>108,9,216,17</rect>
                <key>scale9Paddings</key>
                <rect>108,9,216,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/screen_enemies/drone_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>96,36,192,72</rect>
                <key>scale9Paddings</key>
                <rect>96,36,192,72</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/screen_enemies/human_r1_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>96,48,192,96</rect>
                <key>scale9Paddings</key>
                <rect>96,48,192,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/screen_enemies/human_r2_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>144,48,288,96</rect>
                <key>scale9Paddings</key>
                <rect>144,48,288,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/screen_enemies/rex2_triangle_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>78,10,156,20</rect>
                <key>scale9Paddings</key>
                <rect>78,10,156,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/screen_enemies/shipman_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,16,128,32</rect>
                <key>scale9Paddings</key>
                <rect>64,16,128,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/screen_enemies/train_derailed_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,16,48,32</rect>
                <key>scale9Paddings</key>
                <rect>24,16,48,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/screen_enemies/train_for_rex.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/screen_enemies/train_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,48,128,96</rect>
                <key>scale9Paddings</key>
                <rect>64,48,128,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/screen_enemies/ufo_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,12,128,24</rect>
                <key>scale9Paddings</key>
                <rect>64,12,128,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites/turret/turrets_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>80,96,160,192</rect>
                <key>scale9Paddings</key>
                <rect>80,96,160,192</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>sprites/environment/cloud_original.png</filename>
            <filename>sprites/environment/debri_originals1.png</filename>
            <filename>sprites/environment/debri_originals2.png</filename>
            <filename>sprites/environment/explosion_particles_original.png</filename>
            <filename>sprites/environment/explosions_original.png</filename>
            <filename>sprites/environment/killed_pickup_original.png</filename>
            <filename>sprites/environment/shooting_star_original.png</filename>
            <filename>sprites/environment/spawn_ship_original1.png</filename>
            <filename>sprites/environment/spawn_ship_original2.png</filename>
            <filename>sprites/moving/bounce_triangle_original.png</filename>
            <filename>sprites/moving/rex2_caterpillar_original.png</filename>
            <filename>sprites/moving/rex2_drip_original.png</filename>
            <filename>sprites/moving/rex2_spore_original.png</filename>
            <filename>sprites/player/bullet_spinning_container_original.png</filename>
            <filename>sprites/player/defence_bubble_original.png</filename>
            <filename>sprites/player/rex_anims_original.png</filename>
            <filename>sprites/player/rex_mini_launcher_original.png</filename>
            <filename>sprites/projectiles/bullets_original.png</filename>
            <filename>sprites/screen_enemies/drone_original.png</filename>
            <filename>sprites/screen_enemies/floater_original.png</filename>
            <filename>sprites/screen_enemies/human_r1_original.png</filename>
            <filename>sprites/screen_enemies/human_r2_original.png</filename>
            <filename>sprites/screen_enemies/rex2_triangle_original.png</filename>
            <filename>sprites/screen_enemies/shipman_original.png</filename>
            <filename>sprites/screen_enemies/train_derailed_original.png</filename>
            <filename>sprites/screen_enemies/train_for_rex.png</filename>
            <filename>sprites/screen_enemies/train_original.png</filename>
            <filename>sprites/screen_enemies/ufo_original.png</filename>
            <filename>sprites/turret/turrets_original.png</filename>
            <filename>sprites/environment/room_arrow_original.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
