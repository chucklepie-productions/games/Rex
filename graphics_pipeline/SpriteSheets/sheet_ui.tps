<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>5</int>
        <key>texturePackerVersion</key>
        <string>6.0.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>godot3-spritesheet</string>
        <key>textureFileName</key>
        <filename>../../game/assets/graphics/sheet_ui.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrQualityLevel</key>
        <uint>3</uint>
        <key>astcQualityLevel</key>
        <uint>2</uint>
        <key>basisUniversalQualityLevel</key>
        <uint>2</uint>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1QualityLevel</key>
        <uint>70</uint>
        <key>etc2QualityLevel</key>
        <uint>70</uint>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../game/assets/graphics/sheet_ui.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">ui/achievements/achievement-border.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>79,30,157,59</rect>
                <key>scale9Paddings</key>
                <rect>79,30,157,59</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/achievements/achievement-bubbles.png</key>
            <key type="filename">ui/achievements/achievement-bunny.png</key>
            <key type="filename">ui/achievements/achievement-faking.png</key>
            <key type="filename">ui/achievements/achievement-first.png</key>
            <key type="filename">ui/achievements/achievement-fulltime.png</key>
            <key type="filename">ui/achievements/achievement-neo.png</key>
            <key type="filename">ui/achievements/achievement-notdone.png</key>
            <key type="filename">ui/achievements/achievement-omwf.png</key>
            <key type="filename">ui/achievements/achievement-onslaught.png</key>
            <key type="filename">ui/achievements/achievement-oxygen.png</key>
            <key type="filename">ui/achievements/achievement-parttime.png</key>
            <key type="filename">ui/achievements/achievement-random.png</key>
            <key type="filename">ui/achievements/achievement-recoil.png</key>
            <key type="filename">ui/achievements/achievement-retro.png</key>
            <key type="filename">ui/achievements/achievement-rex.png</key>
            <key type="filename">ui/achievements/achievement-rooms.png</key>
            <key type="filename">ui/achievements/achievement-shield.png</key>
            <key type="filename">ui/achievements/achievement-sleuth.png</key>
            <key type="filename">ui/achievements/achievement-smart.png</key>
            <key type="filename">ui/achievements/achievement-sure.png</key>
            <key type="filename">ui/achievements/achievement-wdouble.png</key>
            <key type="filename">ui/achievements/achievement-wdrone.png</key>
            <key type="filename">ui/achievements/achievement-wlaser.png</key>
            <key type="filename">ui/achievements/achievement-wmd.png</key>
            <key type="filename">ui/achievements/achievement-wsingle.png</key>
            <key type="filename">ui/achievements/achievement-wspray.png</key>
            <key type="filename">ui/achievements/enemy-bouncer.png</key>
            <key type="filename">ui/achievements/enemy-caterpillar.png</key>
            <key type="filename">ui/achievements/enemy-dome.png</key>
            <key type="filename">ui/achievements/enemy-drip.png</key>
            <key type="filename">ui/achievements/enemy-drone.png</key>
            <key type="filename">ui/achievements/enemy-flat.png</key>
            <key type="filename">ui/achievements/enemy-floater.png</key>
            <key type="filename">ui/achievements/enemy-human.png</key>
            <key type="filename">ui/achievements/enemy-launcher.png</key>
            <key type="filename">ui/achievements/enemy-miniboss.png</key>
            <key type="filename">ui/achievements/enemy-ship.png</key>
            <key type="filename">ui/achievements/enemy-spore1.png</key>
            <key type="filename">ui/achievements/enemy-spore2.png</key>
            <key type="filename">ui/achievements/enemy-spore3.png</key>
            <key type="filename">ui/achievements/enemy-tback.png</key>
            <key type="filename">ui/achievements/enemy-tcart.png</key>
            <key type="filename">ui/achievements/enemy-tfront.png</key>
            <key type="filename">ui/achievements/enemy-triangle.png</key>
            <key type="filename">ui/achievements/enemy-turret.png</key>
            <key type="filename">ui/achievements/enemy-ufo.png</key>
            <key type="filename">ui/achievements/enemy-venus.png</key>
            <key type="filename">ui/achievements/enemy-vine.png</key>
            <key type="filename">ui/achievements/saving.png</key>
            <key type="filename">ui/hud/weapon_inlays_original1.png</key>
            <key type="filename">ui/hud/weapon_inlays_original2.png</key>
            <key type="filename">ui/hud/weapon_inlays_original3.png</key>
            <key type="filename">ui/hud/weapon_inlays_original4.png</key>
            <key type="filename">ui/hud/weapon_inlays_original5.png</key>
            <key type="filename">ui/hud/weapon_inlays_original6.png</key>
            <key type="filename">ui/hud/weapon_inlays_original7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/hud/extra_original1.png</key>
            <key type="filename">ui/hud/extra_original2.png</key>
            <key type="filename">ui/hud/extra_original3.png</key>
            <key type="filename">ui/hud/extra_original4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,8,20,16</rect>
                <key>scale9Paddings</key>
                <rect>10,8,20,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/hud/gauge_bar_original1.png</key>
            <key type="filename">ui/hud/gauge_bar_original2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>96,6,192,12</rect>
                <key>scale9Paddings</key>
                <rect>96,6,192,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/hud/gauge_progress_original.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,4,128,8</rect>
                <key>scale9Paddings</key>
                <rect>64,4,128,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/menu/menu_selected.png</key>
            <key type="filename">ui/menu/mouse.png</key>
            <key type="filename">ui/tv80/selection_items_alt_menu5.png</key>
            <key type="filename">ui/tv80/selection_items_menu4.png</key>
            <key type="filename">ui/tv80/selection_items_off2.png</key>
            <key type="filename">ui/tv80/selection_items_on1.png</key>
            <key type="filename">ui/tv80/selection_items_slider3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/menu/menu_splatter1.png</key>
            <key type="filename">ui/menu/menu_splatter2.png</key>
            <key type="filename">ui/menu/menu_splatter3.png</key>
            <key type="filename">ui/menu/menu_splatter4.png</key>
            <key type="filename">ui/menu/menu_splatter5.png</key>
            <key type="filename">ui/menu/menu_splatter6.png</key>
            <key type="filename">ui/menu/menu_splatter7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>56,56,112,112</rect>
                <key>scale9Paddings</key>
                <rect>56,56,112,112</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/menu/menu_wink.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>89,8,177,15</rect>
                <key>scale9Paddings</key>
                <rect>89,8,177,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/tv80/menu_checkpoint_quit.png</key>
            <key type="filename">ui/tv80/menu_checkpoint_reset.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,31,61,61</rect>
                <key>scale9Paddings</key>
                <rect>31,31,61,61</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/tv80/off.png</key>
            <key type="filename">ui/tv80/on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,8,22,16</rect>
                <key>scale9Paddings</key>
                <rect>11,8,22,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/tv80/power-hover.png</key>
            <key type="filename">ui/tv80/power.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,7,22,14</rect>
                <key>scale9Paddings</key>
                <rect>11,7,22,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/tv80/tv80_aerial.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>213,6,425,12</rect>
                <key>scale9Paddings</key>
                <rect>213,6,425,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ui/tv80/tv80_disabled.png</key>
            <key type="filename">ui/tv80/tv80_hover.png</key>
            <key type="filename">ui/tv80/tv80_off.png</key>
            <key type="filename">ui/tv80/tv80_on.png</key>
            <key type="filename">ui/tv80/tv80_tuner.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,16,14</rect>
                <key>scale9Paddings</key>
                <rect>8,7,16,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>ui/achievements/achievement-border.png</filename>
            <filename>ui/achievements/achievement-bubbles.png</filename>
            <filename>ui/achievements/achievement-bunny.png</filename>
            <filename>ui/achievements/achievement-faking.png</filename>
            <filename>ui/achievements/achievement-first.png</filename>
            <filename>ui/achievements/achievement-fulltime.png</filename>
            <filename>ui/achievements/achievement-neo.png</filename>
            <filename>ui/achievements/achievement-notdone.png</filename>
            <filename>ui/achievements/achievement-omwf.png</filename>
            <filename>ui/achievements/achievement-onslaught.png</filename>
            <filename>ui/achievements/achievement-oxygen.png</filename>
            <filename>ui/achievements/achievement-parttime.png</filename>
            <filename>ui/achievements/achievement-random.png</filename>
            <filename>ui/achievements/achievement-recoil.png</filename>
            <filename>ui/achievements/achievement-retro.png</filename>
            <filename>ui/achievements/achievement-rex.png</filename>
            <filename>ui/achievements/achievement-rooms.png</filename>
            <filename>ui/achievements/achievement-shield.png</filename>
            <filename>ui/achievements/achievement-sleuth.png</filename>
            <filename>ui/achievements/achievement-smart.png</filename>
            <filename>ui/achievements/achievement-sure.png</filename>
            <filename>ui/achievements/achievement-wdouble.png</filename>
            <filename>ui/achievements/achievement-wdrone.png</filename>
            <filename>ui/achievements/achievement-wlaser.png</filename>
            <filename>ui/achievements/achievement-wmd.png</filename>
            <filename>ui/achievements/achievement-wsingle.png</filename>
            <filename>ui/achievements/achievement-wspray.png</filename>
            <filename>ui/achievements/enemy-bouncer.png</filename>
            <filename>ui/achievements/enemy-caterpillar.png</filename>
            <filename>ui/achievements/enemy-dome.png</filename>
            <filename>ui/achievements/enemy-drip.png</filename>
            <filename>ui/achievements/enemy-drone.png</filename>
            <filename>ui/achievements/enemy-flat.png</filename>
            <filename>ui/achievements/enemy-floater.png</filename>
            <filename>ui/achievements/enemy-human.png</filename>
            <filename>ui/achievements/enemy-launcher.png</filename>
            <filename>ui/achievements/enemy-miniboss.png</filename>
            <filename>ui/achievements/enemy-ship.png</filename>
            <filename>ui/achievements/enemy-spore1.png</filename>
            <filename>ui/achievements/enemy-spore2.png</filename>
            <filename>ui/achievements/enemy-spore3.png</filename>
            <filename>ui/achievements/enemy-tback.png</filename>
            <filename>ui/achievements/enemy-tcart.png</filename>
            <filename>ui/achievements/enemy-tfront.png</filename>
            <filename>ui/achievements/enemy-triangle.png</filename>
            <filename>ui/achievements/enemy-turret.png</filename>
            <filename>ui/achievements/enemy-ufo.png</filename>
            <filename>ui/achievements/enemy-venus.png</filename>
            <filename>ui/achievements/enemy-vine.png</filename>
            <filename>ui/achievements/saving.png</filename>
            <filename>ui/hud/extra_original1.png</filename>
            <filename>ui/hud/extra_original2.png</filename>
            <filename>ui/hud/extra_original3.png</filename>
            <filename>ui/hud/extra_original4.png</filename>
            <filename>ui/hud/gauge_bar_original1.png</filename>
            <filename>ui/hud/gauge_bar_original2.png</filename>
            <filename>ui/hud/gauge_progress_original.png</filename>
            <filename>ui/hud/weapon_inlays_original1.png</filename>
            <filename>ui/hud/weapon_inlays_original2.png</filename>
            <filename>ui/hud/weapon_inlays_original3.png</filename>
            <filename>ui/hud/weapon_inlays_original4.png</filename>
            <filename>ui/hud/weapon_inlays_original5.png</filename>
            <filename>ui/hud/weapon_inlays_original6.png</filename>
            <filename>ui/hud/weapon_inlays_original7.png</filename>
            <filename>ui/menu/menu_selected.png</filename>
            <filename>ui/menu/menu_splatter1.png</filename>
            <filename>ui/menu/menu_splatter2.png</filename>
            <filename>ui/menu/menu_splatter3.png</filename>
            <filename>ui/menu/menu_splatter4.png</filename>
            <filename>ui/menu/menu_splatter5.png</filename>
            <filename>ui/menu/menu_splatter6.png</filename>
            <filename>ui/menu/menu_splatter7.png</filename>
            <filename>ui/menu/menu_wink.png</filename>
            <filename>ui/menu/mouse.png</filename>
            <filename>ui/tv80/menu_checkpoint_quit.png</filename>
            <filename>ui/tv80/menu_checkpoint_reset.png</filename>
            <filename>ui/tv80/off.png</filename>
            <filename>ui/tv80/on.png</filename>
            <filename>ui/tv80/power-hover.png</filename>
            <filename>ui/tv80/power.png</filename>
            <filename>ui/tv80/selection_items_alt_menu5.png</filename>
            <filename>ui/tv80/selection_items_menu4.png</filename>
            <filename>ui/tv80/selection_items_off2.png</filename>
            <filename>ui/tv80/selection_items_on1.png</filename>
            <filename>ui/tv80/selection_items_slider3.png</filename>
            <filename>ui/tv80/tv80_aerial.png</filename>
            <filename>ui/tv80/tv80_disabled.png</filename>
            <filename>ui/tv80/tv80_hover.png</filename>
            <filename>ui/tv80/tv80_off.png</filename>
            <filename>ui/tv80/tv80_on.png</filename>
            <filename>ui/tv80/tv80_tuner.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
