extends Node2D

signal fireworks_ended

var orchestration_array = []
var orchestration_index = 0

onready var timer = $Timer
onready var screen_width = 960 #get_viewport().size.x
onready var screen_height = 672 #get_viewport().size.y

func _ready():
	orchestration_array = temp_show().split("\n")
	#orchestration_array = _get_show().split("\n")
	continue_orchestration()

func _on_Timer_timeout():
	continue_orchestration()
	return

func continue_orchestration():
	if orchestration_index >= 0:
		var instruction = orchestration_array[orchestration_index]
		orchestration_index += 1
		if orchestration_index >= orchestration_array.size():
			orchestration_index = -1
		execute_instruction(instruction)


func execute_instruction(instruction):
	var result = instruction.split(" ", false)
	if result!=null && result.size()>0 && result[0]=="end":
		#emit_signal("fireworks_ended")
		queue_free()
		
	if result == null || result.size() < 2 || result[0].begins_with("#"):
		continue_orchestration()
		return
	var command = result[0].to_lower()
	if command == "fountain" || command == "rocket" || command == "flare" || command == "wheel":
		var x = float(result[1])
		var newFirework = load("res://fireworks/" + command.capitalize() + ".tscn").instance()
		newFirework.position = Vector2(screen_width * x, screen_height)
		for i in range(2, result.size()):
			var set = result[i].split(":")
			if (set.size() == 2):
				newFirework.set_attribute(set[0], set[1])
		add_child(newFirework)
	
	if command == "wait":
		var time_ms = float(result[1])
		timer.wait_time = time_ms / 1000
		timer.start()
	else:
		continue_orchestration()

func temp_show():
	#$FinalBoss_FakeAndReal.enable(true)
	return """
# big bang
# use this to prepare your personal fireworks
# see instructions at the right side

# let's start simple


wait 4000
wheel 0.5 height:0.6 size:2.5 count:7 color:0 lifetime:5
wait 50
wheel 0.5 height:0.6 size:2.4 count:4 color:2 lifetime:6
wait 3000

fountain 0.2 color:1 size:1.0
fountain 0.5 color:3 size:1.0
fountain 0.8 color:5 size:1.0
wait 1000

# flares
flare 0.2 lifetime:7
flare 0.8 lifetime:7

# rockets at the sides
rocket 0.2
rocket 0.8
wait 100
rocket 0.2
rocket 0.8
wait 3000



# big bang
rocket 0.5 effect:cluster
wait 3000
rocket 0.5 effect:glitter
wait 100
rocket 0.75 effect:glitter
wait 100
rocket 0.25 effect:glitter
wait 7000



end
"""

	
func _get_show():
	return """
# big bang
# use this to prepare your personal fireworks
# see instructions at the right side

# let's start simple

fountain 0.2 color:1 size:1.0
fountain 0.8 color:1 size:1.0
end

wait 1000
rocket 0.5 arandom:0
wait 500
rocket 0.6 arandom:0 angle:1.0
wait 500
rocket 0.4 arandom:0 angle:-1.0
wait 2000
rocket 0.1 color:0
rocket 0.9 color:0
wait 3000

# now more rockets
rocket 0.5 color:3
wait 500
rocket 0.45 color:3
wait 500
rocket 0.55 color:3
wait 500
fountain 0.1 color:0
wait 500
fountain 0.3 color:2
wait 500
fountain 0.5 color:3
wait 500
fountain 0.7 color:4
wait 500
fountain 0.9 color:5
wait 500

# rockets at the sides
rocket 0.2
rocket 0.8
wait 100
rocket 0.2
rocket 0.8
wait 100
rocket 0.2
rocket 0.8
wait 3000

# rockets closer to the middle
rocket 0.4
rocket 0.6
wait 100
rocket 0.4
rocket 0.6
wait 100
rocket 0.4
rocket 0.6
wait 3000

# flares
flare 0.2 lifetime:7
flare 0.4 lifetime:7
flare 0.6 lifetime:7
flare 0.8 lifetime:7

# rocket zig zag
rocket 0.2
wait 300
rocket 0.4
wait 300
rocket 0.6
wait 300
rocket 0.8
wait 300
rocket 0.6
wait 300
rocket 0.4
wait 300
rocket 0.2
wait 300
rocket 0.4
wait 300
rocket 0.6
wait 300
rocket 0.8
wait 3000

# big bang
rocket 0.5 effect:cluster
wait 3000
rocket 0.5 effect:glitter
wait 100
rocket 0.75 effect:glitter
wait 100
rocket 0.25 effect:glitter
wait 7000



end
"""
