extends Control

var character_data = {
	'name': 'Default',
	'image': "res://addons/dialogic/Images/portraits/df-3.png",
	'color': Color(0.973511, 1, 0.152344)
}
var positions = {'left': Vector2(-250,0), 'right': Vector2(+100,0), 'center': Vector2(0,0)}
var direction = 'left'
var debug = false
var initial_position=Vector2.ZERO

func init(position_offset = 'left'):
	rect_position += positions[position_offset]
	direction = position_offset
	modulate = Color(1,1,1,0)
	if character_data.image == null:
		push_error('The DialogCharacterResource [' + character_data.name + '] doesn\'t have an Image set.')
		character_data.image = load("res://addons/dialogic/Images/portraits/df-1.png")
	$TextureRect.texture = character_data.image
	rect_position -= Vector2($TextureRect.texture.get_width() * 0.5, $TextureRect.texture.get_height())
	initial_position=rect_position

func _ready():
	if debug:
		print('Character data loaded: ', character_data)
		print(rect_position, $TextureRect.rect_size)
		yield(get_tree().create_timer(2), "timeout")
		focusout()
		yield(get_tree().create_timer(2), "timeout")
		focus()
		yield(get_tree().create_timer(2), "timeout")
		fade_out()

func fade_in(node = self, time = 1.0):
	tween_modulate_with_tweener(Color(1,1,1,0), Color(1,1,1,1), time)
	rect_position=initial_position
	var end_pos = Vector2(0, -40) # starting at center
	if direction == 'right':
		end_pos = Vector2(+40, 0)
	elif direction == 'left':
		end_pos = Vector2(-40, 0)
	
	var tween_node = create_tween()
	tween_node.tween_property(node,"rect_position",node.rect_position + end_pos,time).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)

func fade_out(node = self, time = 0.5):
	tween_modulate_with_tweener(modulate, Color(1,1,1,0), time, true)

func focus():
	fade_in()

func focusout():
	tween_modulate_with_tweener(modulate, Color(0.5,0.5,0.5,0.0))

func tween_modulate_with_tweener(from_Value, to_value, time = 0.5, to_delete:=false):
	var tween_node = create_tween()
	modulate = from_Value
	tween_node.tween_property(self,"modulate",to_value,time).set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_IN_OUT)
	if to_delete:
		tween_node.tween_callback(self,"queue_free")
