extends Resource
class_name DialogCharacterResource

export var name : String
export var image : Texture
export var color : Color
export var intro_ditty : String
