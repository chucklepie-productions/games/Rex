extends Panel

signal score_selected(pos)

onready var pos=$MarginContainer/HBoxContainer/MarginContainer/VBoxContainer/HBoxContainer/Position

onready var person=$MarginContainer/HBoxContainer/MarginContainer/VBoxContainer/HBoxContainer/Name

onready var time=$MarginContainer/HBoxContainer/MarginContainer/VBoxContainer/HBoxContainer/Time

onready var date=$MarginContainer/HBoxContainer/MarginContainer/VBoxContainer/Date

func _ready() -> void:
	pass # Replace with function body.


func set_data(position:int,id:String,value:String,timestamp:String):
	pos.text=str(position)
	
	if position==1:
		person.bbcode_text="[shake rate=5 level=15][color=#ff00ff]"+id+"[/color][/shake]"
	else:
		person.bbcode_text=id
	time.text=value
	date.text=timestamp
	
func get_entry_position():
	return pos.text

func _on_MarginContainer_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton && event.button_index==1:
		emit_signal("score_selected",pos.text)
