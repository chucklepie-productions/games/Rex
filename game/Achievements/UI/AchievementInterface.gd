extends MarginContainer
#NOT FOUND
#rex
#miniboss?
#
#FOUND (not checked)
#back
#bouncer
#bubbles
#bunny
#caterpillar
#dome
#double
#drip
#drone
#edrone
#energy
#faking
#first
#flat
#floater
#front
#fulltime
#human
#laser
#middle
#missile
#neo
#onslaught
#oxygen
#parttime
#random
#recoil
#retro
#rooms
#shield
#ship
#single
#sleuth
#smart
#spore
#spore2
#spore3
#spray
#sure
#triangle
#turret
#ufo
#venus
#vine
#wmd


export(bool) var is_achievement_type=true
const ACHIEVEMENT_ITEM = "res://Achievements/UI/AchievementItem.tscn";
const INTERFACE_SCREEN = "res://Achievements/UI/AchievementInterface.tscn";

onready var title=$VBoxContainer/Panel/MarginContainer/RichTextLabel
var achievement_interface = null;

var achievementPanel = null;
var achievementExtendedText = null
var achievementExtendedContainer=null
var achievementsNodes = {};
var popup=preload("res://Achievements/UI/AchievementPopup.tscn")

func _ready() -> void:
	pass

func init(achievements):
	var first=true
	achievementPanel = $VBoxContainer/ScrollContainer/VBoxContainer;
	achievementExtendedText=$VBoxContainer/Panel/MarginContainer/RichTextLabel
	achievementExtendedContainer=$VBoxContainer/Panel/
	#var has_info=false
	for i in achievements:
		if (achievements[i].is_achievement_type() && is_achievement_type) || (!achievements[i].is_achievement_type() && !is_achievement_type):
			var achievement = load(ACHIEVEMENT_ITEM).instance();
			achievement.set_achievement(achievements[i]);
			achievementsNodes[i] = achievement;
			#if achievements[i].is_complete():
				#has_info=true	#if first entry is completed
			achievementPanel.add_child(achievement);
			
			if first:
				_on_selected_item(achievement.achievement_name,false,false)
			first=false
			achievement.connect("achievement_selected",self,"_on_selected_item")
	
func _on_selected_item(name,is_pressed:=true,play_sound:=true):
	achievementExtendedContainer=$VBoxContainer/Panel/
	for i in achievementPanel.get_children():
		var p=i.get("custom_styles/panel")
		if i.achievement_name==name:
			achievementExtendedContainer.visible=i.is_complete
			p.bg_color=Color(0,0,192.0/256.0)
			if i.is_complete:
				achievementExtendedText.bbcode_text=i.extended
				if play_sound && is_pressed:
					AudioManager.play_sfx(Globals.SFX_TV80_CLICK)
			else:
				achievementExtendedText.bbcode_text="Description shown when found/achieved"
				if play_sound && is_pressed:
					AudioManager.play_sfx(Globals.SFX_MENU_CANCEL)
		else:
			p.bg_color=Color(0,0,0)
	
func _update_bar(achievement_name, achievement):
	if achievementsNodes.has(achievement_name):
		achievementsNodes[achievement_name].update_progress_bar(achievement)
	
func _completed(name,path,no_popup,x_offset:=0):
	if no_popup:
		return
		
	var p=popup.instance()
	add_child(p)
	p.set_position_x_offset(x_offset)
	p.show_achievement(name,path)

func show_checkpoint_saved(message="",offset_box:=0):
	var p=popup.instance()
	add_child(p)
	p.show_save(message,offset_box)
	
func reset_achievement(achievement_data,achievement_name,reset_if_complete:=false):
	var achievements = achievement_data.get_achievements();
	if achievements.has(achievement_name):
		if !achievements[achievement_name].is_complete() || reset_if_complete:
			achievements[achievement_name].reset()
			achievement_data.save();
			_update_bar(achievement_name,achievements[achievement_name])
	
func increment_achievement(achievement_data,achievement_name, amount, amount_string:="", no_popup=false, x_offset:=0,achievement_int=null, sfx_if_already_got:=""):
	#some achievements have strings as progress indicator
	#this code is only for this interface
	#but because we update rex directly here we want to update
	#that achievement interface not the log
	
	var achievements = achievement_data.get_achievements();
	if !achievements.has(achievement_name):
		print("ERROR! achievement does not exist "+str(achievement_name))
		return
		
	var current=achievements[achievement_name].get_progress()
	
	if achievements[achievement_name].is_complete():
		if sfx_if_already_got!="":
			AudioManager.play_sfx(sfx_if_already_got)
			
	if !achievements[achievement_name].increment(amount,amount_string):
		#already done or not applicable
		return
		
	achievement_data.save();
	_update_bar(achievement_name,achievements[achievement_name])
	
	if !achievements[achievement_name].is_complete():
		return
		
	if current>=achievements[achievement_name].total:	#only trigger once
		return
		
	_completed(achievements[achievement_name].get_name(),achievements[achievement_name].get_picture(),no_popup,x_offset)
	_on_selected_item(achievements[achievement_name].title,false,false)

	achievements["rex"].increment(1,"")
	if achievement_int==null:
		_update_bar("rex",achievements["rex"])
	else:
		achievement_int._update_bar("rex",achievements["rex"])
	achievement_data.save();
		
	#check if all completed
	if achievements["rex"].get_progress()==achievements["rex"].total:
		yield(get_tree().create_timer(7.0), "timeout")
		_completed("I am Rex",achievements["rex"].get_picture(),no_popup,x_offset)
		achievement_data.save();


func get_focus():
	var item=achievementPanel.get_child(1)
	item.grab_focus()
