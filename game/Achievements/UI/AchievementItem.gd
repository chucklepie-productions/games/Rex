extends Panel

signal achievement_completed(name,path)
signal achievement_selected(name)

var progress_bar:TextureProgress = null;
var progress_text:Label = null;
var picture_parent = null;
var picture = null;
var achievement_name=""
var extended=""
var is_complete=false

func set_achievement(achievement):
	achievement_name=achievement.get_name()
	extended=achievement.get_info()
	progress_bar = $MarginContainer/HBoxContainer/MarginContainer/VBoxContainer/MarginContainer/TextureProgress;
	progress_text = $MarginContainer/HBoxContainer/MarginContainer/VBoxContainer/MarginContainer/TextureProgress/Label;
	picture_parent = $MarginContainer/HBoxContainer/Picture;
	picture = $MarginContainer/HBoxContainer/Picture/TextureRect;
	
	set_text(achievement);
	update_progress_bar(achievement);
	set_or_remove_picture(achievement);
	is_complete=achievement.is_complete()
	progress_bar.max_value=achievement.get_total()
	if achievement.get_total()<2 && !achievement.is_complete():
		progress_bar.visible=false

func set_text(achievement):
	$MarginContainer/HBoxContainer/MarginContainer/VBoxContainer/Name.set_text(achievement.get_name());
	$MarginContainer/HBoxContainer/MarginContainer/VBoxContainer/Description.set_text(achievement.get_desc());

func update_progress_bar(achievement):
	#progress_bar.max_value = achievement.get_total();
	progress_bar.value = achievement.get_progress();
	update_progress_bar_text(achievement)

func update_progress_bar_text(achievement):
	if achievement.get_progress() >= achievement.get_total():
		set_or_remove_picture(achievement,true)
		
		show_complete(achievement)
	else:
		progress_text.set_text("%s / %s" % [achievement.get_progress(), achievement.get_total()]);
		
func show_complete(achievement):
	progress_bar.texture_under=null
	progress_bar.texture_progress=null
	progress_text.align=Label.ALIGN_LEFT
	progress_text.modulate=Color(0,192.0/255.0,0)
	progress_text.set_text("Completed!");
	is_complete=true
	emit_signal("achievement_completed",achievement.get_name(),achievement.get_picture())
	progress_bar.visible=true

func set_or_remove_picture(achievement, completed:=false):
	if achievement.get_picture()=="":
		
		picture_parent.queue_free();
		return;
	
	if completed:
		picture.texture=load(achievement.get_picture())

func _on_MarginContainer_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton && event.button_index==1:
		emit_signal("achievement_selected",achievement_name,event.pressed)
