extends Tabs

onready var score_location=find_node("VBoxContainerScores",true,true)
var popup=preload("res://Achievements/UI/HighScoreEntry.tscn")
var highscores={}

func _ready() -> void:
	init()

func init():
	if score_location==null:
		return
		
	if highscores.size()==0:
		highscores=_get_scores()
	
	var p=1
	for i in highscores:
		var hs = popup.instance();
		score_location.add_child(hs);
		hs.set_data(p,i["name"],i["time"],i["date"])
		p+=1
		
		if p==1:
			_on_selected_item("1")
		hs.connect("score_selected",self,"_on_selected_item")

func _on_selected_item(pos:String):
	for i in score_location.get_children():
		var p=i.get_entry_position()
		var pan=i.get("custom_styles/panel")
		if p==pos:
			pan.bg_color=Color(0,0,192.0/256.0)
		else:
			pan.bg_color=Color(0,0,0)
	
func get_focus():
	var item=score_location.get_child(1)
	if item!=null:
		item.grab_focus()

func _get_scores():
	var temp=[
		{
			"name": "Neil",
			"time": "00:40:23",
			"date": "25 Jul 2021"
		},
		{
			"name": "Bob",
			"time": "00:41:23",
			"date": "26 Jul 2021"
		},
		{
			"name": "Jim",
			"time": "00:42:23",
			"date": "27 Jul 2021"
		},
		{
			"name": "Jim",
			"time": "00:42:23",
			"date": "27 Jul 2021"
		},
		{
			"name": "Jim",
			"time": "00:42:23",
			"date": "27 Jul 2021"
		},
		{
			"name": "Jim",
			"time": "00:42:23",
			"date": "27 Jul 2021"
		},
		{
			"name": "Jim",
			"time": "00:42:23",
			"date": "27 Jul 2021"
		},
		{
			"name": "Jim",
			"time": "00:42:23",
			"date": "27 Jul 2021"
		},
		{
			"name": "Jim",
			"time": "00:42:23",
			"date": "27 Jul 2021"
		},
		{
			"name": "Jim",
			"time": "00:42:23",
			"date": "27 Jul 2021"
		},
		{
			"name": "Jim",
			"time": "00:42:23",
			"date": "27 Jul 2021"
		},
		{
			"name": "Jim",
			"time": "00:42:23",
			"date": "27 Jul 2021"
		}
	]
	return temp
