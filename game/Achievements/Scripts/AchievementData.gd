extends Node

const DEFAULT_ACHIEVEMENT_DATA_FILE = "res://Achievements/Data/Default_Achievement_File.json";
const ACTUAL_ACHIEVEMENT_DIRECTORY = "res://achievements/";
const ACTUAL_ACHIEVEMENT_DATA_FILE = ACTUAL_ACHIEVEMENT_DIRECTORY + "Actual_Achievement_File.json";
const ACHIEVEMENT_SCRIPT = "res://Achievements/Scripts/Achievement.gd";

var achievements = {};

func _init():		
	achievements = get_default_achievements();	#"neo","first","rooms", etc
	load_actual_achievements();
	save();

func get_default_achievements():
	var file = File.new();
	file.open(DEFAULT_ACHIEVEMENT_DATA_FILE, File.READ);
	var data = parse_json(file.get_as_text());
	
	for key in data:
		var achievement = load(ACHIEVEMENT_SCRIPT).new(data[key]);
		data[key] = achievement;
	
	return data;

func load_actual_achievements():
	#load actual achievements
	#if brand new file or empty then default values will be kept
	var file = File.new();
	
	if !file.file_exists(ACTUAL_ACHIEVEMENT_DATA_FILE):
		print("ERROR/WARNING. Achievement file not found. Maybe it's the first time...")
		return

	file.open(ACTUAL_ACHIEVEMENT_DATA_FILE, File.READ);
	var data = parse_json(file.get_as_text());
	
	if data == null:
		print("ERROR. Achievement file opened but no data, possible parse error")
		return

	for achievementName in data:
		if achievements[achievementName] == null:
			continue
			
		#copy loaded data into current achievements, which is the default values
		if achievements[achievementName].persist:
			achievements[achievementName].progress=data[achievementName].progress
			achievements[achievementName].total=data[achievementName].total
			achievements[achievementName].override=data[achievementName].override

func save():
	var dictSave = {};
	
	for entry in achievements:
		dictSave[entry] = {};
		if achievements[entry].persist:
			dictSave[entry].progress = achievements[entry].get_progress()
		else:
			dictSave[entry].progress = 0

		dictSave[entry].total = achievements[entry].get_total()
		dictSave[entry].override = achievements[entry].get_override()
		dictSave[entry].persist_progress = achievements[entry].get_persist()

	var directory = Directory.new();
	directory.make_dir(ACTUAL_ACHIEVEMENT_DIRECTORY);

	var file = File.new();
	file.open(ACTUAL_ACHIEVEMENT_DATA_FILE, File.WRITE);
	file.store_string(to_json(dictSave));
	file.close();

func get_achievements():
	return achievements;
