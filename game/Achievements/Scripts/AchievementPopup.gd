extends CanvasLayer

onready var icon=$MarginContainer/MarginContainer/VBoxContainer/HBoxContainer/TextureRect
onready var text=$MarginContainer/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/Label
onready var text_title=$MarginContainer/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/Label2
func _ready() -> void:
	$AnimationPlayer.play("default")

func set_position_x_offset(value):
	$MarginContainer.rect_position.x+=value
	
func show_achievement(achievement_name,image_path):
	var file=File.new()
	if image_path!="" && file.file_exists(image_path):
		icon.texture=load(image_path)
		
	text.text=achievement_name
	$AnimationPlayer.play("reveal")

func show_save(message,offset_box:=0):
	if offset_box:
		$MarginContainer.rect_position.x+=offset_box
	var image_path="res://Achievements/Images/saving.png"
	var file=File.new()
	if file.file_exists(image_path):
		icon.texture=load(image_path)
	text.text=message
	text_title.text="Game Saved"
	$AnimationPlayer.play("reveal")
	
func play_audio():
	AudioManager.play_sfx(Globals.SFX_GAME_ACHIEVEMENT)
