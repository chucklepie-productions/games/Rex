extends Node

var title:String=""	#name
var desc:String=""
var progress:int=0
var total:int=0
var persist:bool=true
var picture:String=""
var info:String=""
var type:String=""
var override:String=""
var must_match:String=""

var completed=false

func _init(value):
	title=_get_value_string("Name not set",value["name"])
	desc=_get_value_string("Description not set",value["desc"])
	progress=_get_value_int(0,value["progress"])
	total=_get_value_int(1,value["total"])
	persist=_get_value_bool(true,value["persist_progress"])
	picture=_get_value_string("",value["picture"])
	info=_get_value_string("",value["extended_info"])
	type=_get_value_string("Achievement",value["achievement_type"])
	override=_get_value_string("",value["progress_match_override"])
	must_match=_get_value_string("",value["must_match"])

func _get_value_string(default,value):
	if value==null || str(value)=="":
		return default
	return value
		
func _get_value_int(default,value):
	if value==null || str(value)=="":
		return default
	return int(value)

func _get_value_bool(default,value):
	if value==null || str(value)=="":
		return default
	return bool(value)

func is_achievement_type():
	return _get_value_string("Achievement",type)=="Achievement"
func get_name():
	return title
func get_desc():
	return desc
func get_progress():
	return progress 
func get_persist():
	return persist
func get_info():
	return info
func get_total():
	return total
func get_picture():
	return picture
func get_type():
	return type
func get_override():
	return override

func reset():
	progress = 0
	
func increment(amount,amount_string):
	#if have achievement string, made up of multiple parts the this is used as a check
	#e.g. "Room1Room2Room3" may be the goal and we pass in "Room2"
	#we still use total and progress
	var incrementing=true
	if amount_string!="" && must_match!="":
		if override.findn(amount_string)>=0:
			incrementing=false
		else:
			override+=amount_string
			
	if incrementing:
		if progress<total:
			progress+=amount;
			if override!="":
				#print("total   : "+must_match)
				#print("progress: "+override)
				pass
			else:
				pass
				#print("No override: " + amount_string)
			return true
			
		else:
			return false
	else:
		return false

func is_complete():
	return progress>=total
	
