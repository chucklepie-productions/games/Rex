extends Weapon

const BULLET_OFFSET:=37
const ANGLE_E=0
const ANGLE_SE=45
const ANGLE_S=90
const ANGLE_SW=135
const ANGLE_W=180
const ANGLE_NW=225
const ANGLE_N=270
const ANGLE_NE=315
const SPRAY_ANGLE_INCREMENT=7.5
onready var single_bullet=preload("res://entities/projectiles/Player_Bullet_Spray.tscn")
onready var homing=preload("res://entities/projectiles/Missile_Launcher_Missile_Homing.tscn")
var can_fire_missile:=true

#scatter
#fire all the time with gap
#1 mag 3 way start together then expands at same angle
#2 green 5
#3 cyan 7 
#4 yellow - 4 way 
#5 white 8 directions
#rapid fire gives 100 level 5 shots

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$Launcher.visible=false

func _unhandled_input(event: InputEvent) -> void:
	if !is_firing && !secondary_fire:
		return

	if event.is_action_pressed("fire") && event.is_pressed():
		fire()

	
func _physics_process(_delta: float) -> void:
	#if Input.is_action_just_pressed("fire"):
	#	fire()
	pass
			
func fire():
	var direction=1
	
	if player:
		direction=player.last_direction
		
	fire_bullet(direction,single_bullet)
	fire_missile()
	if !override_fire_cost:
		#essentially the same as level_never_decreases, different intent
		#used for drones bullets to ignore code or for temporary use
		.bullet_decrease()
	
func fire_missile():
	
	if current_level!=4:
		return
		
	if !can_fire_missile:
		return
		
	
	if GlobalTileMap.get_random_screen_enemy()==null:
		return
		
	if !player._check_is_on_ground_raycast():
		return
		
	can_fire_missile=false
	$HomingDelayTimer.start()
	var direction=1
	var dir="right"
	if player:
		direction=player.last_direction
	if direction==-1:
		dir="left"
		
	if randf()<0.5:
		$AnimationPlayer.play("minilauncher_popup_"+dir)
	else:
		$AnimationPlayer.play("minilauncher_swivel_"+dir)
	yield(get_tree().create_timer(0.5), "timeout")
	
		
	var missile_object=homing.instance()
	_add_bullet(missile_object)
	missile_object.global_position=global_position+Vector2(direction*5,-46)
	missile_object.launch(direction)
	AudioManager.play_sfx(Globals.SFX_WEAPON_SPRAYMISSILE)

func fire_bullet(direction, _bullet):
	#have to wait
	if fire_paused:
		AudioManager.play_sfx(Globals.SFX_WEAPON_NOBULLETS)
		return
	var level=current_level
	if rapid_fire>0:
		level=5
		
	AudioManager.play_sfx(Globals.SFX_WEAPON_SPRAY)
	match level:
		1:
			#3 way
			_fire_spray(direction,[0,SPRAY_ANGLE_INCREMENT,
				-SPRAY_ANGLE_INCREMENT])
		2:
			#5 way
			_fire_spray(direction,[0,SPRAY_ANGLE_INCREMENT,
						SPRAY_ANGLE_INCREMENT*2, 
						-SPRAY_ANGLE_INCREMENT,-SPRAY_ANGLE_INCREMENT*2])
		3:
			#7 way
			_fire_spray(direction,[0,SPRAY_ANGLE_INCREMENT,
					SPRAY_ANGLE_INCREMENT*2, SPRAY_ANGLE_INCREMENT*3, 
					-SPRAY_ANGLE_INCREMENT,-SPRAY_ANGLE_INCREMENT*2,
					-SPRAY_ANGLE_INCREMENT*3])
		4:
			#orthogonal
			_fire_directions([ANGLE_N, ANGLE_E,ANGLE_S, ANGLE_W])
		5:
			#8 way
			_fire_directions([ANGLE_N, ANGLE_E,ANGLE_S, ANGLE_W,
					ANGLE_NE, ANGLE_NW, ANGLE_SE, ANGLE_SW])

func _fire_spray(direction, positions):
	#get each position, turn to angle and call _fire_single
	var start_angle=0 if direction==1 else 180
	var offset=BULLET_OFFSET if direction==1 else -BULLET_OFFSET
	var speed_factor
	for point in positions:
		if point==0:
			speed_factor=1
		else:
			#get the hypotenuse of the angle as a normal
			speed_factor=1.0/cos(deg2rad(abs(point)))
		_fire_single(offset,0,start_angle+point,speed_factor)
	
func _fire_directions(positions):
	#get each position, turn to angle and call _fire_single
	var speed_factor=1
	for point in positions:
		#get the hypotenuse of the angle as a normal as above
		if [ANGLE_NE,ANGLE_NW,ANGLE_SE,ANGLE_SW].has(point):
			speed_factor=1.0/cos(deg2rad(abs(45)))
		match point:
			ANGLE_N:
				_fire_single(0,-BULLET_OFFSET,point,speed_factor)
			ANGLE_NE:
				_fire_single(BULLET_OFFSET,-BULLET_OFFSET,point,speed_factor)
			ANGLE_E:
				_fire_single(BULLET_OFFSET,0,point,speed_factor)
			ANGLE_SE:
				_fire_single(BULLET_OFFSET,BULLET_OFFSET,point,speed_factor)
			ANGLE_S:
				_fire_single(0,BULLET_OFFSET,point,speed_factor)
			ANGLE_SW:
				_fire_single(-BULLET_OFFSET,BULLET_OFFSET,point,speed_factor)
			ANGLE_W:
				_fire_single(-BULLET_OFFSET,0,point,speed_factor)
			ANGLE_NW:
				_fire_single(-BULLET_OFFSET,-BULLET_OFFSET,point,speed_factor)
	
func _fire_single(offx,offy,angle,speed_factor):
	var new_bullet = single_bullet.instance()
	new_bullet.self_rotate_speed=360

	_add_bullet(new_bullet)
	new_bullet.global_position=Vector2(global_position.x+offx,global_position.y+offy)
	new_bullet.set_angle(angle)
	
	new_bullet.set_speed(new_bullet.speed*speed_factor)
	
	new_bullet.start(false)

func _add_bullet(bullet):
	var a1=_bullet_container
	if is_instance_valid(a1):
		a1.add_child(bullet)
	else:	
		add_child(bullet)
		print("ERROR. bullet container not set")


func _on_HomingDelayTimer_timeout() -> void:
	can_fire_missile=true
