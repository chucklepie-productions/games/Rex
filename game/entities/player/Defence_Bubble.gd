extends Node2D
#defence bubble
#human has 0 point, i.e. anything will kill
#bubble 100 points, depletes in 30 seconds
#bubble gives 100 damage, i.e. will kill all enemies immediately except mini boxx
#enemy hitboxes give damage as follows:
#hit by missile or weapon 5 points
#10 points for humans and ships
#hit static enemies 30 points
#don't die, just goes to zero
#always kills enemy - except mini

signal bubble_depleted
signal new_bubble_level(level)

#the defence bubble area2d is only for detecting missiles
#everything else works from the player collision
#how long it lasts in seconds, being hit removes percentages
const BUBBLE_TIME:float=30.0
const MAX_HEALTH:=100
const TIMER_DEC:=BUBBLE_TIME/MAX_HEALTH
var health_percentage:float=100.0
var health_warning:=false

onready var bubble:Sprite=$DefenceBubble
onready var hurt=$Hurtbox
onready var bubble_timer=$Timer

var charging:=false
func _ready() -> void:
	full_tank()
	set_defence_bubble(false)
	_shockwave(false)
	health_warning=false

#everything goes through here
func set_new_health(health):
	#typically called by energy pod or here
	#bubble_material.set_shader_param("value",float(health)/MAX_HEALTH+0.2)
	var i=10-int(health/10)-1
	bubble.frame=int(clamp(i,0,9))
	hurt.health=int(health)
	health_percentage=health
	$Label.text=str(hurt.health)
	if health>20:
		health_warning=false
	
	if hurt.health<20 and !health_warning:
		AudioManager.play_sfx(Globals.SFX_GAME_WARNING)
		health_warning=true
	if hurt.health<=0:
		hurt.health=0
		bubble_timer.stop()
		emit_signal("bubble_depleted")
		AudioManager.play_sfx(Globals.SFX_GAME_DEFENCE_GONE)
	emit_signal("new_bubble_level",int(hurt.health))
	
func get_health():
	return ceil(hurt.health)

func full_tank():
	set_new_health(MAX_HEALTH)

func shield_charging(enabled):
	charging=enabled
		
func _on_new_shield_level(new_level):
	#from signal from pod
	set_new_health(new_level)
	
func set_defence_bubble(is_on:bool):
	#called when turning bubble on and off by player
	#and on startup
	bubble_timer.wait_time=TIMER_DEC
	if is_on && hurt.health>0.1:
		$Hitbox.enable()
		#this resets value so need to quickly set it back
		var temp=hurt.health
		$Hurtbox.enable()
		hurt.health=temp
		visible=true
		bubble_timer.start()
		
		#get shield achievement, but speed things up, only check once
		if !Globals.used_shield_this_game:
			Globals.used_shield_this_game=true
			OptionsAndAchievements.increment_log("shield",1)
	else:
		$Hitbox.disable()
		$Hurtbox.disable()
		#print("off bubble health " + str(hurt.health)+", time left " + str($Timer.time_left))
		bubble_timer.stop()
		visible=false
		
func _on_Timer_timeout() -> void:
	#timer is 1%
	bubble_timer.wait_time=TIMER_DEC
	if !charging:
		health_percentage-=1
	set_new_health(health_percentage)

func _on_Hurtbox_take_hit(health) -> void:
	#sync clock with hurt
	print("shield hit")
	_shockwave(true)
	health_percentage=health
	bubble_timer.stop()
	bubble_timer.wait_time=1.0
	bubble_timer.start()
	AudioManager.play_sfx(Globals.SFX_GAME_DEFENCE_HIT)
	set_new_health(health_percentage)
	
func _shockwave(enable):
	if !enable:
		return
	var shake=$ScreenShake
	shake.add_trauma(0.25,$DefenceBubble)


