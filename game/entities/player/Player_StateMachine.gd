extends StateMachine

#once working, create locked state
#look at 2 state machines and watch wall jump as that does more to the fsm
#see about redoing logic better once working

func _ready() -> void:
	add_state("idle")
	add_state("walk")
	add_state("jump")
	add_state("fall")
	add_state("locked")
	add_state("jetpac")
	call_deferred("set_state",states.locked)
	parent.lock_player=true

#func _unhandled_input(event: InputEvent) -> void:
#	if [states.idle, states.walk].has(state) || parent.jetpac_jump:
#
#		if event.is_action_pressed("jump"):
#			parent.jump()
#		else:
#			parent.no_jump()
	
func manage_jump() -> void:
	if [states.jump, states.fall].has(state):
		parent.handle_jump_movement()
		
#apply velocity, apply gravity, slide, etc
#stop player from doing an action if in a specific state, etc
#our physics process
func _state_logic(delta):
	#equivalent to physics_process
	if state!=states.locked || parent.forced_direction!=0:
		if [states.idle, states.walk].has(state) || parent.jetpac_jump:
			if Input.is_action_pressed("jump"):
				parent.jump()
			else:
				parent.no_jump()
			
		parent._get_input()
		manage_jump()
			
		parent._apply_gravity(delta)
		parent._move_player(delta)
		parent._check_death()
	
#virtual
#returns what state we should be going into
#handles whether we can move from one state to another, 
#e.g. idle to jump but not jump to walking
func _get_transition(_delta):
	
	if parent.lock_player && parent.forced_direction==0:
		return states.locked
		
	match state:
		states.idle:
			if !parent._check_is_on_ground_raycast():
				if parent.velocity_rex.y<0:
					return states.jump
				elif parent.velocity_rex.y >=0:
					return states.fall
			elif abs(parent.velocity_rex.x) >0.4:
				return states.walk
		states.walk:
			if !parent._check_is_on_ground_raycast():
				if parent.velocity_rex.y<0:
					return states.jump
				elif parent.velocity_rex.y >=0:
					return states.fall
			elif abs(parent.velocity_rex.x) <0.4:
				return states.idle
		states.jump:
			if parent._check_is_on_ground_raycast():
				return states.idle
			elif parent.velocity_rex.y >= 0:
				return states.fall
			else:
				return states.jump	#change direction while jumping
				
		states.fall:
			if parent._check_is_on_ground_raycast():
				return states.idle
			elif parent.velocity_rex.y < 0:
				return states.jump
			else:
				return states.fall	#change direction while falling
		
		states.locked:
			#here so locked is not on. go back to whatever we came from
			if previous_state!=null:
				return previous_state
			else:
				return states.idle
				
	#this used to return null. trying it to see if it fixes
	#moonwalking with no issues
	#moonwalks by walking then very rapidly moving to other walk direction
	#if not work, may have to update state machine tranisition to not skip same transitions
	#as walk is the same
	return state
	#return null

func reset_to_idle():
	parent.velocity_rex=Vector2.ZERO
	state=states.idle
	previous_state=null
	
#virtual 
#actual changing from one to the next, either going in or coming out
#e.g. changing animations, timers, etc
func _enter_state(new_state, _old_state):
	var anim2="right"
	if parent.last_direction== -1:
		anim2="left"

	match new_state:
		states.idle:
			parent.state_machine.travel("idle"+"_"+anim2)
		states.walk:
			parent.state_machine.travel("walk"+"_"+anim2)
		states.jump:
			parent.state_machine.travel("jump"+"_"+anim2)
		states.fall:
			parent.state_machine.travel("jump"+"_"+anim2)
		states.locked:
			parent.state_machine.travel("idle"+"_"+anim2)
		states.jetpac:
			parent.state_machine.travel("jump"+"_"+anim2)

	#with moonwalking check you can't jump when idle
	#this function was pass so trying this
	if (new_state==states.idle || new_state==states.walk) && _old_state!=states.jump:
		parent.can_jump=true
		pass
		
#virtual
#as enter state	
func _exit_state(_old_state, _new_state):
	if [states.fall].has(_old_state) && !parent.jetpac_jump:
		parent.pause_jumping()
