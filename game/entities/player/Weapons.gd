extends Node2D
class_name Weapons_System

enum WEAPON {NONE=0, BULLET_SINGLE=1, BULLET_DOUBLE=2,LASER=3, DRONE=4,SPRAY=5,SPECIAL=6}

#this replaces the old bullet manager
#weapons are cycled in order
#each weapon has a lifespan and reverts back to the next available
#1. single bullet : always available
#2. double bullet
#3. laser
#4. drones
#5. scatter

# 1 pink, 2 green, 3 cyan, 4 yellow, 5 white
# single/double bullet: increases rate from 4 then a pause to full on
#		1 point damage, or 2 point damage
#		whole screen travel
# laser: increases lifespan (20% to 100%)
#		216 pixels wide
#		same distance always
#		stops on enemies, continues through humans
#		reduces by 20%
#		laser is random pixel blocks of 0-5 solid or blank
#		after each then a solid or blank is placed to counter it
#		when near end or hit something random changes to 0 or 1
#		and decreases in size
# drone: increases drones from 4 - each adds a single bullet
#		single point
#		max 2 fire backwards. adds a small collision delay to get past platforms
# spray: 3,5,7 spray in one direction
#         then 4 way, then 8 way
#		1 point damage per bullet
#		? distance

# damage to kill
#- rocket launcher: 7 points
#- bouncy: 7 hits
#- roof turret: 9 hits
#- ground turret: 9 hits
#- spacement: 1 hit
#- xy lock enemy: 12 hits
#- mini boss: 40 half way, then 40 again for bullets
#			but different per weapon

#each weapon has 5 levels and each affects it differently
#a level is increased by collecting dead enemy bubbles
#a level is decreased by fire count
#when level reaches 0 it goes back to the next active one
#signals are passed back to allow UI, etc to update themselves
#the order in container is the weapon order

#USAGE
#INITIALISE: call initialise_bullet_container to place all weapons in container, typically transient to be removed by system
# to use specific for a weapon, get weapon object and call set_bullet_container
#
# ACQUIRE: call acquire_weapon to get or remove weapon. normally once acquired, keep and let system manage it
#  system will manage active status, level and energy automatically 
#
# LEVELS/ENERGY: call increase_current_weapon_energy to increase energy, typically use small/large energy value supplied
#  can also call increase_weapon_energy for a specific weapon, but not normally
#  system will manage levelling up and down
#
# FIRING:
# active weapon enables process/physics
# each weapon handles fire input from within here
# i.e. player does not handle firing only control of energy and
# any overrides such as locking fire, temporary upgrades
#
# current_weapon returns weapon being used, probably not required
#
#OTHER
# all weapons have the following standard methods on top of above
#  -

signal new_weapon_acquired(wposition,wtype)
signal weapon_status_change

var _bullet_container:Node=self
var current_weapon:Weapon=null
var debug=false
var player=null

func _process(_delta: float) -> void:
	#temp
	if Input.is_action_just_pressed("ui_focus_next"):
		_debug_print_weapons()
		
func _debug_print_weapons():
	var line="%s t:%d l:%d e:%d b:%d f:%d"
	print("Score "+str(GlobalPersist.score))
	for w in get_children():
		print(line % [w.name,int(w.weapon_type), w.current_level,w.current_energy, w.current_ammo, int(w.is_firing)])
	print("---")

func _ready() -> void:
	#connect all signals for design time weapons
	#set the first weapon, it is always available and infinite fire
	for weapon in get_children():
		if weapon is Weapon:
			if current_weapon==null:
				current_weapon=weapon
			_connect(weapon)
	#set_weapons_firing_status()
	set_graphics_sprite_type()
	
	#temp to get each weapon
	#for i in 15:
	#	increase_current_weapon_energy(6)

	if debug:
		_test_basic_up()
		_test_basic_down()
		_test_level_up_all()
		_test_level_down_all()
		_test_level_up_gaps()
		_test_level_down_gaps()

func get_current_weapon():
	return current_weapon
	
func set_graphics_sprite_type():
	for w in get_children():
		if w.has_method("set_graphics_sprite_type"):
			w.set_graphics_sprite_type()
			
func _connect(weapon):
	weapon.connect("weapon_max",self,"_weapon_upgrade")
	weapon.connect("weapon_min",self,"_weapon_downgrade")
	weapon.connect("weapon_change_level",self,"_on_weapon_level")

func set_player(owner:Player):
	for weapon in get_children():
		if weapon is Weapon:
			weapon.set_player(owner)
	
func get_weapons_status():
	#used by ui
	var weapons=[]
	for weapon in get_children():
		if weapon is Weapon:
			var item=[]
			item.append(weapon.name)
			item.append(weapon.is_firing)
			item.append(weapon.current_level)	#0 not acquired yet
			item.append(weapon.weapon_order_index)
			weapons.append(item)
	return weapons
			
func disable_current_weapon():
	if current_weapon!=null:
		current_weapon.propagate_call("set_process",[false])
		current_weapon.propagate_call("set_physics_process",[false])
		current_weapon.propagate_call("set_process_input",[false])
		current_weapon.propagate_call("set_process_unhandled_input",[false])
		#current_weapon.set_process(false)
		#current_weapon.set_physics_process(false)

func display_visible_weapons(visible_flag):
	#call this to hide any weapon we think has visible artefacts
	#currently only drone
	#this is used in death currently
	if current_weapon!=null:
		if current_weapon.weapon_type==WEAPON.DRONE:
			current_weapon.visible=visible_flag
	
func initialise_bullet_container(container):
	#leave or pass in self to have weapons follow
	#otherwise pass in a node to hold them
	#this will be the transient for most bullets
	_bullet_container=container
	for weapon in get_children():
		if weapon is Weapon:
			weapon.set_bullet_container(_bullet_container)

func set_weapon_level(weapon_type,level):
	#set current weapon at a specific level
	#used by training mode currently
	for weapon in get_children():
		if weapon is Weapon:
			if weapon_type==weapon.weapon_type:
				weapon.set_energy_level(level)
	
func acquire_weapon(weapon_type, acquired:=true, force_achievement:=false) ->Weapon:
		
	var w:Weapon=_get_weapon_from_type(weapon_type)
	if w.weapon_found(acquired,force_achievement):
		if Globals.training_mode:
			return w
	OptionsAndAchievements.increment_achievement("wmd",1,WEAPON.keys()[weapon_type])
	return w


func increase_current_weapon_energy(amount:int):
	increase_weapon_energy(amount,current_weapon.weapon_type)
		
func decrease_current_weapon_energy(amount:int):
	#called by lottery
	increase_weapon_energy(-amount,current_weapon.weapon_type)
	
func gain_rapid_fire():
	#by lottery win
	#manages itself
	current_weapon.rapid_fire=100

func increase_weapon_energy(amount:int, weapon_type:int):
	#amount: how much is being given
	#weapon: which weapon
	var available_weapons=_get_available_weapon_count()
	
	if available_weapons<1:
		print("ERROR: No active weapons to receive more energy")
		return
		
	for weapon in get_children():
		if weapon is Weapon:
			if weapon_type==weapon.weapon_type:
				if amount<0:
					weapon.energy_decrease(amount)
				else:
					weapon.energy_increase(amount)

func _on_new_weapon(_location, _weapon_type):
	acquire_weapon(_weapon_type)
	emit_signal("new_weapon_acquired",_location,_weapon_type)
	
func _get_available_weapon_count():
	var count=0
	for weapon in get_children():
		if weapon is Weapon && weapon.current_level>0:
			count+=1
			
	return count
	
func allow_firing(allow:bool=true):
	for weapon in get_children():
		weapon.fire_paused = !allow
		
func set_weapons_firing_status():
	#turn off firing for all except current
	for weapon in get_children():
		if weapon is Weapon:
				#let each weapon decide their fate
				if weapon.weapon_type==current_weapon.weapon_type:
					weapon.can_fire(true)
				else:
					weapon.can_fire(false)
		else:
			print("item ignored as not weapon: " + str(weapon.name))
	emit_signal("weapon_status_change")

func _get_weapon_from_type(weapon_type):
	for weapon in get_children():
		if weapon is Weapon:
			if weapon.weapon_type==weapon_type:
				return weapon
	print("ERROR. Cannot find weapon of id " + str(weapon_type))
	return null
	
func _weapon_upgrade(energy):
	#find next level up that is not 0
	var next=false
	for w in get_children():
		if current_weapon.weapon_type==w.weapon_type:
			next=true
		elif next:
			if w.current_level>0:
				current_weapon=w
				current_weapon.current_energy=energy
				break
	set_weapons_firing_status()
	
func _on_weapon_level():
	emit_signal("weapon_status_change")
	
func _weapon_downgrade():
	#find the next weapon down that is not level 0
	var prev=current_weapon	#for catching weapon 1 or if no weapons available
	for w in get_children():
		if w.weapon_type==current_weapon.weapon_type:
			current_weapon=prev
		else:
			if w.current_level>0:
				prev=w
	set_weapons_firing_status()

func _test_basic_up():
	#test increasing energy moves level
	_debug_print_weapons()
	increase_current_weapon_energy(2)
	increase_current_weapon_energy(3)
	_debug_print_weapons()	#should be at 5
	increase_current_weapon_energy(3)	
	_debug_print_weapons()		#should be at level 2 with 2

func _test_basic_down():
	#at l2 energy 2. Have 100 bullets
	current_weapon.bullet_decrease()
	_debug_print_weapons()		#should be at level 2 with 2 and 100 bullets as first weapon never runs out
	current_weapon.current_ammo=1
	current_weapon.bullet_decrease()
	_debug_print_weapons()		#should be at level 1 full energy 100 bullets
	current_weapon.current_level=2
	current_weapon.current_energy=2
	current_weapon.energy_decrease(3)	#should move to level 1 with 100 bullets and no energy
	_debug_print_weapons()		#should be at level 1 full energy 100 bullets
	
func _test_level_up_all():
	#test increasing to max ensure never goes over
	#currently at w1 l1 e0
	acquire_weapon(WEAPON.BULLET_DOUBLE)
	acquire_weapon(WEAPON.DRONE)
	acquire_weapon(WEAPON.LASER)
	acquire_weapon(WEAPON.SPRAY)
	_debug_print_weapons()		#should be at level 1 full energy 100 bullets
	set_weapons_firing_status()
	for _i in range(1):
		increase_current_weapon_energy(6)
		_debug_print_weapons()
	#at level 4, e6
	increase_current_weapon_energy(5)	#level 5 e5
	increase_current_weapon_energy(3)	#move to double
	for _i in range(19):
		increase_current_weapon_energy(6)
		_debug_print_weapons()
	#should have all weapons and be at max and never increase
	
func _test_level_down_all():
	#at level 5 on last weapon
	#first test level down using bullets to ensure go to bottom but never go beyond
	for _i in range(52):
		current_weapon.bullet_decrease()
	_debug_print_weapons()
	#bullets 48, level 4, go down to next weapon
	for _l in range(3):
		for _i in range(50):
			current_weapon.bullet_decrease()
		_debug_print_weapons()
	#at weapon 4, level 5, 48 bullets
	for _i in range(50):
		current_weapon.bullet_decrease()
	_debug_print_weapons()
	#at w4 l5 b48
	#test going down by reducing energy
	current_weapon.energy_decrease(2)
	_debug_print_weapons()	#should go down to level 4, e5 50 bullets
	#test cannot go pas zero
	current_weapon=_get_weapon_from_type(WEAPON.BULLET_SINGLE)
	current_weapon.current_level=1
	current_weapon.current_ammo=2
	set_weapons_firing_status()
	_debug_print_weapons()	#weapon 1 l1 1 bullet
	current_weapon.energy_decrease(2)
	_debug_print_weapons()	#weapon 1 l1 
	current_weapon.current_ammo=1
	current_weapon.bullet_decrease()
	_debug_print_weapons()	#weapon 1 l1 
	
func _test_level_up_gaps():
	acquire_weapon(WEAPON.BULLET_DOUBLE,false)
	acquire_weapon(WEAPON.LASER,false)
	acquire_weapon(WEAPON.DRONE,false)
	acquire_weapon(WEAPON.SPRAY,false)
	_debug_print_weapons()
	current_weapon=_get_weapon_from_type(WEAPON.BULLET_SINGLE)
	current_weapon.current_level=5
	current_weapon.current_energy=5
	_debug_print_weapons()	#weapon 1 l5 e0
	current_weapon.energy_increase(3)
	_debug_print_weapons()	#weapon 1 l5 e0 - should not have moved as no other weapons
	#test going up
	acquire_weapon(WEAPON.DRONE)
	current_weapon.energy_increase(6)
	current_weapon.energy_increase(1)	#go to drone
	_debug_print_weapons()	#weapon 4 e3
		
func _test_level_down_gaps():
	#at drone, take it to level 0
	current_weapon.energy_decrease(6)
	_debug_print_weapons()
	acquire_weapon(WEAPON.BULLET_DOUBLE,true)
	current_weapon=_get_weapon_from_type(WEAPON.DRONE)
	set_weapons_firing_status()
	_debug_print_weapons()
	current_weapon.energy_decrease(6)
	_debug_print_weapons()

	current_weapon.energy_decrease(6)
	_debug_print_weapons()
	
