extends Node2D
class_name Weapon

export var current_level:int=0				#1,2,3,4,5. Or 0 if not yet found
export var current_energy:int=0				#current level
export var energy_per_level:int=6			#when to increase to next level
export var max_ammo:int=15				#how much ammo
export var never_runs_out:bool=false		#can be used even if no energy (default weapon)
export var level_never_decreases:bool=false	#first weapon only goes up unless die or pickup that loses energy

export(int) var weapon_order_index				#1 to 5 in sequence

#cannot put enums in export, get cyclic errors if use preload, so
#trying this which is a mirror as you cannot pass by enum so as long as 
#values stay the same it should be ok
enum WEAPON_MIRROR {NONE=0, BULLET_SINGLE=1, BULLET_DOUBLE=2,LASER=3, DRONE=4,SPRAY=5}
#this silly line is required, otherwise it breaks with cyclic dependencies and all sorts
#export(preload("res://singletons/Globals.gd").WEAPON) var weapon_type
export(WEAPON_MIRROR) var weapon_type

const MAX_LEVEL=5

signal weapon_max(excess_energy)
signal weapon_min()
signal weapon_acquired(type)
signal weapon_change_level

var _bullet_container:Node=self
var is_firing:bool=false				#is the weapon firing
var fire_paused:bool = false
var current_ammo
var player:Player=null
var override_fire_cost:=false
var rapid_fire=0				#lottery win 100 rapid rounds
var secondary_fire:=false


#normally called by parent weapon manager
# weapon_found: 		normally performed by parent weapon manager
# can_fire:     		normally performed by parent weapon manager
# set_bullet_container:	can be called to override parent default
# energy_decrease/increase: normally performed by parent weapon manager
# level_down/level_up:	normally performed by parent weapon manager
# set_player			normally performed by parent weapon manager

# fire					normally called by weapon but use this for overrides
# physics and process only called when this is active weapon
# so handle all firing and other weapon events here
# i.e. handle fire button

func _ready() -> void:
	current_ammo=max_ammo
	can_fire(false)

func fire():
	bullet_decrease()
	
func weapon_found(is_found,force_achievement):
	#mark as found or not, setting level at least to 1
	#0 is disabled
	var found = is_found
	if !is_found:
		current_level=0
	elif current_level==0:
		current_level=1
	if !Globals.training_mode or force_achievement:
		emit_signal("weapon_acquired",weapon_type)
		
	if Globals.training_mode and !force_achievement:
		found = false
	return found
		
func can_fire(allow):
	#enable firing and set level to 1 if setting to allow
	rapid_fire=0	#reset in case rapid fire is on and change weapon
	is_firing=allow
	if current_level<1 && allow:
		current_level=1
	visible=allow
	propagate_call("set_process",[allow])
	propagate_call("set_process_input",[allow])
	propagate_call("set_physics_process",[allow])
	propagate_call("set_process_unhandled_input",[allow])
	#set_process(allow)
	#set_physics_process(allow)
	
func set_player(owner:Player):
	player=owner
	
func set_bullet_container(container):
	_bullet_container=container
	
func energy_decrease(amount:int):
	#this is normally only called by pickups or death
	#bullets set energy
	current_energy-=abs(amount)
	if current_energy<0:
		level_down()
		
func energy_increase(amount:int):
	current_energy+=amount
	if current_energy>energy_per_level:
		level_up(current_energy-energy_per_level)
	
func set_energy_level(level):
	#set specific energy level
	#currently training mode
	current_level = level
	current_energy = 6
	current_ammo = 25
	emit_signal("weapon_change_level")
	emit_signal("weapon_max",current_ammo)
	pass
	
func bullet_decrease():
	if rapid_fire>0:
		rapid_fire-=1
		return
		
	if level_never_decreases:
		return
	current_ammo-=1
	if current_ammo<=0:
		current_ammo=0
		level_down()
		
func level_down():
	#if 0 then disable unless starter weapon
	current_energy=0	#set back to zero for when next go up
	current_ammo=max_ammo
	if current_level==1 && never_runs_out:
		return
	current_level-=1	#decrease level
	if current_level==0:
		current_level=1
		can_fire(false)
		emit_signal("weapon_min")
	emit_signal("weapon_change_level")
	
func level_up(start_amount):
	#move up and set starting amount
	#after checking, this might be a start value
	current_ammo=max(current_ammo,25)	#level up get a boost
	emit_signal("weapon_change_level")
	if current_level==MAX_LEVEL:
		can_fire(false)
		current_energy=0		#if come back down, start at beginning
		emit_signal("weapon_max",start_amount)
	else:
		current_level+=1
		current_energy=start_amount
