extends Weapon

#level 1: 4 drones
#leavel 2,3,4,5: 5,6,7,8 drones
#each weapon fires single weapons
#rapid fire lottery gives full power for 100 shots

var last_level=0
var rapid=false
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.
	#test below. why is spinning putting bullets in wrong place...

func level_down():
	.level_down()
	#our drones have bullet weapons
	for d in get_children():
		if d.has_method("set_bullet_container"):	#quick check it's a drone
			d.level_down()
	
func level_up(amount):
	.level_up(amount)
	for d in get_children():
		if d.has_method("set_bullet_container"):	#quick check it's a drone
			d.level_up(amount)
	
func set_player(owner):
	player=owner
	for w in get_children():
		if w.has_method("set_player"):
			w.set_player(owner)

func set_bullet_container(container):
	_bullet_container=container
	for w in get_children():
		if w.has_method("set_bullet_container"):
			w.set_bullet_container(container)	

func set_graphics_sprite_type():
	for d in get_children():
		if d.has_method("set_graphics_sprite_type"):
			d.set_graphics_sprite_type()

func _process(_delta: float) -> void:
	if rapid_fire>0 && !rapid:
		rapid=true
		_set_drones()
	if rapid && rapid_fire<=0:
		rapid=false
		_set_drones()
		
	if current_level!=last_level || current_level==1:
		_set_drones()
		
func _unhandled_input(event: InputEvent) -> void:
	if !is_firing && !secondary_fire:
		return

	if event.is_action_pressed("fire") && event.is_pressed():
		#every drone fire is a cost of 1, regardless of number of bullets
		#the bullets inside drones do not decrease cost
		if !override_fire_cost:
			.bullet_decrease()
	
func _physics_process(_delta: float) -> void:
	#if Input.is_action_just_pressed("fire"):
		#every drone fire is a cost of 1, regardless of number of bullets
		#the bullets inside drones do not decrease cost
	#	.bullet_decrease()
	pass

func can_fire(allow):
	if allow:
		_set_drones()	#really for when we go back in from dropping down
	for d in get_children():
		if d.has_method("set_bullet_container"):	#quick check it's a drone
			d.set_secondary_fire(allow)
	.can_fire(allow)
	
func _set_drones():
	var required=current_level+3	#always start with 4
	if rapid:
		required=8
		
	for d in get_children():
		if d.has_method("set_bullet_container"):	#quick check it's a drone
			#set correct number of drones firing
			if required>=1:
				d.visible=true
				d.set_secondary_fire(true)
				d.propagate_call("set_physics_process",[true])
				d.propagate_call("set_process_input",[true])
				required-=1
				#we want two of them to fire backwards if over level 2
				if current_level>2 && required<2:
					d.reverse_fire(true)
				else:
					d.reverse_fire(false)
			else:
				d.set_secondary_fire(false)
				d.visible=false
				d.propagate_call("set_physics_process",[false])
				d.propagate_call("set_process_input",[false])
		
	last_level=current_level
