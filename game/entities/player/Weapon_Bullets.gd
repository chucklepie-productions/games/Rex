extends Weapon

#if -1 then let bullet decide points
#this is to limit the damage given by drone multi as too powerful
export(int) var damage_points_override:=-1
export var make_sound:bool=false				#whether to make sound. for drone gun this is to suppor only one sound

onready var single_bullet=preload("res://entities/projectiles/Player_Bullet_Single.tscn")
onready var double_bullet=preload("res://entities/projectiles/Player_Bullet_Double.tscn")

#always across whole screen
#1 magenta 4 then pause (3 bullets worth)
#2 green 5 then shorter pause (2 bullets worth)
#3 cyan 6 then shorter pause (1 bullet)
#4 yellow 7 then shorter pause
#5 white no pause
#rapid fire gives 100 rapid fire shots

#single bullet data - all to be moved to single bullet
onready var container_bullet_left=$Bullet_Left
onready var container_bullet_right=$Bullet_Right
onready var bullet_timer=$Cooldown
var current_bullets:int = 0
var current_bullets_max:int = 3
var double_gun:=false
var ignore_environment_delay:=false
var use_centre_point:=false
var reverse_fire_override:=false

onready var last_fire_pause:int = 0

func _ready() -> void:
	if weapon_type==Weapons_System.WEAPON.BULLET_DOUBLE:
		double_gun=true
		position.y-=4	#recentre from single

func _unhandled_input(event: InputEvent) -> void:
	#for drone weapon
	#drone does the firing not the bullet to avoid multiple bullet decreases on firing
	#so is_firing only set for weapon 1 and 2, not 4. seconary_Fire allows this
	if !is_firing && !secondary_fire:
		return
		
	if event.is_action_pressed("fire")  && event.is_pressed():
		fire()
	
func _physics_process(_delta: float) -> void:
	#if Input.is_action_just_pressed("fire"):
	#	fire()
	pass
			
func fire():
	var direction=1
	var bullet=single_bullet
	
	if player:
		direction=player.last_direction
		
	if double_gun:
		bullet=double_bullet
	fire_bullet(direction,bullet)
	#fire_double_bullet(direction)
	
func fire_bullet(direction, bullet):
	#have to wait
	$ResetBullet.stop()
	$ResetBullet.start()
	if fire_paused:
		AudioManager.play_sfx(Globals.SFX_WEAPON_NOBULLETS)
		return

	#if we've fired our limit then wait
	#max level can always fire
	
	if reverse_fire_override:
		direction*=-1
		
	if current_level<5 && rapid_fire==0:
		if current_bullets>=current_bullets_max+current_level:
			print("cooldown")
			fire_paused=true
			bullet_timer.start((1-current_level+4)/10.0)
			return
	
		current_bullets+=1
		last_fire_pause=OS.get_ticks_msec()
		#print(last_fire_pause)
	var new_bullet = bullet.instance()
	if damage_points_override>=0:
		new_bullet.override_hitscore(damage_points_override)

	var a1=_bullet_container
	if is_instance_valid(a1):
		a1.add_child(new_bullet)
	else:	
		add_child(new_bullet)
		print("ERROR. bullet container not set")

	if direction==1:
		if use_centre_point:
			new_bullet.global_position=global_position
		else:
			new_bullet.global_position=container_bullet_right.global_position
		new_bullet.set_angle(0)
	else:
		if use_centre_point:
			new_bullet.global_position=global_position
		else:
			new_bullet.global_position=container_bullet_left.global_position
		new_bullet.set_angle(180)
		
	if make_sound:
		AudioManager.play_sfx(Globals.SFX_WEAPON_BULLET)
	
		
	#for drone fire, will ignore first fraction of a second any environment
	new_bullet.start(ignore_environment_delay)
	if !override_fire_cost:
		#essentially the same as level_never_decreases, different intent
		#used for drones bullets to ignore code or for temporary use
		.bullet_decrease()


func _on_Cooldown_timeout() -> void:
	_reset_bullet()
	print("cooldown end")
	
func _reset_bullet():
	fire_paused=false
	current_bullets=0
	last_fire_pause=0


func _on_ResetBullet_timeout() -> void:
	#not fired in a while
	_reset_bullet()
	print("Reset bullet")
