extends Weapon

onready var laser=preload("res://entities/projectiles/Player_Bullet_Laser.tscn")
#each level fires same distance
#each level reduces it's beam length after each hit by a percentage
#each level decreases the amount lost
#each level is 100%/level, i.e. 1=1 hit, 5=5 hits
#beam is stopped normally except with humans where laser continues
#rapid fire gives 100 level 5 shots

#single bullet data - all to be moved to single bullet
onready var container_bullet_left=$Laser_Left
onready var container_bullet_right=$Laser_Right

func _ready() -> void:
	pass

func _unhandled_input(event: InputEvent) -> void:
	if !is_firing && !secondary_fire:
		return
	if event.is_action_pressed("fire")  && event.is_pressed():
		fire()
	
func _physics_process(_delta: float) -> void:
	#if Input.is_action_just_pressed("fire"):
	#	fire()
	pass
			
func fire():
	var direction=1
	
	if player:
		direction=player.last_direction
	fire_bullet(direction,laser)
	
func fire_bullet(direction, laser_weapon):
	
	if fire_paused:
		AudioManager.play_sfx(Globals.SFX_WEAPON_NOBULLETS)
		return
		
	var power=current_level*20	#percentage per hit
	if rapid_fire>0:
		power=100
		
	var new_bullet = laser_weapon.instance()

	var a1=_bullet_container
	if is_instance_valid(a1):
		a1.add_child(new_bullet)
	else:	
		add_child(new_bullet)
		print("ERROR. bullet container not set")

	new_bullet.direction=direction
	new_bullet.power=power
	new_bullet.global_position=global_position+Vector2(40*direction,0)
		
	AudioManager.play_sfx(Globals.SFX_WEAPON_LASER)
	new_bullet.start(false)
	
	if !override_fire_cost:
		#essentially the same as level_never_decreases, different intent
		#used for drones bullets to ignore code or for temporary use
		.bullet_decrease()
