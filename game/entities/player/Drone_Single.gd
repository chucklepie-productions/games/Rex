extends Node2D

onready var single_bullet=preload("res://entities/projectiles/Player_Bullet_Single.tscn")

export(int) var bullet_damage=10
export var make_sound:bool=false				#whether to make sound. for drone gun this is to suppor only one sound

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$Weapon_Bullet.ignore_environment_delay=true
	$Weapon_Bullet.use_centre_point=true
	$Weapon_Bullet.override_fire_cost=true
	$Weapon_Bullet.current_level=1	#set at same level as drone when starting
	$Weapon_Bullet.damage_points_override=bullet_damage
	$Weapon_Bullet.make_sound=make_sound

func _process(_delta: float) -> void:
	global_rotation_degrees=0
	
func set_secondary_fire(is_secondary):
	#drone does the firing not the bullet to avoid multiple bullet decreases on firing
	#so this sets the flag to say it is firing
	#required due to using _input and not _process for fire
	$Weapon_Bullet.secondary_fire=is_secondary
	
func set_graphics_sprite_type():
	var m=$Sprite.material
	m.set_shader_param("all_white",GlobalPersist.graphics_type==0)

func set_player(owner: Player):
	$Weapon_Bullet.set_player(owner)
	
func set_bullet_container(container):
	$Weapon_Bullet.set_bullet_container(container)

func reverse_fire(set_reverse):
	$Weapon_Bullet.reverse_fire_override=set_reverse
	
func level_up(amount):
	$Weapon_Bullet.level_up(amount)
	
func level_down():
	$Weapon_Bullet.level_down()
