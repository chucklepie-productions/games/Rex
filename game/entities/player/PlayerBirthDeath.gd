extends Node2D

signal birth_finished
signal death_finished

var colour

func _ready() -> void:
	stop_all()
	_reset()
	#_test()
	
func _test():
	#position+=Vector2(300,300)
	#var t=GlobalMethods.get_texture_from_region($Sprite2.texture,Rect2(0,0,176/4,64))
	#start_birth(true,t)
	#start_death(false,t)
	pass
	
func start_death(tex:Texture):
	_start_animation(tex,"Die_","Die")
	
func start_birth(tex:Texture,anim_type):
	_start_animation(tex,"Respawn_",anim_type)
	
func _start_animation(tex:Texture,particle_start, anim_name):
	stop_all()
	if tex!=null:
		$Sprite.texture=tex
	_reset()
	var start=particle_start+_get_colour()
	var pn=get_node(start)
	if pn.emitting:
		pn.restart()
	else:
		pn.emitting=true
	$AnimationPlayer.play(anim_name)

func play_audio_rex1():
	AudioManager.play_sfx(Globals.SFX_PLAYER_WARP1)
	
func play_audio_rex2():
	AudioManager.play_sfx(Globals.SFX_PLAYER_WARP2)
	
func _get_colour():
	if GlobalPersist.graphics_type==0:
		return "Original"
	return "New"

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name.begins_with("Die"):
		emit_signal("death_finished")
	else:
		emit_signal("birth_finished")
	queue_free()

#these not required really as we are adding and deleting dynamically from player
func stop_all():
	$AnimationPlayer.stop()
	$Die_New.one_shot=false
	$Respawn_New.one_shot=false
	$Die_New.one_shot=false
	$Respawn_Original.one_shot=false

func _reset():
	$Sprite.flip_h=false
	$AnimationPlayer.stop()
	$Die_New.one_shot=true
	$Respawn_New.one_shot=true
	$Die_New.one_shot=true
	$Respawn_Original.one_shot=true
	$Sprite.visible=false

