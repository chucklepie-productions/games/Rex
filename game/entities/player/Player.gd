extends KinematicBody2D
class_name Player

signal spawn_point_enabled
signal spawn_point_centre
signal spawn_point_exit
signal smart_bomb
signal life_lost
signal new_room_warp(position)

const MAX_SPEED:= 90.0
const JUMP_SPEED:= -255.0
const JETPAC_FORCE:=-5.0
const BOOST_SPEED: = -300.0
const H_FACTOR_GROUND = 1
const H_FACTOR_AIR = 1
var speed_factor=1.0	#lottery gives speed increase for 30 seconds
onready var tree=$AnimationTree
onready var state_machine=tree["parameters/playback"]
onready var raycasts=$raycasts
onready var show_material=$Sprite.get_material()
onready var animation=$AnimationPlayer
onready var fsm=$PlayerStateMachine
onready var weapons=$Weapons
onready var defence=$Defence

onready var clusterdead=preload("res://entities/base_classes/ExplosionCluster.tscn")

onready var birth_death_transition=preload("res://entities/player/PlayerBirthDeath.tscn")

var only_gravity_allowed:=false
var velocity_rex = Vector2.ZERO
var direction:int = 0
var last_direction:int = 1
var jump_area_trigger:bool = false
var bubble=false
var lock_player=true
var double_lock_player:=false	#do not allow unlocking of player (useful when other systems enable player and we do not want to)
var can_jump=true
var blocked:Vector2=Vector2.ZERO
var applying_boost=false
var applying_boost_first=false
var temp_move_latch:=0
var forced_direction:=0
var player_death=false	#used for kill room
var jetpac_jump:=false

func _ready() -> void:
	visible=true
	weapons.initialise_bullet_container(GlobalMethods.assign_temp_location())
	weapons.set_player(self)
	set_graphics_sprite_type()
	enable_jetpac(false)
	
func enable_jetpac(value:bool):
	jetpac_jump=value
	if !value:
		no_jump()
	
func set_graphics_sprite_type():
	show_material.set_shader_param("all_white",GlobalPersist.graphics_type==0)
	weapons.set_graphics_sprite_type()

	

func shield_charging(enabled):
	defence.shield_charging(enabled)
	
func gain_speedup():
	speed_factor=1.5
	$SpeedBoostTimer.start()
	
func room_oxygen_depleted():
	if GlobalPersist.has_oxygen:
		return false
	else:
		death(true,true)
		return true
		
func has_oxygen_mask(has_it):
	GlobalPersist.has_oxygen=has_it
	if has_it:
		OptionsAndAchievements.increment_log("oxygen",1)
	
func pause_jumping():
	can_jump=false
	$JumpTimer.start()
	
func apply_boost(is_exiting):
	if is_exiting:
		applying_boost=false
		applying_boost_first=false
		jump(BOOST_SPEED*0.9,true)
	else:
		applying_boost=true
		#clamp x. maybe tween it later for better effect
		velocity_rex.x=0
		if !applying_boost_first:
			jump(BOOST_SPEED,true)
			applying_boost_first=true
	
func _apply_gravity(delta):
	if applying_boost:
		velocity_rex.y-=delta/100.0
		velocity_rex.x=0
	else:
		if Globals.bullet_time==1.0:
			velocity_rex.y+=Globals.GRAVITY*delta
		else:
			velocity_rex.y+=Globals.GRAVITY*delta*5.0#(0.5)

func _check_death():
	#if fsm.state==fsm.states.jump:
		#currently only room exit check is here
		#this is called during room change so player is in bottom
		#of new room while jumping
		#so ignore if going upwards
	#	return
	for index in get_slide_count():
		var collision = get_slide_collision(index)
		if collision.collider.name=="RexTileMapInstantKill":
			death(true,true)
			OptionsAndAchievements.increment_log("vine",1)
			return
			
		if collision.collider.name=="RoomBlocker":	#ExitBottom
			if collision.collider_shape_index==1:	#bottom
				print("DEATH at "+str(velocity_rex))
				death(true,true)
	
		
func update_player_invincible():
	if Globals.invincible:
		Globals.game_scene.player.modulate.a=0.6
	else:
		Globals.game_scene.player.modulate.a=1.0

func _get_input():
	#$Label.text="%s%s%s:%s"%[str(direction),str(last_direction),$Sprite.flip_h,state_machine.get_current_node()]
	if lock_player:
		return
		
	if Input.is_action_just_pressed("temp_invincible"):
		Globals.invincible=!Globals.invincible
		update_player_invincible()
		
	if Input.is_action_just_pressed("shield"):
		bubble=!bubble
		if set_bubble(bubble):
			if bubble:
				AudioManager.play_sfx(Globals.SFX_PLAYER_SHIELDON)
			else:
				AudioManager.play_sfx(Globals.SFX_PLAYER_SHIELDOFF)
		else:
			AudioManager.play_sfx(Globals.SFX_PLAYER_SHIELDERROR)
			
	if Input.is_action_just_pressed("smart_bomb"):
		if GlobalPersist.smart_bombs>0: #|| Globals.debug:
			GlobalPersist.smart_bombs-=1
			OptionsAndAchievements.increment_log("smart",1)
			emit_signal("smart_bomb")
			yield(get_tree().create_timer(0.2), "timeout")
			GlobalTileMap.rex_room_manager.kill_room(true,0.1,true,true,true,true)
		else:
			print("DO SOMETHING WITH NO SMARTS LIKE A SOUND")
		
	direction = int(Input.is_action_pressed("walk_right")) - \
						int(Input.is_action_pressed("walk_left"))
	
	#training requires forcing player to move on jump out of ship
	#but could be used anywhere as long as latch is remove
	if temp_move_latch!=0:
		direction=temp_move_latch
		yield(get_tree().create_timer(0.75), "timeout")
		temp_move_latch=0
		
	#block left/right
	if direction==1 and blocked==Vector2.LEFT:
		direction=0
	elif direction==-1 and blocked==Vector2.RIGHT:
		direction=0
	else:
		#unblock
		blocked=Vector2.ZERO
	
	#currently only set by tutorial
	if forced_direction!=0:
		direction=forced_direction
		
	#allow directional movement while in the air
	#and if we were jumping while moving then continue moving
	#if direction is pressed then remember our last position
	if direction!=0:
		last_direction=direction

func no_jump():
	#if !jetpac_jump:
	#	return
		
	$Particles2DJetpac.emitting=false
	
func jump(yvel:=JUMP_SPEED,can_jump_override:=false,force_direction:=0):
	#if parent._check_is_on_ground_raycast():
	if jetpac_jump:
		$Particles2DJetpac.emitting=true
		if _check_is_on_ground_raycast():
			velocity_rex.y=yvel*0.6
		elif velocity_rex.y<0.0 && velocity_rex.y>JUMP_SPEED:
			#rising already
			velocity_rex.y+=JETPAC_FORCE
		else:
			#falling or at peak
			velocity_rex.y=yvel/2.0
	if !jetpac_jump:
		if (velocity_rex.y>=0 && can_jump) || can_jump_override:
			AudioManager.play_sfx(Globals.SFX_PLAYER_JUMP)
			if last_direction==1:
				$Particles2DJumpR.emitting=true
			else:
				$Particles2DJumpL.emitting=true
			velocity_rex.y=yvel
		#used mainly in tutorial to force jump in a direction
		temp_move_latch=force_direction

#these are used by tutorial only currently	
#they remain until stopped
func tutorial_walk():
	animation.play("tutorial_platform_walk")
func force_walk_right():
	forced_direction=1
func force_walk_left():
	forced_direction=-1
func force_jump_right():
	forced_direction=1
	jump(JUMP_SPEED,true,1)
func forced_walk_stop():
	forced_direction=0

func handle_jump_movement():
	#if [fsm.states.jump, fsm.states.fall].has(fsm.state):
	#if we are not on the ground and we haven't changed direction
	#and we are moving then pretend we are moving
	if direction==0 and velocity_rex.x!=0:
	#if !parent._check_is_on_ground_raycast() and parent.direction==0 and parent.velocity_rex.x!=0:
		direction=last_direction
	
func _move_player(_delta):
	#TEMP
	Globals.invincible=true
	#END TEMP
	if Globals.bullet_time==1.0:
		velocity_rex.x=lerp(velocity_rex.x,MAX_SPEED*direction*speed_factor,_get_h_weight())
	else:
		velocity_rex.x=lerp(velocity_rex.x,MAX_SPEED*direction,_get_h_weight())*(0.5)

	if only_gravity_allowed:
		velocity_rex.x=0.0

	velocity_rex=move_and_slide(velocity_rex,Vector2.UP)
	
func _get_h_weight():
	#factor for speeding up/slowing down, e.g. in air, on ground
	#currently all the same
	if _check_is_on_ground_raycast():
		return H_FACTOR_GROUND
	else:
		return H_FACTOR_AIR
	
func _check_is_on_ground_raycast():
	#this method is to replace is_on_floor()
	#because it is making player not drop properly
	#drawback is is requires extra stilljumping collision
	#to stop double jumps
	#jump_area_trigger=false
	if jump_area_trigger && abs(velocity_rex.y)>0.5:
		return false
	
	for raycast in raycasts.get_children():
		if raycast.is_colliding() && (velocity_rex.y)>=0:
			return true
			
	return false
	
func _on_StillJumping_body_entered(_body: Node) -> void:
	#print("colliding with " + body.name)
	jump_area_trigger = true

func _on_StillJumping_body_exited(_body: Node) -> void:
	#print("left colliding with " + body.name)
	jump_area_trigger = false

func fake_birth():
	$Sprite.visible=false
	visible=true
	$PlayerStateMachine.reset_to_idle()
	direction=0
	last_direction=1
	var b=birth_death_transition.instance()
	
	enable_lock(true)
	add_child(b)
	var t=GlobalMethods.get_texture_from_region($Sprite.texture,Rect2(0,0,320/4,64))
	b.start_birth(t,"Spawn")
	yield(b,"birth_finished")
	enable_lock(false)
	$Sprite.visible=true

func birth():
	#disable edge areas
	#can only enable after animation finished
	#as causing issues when player is at edge of screen
	
#	birth:
#	_on_Player_new_room_warp
#	change_room
#	_transition_camera
#	_on_changed_new_room
#	changed_new_room
#	map_change_room
#	_level_setup
#	_room_ready
#	_start_room -> set_exits
	Globals.player_being_born=true
	if Globals.training_mode && Globals.is_game_over:
		GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.TUTORIAL,"train_game_over",Globals.TUTORIAL_TEXT["train_game_over"],true)
		Globals.lives=Globals.START_LIVES
		Globals.is_game_over=false
	elif !Globals.training_mode && Globals.is_game_over:
		Globals.is_game_over=false
		SceneManager.sequence_shift()
		return
	player_death=false
	applying_boost=false
	if !Globals.quick_spawn:
		enable_lock(true)
	forced_direction=0
	$PlayerStateMachine.reset_to_idle()

	var spn = GlobalMethods.get_spawn_point_node(global_position)
	if spn:
		spn.enable_after_save()
	var startpos:Vector2=GlobalMethods.get_spawn_point(global_position)
	GlobalTileMap.set_room_edge_detection(false)
	global_position=startpos
	#reset defence
	_bubble_full_tank()
	set_bubble(false)
	
	#switch to last spawn room and start player again
	emit_signal("new_room_warp",startpos)
	
	#when animation finished, collision is unlocked
	animation.clear_queue()
	animation.clear_caches()
	animation.stop()

	var b=birth_death_transition.instance()
	enable_lock(true)
	$Sprite.visible=false
	add_child(b)
	var t
	direction=1
	last_direction=1
	t=GlobalMethods.get_texture_from_region($Sprite.texture,Rect2(0,0,320/4,64))
	b.connect("birth_finished",self,"_birth_animation_finished")
	
	if Globals.quick_spawn:
		#this is just a hack for the prologue screen to not show particles
		b.start_birth(t,"Spawn_NoParticles")
	else:
		yield(get_tree().create_timer(1.0), "timeout")
		if Globals.birth_invisible_override || Globals.invincible:
			b.start_birth(t,"Spawn_Invincible")
		else:
			b.start_birth(t,"Spawn")
		
	#let player be invincible for a few seconds
	var old_inv=Globals.invincible
	Globals.invincible=true
	update_player_invincible()
	yield(get_tree().create_timer(7.0), "timeout")
	Globals.invincible=old_inv
	update_player_invincible()
	#edge detection added in event

#event after birth
func _birth_animation_finished():
	enable_lock(false)
	$Sprite.visible=true
	weapons.display_visible_weapons(true)
	GlobalTileMap.set_room_edge_detection(true)
	yield(get_tree().create_timer(2), "timeout")
	Globals.player_being_born=false

	
#event after death
func _death_animation_finished():
	birth()

func _on_room_killed():
	#this is so on player death we can wait until the room is fully killed
	if player_death:
		$PlayerStateMachine.reset_to_idle()
		if GlobalTileMap.current_room_id==52 and (Globals.lives==Globals.START_LIVES || Globals.lives==99):
			#technically if easy mode and get down to 4 then faking, but never happen
			OptionsAndAchievements.increment_achievement("faking",1)

		emit_signal("life_lost")
		#death animation calls birth when finished
		animation.clear_queue()
		animation.clear_caches()
		animation.stop()
		
		var b=birth_death_transition.instance()
		enable_lock(true)
		$Sprite.visible=false
		add_child(b)
		var t:Texture=null
		if last_direction>=0:
			t=GlobalMethods.get_texture_from_region($Sprite.texture,Rect2(0,0,320/4,64))
		else:
			t=GlobalMethods.get_texture_from_region($Sprite.texture,Rect2(0,64,320/4,64))
			
		#b.position=position
		b.connect("death_finished",self,"_death_animation_finished")
		b.start_death(t)

func death(force_death, room_death):
	#the bubble will handle itself
	if Globals.invincible && !room_death:
		return
		
	if player_death:
		#player already dead so let things wait until animations, etc finish
		return
		
	forced_direction=0
	applying_boost=false
	weapons.display_visible_weapons(false)
	if !bubble || force_death:
		player_death=true
		set_bubble(false)
		
		#cluster explosion
		#create explosion
		var e=clusterdead.instance()
		e.cluster_count=3
		e.min_delay_between_explosions=0.1
		e.max_delay_between_explosions=0.2
		add_child(e)
		e.global_position=global_position
		e.start_explosions()
		
		#fsm.set_state(fsm.states.locked)
		enable_lock(true)
		GlobalTileMap.rex_room_manager.kill_room(true,0.1,true,false)
		#rest of code is now in _on_room_killed
	else:
		print("death avoided by bubble but did bubble see it?")
	
func _bubble_full_tank():
	#sets tank to full, does not start or stop the timer
	$Defence.full_tank()
	
func get_shield_level():
	return $Defence.get_health()
	
func get_bubble():
	return $Defence.visible

func set_bubble(is_on:bool):
	$Defence.set_defence_bubble(is_on)
	if is_on:
		if $Defence.get_health()<=0.0:
			print("player bubble empty. turning off")
			bubble=false
			return false
	return true

func _on_PlayerSpawnPointCentre_area_shape_entered(_area_id: RID, area: Area2D, area_shape: int, _self_shape: int) -> void:
	#print (area.name)
	#print(area_id)
	#print(area_shape)
	#print(self_shape)
	#print("spawn point " + area.name)
	if abs(velocity_rex.y)<1:		#is on floor is shit and doesn't work on tiles very well
		#THIS SHOULD BE SELF_SHAPE BUT IT'S AREA_SHAPE
		#OR IT'S AREA_SHAPE AND THE OTHER METHOD USING SELF_SHAPE IS WRONG
		if area_shape==1:
			area.disable(false)
			GlobalPersist.player_spawn_point=area.name
			emit_signal("spawn_point_centre",global_position)
			if area.create_checkpoint:
				GlobalPersist.has_continue=true
				GlobalPersist.save_all_world_data(1,"Microdrive write success")
			area.enable_after_save()

		if area_shape==0:
			var offset:=Vector2(-GlobalTileMap.map_tile_size,0)
			if direction==1:
				offset.x=GlobalTileMap.map_tile_size
			emit_signal("spawn_point_enabled",global_position+offset)
		#we cannot disable it as going back a spawn point might
		#after dying the spawn point may be dead
		#we only ever want one
	else:
		#toggle box for a fraction of a second to get it to refresh
		$PlayerSpawnPointCentre/CollisionShape2D.set_deferred("disabled",true)
		yield(get_tree(),"idle_frame")
		$PlayerSpawnPointCentre/CollisionShape2D.set_deferred("disabled",false)

func set_walk_sound(is_on):
	if is_on && not $AudioStreamPlayerWalk.playing:
		$AudioStreamPlayerWalk.playing=true
	else:
		$AudioStreamPlayerWalk.stop()
		
func _on_PlayerSpawnPointCentre_area_shape_exited(_area_id: RID, _area: Area2D, area_shape: int, _self_shape: int) -> void:
	if area_shape==0:
		emit_signal("spawn_point_exit", global_position)

func monty_colours(flag):
	if flag:
		Globals.game_scene.player.modulate.a=1.0
		show_material.set_shader_param("just_black",true)
	else:
		show_material.set_shader_param("just_black",false)
		update_player_invincible()
		
	
func _on_JumpTimer_timeout() -> void:
	#rex has a slight flat curve at the jump top
	#this has an effect of not auto jumping from one platform to another
	#this timer kind of recreates that. not perfect as it's at the end
	#of the jump not the middle so minor delay between holind jump
	#on the ground
	can_jump=true

func only_gravity(enable):
	only_gravity_allowed=enable
	
func double_lock(enable, _enable_gravity:=false):
	#double lock is used to stop enable_lock from being enabled
	#useful for animations we do not want to stop
	#but allows y movement
	double_lock_player=enable
	enable_lock(enable)

func enable_lock(is_enabled):
	#if anywhere enabled double lock they must call disable_double_lock
	#otherwise player will be permanently locked
	#if double locked and enable is called, return out
	if double_lock_player && !is_enabled:
		return
		
	lock_player=is_enabled
	$PlayerHitbox/CollisionShape2D.set_deferred("disabled",is_enabled)
	$PlayerHurtbox/CollisionShape2D.set_deferred("disabled",is_enabled)

	if !is_enabled:
		$Weapons.set_weapons_firing_status()
	else:
		$Weapons.disable_current_weapon()

func _on_PlayerHurtbox_hurt_started() -> void:
	#refer to player hit but not dead issue on gitlab
	$PlayerHitbox/CollisionShape2D.set_deferred("disabled",true)
	$PlayerHurtbox/CollisionShape2D.set_deferred("disabled",true)

func _on_PlayerHurtbox_hurt_ended() -> void:
	#refer to player hit but not dead issue on gitlab
	if !lock_player:
		$PlayerHitbox/CollisionShape2D.set_deferred("disabled",false)
		$PlayerHurtbox/CollisionShape2D.set_deferred("disabled",false)
		pass

func _on_PlayerHurtbox_take_hit(health) -> void:
	if $Defence.get_health()>0 and $Defence.visible:
		print("player took hit but shields protected")
		return
	if health<=0:
		death(true,false)


func _on_Defence_bubble_depleted() -> void:
	$Defence.set_defence_bubble(false)
	GlobalMethods.explosion_shards(global_position,
		"Defence",null,0.0,1.0,4,10,70)

func _on_SpeedBoostTimer_timeout() -> void:
	speed_factor=1


