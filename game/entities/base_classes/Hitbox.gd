extends Area2D

#collisions on bodies are left as just collisions for blocking movement, etc
#attacks are done via area2d hit/hurtboxes
#this hurt/hitbox is instanced per object then editable childrens shown 
# and collision drawn and the relevant mask bits given

#hurtbox: set LAYER bit to be its hurtbox (playerhurtbox if player, etc)
#hitbox: set MASK bit to be the other hurtbox (enemyhurtbox if player, etc)
#i.e. only one collision is raised not two

#hurtbox gets the event, deals it's own damage by asking hit body it's damage
#we can either have hit/hurt box on all objects or just one side to manage
#e.g. player or enemy has hit/hurt
#however to become more self-reliant, though more wastefu
#we will add both where necessary, e.g. enemy/player both have hurt/hit and 
#damage themeselves
export(int) var damage_given = 40

func enable():
	$CollisionShape2D.call_deferred("set_disabled",false)
	_process_toggle(true)
	
func deactivate():
	#disable from hitting
	$CollisionShape2D.call_deferred("set_disabled",true)
	pass
	
func disable():
	$CollisionShape2D.call_deferred("set_disabled",true)
	_process_toggle(false)
	
func get_damage_amount():
	return damage_given

func _process_toggle(status):
	set_process(status)
	set_physics_process(status)
