extends Area2D
class_name ExplosionCluster

#use as base for inherited node
#Used wherever a more dramatic effect is required
#draw hit area to define where entity needs to go to start process
#change area to show explosions to show area explosions might occur
#should probably use this in place of enemy and player deaths...

export(int) var cluster_count:=1
export(float) var min_delay_between_explosions:=0.0
export(float) var max_delay_between_explosions:=0.0
var current_count:=0
onready var coverage=$Coverage/AreaToShowExplosions
onready var area_range=coverage.shape.radius

func _ready() -> void:
	randomize()

func enable(_visible):
	_process_toggle(true)
	
func disable(_visible):
	_process_toggle(false)

func start_explosions(zindex_override:=0):
	current_count=0
	if min_delay_between_explosions<=0.0 && max_delay_between_explosions<=0.0:
			for i in cluster_count:
				_create_explosion(zindex_override)
	else:
		var w=rand_range(min_delay_between_explosions,max_delay_between_explosions)
		if w==0.0:
			_create_explosion(zindex_override)
		else:
			$ExplosionGapTimer.wait_time=w
			$ExplosionGapTimer.one_shot=true
			$ExplosionGapTimer.start()
		
func _create_explosion(_zindex_override:=0):
	#for now just the simple rex one
	AudioManager.play_sfx(Globals.SFX_WEAPON_SPRAYMISSILE)
	current_count+=1
	var x=rand_range(-area_range,area_range)
	var y=rand_range(-area_range,area_range)
	var loc=$Coverage.global_position+Vector2(x,y)
	
	if current_count==1 || current_count>=cluster_count:
		GlobalMethods.explosion_standard(loc, true,false, 0.2, 0,Globals.SFX_ENEMY_EXPLODE1)
	else:
		GlobalMethods.explosion_standard(loc, false,false, 0.2, 0,Globals.SFX_ENEMY_EXPLODE3)
		
	if current_count<=cluster_count:
		$ExplosionGapTimer.one_shot=true
		$ExplosionGapTimer.start()
	else:
		finished()

func finished():
	queue_free()
	
func _process_toggle(status):
	var ok=status
	if $HitArea.shape==null:
		ok=false
	$HitArea.call_deferred("set_disabled",!ok)
	coverage.call_deferred("set_disabled",!ok)
	set_deferred("monitoring",ok)
	set_deferred("monitorable",ok)
	set_process(ok)
	set_physics_process(ok)


func _on_ExplosionGapTimer_timeout() -> void:
	_create_explosion()


func _on_ExplosionCluster_body_entered(_body: Node) -> void:
	start_explosions()


func _on_ExplosionCluster_area_entered(_area: Area2D) -> void:
	start_explosions()
