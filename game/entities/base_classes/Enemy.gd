extends KinematicBody2D
class_name Enemy

enum ENEMY_TYPE {HUMAN,SHIP,DRONE,TRAIN,FLOATER,BOUNCER,DOME,TURRET,MISSILE_LAUNCHER,TRIANGLE}

#we want all our enemies to start not moving
#we enable them when they are in the same location
#or we start them

#if true will bounce once entered screen
#currently all ships and drones will bounce, only humans leave


#used by virtuals to determine what to do if they leave the screen, if anything
export var screen_based=true	#if yes then typically reverses, otherwise is removed. Humans always remove themselves
export var smart_bomb_kills=true
export(String) var achievement_sure_name

#used for adding to totals on game over
export(bool) var is_bio_growth:=false
export(bool) var is_human:=false

#true means queue free on death()
#false means hide, these are for enemies we want in room repeatedly
export var transient_contained=false
#used when adding enemies to a room, mainly to differentiate ufo/human ship
export(ENEMY_TYPE) var transient_indicated_type

export var gives_energy_on_death=true
export var score:=0
export(String) var bubble_death_drop_tip:=""

var sprite_for_colouring=null


var is_enabled=false
var screen_constraint:Rect2
var my_place:Rect2
var my_half_size:int
var has_entered_screen:=false
var has_enemy_layer_mask
var has_human_layer_mask
var weapon_container
var is_dead=false	#to stop multiple deaths, always reset
var has_died=false	#latch to know if to create enemy on respawning dead player

func _ready() -> void:
	has_enemy_layer_mask=get_collision_layer_bit(3)
	has_human_layer_mask=get_collision_layer_bit(14)
	disable(false)

func enable(make_visible:bool):
	weapon_container=GlobalMethods.assign_temp_location()
	is_dead=false
	visible=make_visible
	is_enabled=true
	
	_process_toggle(true)
	invincible(false)
	_set_player_collision(true)
	

func disable(make_visible:bool):
	visible=make_visible
	is_enabled=false
	_process_toggle(false)
	invincible(true)
	_set_player_collision(false)

func set_graphics_sprite_type():
	#interface
	pass
	
func _set_sprite_type(sprite:Node2D):
	if GlobalPersist.graphics_type==0 || GlobalPersist.graphics_type==1:
		if is_instance_valid(sprite) && sprite.has_method("get_material"):
			var material=sprite.material
			material.set_shader_param("all_white",GlobalPersist.graphics_type==0)
			
func _set_player_collision(player_collides):
	if has_enemy_layer_mask:
		set_collision_layer_bit(3,player_collides)
	if has_human_layer_mask:
		set_collision_layer_bit(14,player_collides)
	
	
func invincible(status):
	_set_collision_box("Hitbox",status)
	_set_collision_box("Hurtbox",status)
	
func _set_collision_box(node_name,status):
	#assumes node called CollisionShape2D within
	if is_dead && status==false:
		print(node_name+" already dead so cannot set invincible to off")
		return
		
	var node_found=find_node(node_name,true,true)
	if node_found!=null:
		var coll=node_found.find_node("CollisionShape2D",false,true)
		if coll!=null:
			coll.set_deferred("disabled",status)
		else:
			print(name+ " collision shape CollisionShape2D on "+node_name+" not found for found node : " + node_found.name)
	else:
		print(name+" collision "+node_name+" not found for : " + name)

func death(include_score:=true):
	#let each enemy handle their own explosion
	
	is_dead=true
	if include_score:
		_update_log()
		OptionsAndAchievements.increment_achievement("onslaught",1)
		if achievement_sure_name!="":
			OptionsAndAchievements.increment_achievement("sure",1,achievement_sure_name)
		GlobalPersist.score+=score
		
		if !Globals.training_mode:
			if is_bio_growth && !Globals.training_mode:
				Globals.biogrowths_killed+=1
				
			if is_human:
				Globals.humans_killed+=1
				#print("humans dead:"+str(Globals.humans_killed))
	
	if bubble_death_drop_tip!="":
		GlobalMethods.do_drop_hint(global_position,bubble_death_drop_tip)
		
	if transient_contained:
		queue_free()
	else:
		#perma death if in same room as spawn point
		var pos=GlobalPersist.player_spawn_point
		var p=GlobalTileMap.current_room_id
		
		var node=get_tree().get_root().find_node(pos,true,false)
		if node!=null:
			var sroom=GlobalTileMap.get_room_id_from_position(node.global_position)
			if sroom==p:
				has_died=true
		disable(false)
	
	
func make_ready(_direction:Vector3):
	#typically set direction, disable leave room, set facing, force direction
	#actual code for entering room offscreen
	#print(name+" entering room")
	pass

func has_entered_room():
	#raised by event
	has_entered_screen=true

func leave_room():
	if has_entered_screen:
		if has_method("leave_room_override_no_delete"):
			if call("leave_room_override_no_delete"):
				#print("override on, not removing from screen")
				return
			else:
				#print("override on but set to false, removing from screen")
				queue_free()
		else:
			queue_free()
	else:
		print("not leaving room as never entered " + name)


func has_left_screen():
	if screen_constraint.size==Vector2.ZERO:
		var tl=GlobalTileMap.main_camera.global_position
		var size=GlobalTileMap.found_screen_size
		screen_constraint=Rect2(tl,size)
	
	my_place=Rect2(global_position.x-my_half_size,
					global_position.y-my_half_size,
					my_half_size*2,
					my_half_size*2
	)

	my_place=Rect2(global_position.x-my_half_size,
					global_position.y-my_half_size,
					my_half_size*2,
					my_half_size*2
	)
	
	if screen_constraint.encloses(my_place):
		return false
	else:
		return true
	
func _process_toggle(status):
	set_process(status)
	set_physics_process(status)
	#belt and braces
	propagate_call("set_process",[status])
	propagate_call("set_physics_process",[status])
	
func _update_log():
	var log_type=""
	log_type=achievement_sure_name
	if log_type!="":
		OptionsAndAchievements.increment_log(log_type,1)	
