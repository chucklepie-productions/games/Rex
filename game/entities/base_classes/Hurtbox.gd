extends Area2D

#collisions on bodies are left as just collisions for blocking movement, etc
#attacks are done via area2d hit/hurtboxes
#this hurt/hitbox is instanced per object then editable childrens shown 
# and collision drawn and the relevant mask bits given

#hurtbox: set LAYER bit to be its hurtbox (playerhurtbox if player, etc)
#hitbox: set MASK bit to be the other hurtbox (enemyhurtbox if player, etc)
#i.e. only one collision is raised not two

#hurtbox gets the event, deals it's own damage by asking hit body it's damage
#we can either have hit/hurt box on all objects or just one side to manage
#e.g. player or enemy has hit/hurt
#however to become more self-reliant, though more wasteful
#we will add both where necessary, e.g. enemy/player both have hurt/hit and 
#damage themeselves
signal take_hit(damage_taken)
signal hurt_started
signal hurt_ended

export(int) var health = 10
#hurt timer normally used by concrete version to flash and play hit sound
export(float) var hurt_timer=0.6
export(bool) var hurt_audio:=true

var parent_is_dead=false	#try to avoid timers triggering after dead

onready var initial_health=health

func _ready() -> void:
	$HurtTimer.wait_time=hurt_timer
	_process_toggle(false)
	
func enable():
	$CollisionShape2D.call_deferred("set_disabled",false)
	health=initial_health
	_process_toggle(true)
	
func reset_health():
	health=initial_health
	
func deactivate():
	#stop being hurt or hitting
	$CollisionShape2D.call_deferred("set_disabled",true)
	pass
	
func disable():
	deactivate()
	_process_toggle(false)

func _on_Hurtbox_area_entered(what_hit: Area2D) -> void:
	$HurtTimer.start()
	emit_signal("hurt_started")
	if what_hit.has_method("get_damage_amount"):
		var damage=what_hit.get_damage_amount()
		health-=damage
		if damage>0 and health>0 and hurt_audio:
			AudioManager.play_sfx(Globals.SFX_ENEMY_EXPLODE3)
		emit_signal("take_hit",health)
	else:
		print("ERROR in enemy " + name+", no method called get_damage_amount from area  "+what_hit.name)
		

func _on_HurtTimer_timeout() -> void:
	#hurt start/stop and flash mechanism inspired by 
	#heartbest https://www.youtube.com/watch?v=Ot9M0TlxApU
	emit_signal("hurt_ended")

func _process_toggle(status):
	set_process(status)
	set_physics_process(status)
