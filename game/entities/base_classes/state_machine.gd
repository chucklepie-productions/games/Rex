extends Node
class_name StateMachine

#abstract state machine
#from game endeavour
# https://www.youtube.com/watch?v=BNU8xNRk_oU
# https://www.youtube.com/watch?v=j_pM3CiQwts
# https://www.youtube.com/watch?v=BQwNiU5v9as

#1. in concrete state copy across _enter_state,_exit_state,_get_transistion,_state_logic
#2. call add_state to add states in _ready
#3. call set_state to set initial state. put in call_deferred as will be called before parent
#4. then code virtuals, usually within concrete physics process

#example
# _state_logic(): apply movement, apply code based on state, e.g. follow player
# _get_transistion(): check state via match and return state to change to
# _enter_state(): match state then change animation and do actions
# _exit_state() may or may not be used

var state = null setget set_state	#if we want to change based on value not calling method
var previous_state = null
var states = {}				#our states. not enum as cannot add to it. Call add_state

#create Node at top level of parent (this node)
#add this script. parent is access to the parent
onready var parent = get_parent()	

# call the virtual logic, e.g. move, apply jump
# if we are transitioning state then call set change
#	this calls exit and enter state (where not null)
func _physics_process(delta: float) -> void:
	if state != null:
		_state_logic(delta)
		var transition = _get_transition(delta)
		if transition !=null:
			set_state(transition)

##VIRTUALS - ALL physics based take delta
#virtual
#apply velocity, apply gravity, slide, etc
#stop player from doing an action if in a specific state, etc
func _state_logic(_delta):
	pass
	
#virtual
#returns what state we should be going into
#handles whether we can move from one state to another, 
#e.g. idle to jump but not jump to walking
func _get_transition(_delta):
	pass
	
#virtual 
#actual changing from one to the next, either going in or coming out
#e.g. changing animations, timers, etc
func _enter_state(_new_state, _old_state):
	pass

#virtual
#as enter state	
func _exit_state(_old_state, _new_state):
	pass

#do the actual state change
func set_state(new_state):
	#nw. don't allow settings same state again to overwrite previous state
	if new_state!=state:
		previous_state = state
		state = new_state
	
	if previous_state != null:
		_exit_state(previous_state, new_state)
	
	if new_state != null:
		_enter_state(new_state, previous_state)
		
# add a state to the state machine. Value is a number (just an arbitrary thing)
func add_state(state_name):
	states[state_name] = states.size()
	
