extends Node2D

export(float) var lifespan=1.0
func _ready() -> void:
	$Timer.wait_time=lifespan
	$Timer.one_shot=true
	$Timer.start()
	rotation_degrees=rand_range(358,362)

func _process(delta: float) -> void:
	pass

func _on_Timer_finished() -> void:
	queue_free()


func _on_Timer2_timeout() -> void:
	AudioManager.play_sfx(Globals.SFX_WEAPON_SMARTEND)
