extends Area2D

export var num_particles:=100
export var life_span:=10.0
export var is_functioning:=true

func _ready() -> void:
	disable(false)

func enable(display:=true):
	print("ENABLE GRAVITY: " + name)	#temp
	if is_functioning:
		_process_toggle(true)
		yield(get_tree(),"idle_frame")
		_set_status(true)
		$CollisionShape2D.set_deferred("disabled",false)
		$Particles.amount=num_particles
		$Particles.lifetime=life_span
		$Particles.preprocess=10#clamp(life_span-1,0,25)
		$Particles.emitting=true
		visible=display
	else:
		disable()
		
func disable(display:=false):
	_set_status(false)
	visible=display
	$CollisionShape2D.set_deferred("disabled",true)
	$Particles.emitting=false
	_process_toggle(false)

func _on_GravityUp_body_entered(body: Node) -> void:
	if body is Player or body is Human:
		body.apply_boost(false)
		AudioManager.play_sfx(Globals.SFX_GAME_LIFTJUMP)


func _on_GravityUp_body_exited(body: Node) -> void:
	if body is Player or body is Human:
		body.apply_boost(true)

func _process_toggle(status):
	set_process(status)
	set_physics_process(status)

func _set_status(enabled:bool):
	set_deferred("monitorable",enabled)
	set_deferred("monitoring",enabled)
	#monitoring=enabled
	
