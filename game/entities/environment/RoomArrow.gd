extends Node2D

export(int) var start_delay:=0
export(bool) var start_playing:=true
export(bool) var start_disabled:=false

func _ready() -> void:
	disable(true)
	
func enable(make_visible:bool):
	
	if start_disabled:
		disable(false)
		return
		
	_process_toggle(true)
	
	if make_visible:
		if start_delay>0:
			visible=false
			$DelayedStart.stop()
			$DelayedStart.wait_time=start_delay
			$DelayedStart.start()
		else:
			if start_playing:
				play_repeat()
			visible=true
	else:
		visible=false
		$AnimationPlayer.stop()

func disable(make_visible:bool):
	_process_toggle(false)
	visible=make_visible
	$AnimationPlayer.stop()
	$DelayedStart.stop()

func _process_toggle(status):
	set_process(status)
	set_physics_process(status)

func _on_DelayedStart_timeout() -> void:
	visible=true
	play_repeat()

func play_once():
	$AnimationPlayer.stop(true)
	$AnimationPlayer.play("once")
	
func play_repeat():
	$AnimationPlayer.play("moving")
