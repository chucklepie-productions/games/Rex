extends Node2D

onready var label=$Label
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	visible=false
	label.text=""

func start(start_text,colour, scale_index,speed:=1.0,_end_y:=96):
	#scale index is 0 to 5
	scale=Vector2(1+scale_index/5,1+scale_index/5)
	label.text=start_text
	if GlobalPersist.graphics_type==0:
		label.set("custom_colors/font_color",Color.white)
	else:
		label.set("custom_colors/font_color",colour)
	visible=true
	$AnimationPlayer.playback_speed=speed
	$AnimationPlayer.play("Score Accumulator")
	
func disable(_display):
	queue_free()
	
func update_text(new_text):
	label.text=new_text
	
func _on_AnimationPlayer_animation_finished(_anim_name: String) -> void:
	queue_free()
