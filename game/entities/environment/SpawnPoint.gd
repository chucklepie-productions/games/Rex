extends Node2D

export(bool) var zoom_on_save=true
export(bool) var create_checkpoint=false
export(bool) var force_off=false
export(bool) var is_rex2:=false
export(bool) var full_birth:=true
export(bool) var nosave_beep:=true
export(bool) var create_sound:=true

onready var c1=$CollisionShape2D
onready var c2=$CollisionShape2D2
onready var c3=$CollisionShapeAfterSave
var can_enter_beep:=false

#signal raised by player not this
func enable(_make_visible:bool):
	if GlobalPersist.player_spawn_point!=null:
		if GlobalPersist.player_spawn_point==name:
			print("spawn already spawned so not enabling")
			return
		else:
			print("spawn not spawned so enabling")
	else:
		print("error with spawn point: not set yet")
	_set_status(true)
	#print(self.name+" ON")
	
func disable(_make_visible:bool):
	#only action is the make monitorable
	_set_status(false)
	#print(self.name+" OFF")

func enable_after_save():
	#if enter when activated then beep
	c3.set_deferred("disabled",false)
	pass
	
func _set_status(enabled:bool):
	#force off is useful when we want to spawn but never check for saving
	#i.e. a load only
	if enabled && force_off:
		return
		
	c1.set_deferred("disabled",!enabled)
	c2.set_deferred("disabled",!enabled)
	c3.set_deferred("disabled",true)		#always disabled unless set
	can_enter_beep=false
	#set_deferred("monitorable",enabled)
	#set_deferred("monitoring",enabled)
	#monitoring=enabled



func _on_SpawnPoint_area_shape_entered(_area_id: RID, _area: Area2D, _area_shape: int, local_shape: int) -> void:
	if local_shape==1:
		Globals.quick_spawn=!full_birth
	if local_shape==2 && can_enter_beep && $TimerPlayDelay.is_stopped() && nosave_beep:
		if !Globals.player_being_born:
			$TimerPlayDelay.start()
			AudioManager.play_sfx(Globals.SFX_GAME_NOCHECKPOINT)


func _on_SpawnPoint_area_shape_exited(area_rid: RID, area: Area2D, area_shape_index: int, local_shape_index: int) -> void:
	if local_shape_index==2:
		can_enter_beep=true
