extends Node2D

signal shield_new_level(new_percent)

onready var particles=$Particles2D
onready var coll=$PlayerDetection/CollisionShape2D
onready var timer=$EnergyTimer

var current_level:=0
var onpod:=false
var last_player_level=0
var recharge_pitch=1.0	#50% is 1.0 everything lese is 0.1 either way
var last_recharch_pitch=0.0

#var current_player_status=false

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	disable()
	$AudioStreamPlayer.stop()

	
func enable(_visible:=true):
	_set_status(true)
	_reset(true)
	
func disable(_visible:=false):
	_set_status(false)
	_reset(false)

func _set_status(enabled:bool):
	set_process(enabled)
	set_physics_process(enabled)
	$PlayerDetection.set_deferred("monitorable",enabled)
	$PlayerDetection.set_deferred("monitoring",enabled)	
	visible=enabled
	
func _process(_delta: float) -> void:
	if GlobalPersist.graphics_type!=0:
		particles.emitting=true
	else:
		particles.emitting=true
	if GlobalTileMap.current_player!=null:
		var l=GlobalTileMap.current_player.get_shield_level()
		current_level=l
		if !onpod && l!=last_player_level:
			#if (l>=100 && last_player_level<100) || (l<100 && last_player_level>=100):
				_set_particles(true)
				last_player_level=l
		else:
			_set_particles(true)

		
func _reset(status):
	visible=status
	particles.emitting=status
	_set_particles(status)
	$AudioStreamPlayer.stop()
	coll.set_deferred("disabled",!status)
	timer.stop()
	timer.wait_time=0.1
	
func _start_process(level):
	particles.lifetime=0.5
	particles.amount=15
	if current_level<99:
		$AudioStreamPlayer.play()
	current_level=level
	if GlobalPersist.graphics_type!=0:
		particles.emitting=true
	timer.start()

func _set_particles(status):
	#we'll have it always
	#if GlobalPersist.graphics_type==0:
	#	return
		
	if GlobalPersist.graphics_type==0:
		particles.process_material.color=GlobalMethods.get_as_colour(Globals.COLOURS.white)
		recharge_pitch = 1.0
	else:
		if current_level<20:
			particles.process_material.color=GlobalMethods.get_as_colour(Globals.COLOURS.red)
			recharge_pitch = 0.6
		elif current_level<40:
			particles.process_material.color=GlobalMethods.get_as_colour(Globals.COLOURS.magenta)
			recharge_pitch = 0.8
		elif current_level<60:
			particles.process_material.color=GlobalMethods.get_as_colour(Globals.COLOURS.cyan)
			recharge_pitch = 1.0
		elif current_level<80:
			particles.process_material.color=GlobalMethods.get_as_colour(Globals.COLOURS.yellow)
			recharge_pitch = 1.4
		elif current_level<99:
			particles.process_material.color=GlobalMethods.get_as_colour(Globals.COLOURS.green)
			recharge_pitch = 1.5
		else:
			particles.process_material.color=GlobalMethods.get_as_colour(Globals.COLOURS.white)

	if status && onpod:
		if current_level>=99:
			$AudioStreamPlayer.stop()	#maybe play nice end
		else:
			recharge_pitch=1.0+(current_level/100.0)-0.5
			if last_recharch_pitch!=recharge_pitch:
				$AudioStreamPlayer.pitch_scale=recharge_pitch
			if !$AudioStreamPlayer.playing:
				$AudioStreamPlayer.play()
			
	else:
		$AudioStreamPlayer.stop()
		
	if !onpod && particles.amount!=15:
		particles.lifetime=0.5
		particles.amount=15
	elif onpod && current_level<100 && particles.amount!=20:
			particles.lifetime=0.8
			particles.amount=20
	elif onpod && current_level>=100 && particles.amount!=15:
			particles.lifetime=0.5
			particles.amount=15

func _completed():
	_set_particles(false)
	timer.stop()
	$AudioStreamPlayer.stop()
	
func _on_PlayerDetection_body_entered(body: Player) -> void:
	#the entrypoint
	#current_player_status=GlobalTileMap.current_player.get_bubble()
	#GlobalTileMap.current_player.set_bubble(true)
	
	if !GlobalPersist.help_pod_seen:
		var loc=GlobalTileMap.get_local_from_global(body.position)
		GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.PICKUP,"pod",Globals.DIALOGUE_TEXT["pod"],true,loc,true)
		GlobalPersist.help_pod_seen=true
		
	body.shield_charging(true)
	onpod=true
	_start_process(body.get_shield_level())
	
func _on_PlayerDetection_body_exited(body: Player) -> void:
	#GlobalTileMap.current_player.set_bubble(current_player_status)
	onpod=false
	_completed()
	body.shield_charging(false)

func _on_EnergyTimer_timeout() -> void:
	current_level+=1
	emit_signal("shield_new_level",current_level)
	if current_level>99:
		timer.stop()
		_completed()
