extends Node2D

onready var poly=$Tesla/Polygon2D
onready var original:PoolVector2Array
onready var moving=false

func _ready() -> void:
	original=poly.points

signal tesla_finished

func start():
	randomize()
	_new_timer()
	$Tesla/AnimationPlayer.play("tesla")
	$Timer.start()
	
func _update():
	poly.points=original
	for x in poly.points.size():
		if x!=0 && x!=poly.points.size()-1:
			poly.points[x].x+=rand_range(-5.0,5.0)
			if moving:
				poly.points[x].y+=rand_range(-5.0,5.0)
	
func start_moving():
	moving=true
	
func stop_moving():
	moving=false


func _on_Timer_timeout() -> void:
	_new_timer()
	
func _new_timer():
	$Timer.wait_time=rand_range(0.2,0.4)
	_update()

func _play_down():
	AudioManager.play_sfx(Globals.SFX_GAME_CHECKPOINTDOWN)
	
func _play_start():
	AudioManager.play_sfx(Globals.SFX_GAME_CHECKPOINTSTART)
	
func _on_AnimationPlayer_animation_finished(_anim_name: String) -> void:
	queue_free()
	emit_signal("tesla_finished")
