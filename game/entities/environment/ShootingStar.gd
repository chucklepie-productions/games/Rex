extends Node2D

export(int) var velocity_star=450
export(int) var height_random=50
export(float) var min_start=5.0
export(float) var max_start=15.0
var flight_angle
var are_we_visible:bool

func _ready() -> void:
	randomize()
	#make angle max relative to the height
	flight_angle=rand_range(-5.0,5.0)
	rotation_degrees=flight_angle
	position.y-=rand_range(-height_random,height_random)
	randomize()
	$Timer.wait_time=rand_range(min_start,max_start)
	disable(false)
	
func _process(delta: float) -> void:
	var direction=Vector2(-velocity_star,0).rotated(deg2rad(flight_angle))
	position+=direction*delta

func _on_VisibilityNotifier2D_viewport_exited(_viewport: Viewport) -> void:
	#print("shooting star has left the sky " + name)
	if !GlobalTileMap.camera_zoom:
		queue_free()

func enable(make_visible:bool):
	#print("shooting star on "+name)
	are_we_visible=make_visible
	set_process(false)
	$Timer.start()

func disable(make_visible:bool):
	#print("shooting star off "+name)
	$Timer.stop()
	set_process(false)
	set_physics_process(false)
	visible=make_visible
	


func _on_Timer_timeout() -> void:
	set_process(true)
	set_physics_process(true)
	AudioManager.play_sfx(Globals.SFX_GAME_SHOOTINGSTAR)
	visible=are_we_visible
