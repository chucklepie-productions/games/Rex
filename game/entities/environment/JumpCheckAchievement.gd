extends Area2D

var one_done
var mid_done

func enable(_make_visible:bool):
	_set_status(true)
	
func disable(_make_visible:bool):
	_set_status(false)

func _set_status(enabled:bool):
	set_deferred("monitorable",enabled)
	set_deferred("monitoring",enabled)
	set_process(enabled)
	set_physics_process(enabled)
	$Timer.stop()
	

func _on_Timer_timeout() -> void:
	one_done=false
	mid_done=false

func _on_JumpCheckAchievement_body_shape_entered(_body_id: RID, _body: Node, _body_shape: int, area_shape: int) -> void:
	if area_shape==0:
		one_done=true
		$Timer.start()
	elif area_shape==1:
		one_done=false
		$Timer.stop()
	elif area_shape==2 && one_done && !mid_done:
		#achievement
		OptionsAndAchievements.increment_achievement("neo",1)
