extends Area2D


export(bool) var fake_bunny=true
var is_bouncing:=true
var needs_jump:=true

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

func _physics_process(delta: float) -> void:
	if $AnimationPlayer.playback_speed==1.0 && Globals.bullet_time!=1.0:
		$AnimationPlayer.playback_speed=0.08
	elif $AnimationPlayer.playback_speed!=1.0 && Globals.bullet_time==1.0:
		$AnimationPlayer.playback_speed=1.0
		
func _on_BunnyHunt_body_entered(_body: Node) -> void:
	if !fake_bunny:
		OptionsAndAchievements.increment_achievement("bunny",1)
		GlobalPersist.bunny_unlocked=true
		GlobalPersist.save_bunny_world_data()
		return
		
	AudioManager.play_sfx(Globals.SFX_GAME_NORABBIT)
	queue_free()


func _set_status(enabled:bool):
	set_process(enabled)
	set_physics_process(enabled)
	set_deferred("monitorable",enabled)
	set_deferred("monitoring",enabled)	
	$CollisionShape2D.set_deferred("disabled",!enabled)
	visible=enabled
	
#virtual
func enable(_make_visible:bool):
	_set_status(true)

func disable(_make_visible:bool):
	_set_status(false)
