extends Node2D
signal tesla_rex2_finished


func start():
	$AnimationPlayer.play("rotate")
	
func _on_AnimationPlayer_animation_finished(_anim_name: String) -> void:
	emit_signal("tesla_rex2_finished")
	queue_free()
