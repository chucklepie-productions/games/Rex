extends Node2D

export(bool) var is_animated=false

func _ready():
	if is_animated:
		$AnimationPlayer.play("move")
	else:
		$AnimationPlayer.play("RESET")
