extends KinematicBody2D
class_name Pickups
var bubble_type:="large"	#or small
const JUMP_BUBBLE=-140
const JUMP_OTHER=-90
const FLASH_LIFE=3.5


var velocity_pickup:=Vector2.ZERO
var is_bouncing=false
var needs_jump=false
var size
var gravity=JUMP_BUBBLE
var gravity_override=false
var b_location
var b_type

#enum PICKUP {LARGE_ENERGY, SMALL_ENERGY, SMARTBOMB, RANDOMITEM,OXYGEN,HINT}

#these are for the screen based items really, they are all overridden in code
export var life_span:=0
export var bounce_pause:=5
export var start_delay=0.0
export var play_bounce_audio:=false
export var play_got_audio:=false
export var play_notgot_audio:=false

export var metadata:=""			#passed with signal, may or may not be used
#this silly line is required, otherwise it breaks with cyclic dependencies and all sorts
export(preload("res://singletons/Globals.gd").PICKUP) var pickup_type
export var has_gravity:=true
export var trigger_delay:=0.0		#how long after triggering to raise event, e.g. to let player move or do something

var start_flashing=false

export var has_ceiling_animation:=false
export var show_visible:=true

onready var anim=$AnimatedSprite
onready var original_position=global_position

signal picked_up(value,location, meta)

func _ready() -> void:
	disable(false)
	size=anim.frames.get_frame("large",0).get_size()	#to calculate offset (all are same size)
	if life_span>0:
		$AliveTimer.wait_time=life_span
		$FlashBeforeDeath.wait_time=life_span-FLASH_LIFE if life_span>FLASH_LIFE else life_span
		
func enable(_display):
	#called for fixed room pickups
	#give room bubbles an extra bounce
	if !show_visible:
		visible=false
		return
	
		
	velocity_pickup=Vector2.ZERO
	if pickup_type==Globals.PICKUP.LARGE_ENERGY || pickup_type==Globals.PICKUP.SMALL_ENERGY:
		gravity=-220	#just to bounce up platforms for secret rooms
		gravity_override=true
		
	start_bouncing(original_position,pickup_type,start_delay,bounce_pause,life_span)
	
func disable(_display):
	#called for fixed room pickups
	$AliveTimer.stop()
	$BouncePause.stop()
	$DelayStartTimer.stop()
	$FlashBeforeDeath.stop()
	_set_status(false)

	
func start_bouncing(location:Vector2, type:=Globals.PICKUP.LARGE_ENERGY, 
		start_pause:=0.0, bounce_wait:=0, life_time:=10):
	bounce_pause=bounce_wait
	start_delay=start_pause
	life_span=life_time
	b_location=location
	b_type=type
	
	if start_pause>0.0:
		$DelayStartTimer.wait_time=start_pause
		$DelayStartTimer.start()
	else:
		_continue_bounce()
		#yield(get_tree().create_timer(start_pause), "timeout")
		
func _continue_bounce():
	#using yield causes internal errors when nothing to come back to
	global_position=b_location-(size/2)
	pickup_type=b_type
	
	if !gravity_override:
		gravity=JUMP_BUBBLE
	match pickup_type:
		Globals.PICKUP.LARGE_ENERGY:
			bubble_type="large"
			has_ceiling_animation=true
		Globals.PICKUP.SMALL_ENERGY:
			bubble_type="small"
			has_ceiling_animation=true
		Globals.PICKUP.SMARTBOMB:
			gravity=JUMP_OTHER
			bubble_type="smartbomb"
			life_span=0
		Globals.PICKUP.RANDOMITEM:
			life_span=0
			gravity=JUMP_OTHER
			bubble_type="randompickup"
		Globals.PICKUP.OXYGEN:
			bubble_type="oxygen"
			life_span=0
			gravity=JUMP_OTHER
		Globals.PICKUP.HINT:
			bubble_type="hint"
			life_span=0
			gravity=JUMP_OTHER
		_:
			print("ERROR unknown pickup selected, using default " + str(pickup_type))

	if $AliveTimer.is_stopped() && life_span>0:
		print("starting alive/flash " + str(name))
		$AliveTimer.wait_time=life_span
		$FlashBeforeDeath.wait_time=life_span-FLASH_LIFE if life_span>FLASH_LIFE else life_span
		$AliveTimer.start()
		$FlashBeforeDeath.start()
		
	if bounce_pause>0:
		$BouncePause.wait_time=bounce_pause
		$BouncePause.start()

	_set_status(true)
	anim.play(bubble_type)
	
func _set_status(enabled:bool):
	set_process(enabled)
	set_physics_process(enabled)
	$CollisionShape2D.set_deferred("disabled",!enabled)
	$PlayerCheck.set_deferred("monitorable",enabled)
	$PlayerCheck.set_deferred("monitoring",enabled)	
	visible=enabled && show_visible

func _physics_process(delta: float) -> void:
	if is_bouncing:
		return
		
	var collision=move_and_collide(velocity_pickup*delta*Globals.bullet_time)
	if collision:
		#bounce, or wait for timer if set up
		if bounce_pause<=0.0:
			_start_bounce()
	else:
		anim.play(bubble_type)
		
	if has_gravity:
		velocity_pickup.y+=Globals.GRAVITY*delta*1.2	#speed i up a bit
	velocity_pickup.x=0
		
	if !is_equal_approx(anim.speed_scale,Globals.bullet_time):
		anim.speed_scale=Globals.bullet_time
		#if timers are running then pause/unpause them
		if !$AliveTimer.is_stopped():
			$AliveTimer.paused=(Globals.bullet_time!=1.0)
		if !$BouncePause.is_stopped():
			$BouncePause.paused=(Globals.bullet_time!=1.0)
		if !$DelayStartTimer.is_stopped():
			$DelayStartTimer.paused=(Globals.bullet_time!=1.0)
		if !$FlashBeforeDeath.is_stopped():
			$FlashBeforeDeath.paused=(Globals.bullet_time!=1.0)

func _start_bounce():
	#the environment
	is_bouncing=true
	if velocity_pickup.y>=0.0:
		anim.play(bubble_type+"_bounce")
		if play_bounce_audio and !start_flashing and Globals.bullet_time==1.0:
			AudioManager.play_sfx(Globals.SFX_GAME_PICKUP_DEFAULT)
		needs_jump=true
	else:
		if has_ceiling_animation:
			anim.play(bubble_type+"_bounce_ceiling")
		else:
			anim.play(bubble_type+"_bounce")
	velocity_pickup.y=0

func _bounce():
	if has_gravity:
		if needs_jump:
			velocity_pickup.y+=gravity
		else:	#bit of a jolt from ceiling
			velocity_pickup.y+=50
	is_bouncing=false
	needs_jump=false
	if start_flashing:
		$BouncePause.stop()
		velocity_pickup.y=0
	
func _on_AliveTimer_timeout() -> void:
	print("alive ran out for " + str(name))
	if bubble_type=="large":
		GlobalMethods.explosion_shards(global_position,"Bubble",null,0.0,0.4)
	else:
		GlobalMethods.explosion_miss(global_position,Globals.COLOURS.white_bright)
	queue_free()


func _on_AnimatedSprite_animation_finished() -> void:
	_bounce()


func _on_PlayerCheck_body_entered(body: Node) -> void:
	#player only
	if trigger_delay>0.0:
		yield(get_tree().create_timer(trigger_delay), "timeout")
		
	var c1=Globals.COLOURS.white_bright
	if pickup_type==Globals.PICKUP.RANDOMITEM:
		c1=Globals.COLOURS.magenta_bright
	elif pickup_type==Globals.PICKUP.SMARTBOMB:
		c1=Globals.COLOURS.cyan_bright
	elif pickup_type==Globals.PICKUP.OXYGEN:
		c1=Globals.COLOURS.cyan_bright
	elif pickup_type==Globals.PICKUP.HINT:
		c1=Globals.COLOURS.green_bright
	if visible:
		GlobalMethods.explosion_miss(global_position+(size/2),c1)
	
	if body is Player && (!body.lock_player || pickup_type==Globals.PICKUP.HINT):
		emit_signal("picked_up",pickup_type,global_position,metadata)
		_do_help()
	queue_free()

func _do_help():
	var text=null
	var key=null
	match pickup_type:
		Globals.PICKUP.LARGE_ENERGY,Globals.PICKUP.SMALL_ENERGY:
			if !GlobalPersist.help_energy_seen:
				text=Globals.DIALOGUE_TEXT["energy"]
				key="energy"
				GlobalPersist.help_energy_seen=true

		Globals.PICKUP.SMARTBOMB:
			if !GlobalPersist.help_smart_seen:
				text=Globals.DIALOGUE_TEXT["smart"]
				key="smart"
				GlobalPersist.help_smart_seen=true
				
		Globals.PICKUP.OXYGEN:
			if !GlobalPersist.help_oxygen_seen:
				text=Globals.DIALOGUE_TEXT["oxygen"]
				key="oxygen"
				GlobalPersist.help_oxygen_seen=true
				
		_:
			#text=Globals.DIALOGUE_TEXT["rnd"]
			#cannot do here as cannot wait for dialog to finish
			#will require some other node to display it
			pass
			
	if text!=null:
		var loc=GlobalTileMap.get_local_from_global(position)
		GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.PICKUP,key,text,true,loc,true)

func _on_BouncePause_timeout() -> void:
	$BouncePause.wait_time=bounce_pause
	$BouncePause.start()
	_start_bounce()
		


func _on_DelayStartTimer_timeout() -> void:
	print("starting alive/flash as delayed start" + str(name))
	_continue_bounce()


func _on_FlashBeforeDeath_timeout():
	#first time is 3.5 seconds before death
	print("starting flash before death " + str(name))
	if !start_flashing:
		start_flashing=true
		#$FlashBeforeDeath.wait_time=0.15
		#$FlashBeforeDeath.start()
	#else:
	#	visible=!visible
