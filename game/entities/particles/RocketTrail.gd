extends Particles2D

func _ready() -> void:
	#disable(false)
	pass
	
func enable(make_visible:bool):
	_process_toggle(true)
	visible=make_visible
	emitting=true
	#print("enabling rockettrail " + name)

func disable(make_visible:bool):
	_process_toggle(false)
	visible=make_visible
	emitting=false
	#print("disabling rockettrail " + name)

func _process_toggle(status):
	set_process(status)
	set_physics_process(status)
