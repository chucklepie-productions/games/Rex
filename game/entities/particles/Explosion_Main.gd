extends ExplosionBase

onready var anim=$AnimationPlayer
var modifier=""

func _ready():
	pass # Replace with function body.

#overrides
func enable(visible):
	.enable(visible)
	
func disable(_visible):
	.stop()
	
func stop():
	.stop()
	
func play():
	if anim.has_animation(modifier):
		anim.play(modifier)

#bespoke
func initialise(anim_name):
	modifier=anim_name
	
#events
func _on_AnimationPlayer_animation_finished(_anim_name):
	queue_free()

