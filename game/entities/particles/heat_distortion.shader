shader_type canvas_item;

//GLSL: uniform samplerXX iChannel0..3; 
uniform sampler2D iChannel1; // presumed 0 is TEXTURE and 1 is imput image
uniform bool active=true;
uniform float level=6.0;
vec3 permute(vec3 x) { return mod(((x*34.0)+1.0)*x, 289.0); }

float snoise(vec2 v){
  const vec4 C = vec4(0.211324865405187, 0.366025403784439,
           -0.577350269189626, 0.024390243902439);
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);
  vec2 i1;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;
  i = mod(i, 289.0);
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
  + i.x + vec3(0.0, i1.x, 1.0 ));
  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy),
    dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;
  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}
float snoise_octaves(vec2 uv, int octaves, float alpha, float beta, vec2 gamma, float delta) {
    vec2 pos = uv;
    float t = 1.0;
    float s = 1.0;
    vec2 q = gamma;
    float r = 0.0;
    for(int i=0;i<octaves;i++) {
        r += s * snoise(pos + q);
        pos += t * uv;
        t *= beta;
        s *= alpha;
        q *= delta;
    }
    return r;
}


void fragment(){
	if(!active) {
		COLOR=vec4(0.0,0.0,0.0,0.0);
	} else {
		//GLSL: vec2 p_m = fragCoord.xy / iResolution.xy;
		//vec2 p_m = frag.xy / iResolution.xy;
		vec2 p_m=SCREEN_UV.xy;
		
	    vec2 p_d = p_m;
		
		//GLSL: : p_d.t -= iTime * 0.1
	    p_d.y -= TIME * 0.1;
		
	    vec4 dst_map_val = texture(iChannel1, abs(cos(p_d)));	//why abs(cos(p_d)
	    vec2 dst_offset = dst_map_val.xy;
	    dst_offset -= vec2(.5,.5);
	    dst_offset *= level;	
	    dst_offset *= 0.01;
		
		//GLSL: dst_offset *= (1. - p_m.t);
	    dst_offset *= (1. - p_m.y);	//why y
	    vec2 dist_tex_coord = p_m.xy + dst_offset;
		
		//GLSL: fragColor = texture(iChannel0, dist_tex_coord);
	    COLOR = texture(SCREEN_TEXTURE, dist_tex_coord);
	}
}
