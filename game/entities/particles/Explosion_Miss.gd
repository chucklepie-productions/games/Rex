extends ExplosionBase

func _on_Timer_timeout() -> void:
	stop()

#overrides
func enable(visible):
	.enable(visible)
	
func disable(visible):
	.disable(visible)
	
func stop():
	.stop()
	
func play():
	$Particles2D.emitting=true
	#yield(get_tree().create_timer(0.8), "timeout")
	#yield creates error if particle is dead before yield resumes
	$Timer.start()
