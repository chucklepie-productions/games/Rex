extends ExplosionBase

onready var image_shard=$ImageSharding
onready var shard_timer=$ShardTimer
onready var texture_original=$Textures_Original

#overridden by user
var shard_start_delay:float
var shard_rand_colour:bool
var shard_count_max:int
var shard_speed:int
var shard_gravity:float
var shard_time_alive:float
var shard_fade_away:bool

#set by code
var shard_texture:Texture=null
var shard_velocity_map = {}
var shard_width=64
var shard_height=64
var shard_xoffset
var shard_yoffset

var texture_base

func _ready():
	texture_base=texture_original
	#position=Vector2(100,100)
	#initialise("Bouncer")

#overrides
func _process(delta):
	if shard_texture!=null:
		_shard_process(delta)

func enable(visible):
	.enable(visible)
	
func disable(_visible):
	stop()
	
func stop():
	if shard_timer:
		shard_timer.stop()
	_shard_reset()
	.stop()
	
func play():
	if shard_texture==null:
		return
		
	if shard_start_delay>0.0:
		yield(get_tree().create_timer(shard_start_delay),"timeout")
	#sometimes causes issues so using nullcheck
	if shard_timer!=null:
		shard_timer.start()
		_shard_explode()


#bespoke public
func initialise(texture_name, tex,
		start_delay, 
		count_max,
		speed,
		gravity,
		time_alive,
		fade_away=false,		#these two not in this verison, go earlier if want them
		rand_colour=false):
			
	shard_count_max=count_max
	shard_fade_away=fade_away
	shard_gravity=gravity
	shard_rand_colour=rand_colour
	shard_speed=speed
	shard_start_delay=start_delay
	shard_time_alive=time_alive
	
	#TODO: cannot get texture passing to work so until then use images
	tex=null
	if tex!=null:
		shard_texture=tex
		#this should be a create from image but it still doesn't work
	else:
		if texture_name==null || texture_name=="":
			shard_texture=null
			return
		var shard_node=texture_base.get_node_or_null(texture_name)
		if shard_node:
			shard_texture=shard_node.texture
	_ready_shard(texture_name!="Bubble")

#bespoke private
func _ready_shard(centre_offset=true):
	#set positions, note this is top left based
	#so we offset by half
	shard_width=shard_texture.get_width()
	shard_height=shard_texture.get_height()
	
	if centre_offset:
		shard_xoffset=(shard_width/2.0)
		shard_yoffset=(shard_height/2.0)
	else:
		shard_xoffset=0
		shard_yoffset=0
	position-=Vector2(shard_xoffset,shard_yoffset)
	
	#this updates the image fine
	image_shard.texture=shard_texture

	#this works
	var a=[Vector2(0,0),
			Vector2(shard_width,0),
			Vector2(shard_width,shard_height),
			Vector2(0,shard_height)]
	var p=PoolVector2Array(a)
	image_shard.polygon=p
	shard_timer.wait_time=shard_time_alive

func _shard_explode():
	#this will let us add more points to our polygon later on
	var shard_count=randi()%shard_count_max+1
	var points = image_shard.polygon
	for _i in range(shard_count):
		points.append(Vector2(randi()%shard_width, randi()%shard_height))

	var delaunay_points = Geometry.triangulate_delaunay_2d(points)
	
	if not delaunay_points:
		print("serious error occurred no delaunay points found")
	
	#loop over each returned triangle
	for index in int(len(delaunay_points) / 3.0):
		var shard_pool = PoolVector2Array()
		#find the center of our triangle
		var center = Vector2.ZERO
		
		# loop over the three points in our triangle
		for n in range(3):
			shard_pool.append(points[delaunay_points[(index * 3) + n]])
			center += points[delaunay_points[(index * 3) + n]]
			
		# adding all the points and dividing by the number of points gets the mean position
		center /= 3
		
		#create a new polygon to give these points to
		
		var shard = Polygon2D.new()
		shard.name="Polygon2D_"+shard.name
		shard.polygon = shard_pool
		
		if shard_rand_colour:
			shard.color = Color(randf(), randf(), randf(), 1)
		else:
			shard.texture = image_shard.texture
			
		shard_velocity_map[shard] = Vector2(shard_width/2, shard_height/2) - center #position relative to center of sprite
			
		add_child(shard)
		#print(shard)
		
	#this will make our base sprite invisible
	image_shard.color.a = 0
	shard_timer.start()

func _shard_process(delta):
	#we wan't to chuck our traingles out from the center of the parent
	for child in shard_velocity_map.keys():
		child.position -= (shard_velocity_map[child] * delta * shard_speed)
		#ATTEMPT TO SEE IF STOPS DODGY SPINNING
		
		#child.rotation -= shard_velocity_map[child].x * delta * 0.2
		#apply gravity to the velocity map so the triangle falls
		shard_velocity_map[child].y -= delta * shard_gravity
		if shard_fade_away:
			var f=(shard_timer.time_left)/shard_timer.wait_time
			#leave it whole for a bit at least
			if f<0.3:
				child.modulate.a=f

func _shard_reset():
	#image_shard.color.a = 1
	for child in get_children():
		if child.name.findn("Polygon2D_")!=-1:
			child.queue_free()
	shard_velocity_map = {}
	shard_texture=null
	
#events
func _on_ShardTimer_timeout():
	stop()
