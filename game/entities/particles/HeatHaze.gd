extends Sprite

func _ready() -> void:
	pass
	
func enable(make_visible:bool):
	_process_toggle(true)
	visible=make_visible
	if make_visible && GlobalPersist.graphics_type!=0:
		material.set_shader_param("active",true)

func disable(_make_visible:bool):
	visible=false
	material.set_shader_param("active",false)
	_process_toggle(false)
	#print("disabling haze " + name)

func _process_toggle(status):
	set_process(status)
	set_physics_process(status)
