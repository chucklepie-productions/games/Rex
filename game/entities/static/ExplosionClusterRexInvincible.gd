extends ExplosionCluster

func start_explosions(zindex_override:=0):
	#lock player and start normal explosions
	if !Globals.invincible:
		return
	GlobalTileMap.current_player.enable_lock(true)
	.start_explosions(zindex_override)

func finished():
	#wait for explosion, then show dialog
	#do not call parent as it only queue frees
	yield(get_tree().create_timer(max_delay_between_explosions+1.0), "timeout")
	GlobalTileMap.current_player.enable_lock(false)
	Globals.invincible=(GlobalPersist.game_mode==GlobalPersist.GAMEMODE.SIMPLE)
	Globals.game_scene.player.update_player_invincible()
	queue_free()
	GlobalMethods.do_hint(position,"tutorial_full_gone")
	
