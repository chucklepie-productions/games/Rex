extends Area2D

func _on_ExitShip_body_entered(_body: Node) -> void:
	#that's it, the end
	#ride away
	#show tv
	#game over screen
	
	#ride away
	GlobalTileMap.current_player.visible=false
	GlobalTileMap.current_player.enable_lock(true)
	yield(get_tree().create_timer(2), "timeout")	
	Globals.game_scene.hud_ui.show_hud(false)	#put back on after queen dies
	yield(get_tree().create_timer(2), "timeout")	
	var ship=get_tree().root.find_node("rex1_startship3",true,false)
	var anim=ship.get_node("AnimationPlayer")
	anim.play("ride_away_left")
	yield(anim,"animation_finished")

	#go to platform page
	Globals.game_ended=true
	Globals.game_scene.direct_room_change(Vector2.LEFT)
