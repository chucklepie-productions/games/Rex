extends Enemy

signal queen_dead

onready var clusterdead=$ExplosionClusterStart
onready var moredead=$ExplosionClusterFinish
onready var mat:Material=$Sprite.material
onready var fire_points=$FirePoints
onready var bullet=preload("res://entities/projectiles/RoofTurretMissile.tscn")
onready var homing=preload("res://entities/projectiles/Missile_Launcher_Missile_Homing.tscn")

export(bool) var is_real_boss:=false

enum STATES {NONE,FAKE,EXIT}

var boss_progress=STATES.NONE
var container
var health_counter:=1

func _ready() -> void:
	randomize()
	disable(false)
	invincible(!is_real_boss)
	container=GlobalMethods.assign_temp_location()
	set_as_real(is_real_boss)
	
#virtuals
func enable(make_visible:bool):
	.enable(make_visible)
	invincible(!is_real_boss)
	
func disable(make_visible:bool):
	.disable(make_visible)
	invincible(true)
	
func set_graphics_sprite_type():
	.set_graphics_sprite_type()

func enable_wobble(value):
	if mat:
		mat.set_shader_param("wobble",value)
		
func _process(_delta: float) -> void:
	match boss_progress:
		STATES.NONE:
			return
		STATES.FAKE:
			_real_ending_setup()
		STATES.EXIT:
			_real_ending_exit_room()
		
###
### functions for FAKE queen
###
func _on_Area2D_area_entered(_area: Area2D) -> void:
	if boss_progress==STATES.NONE && !is_real_boss:
		_fake_ending()
	
func set_as_real(realmode:bool):
	$FakeWeakPoint.set_deferred("monitoring",!realmode)
	$FakeWeakPoint.set_deferred("monitorable",!realmode)
	invincible(!realmode)
	
func _fake_ending():
	var loc=Vector2(224,250)	#of dialog
	
	#do initial cluster to wipe out the queen
	#wait a few seconds so player can drop to platform then lock it
	Globals.game_scene.fullscreen_shock(true,Vector2(0.4,0.5),true)
	yield(get_tree().create_timer(0.75), "timeout")
	clusterdead.start_explosions()
	yield(get_tree().create_timer(1), "timeout")
	GlobalTileMap.current_player.enable_lock(true)
	yield(get_tree().create_timer(1), "timeout")
	GlobalMethods.explosion_shards(global_position+Vector2(-145,-165),"BigBoss",null,0.0,2.0,30,10,98)	
	$Sprite.visible=false
	yield(get_tree().create_timer(1.7), "timeout")
	
	#now just do more explosions in a bigger area
	moredead.start_explosions()
	yield(get_tree().create_timer(10), "timeout")

	#now show fake ending
	#pause
	GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.TUTORIAL,"squidgy",
		Globals.DIALOGUE_TEXT["squidgy"],true,loc, true, 0.04, false, false,true,false,true)
	yield(GlobalMethods,"dialog_finished")
	boss_progress=STATES.FAKE

func _real_ending_setup():
	#now wait a few seconds and do real ending
	boss_progress=STATES.NONE
	
	yield(get_tree().create_timer(4), "timeout")
	
	#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"boss_fake",Globals.DIALOGUE_TEXT["boss_fake"],true)
	GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["boss_fake"],false, true,true)
	boss_progress=STATES.EXIT

func _real_ending_exit_room():
	boss_progress=STATES.NONE
	GlobalTileMap.set_tiles_from_array(GlobalTileMap.current_map,321, [
		[378,3],
		[379,3],
		[382,6],
		[383,6],
		[377,8],
		[378,8],
		[380,10],
		[381,10],
	])
	GlobalTileMap.current_player.enable_lock(false)
	queue_free()

###
### functions for REAL queen fight
###

func fire(up,down,left,right):
	if up:
		fire_bullet([fire_points.get_node("Position2D7")],Vector2.UP)
	if down:
		fire_bullet([fire_points.get_node("Position2D4")],Vector2.DOWN)
		if randi()%10>7:
			fire_bullet([fire_points.get_node("Position2D6")],Vector2.DOWN)
	if left:
		if randi()%10>4:
			fire_bullet([fire_points.get_node("Position2D2")],Vector2.LEFT)
		if randi()%10>7:
			fire_bullet([fire_points.get_node("Position2D3")],Vector2.LEFT)
	if right:
		if randi()%10>4:
			fire_bullet([fire_points.get_node("Position2D8")],Vector2.RIGHT)
		if randi()%10>7:
			fire_bullet([fire_points.get_node("Position2D1")],Vector2.RIGHT)
		if randi()%10>8:
			fire_bullet([fire_points.get_node("Position2D6")],Vector2.RIGHT)

func fire_bullet(positions:Array,dir:Vector2):
	for pos in positions:
		var bullet_object=bullet.instance()
		bullet_object.get_node("Sprite").modulate=Color(GlobalMethods.get_as_colour(Globals.COLOURS.yellow_bright))
		weapon_container.add_child(bullet_object)
		bullet_object.global_position=pos.global_position
		
		match dir:
			Vector2.UP:
				bullet_object.set_angle(270)
			Vector2.DOWN:
				bullet_object.set_angle(90)
			Vector2.LEFT:
				bullet_object.set_angle(180)
			Vector2.RIGHT:
				bullet_object.set_angle(0)
		bullet_object.start()
	
func fire_missile():
	var node=GlobalMethods.assign_temp_location()
	var missile_object=homing.instance()
	missile_object.target_is_player=true
	node.add_child(missile_object)
	missile_object.global_position=fire_points.get_node("Position2D5").global_position
	missile_object.launch(1)

func _on_Hurtbox_take_hit(_damage_taken) -> void:
	health_counter-=1
	#print(health_counter)
	if health_counter<=0:
		emit_signal("queen_dead")
	
func _on_Hurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("Flash")

func _on_Hurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("Default")


###
### functions for REAL queen final death
###
func final_death_explode():
	clusterdead.start_explosions()
	yield(get_tree().create_timer(1), "timeout")
	moredead.start_explosions()
