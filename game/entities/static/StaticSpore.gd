extends Enemy

var lives
var progress_amount
var max_health
var max_lives
const LIFE_TIME=0.65

onready var anim=$AnimatedSprite
onready var hurt=$Hurtbox

func _ready() -> void:
	randomize()
	anim.frame=0
	lives=anim.frames.get_frame_count("progress")
	max_lives=lives
	
	progress_amount=hurt.health/(lives-1)
	max_health=hurt.health
	anim.frame=0
	$Timer.wait_time=LIFE_TIME
	set_graphics_sprite_type()

func _process(_delta: float) -> void:
	#last frame is just for show
	if anim.frame==max_lives-1:
		yield(get_tree().create_timer(0.1), "timeout")
		bubble_death()

func set_graphics_sprite_type():
	_set_sprite_type($AnimatedSprite)

func enable(make_visible:bool):
	#call parent
	.enable(make_visible)
	$Hurtbox.reset_health()

func disable(make_visible:bool):
	#call parent
	.disable(make_visible)
	$Timer.stop()

func make_ready(direction:Vector3):
	.make_ready(direction)

func bubble_death(create_bubble:=true, via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	if is_dead:
		return
	var tex=$AnimatedSprite.frames.get_frame("progress",0).get_data()
	GlobalMethods.enemy_explosion(global_position,false,"StaticSpore",0.25,via_smart_bomb,tex,Globals.SFX_ENEMY2_EXPLODE2)
	if gives_energy_on_death && create_bubble && bubble_death_drop_tip=="":
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.LARGE_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)
	anim.frame=0
	lives=anim.frames.get_frame_count("progress")

	
func _on_Hurtbox_take_hit(new_health) -> void:
	if new_health<0:
		#health is irrelevant, we deal in lives
		#lives are linked to frames
		#always reset health back to max
		hurt.health=max_health
		lives-=1
		if lives<=0:
			bubble_death()
			#OptionsAndAchievements.increment_achievement("sure",1,"Spore1")
		else:
			anim.frame+=1
			$Timer.wait_time=LIFE_TIME
			$Timer.start()

func _on_Hurtbox_hurt_started() -> void:
	#$FlashAnimationPlayer.play("flash")
	pass

func _on_Hurtbox_hurt_ended() -> void:
	#$FlashAnimationPlayer.play("default")
	pass

func _on_Timer_timeout() -> void:
	if is_dead:
		return
		
	lives+=1
	anim.frame-=1
	hurt.health=lives*progress_amount
	lives=min(lives,max_lives)
	$Timer.wait_time=LIFE_TIME
	$Timer.start()
	

