extends Enemy

var missile=preload("res://entities/projectiles/RoofTurretMissile.tscn")
var waiting:=false
export(Array) var enable_positions:=[true,true,true,true,true]

func _ready() -> void:
	randomize()
	set_graphics_sprite_type()

func _physics_process(_delta: float) -> void:
	$AnimatedSprite.set_speed_scale(Globals.bullet_time)

func set_graphics_sprite_type():
	_set_sprite_type($AnimatedSprite)

func enable(make_visible:bool):
	#call parent
	#print("ENABLE TURRET SPORE: "+name)
	.enable(make_visible)
	$Hurtbox.reset_health()
	
	if make_visible:
		$AnimatedSprite.play("normal")
	else:
		$AnimatedSprite.stop()
	$Timer.wait_time=0.1
	$Timer.start()

func disable(make_visible:bool):
	#call parent
	#print(" DISABLE TURRET SPORE: "+name)
	.disable(make_visible)
	$AnimatedSprite.stop()
	$Timer.stop()

func make_ready(direction:Vector3):
	.make_ready(direction)

func bubble_death(create_bubble:=true, via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	if is_dead:
		return
	var tex=$AnimatedSprite.frames.get_frame("activate",3).get_data()
	GlobalMethods.enemy_explosion(global_position,false,"RoofDome",0.25,via_smart_bomb,tex,Globals.SFX_ENEMY2_EXPLODE2)

	if gives_energy_on_death && create_bubble && bubble_death_drop_tip=="":
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.LARGE_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)

func launch():
	if Globals.bullet_time==1.0:
		for x in range(5):
			if enable_positions[x]:
				var missile_object=missile.instance()
				missile_object.global_position=get_node("Position"+str(x+1)).global_position
				missile_object.set_angle(_get_angle(x+1))
				weapon_container.add_child(missile_object)
				missile_object.start()
			
func _get_angle(position:int):
	var angle:int = 180
	
	match position: 
		0:
			pass
		1:			#ok
			angle = 108-90
		2:			#ok
			angle = 134-90
		3:			#ok
			angle = 180-90
		4:			#ok
			angle = 226-90
		5:
			angle = 252-90
	return angle
	
func _on_Hurtbox_take_hit(damage_taken) -> void:
	if damage_taken<=0:
		bubble_death()
		#OptionsAndAchievements.increment_achievement("sure",1,"Spore2")

func _on_Hurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")

func _on_Hurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")

func _on_Timer_timeout() -> void:
	#we are ready to activate
	$AnimatedSprite.play("activate")

func _on_AnimatedSprite_animation_finished() -> void:
	if is_dead:
		return
		
	if $AnimatedSprite.animation=="activate" && !waiting:
		#if activate finished pause slightly then fire
		launch()
		waiting=true
		yield(get_tree().create_timer(0.1), "timeout")
		waiting=false
		$AnimatedSprite.play("deactivate")
	elif $AnimatedSprite.animation=="deactivate":
		#if deactivate finished go to normal and start next timer
		$AnimatedSprite.play("normal")
		$Timer.wait_time=rand_range(0.3,4.0)
		$Timer.start()
