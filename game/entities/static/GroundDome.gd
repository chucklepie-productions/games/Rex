extends Enemy

export var firing:bool = true
export var fire_left:bool = true
export var fire_right:bool = true
export var fire_random:bool = false
export var hidden:bool=false
export var full_size:bool=true			#for the shard, mostly a hack
onready var timer = $Timer
export(float) var firing_delay:=2.5
#export(String) var achievement_sure_name

var missile=preload("res://entities/projectiles/Enemy_Bullet_Single.tscn")

func _ready() -> void:
	#will call parent ready automatically
	randomize()
	if hidden:
		$Sprite.modulate=Color(1.0,1.0,1.0,0.0)
	_set_timer_fire()
	set_graphics_sprite_type()
	
func set_graphics_sprite_type():
	_set_sprite_type($Sprite)
	
func _set_timer_fire():
	if fire_random:
		timer.wait_time=rand_range(1.5,5)
	
func enable(make_visible:bool):
	.enable(make_visible)
	$Hurtbox.reset_health()

	if make_visible:
		if firing:
			if firing_delay>0:
				$DelayStartTimer.wait_time=firing_delay
				$DelayStartTimer.start()
			else:
				timer.start()
	
func disable(make_visible:bool):
	.disable(make_visible)
	$Timer.stop()

func make_ready(direction:Vector3):
	.make_ready(direction)


func _on_Timer_timeout() -> void:
	launch()
	_set_timer_fire()

func launch():
	if Globals.bullet_time==1.0:
		if fire_right:
			var missile_object=missile.instance()
			missile_object.global_position=get_node("Position1").global_position
			missile_object.set_angle(0)
			weapon_container.add_child(missile_object)
			missile_object.start()
		if fire_left:
			var missile_object=missile.instance()
			missile_object.global_position=get_node("Position2").global_position
			missile_object.set_angle(180)
			weapon_container.add_child(missile_object)
			missile_object.start()


func _on_DomeHurtbox_take_hit(damage_left) -> void:
	if damage_left<=0:
		bubble_death()
		
func bubble_death(create_bubble:=true, via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	if is_dead:
		return
	if full_size:
		var tex=GlobalMethods.get_texture_from_region($Sprite.texture,$Sprite.region_rect)
		GlobalMethods.enemy_explosion(global_position,false,"LargeDome",0.25,via_smart_bomb,tex,Globals.SFX_ENEMY_EXPLODE2)
	else:
		var tex=GlobalMethods.get_texture_from_region($Sprite.texture,$Sprite.region_rect)
		GlobalMethods.enemy_explosion(global_position,false,"FlatDome",0.25,via_smart_bomb,tex,Globals.SFX_ENEMY_EXPLODE3)
		
	if gives_energy_on_death && create_bubble && bubble_death_drop_tip=="":
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.LARGE_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)


func _on_DomeHurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")


func _on_DomeHurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")


func _on_DelayStartTimer_timeout() -> void:
	if is_dead:
		return
	timer.start()
