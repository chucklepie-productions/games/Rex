extends Enemy

enum TURRET_STYLE {FULL,POS1,POS2,POS3,POS4,POS5}
export(TURRET_STYLE) var style = TURRET_STYLE.FULL
export var firing:bool = true
export var start_delay:float=0.0
export var single_ignore_positions:=[0,0,0,0,0]

onready var animation = $AnimatedSprite
onready var timer = $Timer


var missile=preload("res://entities/projectiles/RoofTurretMissile.tscn")

func _ready() -> void:
	#will call parent ready automatically

	randomize()
	#set the animation frame using the style
	if style == 0:
		animation.animation="full"
	else:
		#not moving, static on a frame
		#we could just use full and stop animation then set frame
		animation.animation="pos"+str(style)
		animation.stop()
	set_graphics_sprite_type()

func set_graphics_sprite_type():
	_set_sprite_type($AnimatedSprite)

func _process(_delta):
	animation.set_speed_scale(Globals.bullet_time)
	
func enable(make_visible:bool):
	#call parent
	.enable(make_visible)
	$Hurtbox.reset_health()
	
	if make_visible:
		if style == 0:
			animation.play("full")
		if firing:
			if start_delay>0:
				yield(get_tree().create_timer(start_delay), "timeout")
			timer.start()
	else:
		timer.stop()
		animation.stop()

func disable(make_visible:bool):
	#call parent
	.disable(make_visible)
	$Timer.stop()

func make_ready(direction:Vector3):
	.make_ready(direction)



func _on_Timer_timeout() -> void:
	timer.stop()
	#rotating turrets have more bullets
	if style==0:
		timer.wait_time=0.1+rand_range(0.0,0.3)
	else:
		timer.wait_time=0.15+randf()
	if firing:
		timer.start()
		launch()

func launch():
	if Globals.bullet_time==1.0:
		var missile_object=missile.instance()
		var pos=style	#0 animated, all others are static in one place
		#use style if in a position
		#but if animated then get current frame (0-4)
		if style==0:	#rotating
			pos=animation.frame+1
			#we repeat animation frames so 6/7/8 are actually same as 4/3/2
			if pos==6: pos=4
			if pos==7: pos=3
			if pos==8: pos=2
	
		if single_ignore_positions[pos-1]!=0:
			return
			
		missile_object.global_position=get_node("Position"+str(pos)).global_position
		missile_object.set_angle(_get_angle(pos))
		weapon_container.add_child(missile_object)
		missile_object.start()
	
func _get_angle(position:int):
	var angle:int = 180
	
	match position: 
		0:
			pass
		1:			#ok
			angle = 124-90
		2:			#ok
			angle = 136-90
		3:			#ok
			angle = 180-90
		4:			#ok
			angle = 226-90
		5:
			angle = 238-90
	return angle

func _on_Hurtbox_take_hit(damage_left) -> void:
	if damage_left<=0:
		bubble_death()
		#OptionsAndAchievements.increment_achievement("sure",1,"Turret")
		
func bubble_death(create_bubble:=true, via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	if is_dead:
		return
	var tex=$AnimatedSprite.frames.get_frame("full",0).get_data()
	GlobalMethods.enemy_explosion(global_position,false,"RoofDome",0.25,via_smart_bomb,tex,Globals.SFX_ENEMY_EXPLODE1)

	if gives_energy_on_death && create_bubble && bubble_death_drop_tip=="":
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.LARGE_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)


func _on_Hurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")


func _on_Hurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")
