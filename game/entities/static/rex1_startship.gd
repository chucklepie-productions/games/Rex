extends Node2D

export(float) var start_delay=3.0

func _ready():
	visible=false
	
func enable(_visible:=true):
	visible=_visible
	if Globals.game_ended:
		start_delay=3.0
		scale.x=-1
		
	$AnimationPlayer.play("default")
	yield(get_tree().create_timer(0.1), "timeout")
	yield(get_tree().create_timer(start_delay), "timeout")
	$AnimationPlayer.play("rise")

func disable(_display):
	visible=_display
	$AnimationPlayer.play("default")

func play_animation(name):
	$AnimationPlayer.play(name)
	
func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name=="rise":
		$AnimationPlayer.play("float")
