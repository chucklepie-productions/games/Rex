extends Enemy
class_name Enemy_Launcher

#var missile=preload("res://entities/projectiles/Missile_Launcher_Missile.tscn")
export var facing:int = 1
export var firing_min:float = 0.65		#firing delay. -1 to stop firing, 0 for immediate
										#everything else is a delay
export var firing_max:float = 0.65		#if min/max are different it's random
export(float) var start_delay=0.0
export(PackedScene) var missile
#export(String) var achievement_sure_name

var firing:float
var fire_random:=false

func _ready() -> void:
	#calls parent
	#this reverses everything, but also position
	#so we need to ensure missile reverses itself too
	if firing_min<firing_max:
		fire_random=true
		firing=rand_range(firing_min/3.0,firing_min/2.0)
	else:
		fire_random=false
		firing=firing_min		#just for the first
		
	#venus fly trap has facing 0 to help with direction of fire
	#so this is a kind of hack
	if facing==0:
		scale.x=1
	else:
		scale.x=facing
	set_graphics_sprite_type()
	
func set_graphics_sprite_type():
	_set_sprite_type($AnimatedSprite)
	
func enable(make_visible:bool):
	.enable(make_visible)
	$Hurtbox.reset_health()
	if facing==0:
		scale.x=1
	else:
		scale.x=facing
	if make_visible:
		if firing>0:
			if !fire_random:
				$Timer.wait_time=firing_min
			else:
				$Timer.wait_time=firing_min*rand_range(0.5,1.5)
			if start_delay>0:
				$StartDelayTimer.wait_time=start_delay
				$StartDelayTimer.start()
			else:
				$Timer.start()
		else:
			$Timer.stop()
	else:
		$Timer.stop()

func disable(make_visible:bool):
	.disable(make_visible)
	$Timer.stop()

func make_ready(direction:Vector3):
	.make_ready(direction)

func _on_Timer_timeout() -> void:
	launch()
	if fire_random:
		$Timer.wait_time=rand_range(firing_min,firing_max)
	$Timer.start()

func launch():
	if Globals.bullet_time==1.0:
		$AnimatedSprite.play("fire")
		var missile_object=missile.instance()
		missile_object.global_position=$Position2D.global_position
		weapon_container.add_child(missile_object)
		missile_object.launch(facing)

func _on_AnimatedSprite_animation_finished() -> void:
	$AnimatedSprite.play("normal")

func _on_LauncherHurtbox_take_hit(damage_left) -> void:
	if damage_left<=0:
		bubble_death()
			
func bubble_death(create_bubble:=true, via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	if is_dead:
		return
	
	var tex=$AnimatedSprite.frames.get_frame("normal",0).get_data()
	#var tex_item=GlobalMethods.get_texture_from_region(tex,Rect2(0,0,64,64))
	GlobalMethods.enemy_explosion(global_position,false,"Launcher",0.25,via_smart_bomb,tex,Globals.SFX_ENEMY_EXPLODE1)
	
	if gives_energy_on_death && create_bubble && bubble_death_drop_tip=="":
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.LARGE_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)


func _on_LauncherHurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")


func _on_LauncherHurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")


func _on_StartDelayTimer_timeout() -> void:
	if is_dead:
		return
	$Timer.start()
