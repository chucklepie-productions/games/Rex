extends Enemy_Launcher

#3 directions, 3 heights, same velocity for bullets

export var single_ignore_positions:=[0,0,0,0,0,0,0,0,0]

func _ready() -> void:
	randomize()

func enable(make_visible:bool):
	.enable(make_visible)
	$Hurtbox.reset_health()

func disable(make_visible:bool):
	.disable(make_visible)
	
func launch():
	if Globals.bullet_time==1.0:
		#allow animation to finish
		$AnimatedSprite.play("fire")
		yield(get_tree().create_timer(0.5), "timeout")
		var bullets=[1,2,3,4,5,6,7,8,9]	#positions
		var new_bullets=[]
		#mostly between 1 and 4 but sometimes lots
		var maxn=4
		if randf()>0.7:
			maxn=9
		var counter=randi()%maxn+1
		var item
		while counter>0:
			if bullets.size()>0:
				item=randi()%bullets.size()
				new_bullets.append(bullets[item])
				bullets.remove(item)
				counter-=1
		var pos=$Position2D.global_position
		for bullet in new_bullets:
			var vel:Vector2=Vector2(100,-250)
			match bullet:
				#left/right
				#1,2,3:left
				#7,8,9:right
				#4,5,6:centre
				1,7:
					vel=Vector2(505,-305)
					facing=-1
				2,8:
					vel=Vector2(455,-450)
					facing=-1
				3,9:
					vel=Vector2(485,-535)
					facing=-1
				#centre
				4:
					facing=0
					vel=Vector2(0,-240)
				5:
					facing=0
					vel=Vector2(0,-340)
				6:
					facing=0
					vel=Vector2(0,-570)

			if bullet>6:
				facing=1
			vel*=rand_range(0.9,1.1)
			if single_ignore_positions[bullet-1]!=1:
				var missile_object=missile.instance()
				missile_object.simple_throw_velocity = vel
				missile_object.global_position=pos
				weapon_container.add_child(missile_object)
				missile_object.launch(facing)
