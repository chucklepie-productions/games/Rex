extends Node2D

func _on_Area2D_body_entered(_body: Node) -> void:
	yield(get_tree().create_timer(0.5), "timeout")
	Globals.game_scene.direct_room_change(Vector2.LEFT)

