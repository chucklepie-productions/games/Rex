extends Enemy

export(bool) var immediate_death_on_land=false

var velocity_drip = Vector2.ZERO
enum state {WAITING, DRIPPING, LANDING}

var anim=state.WAITING
onready var start_pos=position

func _ready() -> void:
	#calls parent
	randomize()
	set_graphics_sprite_type()

func set_graphics_sprite_type():
	_set_sprite_type($AnimatedSprite)

func enable(make_visible:bool):
	#print("ENABLE DRIP: " + name)	#temp
	.enable(make_visible)
	$Hurtbox.reset_health()
	velocity_drip=Vector2.ZERO
	if make_visible:
		$AnimatedSprite.play("waiting")
		$Timer.wait_time=rand_range(8.0,14.0)
		$Timer.start()
	else:
		$AnimatedSprite.stop()

func disable(make_visible:bool):
	#print(" DISABLE DRIP: " + name)	#temp
	.disable(make_visible)
	$AnimatedSprite.stop()
	#reset back to start position
	if start_pos!=null:
		position=start_pos
		anim=state.WAITING
	$Timer.stop()
	
func make_ready(ready_direction:Vector3):
	.make_ready(ready_direction)

func _physics_process(delta: float) -> void:
	_move(delta)
	
func _move(delta):
	if anim==state.DRIPPING:
		velocity_drip.y+=Globals.GRAVITY*delta
		velocity_drip.x=0

		var collision=move_and_collide(velocity_drip*delta*Globals.bullet_time)
		if collision:
			if collision.collider is Player:
				pass
			else:
				#avoid collision with tile we are attached to
				if position.y-start_pos.y>16:
					anim=state.LANDING
					$AnimatedSprite.play("landing")
					if immediate_death_on_land:
						yield($AnimatedSprite,"animation_finished")
						death(false)
	
func bubble_death(create_bubble:=true, via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	if is_dead:
		return
	#var tex=$AnimatedSprite.frames.get_frame("landing",1).get_data()
	GlobalMethods.enemy_explosion(global_position,false,"Drip",0.25,via_smart_bomb,null,Globals.SFX_ENEMY2_EXPLODE3)


	if gives_energy_on_death && create_bubble && bubble_death_drop_tip=="":
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.LARGE_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)

func _on_AnimatedSprite_animation_finished() -> void:
	pass

func _on_Timer_timeout() -> void:
	$AnimatedSprite.play("dripping")
	anim=state.DRIPPING

func _on_Hurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")

func _on_Hurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")

func _on_Hurtbox_take_hit(damage_taken) -> void:
	if damage_taken<=0:
		bubble_death()
		#OptionsAndAchievements.increment_achievement("sure",1,"Drip")



func _on_VisibilityNotifier2D_viewport_exited(_viewport: Viewport) -> void:
	if !is_dead:
		if !GlobalTileMap.camera_zoom:
			disable(false)
