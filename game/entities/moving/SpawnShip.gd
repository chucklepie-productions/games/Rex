extends RigidBody2D

#we need to handle this, and know which weapon type it is...
signal new_weapon(w_position,w_type)
signal new_weapon_ship_move
signal new_weapon_ended
var in_animation=false
onready var haze=$HeatHazeArea
var taking_off=false
#cannot put enums in export, get cyclic errors if use preload, so
#trying this which is a mirror as you cannot pass by enum so as long as 
#values stay the same it should be ok
enum WEAPON_MIRROR {NONE=0, BULLET_SINGLE=1, BULLET_DOUBLE=2,LASER=3, DRONE=4,SPRAY=5, SPECIAL=6}

#const g=preload("res://singletons/Globals.gd")
export(WEAPON_MIRROR) var weapon_type
export(bool) var automatic_takeoff:=true

var shader_check_gpos

func _ready() -> void:
	haze.visible=false
	haze.material.set_shader_param("active",false)
	sleeping=true
	disable(false)	#for boss, if not working in game remove this
	
	
func enable(make_visible:bool):
	_process_toggle(true)
	visible=make_visible
	$AnimationPlayer.play("idle")
	audio_idle_toggle(true)

func disable(make_visible:bool):
	_process_toggle(false)
	visible=make_visible
	audio_idle_toggle(false)

func audio_pod_click():
	AudioManager.play_sfx(Globals.SFX_PLAYER_WALK)
	
func audio_idle_toggle(is_on):
	if is_on:
		$AudioStreamPlayer.play()
	else:
		$AudioStreamPlayer.stop()
	
func audio_thrust():
	AudioManager.play_sfx(Globals.SFX_GAME_WSHIP_LIFTOFF)
	
func drop_from_sky():
	#if in air let it drop, for boss
	$AnimationPlayer.animation_set_next("dropship","idle")
	$AnimationPlayer.play("dropship")
	
func _on_VisibilityNotifier2D_viewport_exited(_viewport: Viewport) -> void:
	if in_animation:
		in_animation=false
		emit_signal("new_weapon_ended")
		$AnimationPlayer.play("idle")		#reset so all other ships are back to idle
											#as particles are shared
		print("Spawn ship says goodbye")
		taking_off=false
		queue_free()

func _process(_delta: float) -> void:
	if taking_off:
		emit_signal("new_weapon_ship_move",global_position)
		#visibility is a bit delayed so may cause fullscreen
		#fade circle to move to the bottom
		if GlobalTileMap.get_room_id_from_position(global_position-Vector2(0,15))!=shader_check_gpos:
			emit_signal("new_weapon_ended")

func take_off():
	$AnimationPlayer.play("takeoff")
	
func _on_Area2D_PlayerEnter_body_entered(body: Node) -> void:
	if body is Player:
		in_animation=true
		_do_help()
		emit_signal("new_weapon",global_position,weapon_type)
		if GlobalPersist.graphics_type!=0:
			if automatic_takeoff:
				take_off()
			else:
				#if not taking off, disable detection
				$Area2D_PlayerEnter.set_deferred("monitoring",false)
		else:
		##	$AnimationPlayer.play("takeoff_original")
			taking_off=true
			shader_check_gpos=GlobalTileMap.get_room_id_from_position(global_position)
			$Area2D_PlayerEnter.set_deferred("monitoring",false)
			in_animation=false
			emit_signal("new_weapon_ended")
			$AnimationPlayer.play("idle")		#reset so all other ships are back to idle
												#as particles are shared
			print("Spawn ship says goodbye")
			taking_off=false

func _do_help():
	var loc=GlobalTileMap.get_local_from_global(position)
	match weapon_type:
		Weapons_System.WEAPON.BULLET_SINGLE:
			GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.PICKUP,"single-bullet",Globals.DIALOGUE_TEXT["single-bullet"],true,loc,true)
		Weapons_System.WEAPON.BULLET_DOUBLE:
			GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.PICKUP,"double-bullet",Globals.DIALOGUE_TEXT["double-bullet"],true,loc,true)
			OptionsAndAchievements.increment_log("double",1)
		Weapons_System.WEAPON.LASER:
			GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.PICKUP,"laser",Globals.DIALOGUE_TEXT["laser"],true,loc,true)
			OptionsAndAchievements.increment_log("laser",1)
		Weapons_System.WEAPON.DRONE:
			GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.PICKUP,"drone",Globals.DIALOGUE_TEXT["drone"],true,loc,true)
			OptionsAndAchievements.increment_log("drone",1)
		Weapons_System.WEAPON.SPRAY:
			GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.PICKUP,"spray",Globals.DIALOGUE_TEXT["spray"],true,loc,true)
			OptionsAndAchievements.increment_log("spray",1)
		Weapons_System.WEAPON.SPECIAL:
			GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.PICKUP,"boss_special",Globals.DIALOGUE_TEXT["boss_special"],true,loc,true)
			
	
func _start_haze():
	haze.visible=true
	haze.material.set_shader_param("active",true)

func _process_toggle(status):
	set_process(status)
	set_physics_process(status)
