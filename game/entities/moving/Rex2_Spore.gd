extends Enemy

export(int) var speed=49
export(int) var start_direction=1		#down
export(float) var ignore_direction_change_startup=0.0	#for final boss

var direction=start_direction
onready var velocity_spore:Vector2=Vector2(0,speed)
enum state {OPENING, CLOSING}

var anim=state.OPENING
var anim_active:=false
var no_change_direction=false	#for ignore_direction_change_startup

func _ready() -> void:
	#calls parent
	randomize()
	set_graphics_sprite_type()

func set_graphics_sprite_type():
	_set_sprite_type($AnimatedSprite)

func enable(make_visible:bool):
	#print("ENABLE SPORE "+name)	#temp
	.enable(make_visible)
	$Hurtbox.reset_health()
	direction=start_direction	#for when reset, ensure do not jump anywhere
	velocity_spore=Vector2.ZERO
	anim_active=make_visible
		
	if make_visible:
		$AnimatedSprite.play("opening")
		
	else:
		$AnimatedSprite.stop()

	if ignore_direction_change_startup>0.0:
		no_change_direction=true
		yield(get_tree().create_timer(ignore_direction_change_startup), "timeout")
		no_change_direction=false

func disable(make_visible:bool):
	#print("DISABLE SPORE "+name)	#temp
	.disable(make_visible)
	anim_active=false
	$AnimatedSprite.stop()
	$Timer.stop()
	
func make_ready(ready_direction:Vector3):
	.make_ready(ready_direction)

func _physics_process(delta: float) -> void:
	#if $AnimatedSprite.is_playing():
	if anim_active:
		$AnimatedSprite.set_speed_scale(Globals.bullet_time)
	_move(delta)
	
func _move(delta):
	velocity_spore.y=speed*direction
	velocity_spore.x=0

	var collision=move_and_collide(velocity_spore*delta*Globals.bullet_time)
	if collision:
		if collision.collider is Player:
			pass
		else:
			direction=-direction
	var pos=GlobalTileMap.get_local_from_global(global_position)
	if pos.y<=GlobalTileMap.map_tile_size || pos.y>GlobalTileMap.found_screen_size.y-GlobalTileMap.map_tile_size:
		if !no_change_direction:
			direction=-direction
	
	
func bubble_death(create_bubble:=true, via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	if is_dead:
		return
	var tex=$AnimatedSprite.frames.get_frame("closing",0).get_data()
	GlobalMethods.enemy_explosion(global_position,false,"StaticSpore",0.25,via_smart_bomb,tex,Globals.SFX_ENEMY2_EXPLODE2)


	if gives_energy_on_death && create_bubble && bubble_death_drop_tip=="":
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.LARGE_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)

func _on_AnimatedSprite_animation_finished() -> void:
	#if opening may pause for a short time
	#if closing may pause for a short time
	if is_dead:
		return
	if randf()<0.2:
		anim_active=false
		$Timer.stop()
		$Timer.wait_time=rand_range(0.25,1.5)
		$Timer.start()
	else:
		_switch_anim()

func _on_Timer_timeout() -> void:
	anim_active=true
	_switch_anim()
	
func _switch_anim():
	if anim==state.OPENING:
		$AnimatedSprite.play("closing")
		anim=state.CLOSING
	else:
		$AnimatedSprite.play("opening")
		anim=state.OPENING

func _on_Hurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")

func _on_Hurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")
	$AnimatedSprite.play("closing")
	anim_active=true


func _on_Hurtbox_take_hit(damage_taken) -> void:
	if damage_taken<=0:
		bubble_death()
		#AchievementManager.increment_achievement("sure",1,"Spore3")

