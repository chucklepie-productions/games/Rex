extends Enemy

export(int) var speed=85
export(int) var start_direction=1
export(bool) var flip_animation=false
export (float) var visibility_disable_startup=0.0

#export(String) var achievement_sure_name

var direction=start_direction
var original_position
var noroomcheck=false
onready var velocity_bt:Vector2=Vector2(speed,0)
onready var wall_check:RayCast2D=$RayCastWall
onready var edge_check:RayCast2D=$RayCastEdge
onready var room_check:VisibilityNotifier2D=$VisibilityNotifier2D

func _ready() -> void:
	#calls parent
	original_position=position
	set_graphics_sprite_type()

func set_graphics_sprite_type():
	_set_sprite_type($AnimatedSprite)
	
func enable(make_visible:bool):
	.enable(make_visible)
	_set_visibility()
	$Hurtbox.reset_health()
	direction=start_direction	#for when reset, ensure do not jump anywhere
	$AnimatedSprite.flip_h=true if (flip_animation && direction==1) else false
	position=original_position
	_set_ray_check()
	velocity_bt=Vector2.ZERO
	if make_visible:
		$AnimatedSprite.play("default")
	else:
		$AnimatedSprite.stop()
		

func disable(make_visible:bool):
	.disable(make_visible)
	$AnimatedSprite.stop()	
	
func make_ready(ready_direction:Vector3):
	.make_ready(ready_direction)

func _set_visibility():
	if visibility_disable_startup>0.0:
		noroomcheck=true
		yield(get_tree().create_timer(visibility_disable_startup), "timeout")
		noroomcheck=false

func _physics_process(delta: float) -> void:
	$AnimatedSprite.set_speed_scale(Globals.bullet_time)
	_move_falling(delta)
	
func _move_falling(delta):
	var reverse=false
	velocity_bt.y+=Globals.GRAVITY*delta
	velocity_bt.x=speed*direction

	var collision=move_and_collide(velocity_bt*delta*Globals.bullet_time)
	if collision:
		velocity_bt=velocity_bt.slide(collision.normal)
		if collision.collider is Player:
			pass
	
	if !reverse:
		if (!edge_check.is_colliding() && velocity_bt.y<=0) || wall_check.is_colliding() :
			reverse=true
	
	if !noroomcheck:
		var pos=GlobalTileMap.get_local_from_global(global_position)
		if pos.x<=GlobalTileMap.map_tile_size || pos.x>GlobalTileMap.found_screen_size.x-GlobalTileMap.map_tile_size:
			reverse=true
	
	if reverse:
		reverse_direction()
		
func reverse_direction():
	direction=-direction
	$AnimatedSprite.flip_h=true if (flip_animation && direction==1) else false
	_set_ray_check()
	
func _set_ray_check():
	#set edge to be whatever direction facing
	wall_check.set_transform(Transform2D(Vector2(direction,0), Vector2(0,1), Vector2(wall_check.position.x, wall_check.position.y)))
	room_check.set_transform(Transform2D(Vector2(direction,0), Vector2(0,1), Vector2(room_check.position.x, room_check.position.y)))
	if direction>0:
		edge_check.position.x=abs(edge_check.position.x)
	else:
		edge_check.position.x=-edge_check.position.x
		
	

func _on_VisibilityNotifier2D_viewport_exited(_viewport: Viewport) -> void:
	if noroomcheck:
		return
		
	if !GlobalTileMap.camera_zoom:
		reverse_direction()


func _on_BouncyHurtbox_take_hit(damage_left) -> void:
	if damage_left<=0:
		bubble_death()
		
func bubble_death(create_bubble:=true, via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	if is_dead:
		return
	var tex=$AnimatedSprite.frames.get_frame("default",1).get_data()
	GlobalMethods.enemy_explosion(global_position,false,"Bouncer",0.25,via_smart_bomb,tex, Globals.SFX_ENEMY_EXPLODE1)

	
	if gives_energy_on_death && create_bubble && bubble_death_drop_tip=="":
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.LARGE_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)


func _on_BouncyHurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")

func _on_BouncyHurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")
