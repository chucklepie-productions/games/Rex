extends Enemy

#floaters live in an area2d
#we want to lock them in tiles, but these
#are destructable tiles so don't collide with environment

var speed = 40 setget set_speed
var start_point:=Vector2.ZERO
var direction = Vector2.ZERO
var area_name := ""

onready var sprite = $AnimatedSprite
onready var hurbox= $Hurtbox

func _ready() -> void:
	randomize()
	_get_new_direction()
	_get_colour()
	_setup()
	
func _setup():
	sprite.frame=randi()%sprite.frames.get_frame_count("default")
	$Timer.start()
	pass

#virtual
func enable(make_visible:bool):
	.enable(make_visible)
	_setup()
	sprite.play("default")
	pass

func disable(make_visible:bool):
	.disable(make_visible)
	$Timer.stop()
	$AnimatedSprite.stop()

func make_ready(ready_direction:Vector3):
	.make_ready(ready_direction)
	
func _physics_process(delta: float) -> void:
	$AnimatedSprite.set_speed_scale(Globals.bullet_time)
	var _collide=move_and_collide(direction*speed*delta*Globals.bullet_time)
	
func set_speed(value):
	speed=value

func set_graphics_sprite_type():
	if GlobalPersist.graphics_type==0:
		modulate=Color.white
	else:
		_get_colour()
	
func _get_colour():
	#mostly white
	if GlobalPersist.graphics_type==0:
		return
		
	if randi()%10<5:
		#we really only want spectrum colours so create global here from aesprite palette
		var t = GlobalMethods.get_random_spectrum_colour(["black"])
		var c=Color(t[0],t[1],t[2])
		modulate=c
		
func _get_new_direction():
	#we only want to move diagonally
	randomize()
	var x=-1
	var y=-1
	if randi()%2==1:x=1
	if randi()%2==1:y=1
	direction = Vector2(x,y).normalized()
	
func leaving_area():
	#method called by parent to tell us to change direction
	#as leaving area
	#so set new direction to be the start_point
	direction=(start_point-global_position).normalized()

func _on_Timer_timeout() -> void:
	#ever 1-3 seconds change direction
	$Timer.stop()
	$Timer.wait_time = rand_range(1.0,3.0)
	_get_new_direction()
	$Timer.start()


func _on_Hurtbox_take_hit(damage_left:int) -> void:
	if damage_left<=0:
		bubble_death()
		OptionsAndAchievements.increment_achievement("sure",1,"Floater")
		
func bubble_death(_create_bubble:=false, _via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	#no bubbles for floaters
	if is_dead:
		return
	GlobalMethods.explosion_standard(global_position,true,true,0.05,0,Globals.SFX_ENEMY_EXPLODE3)
	death(include_score)


func _on_Hurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")

func _on_Hurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")
