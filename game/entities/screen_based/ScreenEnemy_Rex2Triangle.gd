extends Enemy

var change_direction=true
var velocity_tri:=Vector2.ZERO

var speed=50
var can_move:=false

func set_graphics_sprite_type():
	_set_sprite_type($AnimatedSprite)
	#var material=$AnimatedSprite.material
	#material.set_shader_param("all_white",GlobalPersist.graphics_type==0)


func bubble_death(_create_bubble:=true, via_smart_bomb:=false, _include_score:=true,_pause:=0.0):
	if is_dead:
		return
		
	var tex=$AnimatedSprite.frames.get_frame("default",2).get_data()
	GlobalMethods.enemy_explosion(global_position,false,"",0.2,via_smart_bomb,tex,Globals.SFX_ENEMY2_EXPLODE4)
	death(true)	#is zero

func _ready() -> void:
	visible=false
	global_position=GlobalTileMap.get_empty_room_location()
	_set_direction()
	set_graphics_sprite_type()

func _physics_process(delta: float) -> void:
	$AnimatedSprite.set_speed_scale(Globals.bullet_time)
	if change_direction:
		_set_direction()
		
	if !can_move:
		return
		
	var collision = move_and_collide(velocity_tri * delta * Globals.bullet_time)
	if collision:
		GlobalMethods.explosion_miss(global_position,Globals.COLOURS.white_bright)
		death(true)
		
func _on_Timer_timeout() -> void:
	change_direction=true
	$Timer.start()
	
func _on_Hurtbox_take_hit(damage_taken) -> void:
	#should only be player, but regardless we simply remove
	if damage_taken<=0:
		bubble_death(gives_energy_on_death,false,true)

func _on_Hurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")

func _on_Hurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")

func _on_VisibilityNotifier2D_viewport_exited(_viewport: Viewport) -> void:
	queue_free()
			
func _set_direction():
	change_direction=false
	var move_right=true
	var move_up=true
	if !is_instance_valid(GlobalTileMap.current_player):
		print("Player not set! default motion")
	else:
		var pos=GlobalTileMap.current_player.position
		if pos.x<position.x:
			move_right=false
		if pos.y>position.y:
			#up/down we want diagonal only
			move_up=false
			
	var angle=0
	if move_right && move_up:
		angle=315
	elif move_right && !move_up:
		angle=45
	elif !move_right && !move_up:
		angle=135
	else:
		angle=225
	velocity_tri=Vector2(speed,0.0).rotated(deg2rad(angle))

	
	#var type=""
	#if GlobalPersist.graphics_type==0:
	#	type="_original"
	if move_right:
		$AnimatedSprite.play("default")
	else:
		$AnimatedSprite.play("default",true)


#virtual
func enable(_make_visible:bool):
	.enable(false)	#animation makes it visible when it wants to
	$AnimationPlayer.play("appear")

func disable(make_visible:bool):
	.disable(make_visible)

func make_ready(direction:Vector3):
	$Timer.start()
	$TimerLifespan.start()
	.make_ready(direction)
	
	yield(get_tree().create_timer(1.5),"timeout")
	can_move=true


func _on_TimerLifespan_timeout() -> void:
	$FlashAnimationPlayer.play("flash")
	yield(get_tree().create_timer(1.0),"timeout")
	queue_free()
