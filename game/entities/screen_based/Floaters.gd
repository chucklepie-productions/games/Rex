extends Area2D
class_name Floaters

#floaters start at a single spot and go diagnally 45 degrees
#then change direction. Between 1 and 3 seconds normall
#speed is slow
#they are constrained by the area

export(int) var number_of_floaters = 1
export(int) var floater_speed = 40
export(int) var random_offset=0

var floater = preload("res://entities/screen_based/Floater.tscn")
var killing_floaters:=false
onready var floaters = $Floaters

func _ready() -> void:
	pass
	
func _create_floaters():
	if number_of_floaters<1:
		return

	killing_floaters=false
	for _i in range(number_of_floaters):
		var f = floater.instance()
		var l=rand_range(-random_offset,random_offset)
		f.global_position=$StartingPoint.global_position+Vector2(l,l)
		f.start_point=$StartingPoint.global_position
		f.speed=floater_speed
		f.area_name=name
		#var loc=GlobalMethods.assign_temp_location()
		#loc.add_child(f)
		floaters.add_child(f)
		f.enable(true)

func set_graphics_sprite_type():
	for c in floaters.get_children():
		c.set_graphics_sprite_type()
		
func bubble_death(_create_bubble:=false, _via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	#never give bubbles
	if killing_floaters:
		return
		
	_kill_floaters(true,include_score)
	
func _kill_floaters(explode_floaters=false,include_score:=true):
	#print("removing floaters " + str(explode_floaters))
	if killing_floaters:
		return
		
	killing_floaters=true
	_process_toggle(false)
	
	for f in floaters.get_children():
		#print("removing floater " + f.name)
		if is_instance_valid(f):
			if explode_floaters:
				if f!=null:
					#print("before bubble: "+f.name)
					f.bubble_death(false,false,include_score)
					yield(get_tree().create_timer(0.1), "timeout")	#sometimes causes issues so using nullcheck
			else:
				f.queue_free()

func _on_Floater_Domain_body_exited(body: Node) -> void:
	#the only body is floater
	#change it's direction back to the starting point
	if killing_floaters:
		return
		
	if body.has_method("leaving_area"):
		if body.area_name==name:
			body.leaving_area()

func enable(_make_visible:bool):
	call_deferred("_create_floaters")
	_process_toggle(true)
	#print("enabling floater domain " + name)

func disable_kill():
	_kill_floaters(true)
	yield(get_tree().create_timer(3), "timeout")
	if is_instance_valid(self):
		_process_toggle(false)
	
func disable(_make_visible:bool):
	_kill_floaters(false)
	_process_toggle(false)
	#print("disabling floater domain " + name)

func _process_toggle(status):
	set_process(status)
	set_physics_process(status)
