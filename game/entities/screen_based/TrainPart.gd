extends KinematicBody2D

signal cart_dead(cart_position)

export(bool) var locomotion=false
export(bool) var firing_left=false
export(bool) var firing_right=false
export var score=0

onready var timer=$FireTimer

var velocity_train:=Vector2.ZERO
var part_position=0		#auto decide if zero
var standard_fps
var is_stuck=false
var speed=50
var can_fire:=true		#for override (boss currently) to stop firing if firing_left/right

var missile_single=preload("res://entities/projectiles/Enemy_Bullet_Single.tscn")
var missile_double=preload("res://entities/projectiles/Enemy_Bullet_Double.tscn")

func _ready() -> void:
	#normally let train decide position based on node location
	if randi()%2==0:
		$AnimatedSprite.frame=1
	else:
		$AnimatedSprite.frame=0
	if part_position!=0:
		set_position(part_position)
	_set_timer_fire(2.0)
	set_graphics_sprite_type()

func set_graphics_sprite_type():
	_set_sprite_type($AnimatedSprite)
	
func _set_sprite_type(_sprite:Node2D):
	if GlobalPersist.graphics_type==0 || GlobalPersist.graphics_type==1:
		var material=$AnimatedSprite.material
		material.set_shader_param("all_white",GlobalPersist.graphics_type==0)

func set_position(index):
	part_position=index
	position.x = index*64

func _physics_process(delta: float) -> void:
	#we don't move it, the conveyor belt linear velocity does via move and slide

	$AnimatedSprite.speed_scale=Globals.bullet_time
	#adjust for bullet time, i.e. almost freeze train
	if !is_stuck:
		velocity_train.y+=Globals.GRAVITY*delta*Globals.bullet_time
		if Globals.bullet_time==1:
			velocity_train.x=-speed
			velocity_train=move_and_slide(velocity_train,Vector2.UP)
		else:
			position.x-=5*delta

func _set_timer_fire(min_time:=0.5):
	timer.wait_time=rand_range(min_time,2.0)
	
func _on_Hurtbox_take_hit(health) -> void:
	if health<=0:
		bubble_death()

func bubble_death(create_bubble:=true, via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	if part_position!=1:
		var tex=null
		if firing_right:
			tex=$AnimatedSprite.frames.get_frame("back",0).get_data()
			GlobalMethods.enemy_explosion(global_position,false,"TrainBack",0.3,via_smart_bomb,tex,Globals.SFX_ENEMY_EXPLODE1)
		else:
			tex=$AnimatedSprite.frames.get_frame("cart",0).get_data()
			GlobalMethods.enemy_explosion(global_position,false,"TrainCart",0.3,via_smart_bomb,null,Globals.SFX_ENEMY_EXPLODE2)
	else:
		#front, which has it's own
		GlobalMethods.enemy_explosion(global_position,false,"",0.3,via_smart_bomb,null,Globals.SFX_ENEMY_EXPLODE1)
	#simple check for front/back
	if create_bubble:
		if firing_left || firing_right:
			GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.LARGE_ENERGY,Globals.BUBBLE_DELAY)
		else:
			GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.SMALL_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)

func death(include_score:=true):
	var t="Middle"
	if firing_left:
		t="Front"
		Globals.humans_killed+=1
	elif firing_right:
		t="Back"
		Globals.humans_killed+=1
	OptionsAndAchievements.increment_achievement("sure",1,t)
	OptionsAndAchievements.increment_log(t,1)
		
	emit_signal("cart_dead",part_position)
	#let the train free the cart so it's not deleted before we adjust the other carts
	if include_score:
		GlobalPersist.score+=score
	
func _on_Hurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")

func _on_Hurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")

func enable(_make_visible:bool):
	pass

func disable(make_visible:bool):
	pass

func stuck():
	is_stuck=true
	$AnimatedSprite.stop()
	
func _on_VisibilityNotifier2D_viewport_exited(_viewport: Viewport) -> void:
	if !GlobalTileMap.camera_zoom:
		queue_free()

func _on_VisibilityNotifier2D_viewport_entered(_viewport: Viewport) -> void:
	if firing_left || firing_right:
		timer.start()


func _on_FireTimer_timeout() -> void:
	_set_timer_fire()
	launch()

func launch():
	if !can_fire:
		return
		
	if Globals.bullet_time==1.0:
		if firing_right:
			var missile_object=missile_double.instance()
			missile_object.position=get_node("RightGun").position
			missile_object.set_angle(0)
			add_child(missile_object)
			missile_object.start()
		if firing_left:
			var missile_object=missile_single.instance()
			missile_object.position=get_node("LeftGun").position
			missile_object.set_angle(180)
			add_child(missile_object)
			missile_object.start()
