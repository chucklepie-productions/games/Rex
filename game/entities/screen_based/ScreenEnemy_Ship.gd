extends Enemy
#always travels up or down until hits a collision
#always at same angle
#always at same speed
#may have a small chance of changing left/right during flight
#when hits collision follows normal bounce direction
#goes through one-way blocks
#at start travels half speed and half angle for a few seconds
#then reverts to normal

onready var animate=$AnimatedSpriteHuman
onready var reverse_timer:Timer=$ReverseTimer
onready var mine=preload("res://entities/projectiles/UfoMine.tscn")

enum SHIP_TYPE {HUMAN,UFO}
export(SHIP_TYPE) var ship_type=SHIP_TYPE.HUMAN
export(float) var start_delay=0.0
export(bool) var delayed_collision:=true

const SPEED_HUMAN=200
const SPEED_UFO=100
const ANGLE_UP_HUMAN=-60
const ANGLE_DOWN_HUMAN=60
const ANGLE_UP_UFO=-10
const ANGLE_DOWN_UFO=10
const FACE_RIGHT=1
const FACE_LEFT=-1

var speed=SPEED_HUMAN
var can_fire=true
var current_face=FACE_RIGHT
var current_direction=ANGLE_DOWN_HUMAN
var bounce_timer_active=false

var velocity_ship:=Vector2.ZERO

func _set_velocity(new_speed, new_angle, new_face):
	velocity_ship=Vector2(new_speed*current_face,0).rotated(deg2rad(new_angle*new_face))
	
func _ready() -> void:
	#automatically calls parent
	disable(false)
	if start_delay>0.0:
		$Start_Timer.wait_time=start_delay
		startup(true)
		#$Start_Timer.start()
		return
	else:
		startup(false)
	
func startup(just_type):
	randomize()
	_configure_ship(false,just_type)
	set_graphics_sprite_type()
	#enable(true)

func set_graphics_sprite_type():
	_set_sprite_type($AnimatedSpriteHuman)
	_set_sprite_type($AnimatedSpriteUfo)
	
func _configure_ship(animate_now, just_type:=false):
	my_half_size=$CollisionShape2D.shape.radius/2
	#var start
	if ship_type==SHIP_TYPE.HUMAN:
		is_human=true
		animate=$AnimatedSpriteHuman
		$AnimatedSpriteUfo.visible=false
		$AnimatedSpriteUfo.playing=false
		$AnimatedSpriteHuman.visible=true
		$AnimatedSpriteHuman.playing=true
		can_fire=false
		speed=SPEED_HUMAN
		score=300
		achievement_sure_name="Ship"
	else:
		is_human=false
		animate=$AnimatedSpriteUfo
		$AnimatedSpriteHuman.visible=false
		$AnimatedSpriteHuman.playing=false
		$AnimatedSpriteUfo.visible=true
		$AnimatedSpriteUfo.playing=true
		current_direction=ANGLE_DOWN_UFO
		can_fire=true
		speed=SPEED_UFO
		score=1000
		achievement_sure_name="UFO"
	
	if !just_type:
		_set_velocity(speed,current_face,current_direction)
	current_direction=ANGLE_DOWN_HUMAN

	if animate_now:
		animate.play("fly")
		
	
func _physics_process(delta: float) -> void:
	animate.set_speed_scale(Globals.bullet_time)
	
	#has not left the room or has left but timer is on
	_move_ship(delta)
		
func _move_ship(delta):
	#decide if want to change direction
	
	#move our body and check if it collides
	var collision = move_and_collide(velocity_ship * delta * Globals.bullet_time)
	var bounce=false
	if collision:
		bounce=true
		bounce_timer_active=true
		$BounceTimer.start()
	if bounce:
		velocity_ship = velocity_ship.bounce(collision.normal)
		print(str(velocity_ship))
		#velocity_ship = -velocity_ship
	else:
		if randi()%120==1 && reverse_timer.is_stopped():
			_change_direction(false)
			
	if velocity_ship.x<0:
		animate.flip_h=true
	else:
		animate.flip_h=false


func _change_direction(reverse_move):
	if reverse_move:
		#force a direction
		#if we have changed direction next frame we will still be offscreen
		#so will constantly reverse direction
		#using timer to compensate
		velocity_ship=-velocity_ship
		reverse_timer.start()
	else:
		if randi()%2==0:
			current_face=FACE_RIGHT
		else:
			current_face=FACE_LEFT
			
		if randi()%2==0:
			if ship_type==SHIP_TYPE.HUMAN:
				current_direction=ANGLE_UP_HUMAN
			else:
				current_direction=ANGLE_UP_UFO
		else:
			if ship_type==SHIP_TYPE.HUMAN:
				current_direction=ANGLE_DOWN_HUMAN
			else:
				current_direction=ANGLE_DOWN_UFO
		#velocity=Vector2(speed*current_face,0).rotated(deg2rad(current_direction*current_face))
		_set_velocity(speed,current_face,current_direction)

#virtual
func enable(make_visible:bool):
	.enable(make_visible)
	$Start_Timer.start()

	$SoftCollision/CollisionShape2D.set_deferred("disabled",false)
	animate.play("fly")
	_set_mine_time()
	if delayed_collision:
		$CollisionShape2D.set_deferred("disabled",true)
		yield(get_tree().create_timer(0.5), "timeout")
	$CollisionShape2D.set_deferred("disabled",false)

func disable(make_visible:bool):
	$SoftCollision/CollisionShape2D.set_deferred("disabled",false)
	$LayMineTimer.stop()
	$AnimatedSpriteHuman.stop()
	$AnimatedSpriteUfo.stop()
	.disable(make_visible)
	#queue_free()
	pass
	
func _set_mine_time():
	if ship_type==SHIP_TYPE.UFO:
		$LayMineTimer.wait_time=rand_range(3.0,8.0)
		$LayMineTimer.start()

func _set_mine():
	#lay up to 3 mines
	var missile_object
	var node=GlobalMethods.assign_temp_location()
	var lay=false
	#we want up to 3 mines
	for i in range(3):
		lay=false
		if i==0:
			lay=true
		else:
			if rand_range(0.0,10)>2.5*(i+1):
				lay=true
		#lay mines with a gap
		if lay:
			missile_object=mine.instance()
			missile_object.global_position=global_position
			node.add_child(missile_object)
			missile_object.start()
			yield(get_tree().create_timer(0.5), "timeout")
	
func make_ready(direction:Vector3):
	.make_ready(direction)
	_configure_ship(true)
		
	#ship type previously set by room manager
	var dir=int(direction.z)
	
	match dir:
		GlobalTileMap.DIRECTION.RIGHT:
			current_face=FACE_LEFT
			#current_direction=0
		GlobalTileMap.DIRECTION.LEFT:
			current_face=FACE_RIGHT
			#current_direction=180
		GlobalTileMap.DIRECTION.TOP:
			current_face=FACE_RIGHT
			current_direction=ANGLE_DOWN_HUMAN
			current_direction=90
			if ship_type==SHIP_TYPE.UFO:
				current_direction=ANGLE_DOWN_UFO
		GlobalTileMap.DIRECTION.BOTTOM:
			current_face=FACE_RIGHT
			current_direction=ANGLE_UP_HUMAN
			if ship_type==SHIP_TYPE.UFO:
				current_direction=ANGLE_UP_UFO
		_:
			print("ERROR. got "+str(dir))
	
	#velocity=Vector2(speed*current_face,0).rotated(deg2rad(current_direction*current_face))

	if ship_type==SHIP_TYPE.HUMAN:
		_set_velocity(speed/2.0,current_face,current_direction/4.0)
	else:
		_set_velocity(speed,current_face,current_direction)
		
	reverse_timer.stop()
	reverse_timer.wait_time=3
	reverse_timer.start()


func _on_OnScreen_viewport_entered(_viewport: Viewport) -> void:
	.has_entered_room()

func _on_OnScreen_viewport_exited(_viewport: Viewport) -> void:
	if has_entered_screen:
		if screen_based:	#&& reverse_timer.is_stopped():
			#in screen as part of design, bring it back in
			_change_direction(true)
			return
		else:
			leave_room()
			


func _on_ReverseTimer_timeout() -> void:
	#code manages timer itself, this section is only for entrance
	velocity_ship=Vector2(speed*current_face,0).rotated(deg2rad(current_direction*current_face))

func _on_ShipHurtbox_take_hit(damage_left) -> void:
	if damage_left<=0:
		bubble_death()
		if ship_type==SHIP_TYPE.HUMAN:
			OptionsAndAchievements.increment_achievement("sure",1,"Ship")
		else:
			OptionsAndAchievements.increment_achievement("sure",1,"UFO")
		
func bubble_death(create_bubble:=true, via_smart_bomb:=false, include_score:=true,_pause:=0.0):
	if is_dead:
		return
		
	$LayMineTimer.stop()
	var stype="ShipMan"
	var tex
	if ship_type==SHIP_TYPE.UFO:
		stype="ShipUFO"
		tex=$AnimatedSpriteUfo.frames.get_frame("fly",0).get_data()
	else:
		tex=$AnimatedSpriteHuman.frames.get_frame("fly",0).get_data()
	GlobalMethods.enemy_explosion(global_position,false,stype,0.2,via_smart_bomb,tex,Globals.SFX_ENEMY_EXPLODE1)
	if gives_energy_on_death && create_bubble  && bubble_death_drop_tip=="":
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.SMALL_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)

func _on_SoftCollision_area_entered(_area: Area2D) -> void:
	if !reverse_timer.is_stopped():
		return
		
	velocity_ship=-velocity_ship


func _on_ShipHurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")

func _on_ShipHurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")


func _on_LayMineTimer_timeout() -> void:
	$LayMineTimer.stop()
	_set_mine_time()
	_set_mine()


func _on_Start_Timer_timeout():
	startup(false)


func _on_BounceTimer_timeout() -> void:
	bounce_timer_active = false
