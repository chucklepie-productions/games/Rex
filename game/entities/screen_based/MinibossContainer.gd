extends Node2D

export(int) var health_override=-1

onready var boss=$Miniboss
func _ready() -> void:
	set_graphics_sprite_type()
	boss.set_health(health_override)
	
func set_graphics_sprite_type():
	boss.update_mode()
