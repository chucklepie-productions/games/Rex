extends Enemy
class_name Human

#no force is only used in lock_State_after_unlock and tells human
#just to do normal stuff and lock_State_after_unlock is not being used
const HUMAN_STATES = {"idle":0,"kneel":1,"walk":2,"jump":3,"fall":4,"no_force":-1}
enum FIRING_MODES {YES, NO, AREA}

export(FIRING_MODES) var fire_mode=FIRING_MODES.YES

export(bool) var lock_state = false					#fixed to one action
export(HUMAN_STATES) var lock_start_state			#only relevant for lock_state
#for the boss really. on first move after unlock force this
export(HUMAN_STATES) var lock_state_after_unlock=HUMAN_STATES.no_force

export(float) var change_action_timeout_min = 3		#timer to change state
export(float) var change_action_timeout_max = 7
export(int) var start_direction=1	#right
export(bool) var just_die=false		#no dying stage, no points, no bubble
export(bool) var is_rex2=false		#dissolves in, double gun
const RAYWAIT = 1.5
const MAX_SPEED = 90
const JUMP_SPEED := -260.0	#a bit more than rex so can reach same places when on edge
const BOOST_SPEED := -300.0

onready var change_timer = $ChangeTimer
onready var ray_jump = $AnimatedSprite/FindBlocks/JumpRay
onready var ray_block= $AnimatedSprite/FindBlocks/BlockedRay
onready var ray_block2=$AnimatedSprite/FindBlocks/BlockedRay2
onready var ray_edge=$AnimatedSprite/FindBlocks/EdgeRay
onready var player_check=$AnimatedSprite/ShootingArea
onready var anim=$AnimatedSprite
onready var ground=$OnGround
onready var raytimer=$RayTimer
onready var ray_stair=$AnimatedSprite/FindBlocks/StairDetector
onready var lock_shader=preload("res://assets/shaders/human_locked_shader.tres")
onready var standard_gun=get_node("AnimatedSprite/GunPosition").position.y

var no_fire_override=false
var missile_single=preload("res://entities/projectiles/Enemy_Bullet_Single.tscn")
var missile_double=preload("res://entities/projectiles/Enemy_Bullet_Double.tscn")
var shader
var making_ready=false
var human_state = HUMAN_STATES.idle
var last_human_state=human_state
var velocity_human = Vector2.ZERO
var move_direction:int = 0
var facing:int = 1
var change_state:bool = false
var ignore_till_next_timer=false
var latched_vertical_jump=false
var firing_area_entered=false
var applying_boost=false
var applying_boost_first=false
var dying_human=preload("res://entities/screen_based/HumanDying.tscn")
var anim_suffix=""
var freeze_appear:=false
var lock_anim_state:=false
var special_training_mode:=false

#humans are always crouching and locked if on screen at start
#random entry are walking or falling on entry (jumping if from below)
#but no screen has human entering from below
func _ready() -> void:
	randomize()
	change_timer.wait_time=3	#whatever we start human as, leave alone

	if is_rex2:
		anim_suffix="_rex2"
		
	disable(false)
	facing=start_direction
	raytimer.wait_time=RAYWAIT
	_set_facing()
	shader=lock_shader.duplicate()
	anim.material=shader
	set_graphics_sprite_type()
	
func set_graphics_sprite_type():
	_set_sprite_type(anim)

func _change_helmet_colour():
	if GlobalPersist.graphics_type==0:
		return
		
	if firing_area_entered:
		anim.material.set_shader_param("helmet",GlobalMethods.get_as_colour(Globals.COLOURS.red_bright))
	else:
		if lock_start_state==HUMAN_STATES.kneel:
			anim.material.set_shader_param("helmet",GlobalMethods.get_as_colour(Globals.COLOURS.magenta))
		else:
			anim.material.set_shader_param("helmet",GlobalMethods.get_as_colour(Globals.COLOURS.green))
			
	
#virtual
func enable(make_visible:bool):
	.enable(make_visible)
	if lock_state:
		#locked to the screen (crouching)
		_change_state(lock_start_state)
		_change_helmet_colour()
		
	#visibility notifier not called if offscreen when start
	#so disabling firing here, but should be ok
	#as notifier is called if drawn on screen when start
	#this is for rex2 scrolling, see if affects game
	no_fire_override=true	#might affect idle

	change_timer.start()
	if fire_mode!=FIRING_MODES.AREA:
		player_check.set_deferred("monitoring",false)
		#$AnimatedSprite/ShootingArea/CollisionShape2D.set_deferred("disabled",true)
	else:
		player_check.set_deferred("monitoring",true)
		
	if fire_mode==FIRING_MODES.YES:
		#auto turns itself on and off
		_set_timer_fire(2.0)
		$FireTimer.start()

func apply_boost(is_exiting):
	if is_exiting:
		applying_boost=false
		applying_boost_first=false
		_jump(BOOST_SPEED*0.7,true)
	else:
		applying_boost=true
		#clamp x. maybe tween it later for better effect
		velocity_human.x=0
		if !applying_boost_first:
			_jump(BOOST_SPEED,true)
			applying_boost_first=true
			
func leave_room_override_no_delete():
	return lock_state
	
func disable(make_visible:bool):
	$ChangeTimer.stop()
	$FireTimer.stop()
	$RayTimer.stop()
	$AnimatedSprite.stop()
	$AnimatedSprite/ShootingArea.set_deferred("monitoring",false)
	.disable(make_visible)

func make_ready(direction:Vector3):
	.make_ready(direction)
	match int(direction.z):
		#auto set direction if on left or right
		GlobalTileMap.DIRECTION.RIGHT, GlobalTileMap.DIRECTION.LEFT:
			facing=1
			if direction.z==GlobalTileMap.DIRECTION.LEFT:
				facing=-1
			_set_facing()
			_change_state(HUMAN_STATES.walk)
		GlobalTileMap.DIRECTION.BOTTOM:
			#panic!
			_change_state(HUMAN_STATES.jump)
		GlobalTileMap.DIRECTION.TOP:
			#if specific direction set then use this
			if start_direction==0:
				if randf()>=0.5:
					facing=1
				else:
					facing=-1
			_set_facing()
			_change_state(HUMAN_STATES.walk)
			#fix for some rooms (52) to allow to fall through blockages when spawning
			$CollisionShape2D.set_deferred("disabled",true)
			yield(get_tree().create_timer(0.4), "timeout")
			$CollisionShape2D.set_deferred("disabled",false)
		
	_ignore_till_next_timer()
	change_timer.stop()
	making_ready=true			#allow a single jump
	change_timer.wait_time=3	#whatever we start human as, leave alone
	change_timer.start()

	if is_rex2:
		_change_state(HUMAN_STATES.idle)
		freeze_appear=true
		lock_anim_state=true
		lock_state=true
		anim.play("appear_rex2")
		yield(anim, "animation_finished")
		lock_state=false
		lock_anim_state=false

func _physics_process(delta: float) -> void:
	
	#simpler rules for leaving screen for humans as they are never screen based
	#and move
	#if !ignore_till_next_timer && .has_left_screen():
	#	.leave_room()
	#	return
		
	_move(delta)
	_apply_gravity(delta)
	
	if !lock_state:
		apply_states()

	_set_animation()
	$TestLabel/Label.text=str(HUMAN_STATES.keys()[human_state])+","+str(facing)+","+str(move_direction)

func _set_timer_fire(enabled:=true,min_time:=0.5,max_time:=4.0):
	if !enabled:
		$FireTimer.stop()
	else:
		$FireTimer.wait_time=rand_range(min_time,max_time)

func launch():
	if no_fire_override || Globals.bullet_time!=1.0:
		return
		
	var missile_object
	var node=GlobalMethods.assign_temp_location()
	if is_rex2:
		missile_object=missile_double.instance()
	else:
		missile_object=missile_single.instance()
	missile_object.global_position=get_node("AnimatedSprite/GunPosition").global_position
	if facing==1:
		missile_object.set_angle(0)
	else:
		missile_object.set_angle(180)
		
	node.call_deferred("add_child",missile_object)
	missile_object.call_deferred("start")
			
func apply_states():
	#all edge cases found
	#if we make a choice then don't choose this path again until timer
	#but if forced then is always the cast
	var done=false
	#stairs come first
	if _stair_ready(true) && !ignore_till_next_timer:
		if randf()>=0.3:
			_stair_jump()
			done=true
		else:
			#let it walk off or do something else
			raytimer.wait_time=0.6
			_ignore_till_next_timer()
			pass
	if !done && _is_grounded() and velocity_human.x!=0 and [HUMAN_STATES.fall,HUMAN_STATES.jump].has(human_state):
		#moving but also falling and on ground, probably sliding on a roof
		if _stair_ready(true)  && !ignore_till_next_timer:
			if randf()>0.3:
				_stair_jump()
				done=true
			else:
				#let it walk off or do something else
				raytimer.wait_time=0.5
				_ignore_till_next_timer()
		else:
			_change_state(HUMAN_STATES.walk)
		
	if !done && _is_grounded() and _is_ray_edge_ready() and _is_ray_block_ready():
		#kind of stuck
		if randf()>0.5:
			_change_state(HUMAN_STATES.jump)
		else:
			_change_state(HUMAN_STATES.walk)
		done=true
	if !done && [HUMAN_STATES.jump].has(human_state) and velocity_human.is_equal_approx(Vector2.ZERO): #velocity.x==0:
		#jumping on a wall or pipe
		var tile=GlobalTileMap.get_tile_at_position(GlobalTileMap.current_map,position)
		if tile!=210 && tile!=206 && tile!=209 && tile!=229:
			_change_direction()
			done=true
	#possibly on stairs
	if !done:
		if _is_grounded():
			if change_state:
				#timer tells us to change
				_get_random_state(false)
			elif _is_ray_block_ready():
				#blocked so change direction
				_change_direction()
			elif [HUMAN_STATES.walk].has(human_state) and _is_ray_block_ready() && velocity_human.x<0.5:
				#we are walking but not moving
				#probably jumped onto a wall and ray has not triggered
				#or on a stair
				if !ray_jump.is_colliding():	#one case for on a stair
					_change_state(HUMAN_STATES.jump)
				else:
					_change_direction()
			elif _is_ray_jump_ready()  and (!ignore_till_next_timer || making_ready):
				#can still jump at entrance to a screen for first 3 seconds
				#see if it breaks things or not...
				if randf()>=0.5:
					_change_state(HUMAN_STATES.jump)
				else:
					_ignore_till_next_timer()
			elif _is_ray_edge_ready() and !_is_grounded(false) and !ignore_till_next_timer: #get right on the edge
				#at an edge so jump, fall or change direction
				if randf()<0.2 && lock_state_after_unlock==HUMAN_STATES.no_force:
					_change_direction()				
				elif randf()>=0.5:
					#50/50 jump
					_change_state(HUMAN_STATES.jump)
				else:
					#let him walk off most of the time if not jump
					pass
				_ignore_till_next_timer()
			elif [HUMAN_STATES.fall].has(human_state):
				#we were falling/jumping
				velocity_human=Vector2.ZERO
				if last_human_state==HUMAN_STATES.fall:
					_get_random_state(true)
				else:
					_change_state(last_human_state)
			else:
				if !_is_ray_block_ready() && GlobalTileMap.get_tile_at_position(GlobalTileMap.current_map,position)==210:
					#pipe, shall we jump up it
					raytimer.wait_time=0.5
					_ignore_till_next_timer()
					if randf()>0.7:
						latched_vertical_jump=true
						_change_state(HUMAN_STATES.jump)
		else:
			#if falling or jumping then that's what we're doing
			#it still uses move direction
			#don't call set as we do not want to update last position
			if velocity_human.y<0:
				human_state=HUMAN_STATES.jump
			else:
				human_state=HUMAN_STATES.fall
				if _is_ray_block_ready():
					_change_direction()
			
func _stair_jump():
	_change_state(HUMAN_STATES.jump)
	#disabale collisions temporarily to get over stairs
	$CollisionShape2D.disabled=true
	anim.stop()
	raytimer.wait_time=0.5
	raytimer.start()
	_ignore_till_next_timer()
	
func _stair_ready(fall_override=false):
	var tile1=0
	var tile2=0
	if fall_override==false && [HUMAN_STATES.jump, HUMAN_STATES.fall].has(human_state):
		return false

	if GlobalTileMap.current_map != null:
		var pos=Vector2(position.x,position.y)
		pos.y+=GlobalTileMap.map_tile_size*2
		tile2=GlobalTileMap.get_tile_at_position(GlobalTileMap.current_map,pos,Vector2.ZERO)	#below us
		
	if ray_stair.is_colliding():
		#get tile offset from player which is his head position
		var p = Vector2(position.x,position.y)
		p.x+=GlobalTileMap.map_tile_size*3*facing
		var collider=ray_stair.get_collider()
		if collider is TileMap:
			tile1=GlobalTileMap.get_tile_at_position(collider,p,Vector2.ZERO)	#to one side

	if tile1==321 && is_on_floor():
		#this is rex 2 and not a stair but jumps are closer
		return true
		
	if tile2==81 || tile2==54 || tile2==85 || tile2==60:
		return true
		
	if facing==1:
		if tile1==81 || tile1==85:
			return true
	else:
		if tile1==54 || tile1==60:
			return true
	return false
		
func _reset_change_timer():
	change_state=false
	change_timer.stop()
	change_timer.start()
	
func _change_direction():
	facing=-facing
	move_direction=facing
	if [HUMAN_STATES.idle,HUMAN_STATES.kneel,HUMAN_STATES].has(human_state):
		move_direction=0
	_set_facing()
	_reset_change_timer()
	
func _set_facing():
	#change scale, called by ready to position at correct place without affecting anything else
	#also by change directcion
	anim.scale.x=facing
	#the below works and means nothing has to change. Problem
	#is everything is done just like scale.x would, so labels, etc get reversed
	#set_transform(Transform2D(Vector2(facing,0), Vector2(0,1), Vector2(position.x, position.y)))
	
	#anim.flip_h=facing==-1
	

func _get_random_state(_reset):
	#get a random state, but observe rules
	if !_is_grounded(): #and [HUMAN_STATES.jump, HUMAN_STATES.fall].has(human_state):
		#no change if falling or jumping, including turning
		return
	
	if lock_state || lock_state_after_unlock!=HUMAN_STATES.no_force:
		return
		
	#get value 0 to 10
	#0-5 walk - prefer walking
	#6   jump
	#7   crouch
	#8,9 idle
	move_direction=0
	
	#switch direction
	if randf()<0.05:
		#don't do another action and reset timer
		_change_direction()
		return

	var value=randi()%13
	if value<8:
		_change_state(HUMAN_STATES.walk)
		move_direction=facing
	elif value==8:
		_change_state(HUMAN_STATES.jump)
		move_direction=facing
	elif value==9 or value==10:
		_change_state(HUMAN_STATES.idle)
		velocity_human=Vector2.ZERO
	else:
		_change_state(HUMAN_STATES.kneel)
		velocity_human=Vector2.ZERO
	
	change_state=false
	_reset_change_timer()

func _set_animation():
	if lock_anim_state:
		return
		
	match human_state:
		HUMAN_STATES.idle:
			anim.play("idle"+anim_suffix)
			pass
		HUMAN_STATES.kneel:
			anim.play("kneel"+anim_suffix)
		HUMAN_STATES.walk:
			anim.play("walk"+anim_suffix)
		HUMAN_STATES.jump:
			anim.play("fly"+anim_suffix)
		HUMAN_STATES.fall:
			anim.play("fly"+anim_suffix)
	
func _move(_delta):
	anim.speed_scale=Globals.bullet_time
	if !lock_state || !is_rex2:
		velocity_human.x=MAX_SPEED*move_direction#*Globals.bullet_time
		velocity_human=move_and_slide(velocity_human*Globals.bullet_time,Vector2.UP)

func _change_state(new_state):
	last_human_state=human_state
	human_state=new_state
	
	if lock_state_after_unlock!=HUMAN_STATES.no_force:
		human_state=lock_state_after_unlock
		
	var node=get_node("AnimatedSprite/GunPosition")
	node.position.y=standard_gun
	no_fire_override=false
	match human_state:
		HUMAN_STATES.idle:
			move_direction=0
			no_fire_override=true
		HUMAN_STATES.kneel:
			node.position.y=standard_gun+12
			move_direction=0
			if fire_mode==FIRING_MODES.YES:
				launch()	#fire as we kneel
		HUMAN_STATES.walk:
			move_direction=facing
		HUMAN_STATES.jump:
			_jump()
			

func _apply_gravity(delta):
	if applying_boost:
		velocity_human
		velocity_human.x=0
	else:
		velocity_human.y+=Globals.GRAVITY*delta*Globals.bullet_time

func _is_grounded(use_collision_floor:=true):
	var on_ground
	if use_collision_floor:
		on_ground = (is_on_floor() or ground.is_colliding()) and velocity_human.y>=0 and velocity_human.y<=20
	else:
		on_ground = ground.is_colliding() and velocity_human.y>=0 and velocity_human.y<=20
	return on_ground
	
func _is_ray_jump_ready():
	#are we looking at a tile above is
	if ray_jump.is_colliding():
		#only ever collides with tilemap
		#if there is a tile above our collision ray then don't allow
		var p = ray_jump.get_collision_point()
		var v=Vector2(p.x,p.y-GlobalTileMap.map_tile_size)
		var offset=ray_jump.get_collision_normal()
		var collider=ray_jump.get_collider()
		if collider is TileMap:
			if GlobalTileMap.get_tile_at_position(ray_jump.get_collider(),v,offset)<=0:
				return true
		else:
			return false
	return false
		
func _is_ray_block_ready():
	#looks for any block in front of us
	if ray_block.is_colliding() || ray_block2.is_colliding():
		return true
	else:
		return false
		
func _is_ray_edge_ready():
	#looks for a drop
	if ray_edge.is_colliding():
		return false
	else:
		return true
		
func _jump(yvel:=JUMP_SPEED,can_jump_override:=false):
	#don't call this directly, let change state do it
	if move_direction==0:
		move_direction=facing
	if (velocity_human.y>=0) || can_jump_override:
		if can_jump_override:
			velocity_human.x=0
		velocity_human.y=yvel
		
	if latched_vertical_jump:
		move_direction=0
		velocity_human.x=0
		latched_vertical_jump=false

func _on_ChangeTimer_timeout() -> void:
	#set a timer for when we want to think about changing
	#will be overridden/ignored by collisions or certain states, e.g. falling
	change_timer.wait_time=rand_range(change_action_timeout_min,change_action_timeout_max)
	change_state=true
	change_timer.start()
	ignore_till_next_timer=false

func _ignore_till_next_timer():
	making_ready=false
	ignore_till_next_timer=true
	raytimer.start()
	
func _on_RayTimer_timeout() -> void:
	ignore_till_next_timer=false
	$CollisionShape2D.disabled=false
	raytimer.wait_time=RAYWAIT


func _on_OnScreen_viewport_entered(_viewport: Viewport) -> void:
	no_fire_override=false	#might affect idle
	.has_entered_room()


func _on_OnScreen_viewport_exited(_viewport: Viewport) -> void:
	#enemies start outside screen so only exit if they have actually entered
	no_fire_override=true
	if special_training_mode:
		#leave screen and training human then just delete
		queue_free()
	elif has_entered_screen:
		leave_room()
	
func _on_Hurtbox_take_hit(_damage_left) -> void:
	#damage is irrelevant as they are one-shot kill and makes death animation easier
	#if shot then play hit_main animation and add to score multiplier
	#if shot and currently shot (animation still playing) then is shot again
	#	apply animation again and increase velocity slightly
	if is_dead:
		return
		
	do_take_hit()
	
func do_take_hit():
	if just_die:
		bubble_death(false,false,false)
		return
		
	var node=GlobalMethods.assign_temp_location()
	var h=dying_human.instance()
	h.drop_bubble=gives_energy_on_death
	
	node.call_deferred("add_child",h)
	var c=GlobalTileMap.current_player.weapons.current_weapon
	h.call_deferred("start_being_shot",global_position,facing, c.weapon_type,is_on_floor())
	death()

func bubble_death(create_bubble:=true, via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	if is_dead:
		return
	var offset=Vector2(72,0)*facing

	offset.x=0
	GlobalMethods.enemy_explosion(global_position,false,"",0.1,via_smart_bomb,null,Globals.SFX_ENEMY_EXPLODE2)


	if gives_energy_on_death && create_bubble && bubble_death_drop_tip=="":
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.SMALL_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)

func _on_ShootingArea_body_entered(_body: Node) -> void:
	#player entered visible area
	if !is_dead:
		firing_area_entered=true
		_change_helmet_colour()
		$FireTimer.start()
		launch()	#fire first regardless

func _on_ShootingArea_body_exited(_body: Node) -> void:
	#player exited visible area
	if !is_dead:
		firing_area_entered=false
		_change_helmet_colour()
		$FireTimer.stop()

func _on_FireTimer_timeout() -> void:
	#triggered so we are firing
	_set_timer_fire()
	launch()


func _on_DeadlyCheck_body_entered(_body: Node) -> void:
	#this is the deadly environment
	if !_is_grounded():
		bubble_death(false,false,false)
		return
		
	#small chance to matter transport out
	if randf()<0.5:
		lock_state=true
		lock_anim_state=true
		anim.play("appear_rex2",false)
		yield(anim, "animation_finished")
		lock_state=false
		lock_anim_state=false
		#yield(get_tree().create_timer(1), "timeout")
		death(false)
	else:
		_change_direction()
