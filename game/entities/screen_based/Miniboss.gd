extends RigidBody2D

signal miniboss_dead

const NOPHYS_SPEED=85
const NOPHYS_SPEED_PHASE2=45
var direction=-1

onready var anim=$AnimationPlayer
onready var flash=$FlashAnimationPlayer
onready var sprite_damaged=$AnimatedSprite
onready var sprite_parts=[
	[$SpritePart1_1,$SpritePart1_2],
	[$SpritePart2_1,$SpritePart2_2],
	[$SpritePart3_1,$SpritePart3_2,$SpritePart3_3]
]
onready var hurt=$Hurtbox
onready var hit=$Hitbox
onready var homing_position=$Position2DHoming
var parts_killed=0
onready var explosion=preload("res://entities/base_classes/ExplosionCluster.tscn")
onready var scorer=preload("res://entities/environment/ScoreRiser.tscn")
onready var bullet=preload("res://entities/projectiles/Enemy_Bullet_Single.tscn")
onready var mine=preload("res://entities/projectiles/UfoMine.tscn")
onready var debris_part=preload("res://entities/projectiles/BasicDebris.tscn")
onready var homing=preload("res://entities/projectiles/Missile_Launcher_Missile_Homing.tscn")

var weapon_container
var path_phase1
var thrust_start=false
var ship=preload("res://entities/screen_based/ScreenEnemy_Ship.tscn")
var pause_firing=false
var in_progress=false
var hit_count=0

enum STATES {INACTIVE, INTRO, PHASE1_SETUP, PHASE1, PHASE2_SETUP, PHASE2, PHASE3}

var state=STATES.INACTIVE
var state_inprogress=false
var debris
var basic_mode=false
var final_mode=false		#final mode is final boss and just gives one phase and limited bullets

onready var defaultpos=global_position

func _ready() -> void:
	$InitialDust.emitting=false
	$ShipThrust1.emitting=false
	$ShipThrust2.emitting=false
	$RayCastDown.enabled=false
	$RayCastUp.enabled=false
	state=STATES.INACTIVE
	visible=false
	in_progress=false
	applied_force=Vector2.ZERO
	disable(false)
	hide_godray()
	
func show_godray():
	var r=get_tree().root.find_node("GodRayMini",true,false)
	if r:
		yield(get_tree().create_timer(2.5), "timeout")
		r.visible=true
		
func hide_godray():
	var r=get_tree().root.find_node("GodRayMini",true,false)
	if r:
		r.visible=false
	

func activate():
	#start of miniboss essentially
	#this is what is called
	randomize()
	applied_force=Vector2.ZERO
	weapon_container=GlobalMethods.assign_temp_location()
	update_mode(false)		#true for testing
	
	Globals.disallow_options=true
	if in_progress:
		#this is for when restarted after killed in same room
		#if !final_mode and in_progress:
		if in_progress:
			AudioManager.play_music(Globals.MUSIC_MINI,false,0.0,false)
		GlobalTileMap.rex_room_manager.kill_room_bullets()
		boss_pause_firing(3.0)
		return
		
	enable(true)
	propagate_call("set_process",[true])
	propagate_call("set_physics_process",[true])
	
	if final_mode:
		state=STATES.PHASE1_SETUP
	else:
		state=STATES.INTRO
		
	_part_alive(0)
	_part_alive(1)
	_part_alive(2)
	if in_progress:
	#if !final_mode and in_progress:
		AudioManager.play_music(Globals.MUSIC_MINI,false,0.0,false)
	in_progress=true
	Globals.disallow_options=false

func set_health(value):
	if value>0:
		$Hurtbox.health=value
		$Hurtbox.initial_health=value
		
func enable(_make_visible:bool):
	#enabled is still not functioning until activated
	#as will be called by room manager automatically along with disable
	#is not in same room as player so essentially never called by manager
	#only by activate
	global_position=defaultpos
	visible=true
	sleeping=false
	propagate_call("set_process",[false])
	propagate_call("set_physics_process",[false])
	hit.set_deferred("monitoring",true)
	
func disable(_make_visible:bool):
	state=STATES.INACTIVE
	global_position=defaultpos
	sleeping=true
	hit.set_deferred("monitoring",false)
	propagate_call("set_process",[false])
	propagate_call("set_physics_process",[false])

func _process(_delta: float) -> void:
	#used to transition states
	if !sleeping:
		pass

	if state==STATES.INACTIVE:
		return
	if state_inprogress:
		return
		
	match state:
		STATES.INTRO:
			_intro()
			#temp
			#global_position.y=2450
			#state=STATES.PHASE1_SETUP
		STATES.PHASE1_SETUP:
			_phase1_setup()
			#if !final_mode:
			AudioManager.play_music(Globals.MUSIC_MINI,false,0.0,false)

		STATES.PHASE2_SETUP:
			_phase2_setup()
		STATES.PHASE3:
			_phase3_setup()
			
func _physics_process(delta: float) -> void:
	#print(global_position.y)
	#print(linear_velocity)
	if !sleeping:
		pass
	if !state_inprogress:
		_move(delta)
		
func _phase1_setup():
	#after the intro, play continues
	state_inprogress=true
	#start timer, the timer does the firing, process the moving
	state=STATES.PHASE1
	anim.play("ZeroData")
	$RayCastDown.enabled=true
	$RayCastUp.enabled=true
	_fire_bullets(true,true)
	$TimerBullets.start()
	$TimerMines.start(rand_range(1,4))
	state_inprogress=false
	Globals.game_scene.player.defence.full_tank()

	
func _phase2_setup():
	#boss outer shell is now removed
	AudioManager.play_sfx(Globals.SFX_ENEMY_MINISTRIPPED)
	GlobalPersist.score+=1000
	state_inprogress=true
	GlobalTileMap.rex_room_manager.kill_room(true,0.0,false,false)
	#wait for explosions to die down
	GlobalTileMap.current_player.double_lock(true)
	sleeping=true
	yield(get_tree().create_timer(1.0), "timeout")
	
	_intro_step_shake(0.05,0.2,3)
	yield(get_tree().create_timer(1.5), "timeout")
	_intro_step_explode_tiles(2)

	yield(get_tree().create_timer(4.0), "timeout")
	#little message
	var c=Globals.game_scene.find_node("Container_Miniboss",true,false)
	if c!=null:
		var mess=scorer.instance()
		c.add_child(mess)
		mess.global_position=global_position-Vector2(GlobalTileMap.map_tile_size*5,GlobalTileMap.map_tile_size*2)
		GlobalTileMap.zoom_camera(true,2.0,global_position)
		yield(get_tree().create_timer(1.5), "timeout")
		anim.play("Angry")
		AudioManager.play_sfx(Globals.SFX_ENEMY_DROID)
		yield(anim,"animation_finished")
		AudioManager.play_sfx(Globals.SFX_ENEMY_CHAT)
		mess.start("Die Rino",GlobalMethods.get_as_colour(Globals.COLOURS.red_bright),1.5,0.3)
		yield(get_tree().create_timer(1.0), "timeout")
		GlobalTileMap.zoom_camera(false)
	
	
	GlobalTileMap.current_player.double_lock(false)
	state=STATES.PHASE2
	hurt.reset_health()


	sleeping=false
	state_inprogress=false
	GlobalTileMap.rex_room_manager.spawn_timer.start()
	boss_pause_firing(1.5)
		
func _phase3_setup():
	#miniboss is killed
	#wait a second for explosions to finish, give extra life
	GlobalPersist.score+=10000

	GlobalTileMap.rex_room_manager.kill_room(true,0.0,false,false)
	state_inprogress=true
	var pos=global_position
	yield(get_tree().create_timer(1.0), "timeout")	#wait for explosions
	disable(false)
	mode=RigidBody2D.MODE_KINEMATIC
	GlobalTileMap.current_player.double_lock(true)
	
	#shard
	GlobalMethods.explosion_shards(pos,"MiniBoss",null,0.0,1.0,20,25,80)
	
	#create pickups
	GlobalTileMap.rex_room_manager.kill_room(true,0.0,true,false)
	GlobalMethods.create_energy_bubble(pos)
	GlobalMethods.create_energy_bubble(pos-Vector2(GlobalTileMap.map_tile_size,0),Globals.PICKUP.SMALL_ENERGY)
	GlobalMethods.create_energy_bubble(pos+Vector2(GlobalTileMap.map_tile_size,0),Globals.PICKUP.SMALL_ENERGY)
	
	GlobalTileMap.rex_room_manager.kill_room(true,0.0,false,false)
	GlobalTileMap.rex_room_manager.spawn_timer.stop()
	#create enemy ship
	yield(get_tree().create_timer(0.5), "timeout")
	var enemy=ship.instance()
	enemy.screen_based=true		#never leave screen
	enemy.speed=enemy.SPEED_HUMAN
	enemy.ship_type=enemy.SHIP_TYPE.HUMAN
	enemy.transient_contained=true	#kill on death
	enemy.global_position=pos-Vector2(GlobalTileMap.map_tile_size*1.5,GlobalTileMap.map_tile_size*1.5)
	if enemy.has_method("set_graphics_sprite_type"):
		enemy.set_graphics_sprite_type()

	var container=GlobalMethods.assign_temp_location()
	container.add_child(enemy)	#ready only called when added to scene
	
	if !final_mode:
		#blow up remaining wall
		_intro_step_shake(0.05,0.2,3)
		yield(get_tree().create_timer(1.0), "timeout")
		_intro_step_explode_tiles(3)
		
		#ArrowLeftMiniBoss and message if miniboss fight, not the end boss
		yield(get_tree().create_timer(3.5), "timeout")
		_enable_arrow()
		#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"end_battle",Globals.MINIBOSS_TEXT["end_battle"],true)
		GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["end_battle"],false, true,true)
		yield(GlobalMethods,"dialog_finished")
		OptionsAndAchievements.increment_log("Miniboss",1)
			
	enemy.enable(true)
	GlobalTileMap.current_player.double_lock(false)
	#used currently by boss level fight, not the miniboss
	#but if do then need to link to dialog box
	emit_signal("miniboss_dead")

func _enable_arrow():
	var a=SceneManager.get_node_from_game("ArrowLeftMiniBoss",false)
	if !a:
		return
		
	a.start_disabled=false
	a.enable(true)
	
func _move(delta):
	var ray_down=false
	if $RayCastDown.is_colliding() && (linear_velocity.y)>=0:
		#downwards and hits environment
		ray_down=true
		direction=-1
	elif $RayCastUp.is_colliding() && (linear_velocity.y)<=0:
		#up and hits environment
		direction=1
		
	if basic_mode:
		if state==STATES.PHASE1:
			global_position.y+=(direction*NOPHYS_SPEED*delta)
		elif state==STATES.PHASE2:
			global_position.y+=(direction*NOPHYS_SPEED_PHASE2*delta)
		return
		
	#physics mode
	if ray_down && state==STATES.PHASE1:
		#stable mode use single impulse
		apply_central_impulse(Vector2(0,-390))
		_thrust(1)
	elif ray_down && state==STATES.PHASE2:
			#unstable mode
			#apply velocity
			if !thrust_start:
				thrust_start=true
				if randi()%10<6:
					anim.play("Phase2bThrust")
				else:
					anim.play("Phase2bThrust")
				_thrust(1)
				yield(anim,"animation_finished")
				thrust_start=false
		
func _intro():
	#overview:	dust appears 													8secs
	#			with small screen shake, player currently locked from 5secs for 5secs
	#			room tiles drop and kill half the humans						7secs
	#			slightly more shaking											4secs
	#			all phase 1 tiles explode										3secs
	#			shaking stops and boss drops in, killing other two humans		4secs
	#			end, return to normal coding
	anim.play("ZeroData")
	hide_godray()

	GlobalTileMap.current_player.double_lock(true)
	state_inprogress=true
	debris=GlobalMethods.find_nodes_like("BasicDebris","Container_Miniboss")
	
	#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"start_battle",Globals.MINIBOSS_TEXT["start_battle"],true)
	GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["start_battle"],false, true,true)
	
	anim.play("enter_room")
	yield(anim,"animation_finished")
	Globals.game_scene.hud_ui.show_hud(true)
	
	GlobalTileMap.current_player.double_lock(false)
	state=STATES.PHASE1_SETUP
	state_inprogress=false

func _enter_miniboss():
	AudioManager.play_sfx(Globals.SFX_GAME_LIFTJUMP)

func _thrust(time:=1.0):
	if $ShipThrust1.emitting:
		return
	
	if basic_mode:
		$ShipThrust1.emitting=false
		$ShipThrust2.emitting=false
		return
		
	if time==null || time<=0.0:
		$ShipThrust1.emitting=false
		$ShipThrust2.emitting=false
		
	$ShipThrust1.emitting=true
	$ShipThrust2.emitting=true
	
	#thrust stops after a time for our case
	yield(get_tree().create_timer(time), "timeout")
	$ShipThrust1.emitting=false
	$ShipThrust2.emitting=false
	AudioManager.play_sfx(Globals.SFX_ENEMY_MINJUMP)
	
func _intro_step_shake(from:float, to:float, duration:float):
	#screen shake, called multiple times
	var tw = create_tween()
	tw.tween_method(GlobalMethods,"screen_shake",from,to,duration)
	tw.set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)

func _intro_step_drop_debris():
	#for each found debris, start it off
	#in our case each has a start delay for offsetting
	for item in debris:
		if is_instance_valid(item):
			item.start()

func _intro_step_explode_tiles(tile_id):
	#this gets the location of the miniboss map tiles, the tiles are 1 or 2
	#so each phase passes in their tile
	Globals.disallow_options=true
	var tiles:Array=GlobalTileMap.get_room_miniboss_tiles(tile_id)
	tiles.shuffle()
	var c=tiles.size()
	var mod=1
	var interval=0.1
	if c<15:
		interval=0.15
	elif c<30:
		mod=2
	else:
		mod=int(c/20)+1
		
	var location:Vector2
	var count=0
	Globals.game_scene.fullscreen_shock(true,Vector2(0.5,0.5),true)
	#go through all tiles in miniboss and clear both main map
	#and destroyable map as it's irrelevant
	#then create explosion
	var found
	for ti in tiles:
		count+=1
		found=0
		var t=ti[0]
		location=GlobalTileMap.get_global_from_cell(null,t)
		found=GlobalTileMap.replace_single_tile(null,t.x,t.y,-1)
		found+=GlobalTileMap.replace_single_tile(GlobalTileMap.current_destroyable_map,t.x,t.y,-1)
		#no trauma, but turn off animation and then try this
		#only explode if there was a tile there
		if found>0:
			if count%mod==0:
				GlobalMethods.explosion_standard(location,true,true,0.2,0,Globals.SFX_ENEMY_EXPLODE3)
				yield(get_tree().create_timer(interval), "timeout")
	Globals.disallow_options=false
	
func _intro_step_kill_humans():
	Globals.disallow_options=true
	var humans=GlobalMethods.find_nodes_like("HumanStartMiniboss","Container_Miniboss")
	for h in humans:
		if !h.is_dead:
			h.bubble_death(false,false,false)
	Globals.disallow_options=false

func _intro_step_phew():
	Globals.disallow_options=true
	var c=Globals.game_scene.find_node("Container_Miniboss",true,false)
	
	if c==null:
		return
		
	var h1=c.find_node("HumanStartMiniboss1",true,false)
	var h2=c.find_node("HumanStartMiniboss2",true,false)
	if h1==null || h2==null:
		return
	
	var score1=scorer.instance()
	var score2=scorer.instance()
	c.add_child(score1)
	c.add_child(score2)
	score1.global_position=h1.global_position-Vector2(0,GlobalTileMap.map_tile_size)
	score2.global_position=h2.global_position-Vector2(0,GlobalTileMap.map_tile_size)
	score1.start(Globals.MINIBOSS_TEXT["phew2"][0]["text"],GlobalMethods.get_as_colour(Globals.COLOURS.white),1.5,0.3)
	AudioManager.play_sfx(Globals.SFX_ENEMY_CHAT)
	yield(get_tree().create_timer(0.75), "timeout")
	score2.start(Globals.MINIBOSS_TEXT["phew"][0]["text"],GlobalMethods.get_as_colour(Globals.COLOURS.white_bright),1.5,0.3,110)

	#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.PICKUP,Globals.MINIBOSS_TEXT["phew"],false, Vector2(100,550))
	#change this to a simple graphic with a timer to hide
	#see about copying score riser
	pass
	Globals.disallow_options=false

func update_mode(force_basic:=false):
	var show_material=$AnimatedSprite.material
	var p11=sprite_parts[0][0].material
	var p12=sprite_parts[0][1].material
	var p21=sprite_parts[1][0].material
	var p22=sprite_parts[1][1].material
	var p31=sprite_parts[2][0].material
	var p32=sprite_parts[2][1].material
	var p33=sprite_parts[2][2].material
	show_material.set_shader_param("all_white",GlobalPersist.graphics_type==0)
	p11.set_shader_param("all_white",GlobalPersist.graphics_type==0)
	p12.set_shader_param("all_white",GlobalPersist.graphics_type==0)
	p21.set_shader_param("all_white",GlobalPersist.graphics_type==0)
	p22.set_shader_param("all_white",GlobalPersist.graphics_type==0)
	p31.set_shader_param("all_white",GlobalPersist.graphics_type==0)
	p32.set_shader_param("all_white",GlobalPersist.graphics_type==0)
	p33.set_shader_param("all_white",GlobalPersist.graphics_type==0)
	
	#basic mode if speccy and do not want extras
	var sleep=sleeping
	
	if force_basic:
		basic_mode=true
		mode=RigidBody2D.MODE_KINEMATIC
	else:
		basic_mode=false
		mode=RigidBody2D.MODE_CHARACTER

	#setting mode enabled sleeping
	sleeping=sleep
		
func _fire_bullets(double:=false, force:=false):
	
	if state!=STATES.PHASE1 && state!=STATES.PHASE2 && !force:
		return
		
	if pause_firing:
		return
		
	_fire_missile()
	var times=2 if double else 1
	
	for loop in times:
		for pos in $Bullets.get_children():
			var bullet_object=bullet.instance()
			bullet_object.bullets_destroy_destructible=true
			bullet_object.global_position=pos.global_position
			bullet_object.set_angle(0)
			weapon_container.add_child(bullet_object)
			bullet_object.start()
		if double:
			yield(get_tree().create_timer(0.2), "timeout")
			
func _fire_missile():
	if state!=STATES.PHASE2:
		return
	if pause_firing:
		return
		
	#only fire missile once every 3 or so
	if randi()%10<7:
		return
	var node=GlobalMethods.assign_temp_location()
	var missile_object=homing.instance()
	missile_object.target_is_player=true
	node.add_child(missile_object)
	missile_object.global_position=homing_position.global_position
	missile_object.launch(1)

func _fire_mines():
	if state!=STATES.PHASE1:
		return
		
	if pause_firing:
		return

	#lay up to 3 mines
	var missile_object
	var node=GlobalMethods.assign_temp_location()
	var lay=false
	#we want up to 3 mines
	for i in range(3):
		lay=false
		if i==0:
			lay=true
		else:
			if rand_range(0.0,10)>2.5*(i+1):
				lay=true
		#lay mines with a gap
		if lay:
			missile_object=mine.instance()
			missile_object.global_position=$Mine.global_position
			missile_object.start_freeze_time=rand_range(0.3,0.7)
			node.add_child(missile_object)
			missile_object.start()
			yield(get_tree().create_timer(rand_range(0,0.3)), "timeout")
	
func _on_TimerBullets_timeout() -> void:
	#may fire double
	if randf()<0.2:
		_fire_bullets(true)
		$TimerBullets.wait_time=rand_range(0.5,3)
	else:
		_fire_bullets()
		if state==STATES.PHASE1:
			$TimerBullets.wait_time=rand_range(1,4)
		else:
			$TimerBullets.wait_time=rand_range(0.5,4)
	$TimerBullets.start()


func _on_TimerMines_timeout() -> void:
	_fire_mines()
	$TimerMines.wait_time=rand_range(0.6,5)
	$TimerMines.start()

func _part_alive(item):
	#enable visible parts of enemy
	for i in sprite_parts[item]:
		i.visible=true

func _part_kill():
	#kill in order
	#print("DEAD "+str(parts_killed))
	parts_killed+=1
	hit_count=0
	
	#create explosion
	var e=explosion.instance()
	e.cluster_count=randi()%5+1
	e.min_delay_between_explosions=0.1
	e.max_delay_between_explosions=0.2
	add_child(e)	#explosions move with boss
	
	if state==STATES.PHASE1:
		hurt.reset_health()
		#e.global_position=sprite_parts[parts_killed-1].global_position
		if parts_killed<=3:
			e.global_position=sprite_parts[parts_killed-1][0].global_position
			for i in sprite_parts[parts_killed-1]:
				#sprite_parts[parts_killed-1].visible=false
				i.visible=false
		if parts_killed>=3:
			#second phase done, move to next
			e.cluster_count=randi()%5+3
			e.global_position=global_position
			
			if final_mode:
				e.cluster_count=randi()%8+5
				e.global_position=global_position
				state=STATES.PHASE3
			else:
				state=STATES.PHASE2_SETUP
				
	if state==STATES.PHASE2:
			e.cluster_count=randi()%8+5
			e.global_position=global_position
			state=STATES.PHASE3
	e.start_explosions()
	yield(get_tree().create_timer(1.0), "timeout")
	_shard()
	
func _shard():
	#part
	if parts_killed>3:
		return
	var c=GlobalMethods.assign_temp_location()
	
	for i in sprite_parts[parts_killed-1]:
		var d=debris_part.instance()
		d.speed=600
		d.hits_environment=false
		d.bullets_destroy_destructible=false
		d.self_rotate_speed=240
		d.set_angle(randi()%360)
		d.rotate_with_angle=true
		d.auto_start=true
		d.delay_start_min=0.0
		d.delay_start_max=0.0
		#d.get_node("Sprite").texture=sprite_parts[parts_killed-1].texture
		#d.global_position=sprite_parts[parts_killed-1].global_position
		d.get_node("Sprite").texture=i.texture
		d.global_position=i.global_position
		d.z_index=5
		d.set_sprite_type()
		c.add_child(d)
		d.enable(true)		


func _on_Hurtbox_hurt_started() -> void:
	flash.play("flash")


func _on_Hurtbox_hurt_ended() -> void:
	flash.play("default")

func _on_Hurtbox_take_hit(damage_taken) -> void:
	hit_count+=1
	print("HIT "+str(hit_count)+":"+str(damage_taken))
	if damage_taken<=0:
		print("DEAD PART")
		_part_kill()

func boss_pause_firing(time:float):
	self.pause_firing=true
	yield(get_tree().create_timer(time), "timeout")
	self.pause_firing=false
