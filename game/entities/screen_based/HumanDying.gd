extends KinematicBody2D

#single/double/drone gives repeat (i.e. can multiply then die)
#spray gives air then die
#laser is immediate death
const JUMP:=-190
const LATERAL:=150
const SCORE_ACCUMULATOR:=100

var score_position
var facing:=1
var shot_count:=0
var dead=true
var velocity_dying:Vector2
var furthest_left:=0
var furthest_right:=0
var points:=0
var points_per_shot:=0
var death_type=Globals.HUMAN_DEATH_TYPE.REPEAT
var scorer=preload("res://entities/environment/ScoreRiser.tscn")
var score_colours=[Globals.COLOURS.white,
					Globals.COLOURS.white_bright,
					Globals.COLOURS.yellow_bright,
					Globals.COLOURS.cyan_bright,
					Globals.COLOURS.magenta_bright,
					Globals.COLOURS.red_bright]
onready var drop_bubble:=false
onready var anim=$AnimatedSprite
onready var timer=$Timer
onready var hit=$AnimatedSprite/Hitbox
onready var hurt=$AnimatedSprite/Hurtbox


func _ready() -> void:
	score_position=global_position
	velocity_dying=Vector2(LATERAL,0.0)
	furthest_left=GlobalTileMap.main_camera.global_position.x
	furthest_right=GlobalTileMap.main_camera.global_position.x+GlobalTileMap.found_screen_size.x
	set_graphics_sprite_type()
	
func set_graphics_sprite_type():
	var material=$AnimatedSprite.material
	material.set_shader_param("all_white",GlobalPersist.graphics_type==0)

func _physics_process(delta: float) -> void:
	#fall human but keep x same speed
	velocity_dying.y+=Globals.GRAVITY*delta*Globals.bullet_time
	velocity_dying=move_and_slide(velocity_dying,Vector2.UP)
		
	var cx=position.x
	if cx<furthest_left or cx>furthest_right:
		_human_dead(false)
		
func _add_score():
	var c=GlobalMethods.assign_temp_location()
	if shot_count==1:
		score_position=global_position+Vector2(0,-48)
	var i=shot_count-1
	if i>5:
		i=5
	if shot_count>1:	#don't show if just 1 human
		var score=scorer.instance()
		c.add_child(score)
		score.global_position=score_position
		score.start(str(shot_count*SCORE_ACCUMULATOR),GlobalMethods.get_as_colour(score_colours[i]),i)
	GlobalPersist.score+=shot_count*SCORE_ACCUMULATOR
	
func _stage2():
	if timer.is_stopped():
		#time is either
		timer.start()
		dead=true
		anim.play("dying")
		hit.deactivate()
		hurt.deactivate()

func _human_dead(via_smart_bomb:=false):
	GlobalMethods.enemy_explosion(global_position,true,"",0.1,via_smart_bomb,null,Globals.SFX_ENEMY_EXPLODE2)
	if drop_bubble:
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.SMALL_ENERGY,Globals.BUBBLE_DELAY)
		OptionsAndAchievements.increment_achievement("sure",1,"Human")
		
	queue_free()
	
func start_being_shot(g_position:Vector2,direction:int, weapon, on_floor:bool):
	#main entry point
	global_position=g_position
	facing=direction
	points_per_shot=Globals.VALUE_HUMAN
	anim.scale.x=facing
	shot_count=1
	#if hit in back then die immediately
	var player=GlobalTileMap.current_player
	var playerpos=player.position
	if playerpos.x>position.x && facing==-1:
		_human_dead(false)
	elif playerpos.x<position.x && facing==1:
		_human_dead(false)
	else:
		_set_death_start(weapon,on_floor)
		
func _set_death_start(weapon,on_floor:bool):
	#called once at start
	velocity_dying.x=velocity_dying.x*(-facing)	#go in opposite direction
	timer.wait_time=0.5	#death

	_add_score()
	#if in air then just die
	if !on_floor:	
		velocity_dying.y=JUMP
		_stage2()
	else:
		match weapon:
			Weapons_System.WEAPON.BULLET_DOUBLE,Weapons_System.WEAPON.BULLET_SINGLE:
				velocity_dying.y=0
				anim.play("first_shot")
				dead=false
			Weapons_System.WEAPON.LASER:
				_human_dead(false)
			Weapons_System.WEAPON.NONE:
				print("ERROR. Weapon none!")
				_human_dead(false)
			_:
				_stage2()
			
func _on_Hurtbox_take_hit(_damage_taken) -> void:
	#only here if on repeat. half second per hit
	if !dead:
		anim.stop()
		anim.frame=0
		anim.play("shot_again")
		shot_count=shot_count+1
		_add_score()
		if shot_count>6:
			OptionsAndAchievements.increment_achievement("recoil",1,"",0,Globals.SFX_ENEMY_RECOIL2)
			_stage2()
		else:
			AudioManager.play_sfx(Globals.SFX_ENEMY_RECOIL1)
		#print("Shot multiplier at " + str(shot_count))

func _on_AnimatedSprite_animation_finished() -> void:
	#if there's a pending shot then take it otherwise next stage
	if !dead:
		_stage2()


func _on_Timer_timeout() -> void:
	_human_dead(false)


func _on_VisibilityNotifier2D_viewport_exited(_viewport: Viewport) -> void:
	#only here really if go below screen
	if !GlobalTileMap.camera_zoom:
		queue_free()
