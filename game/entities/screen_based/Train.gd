extends Enemy

onready var train_parts:=$ChooChoo
var rear_entered_screen_count:=0
var can_move=false
var train_now_derailed:=false

func set_no_firing():
	#override for firing_left/right
	for child in train_parts.get_children():
		child.can_fire=false
	
func _ready() -> void:
	_process_toggle(false)
	$Derailed/DerailedSmoke.emitting=false
	$Derailed.visible=false

		
func _process(_delta: float) -> void:
	#for when leave screen.there will be a remnant when smart bomb
	if $ChooChoo.get_child_count()==0:
		remove_train()

func remove_train():
	if !train_now_derailed:
		queue_free()
		
func _start_train():
	for child in train_parts.get_children():
		#child.set_physics_process(true)
		child.enable(true)
		
func _set_carts():
	var x:=1
	#set position if not already set, i.e. based on node order
	#if some are not set and others are, bad things happen with ordering
	for child in train_parts.get_children():
		if train_now_derailed:
			_start_train()
			break
		if child.firing_right:
			child.get_node("Hurtbox").health=child.get_node("Hurtbox").health/3.5
		if child.part_position==0:
			child.set_position(x)
			child.set_physics_process(false)
			x=x+1
			#if x==0:
			child.connect("cart_dead",self,"_cart_dead")

func override_speed(value:int):
	for child in train_parts.get_children():
		child.speed=value
		
func bubble_death(create_bubble:=true, via_smart_bomb:=false,_include_score:=true,_pause:=0.0):
	if is_dead:
		return
	is_dead=true	#set it here
	for t in $ChooChoo.get_children():
		if is_instance_valid(t) and t.part_position==1:
			_derailed(t.position)
		if is_instance_valid(t):
			t.bubble_death(create_bubble,via_smart_bomb)
			yield(get_tree().create_timer(.2), "timeout") #sometimes causes issues so using nullcheck
	remove_train()
	
func set_graphics_sprite_type():
	for child in train_parts.get_children():
		child.set_graphics_sprite_type()
		 
func _derailed(cart_position):
	train_now_derailed = true
	_process_toggle(false)
	$Derailed.position=cart_position+Vector2(32,-32)
	$Derailed.visible=true
	$Derailed/DerailedSmoke.emitting=true
	
func _cart_dead(cart_position):
	#when a cart is dead every cart after that is stuck
	var to_delete=null
	for child in train_parts.get_children():
		if is_instance_valid(child):
			if child.part_position==cart_position:
				to_delete=child
			elif child.part_position>cart_position:
				child.stuck()
	if is_instance_valid(to_delete):
		if cart_position==1:
			_derailed(to_delete.position)
		to_delete.queue_free()
	
func enable(_make_visible:bool):
	.enable(_make_visible)
	_set_carts()
	if !train_now_derailed:
		yield(get_tree().create_timer(.8), "timeout")
		AudioManager.play_sfx(Globals.SFX_GAME_TRAIN)
		$DelayStartTimer.wait_time=rand_range(0.5,4.0)
		$DelayStartTimer.start()

func disable(make_visible:bool):
	.disable(make_visible)
	if !train_parts:
		return
		
	for child in train_parts.get_children():
		child.disable(false)
		
	_process_toggle(false)
	$Derailed/DerailedSmoke.emitting=false
	$Derailed.visible=false


func _on_DelayStartTimer_timeout() -> void:
	_start_train()
	.enable(true)
