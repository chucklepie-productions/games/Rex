extends Enemy
#always travels up or down until hits a collision
#always at same angle
#always at same speed
#may have a small chance of changing left/right during flight
#when hits collision follows normal bounce direction
#goes through one-way blocks
#at start travels half speed and half angle for a few seconds
#then reverts to normal

onready var animate=$AnimatedSprite
onready var rays=$FindPlayer
var last_location=Vector2.ZERO

enum DIRECTION {UP,DOWN,LEFT,RIGHT}
const SPEED=40
const evaluate_change_distance=64

var angry=false
var panic=false
var panic_speed_factor=1

var velocity_drone:=Vector2.ZERO
var current_direction=DIRECTION.RIGHT
var missile=preload("res://entities/projectiles/RoofTurretMissile.tscn")
var found_flags=[false,false,false,false]
var can_fire=true
var terminate_pattern:=false

func _ready() -> void:
	#automatically calls parent
	randomize()
	my_half_size=$CollisionShape2D.shape.radius/2
	_get_new_direction(false)
	set_graphics_sprite_type()

func set_graphics_sprite_type():
	_set_sprite_type($AnimatedSprite)

func _physics_process(delta: float) -> void:
	$AnimatedSprite.set_speed_scale(Globals.bullet_time)

	#has not left the room or has left but timer is on
	_move_ship(delta)
	_check_rays()

func _check_rays():
	#check if any rays are hitting player
	if angry || !can_fire:
		return
		
	found_flags=[false,false,false,false]
	var found=false
	for r in rays.get_children():
		if r.is_colliding():
			var b = r.get_collider()
			#print(b.name+":"+name)
			#if b is Enemy:
			#	print("enemy blocking raycast")
			#	pass
			#if b is TileMap:
			#	print("tilemap blocking raycast")
			if b is Player:
				found=true
				if r.name.ends_with("Up"):
					found_flags[0]=true
				if r.name.ends_with("Down"):
					found_flags[1]=true
				if r.name.ends_with("Left"):
					found_flags[2]=true
				if r.name.ends_with("Right"):
					found_flags[3]=true
	if found:
		_player_found()
	else:
		$AnimatedSprite.material.set_shader_param("all_white_turn_black",Color(0.0,0.75,0.75))

func _play_found_sound():
	AudioManager.play_sfx(Globals.SFX_ENEMY_DROID)
	
func _player_found():
	angry=true
	can_fire=false
	$AnimatedSprite.material.set_shader_param("all_white_turn_black",Color(1.0,0.0,0.0))
	animate.play("attack")
	$AngryAnimationPlayer.play("angry")
	$AngryAnimationPlayer.queue("normal")

func launch():
	#if in panic mode, we don't let them fire, just go fast
	if Globals.bullet_time!=1.0:
		return
	
	#can fire up to 3 times, so randomise it so get 0 to 3 bullets
	if randf()>0.5:
		return
	#can fire either in the direction of the player left/right or up/down
	#cross shape
	#cross+diagonal
	var cross=false
	var diagonal=false
	var r=randf()
	if r<=0.15:				#15%
		#cross+diagonal
		diagonal=true
	elif r<=0.4:			#25%
		#cross
		cross=true

	var offset=24
	#always in axis found or if cross or diagonal
	if (found_flags[0] || found_flags[1]) || cross || diagonal:
		#up/down
		_launch_weapons(90,0,offset)
		_launch_weapons(90+180,0,-offset)
	#always in axis found or if cross or diagonal
	if (found_flags[2] || found_flags[3]) || cross || diagonal:
		#left/right
		_launch_weapons(0,offset)
		_launch_weapons(180,-offset)
	#we have fired cross or valid axis, all left is diagonal
	if diagonal:
		#diagonals
		var factor=1.0/cos(deg2rad(45))	#1.414
		_launch_weapons(45,offset,offset,factor)
		_launch_weapons(45+90,-offset,offset,factor)
		_launch_weapons(45+90+90,-offset,-offset,factor)
		_launch_weapons(45+90+90+90,offset,-offset,factor)
		
func _launch_weapons(angle,x_off:=0, y_off:=0,speed_factor:=1.0):
	var missile_object
	var node=GlobalMethods.assign_temp_location()
	missile_object=missile.instance()
	#set speed
	#factor is same if orthogonal but 1.4 if diagonal to make them
	#be at same positions when moving
	#but need to speed up diagonal to do this, making it faster
	#so reducing overall speed to counter this
	missile_object.set_speed(missile_object.speed*.6*speed_factor)
	missile_object.global_position=global_position+Vector2(x_off,y_off)
	missile_object.set_angle(angle)
	node.add_child(missile_object)
	missile_object.start()
	
func _move_ship(delta):
	#decide if want to change direction
	if angry:	#still angry, so staying still
		return
		
	#move our body and check if it collides
	var collision = move_and_collide(velocity_drone * delta * Globals.bullet_time)
	var bounce=false
	if collision:
		bounce=true
		#this hideous code is to figure out which tile we are on
		#if collision.collider is TileMap:
			# Find the character's position in tile coordinates
			#var tile_pos = collision.collider.world_to_map(position)
			# Find the colliding tile position
			#tile_pos -= collision.normal
			# Get the tile id
			#var tile_id = collision.collider.get_cellv(tile_pos)
			
			#now we have our tile get its one way status
			#bounce=!collision.collider.tile_set.tile_get_shape_one_way (tile_id,0) 
			
			#if it's one way (not bounce back) then temporarily disable our collision
			#if !bounce:
			#	$CollisionShape2D.set_deferred("disabled",true)
			#	yield(get_tree().create_timer(0.1), "timeout")
			#	$CollisionShape2D.set_deferred("disabled",false)
	if bounce:
		_change_direction(true)
	elif abs(position.distance_to(last_location))>evaluate_change_distance:
		_change_direction(false)
		
	velocity_drone=SPEED*panic_speed_factor*_current_vector()
	
func _current_vector():
	match current_direction:
		DIRECTION.LEFT:
			return Vector2.LEFT
		DIRECTION.RIGHT:
			return Vector2.RIGHT
		DIRECTION.UP:
			return Vector2.UP
		DIRECTION.DOWN:
			return Vector2.DOWN
#virtual
func enable(make_visible:bool):
	.enable(make_visible)
	$SoftCollision/CollisionShape2D.set_deferred("disabled",false)

func disable(make_visible:bool):
	.disable(make_visible)
	$AnimatedSprite.stop()
	$SoftCollision/CollisionShape2D.set_deferred("disabled",true)

func _reverse_direction():
	match current_direction:
		DIRECTION.LEFT:
			current_direction=DIRECTION.RIGHT
		DIRECTION.RIGHT:
			current_direction=DIRECTION.LEFT
		DIRECTION.UP:
			current_direction=DIRECTION.DOWN
		DIRECTION.DOWN:
			current_direction=DIRECTION.UP
	velocity_drone=SPEED*panic_speed_factor*_current_vector()
	
func _change_direction(reverse_move):
	if reverse_move:
		#force a direction
		#if we have changed direction next frame we will still be offscreen
		#so will constantly reverse direction
		#using timer to compensate
		_reverse_direction()
		_set_animation()
	else:
		_get_new_direction(true)

func _get_new_direction(animate_now):
	last_location=position

	$AnimatedSprite.flip_h=false
	#stay the same most of the time
	if randf()>0.3:
		return
		
	var last_direction=current_direction
	while last_direction==current_direction:
		var r = randi()%4
		match r:
			0:
				current_direction = DIRECTION.UP
			1:
				current_direction = DIRECTION.DOWN
			2:
				current_direction = DIRECTION.LEFT
			3:
				current_direction = DIRECTION.RIGHT
				
	if current_direction==DIRECTION.LEFT:
		$AnimatedSprite.flip_h=true
	
	if animate_now:
		_set_animation()

func _set_animation():
	if current_direction==DIRECTION.UP:
		animate.play("up")
		$Particles2D.emitting=true
	else:
		$Particles2D.emitting=false
		animate.play("left_right_down")
		
func _on_AngryAnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name=="normal":
		_get_new_direction(true)
		angry=false
		#pause after firing to give player a chance
		$CooldownTimer.start()

func fire_weapon():
	launch()

func make_ready(direction:Vector3):
	#ship type previously set by room manager
	.make_ready(direction)
	var dir=int(direction.z)
	match dir:
		GlobalTileMap.DIRECTION.RIGHT:
			current_direction=DIRECTION.LEFT
		GlobalTileMap.DIRECTION.LEFT:
			current_direction=DIRECTION.RIGHT
		GlobalTileMap.DIRECTION.TOP:
			current_direction=DIRECTION.DOWN
		GlobalTileMap.DIRECTION.BOTTOM:
			current_direction=DIRECTION.UP
		_:
			print("ERROR. got "+str(dir))
	last_location=position
	velocity_drone=SPEED*panic_speed_factor*_current_vector()


func _on_OnScreen_viewport_entered(_viewport: Viewport) -> void:
	.has_entered_room()


func _on_OnScreen_viewport_exited(_viewport: Viewport) -> void:
	if has_entered_screen:
		if screen_based:	#&& reverse_timer.is_stopped():
			#in screen as part of design, bring it back in
			_change_direction(true)
			return
		else:
			leave_room()
		#elif reverse_timer.is_stopped():
		#spawning enemy and no timer override
		#	.leave_room()
		#	return

func _on_ShipHurtbox_take_hit(damage_left) -> void:
	panic=true
	panic_speed_factor=3
	if damage_left<=0:
		bubble_death()
		OptionsAndAchievements.increment_achievement("sure",1,"Drone")
		
func bubble_death(create_bubble:=true, via_smart_bomb:=false,include_score:=true,_pause:=0.0):
	if is_dead:
		return
		
	var tex=$AnimatedSprite.frames.get_frame("attack",0).get_data()
	GlobalMethods.enemy_explosion(global_position,false,"Drone",0.15,via_smart_bomb,tex,Globals.SFX_ENEMY_EXPLODE1)

	if gives_energy_on_death && create_bubble  && bubble_death_drop_tip=="":
		GlobalMethods.create_energy_bubble(global_position,Globals.PICKUP.LARGE_ENERGY,Globals.BUBBLE_DELAY)
	death(include_score)

func _on_SoftCollision_area_entered(_area: Area2D) -> void:
	_change_direction(false)


func _on_ShipHurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")

func _on_ShipHurtbox_hurt_ended() -> void:
	##$FlashAnimationPlayer.play("default")
	#once they get hit we just let them flash forever as panic mode never goes
	pass


func _on_CooldownTimer_timeout() -> void:
	can_fire=true
