extends StraightProjectile

#debris being used as a simple projectile, only change is delay start

export(float) var delay_start_min:=0.0
export(float) var delay_start_max:=0.0
export(bool) var auto_start:=false

var ignore_passthrough:=false

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	#test
	#position+=Vector2(150,150)
	propagate_call("set_process",[false])
	propagate_call("set_physics_process",[false])
	$Hitbox.set_deferred("monitorable",false)
	#enable(true)
	pass

func enable(_is_visible):
	if auto_start:
		start(ignore_passthrough)
		
func disable(_display):
	#required override
	#otherwise is deleted on game start
	pass
	
#overrides
func start(ignore_one_way:=false):
	if delay_start_max!=0.0:
		ignore_passthrough=ignore_one_way
		$TimerDelayStart.wait_time=rand_range(delay_start_min,delay_start_max)
		$TimerDelayStart.start()
	else:
		.start(ignore_one_way)
		propagate_call("set_process",[true])
		propagate_call("set_physics_process",[true])
		$Hitbox.set_deferred("monitorable",true)

func _on_TimerDelayStart_timeout() -> void:
	.start(ignore_passthrough)
	propagate_call("set_process",[true])
	propagate_call("set_physics_process",[true])
	$Hitbox.set_deferred("monitorable",true)
	AudioManager.play_sfx(Globals.SFX_GAME_IMPACT)

func set_sprite_type():
	if GlobalPersist.graphics_type==0 || GlobalPersist.graphics_type==1:
		var material=$Sprite.material
		material.set_shader_param("all_white",GlobalPersist.graphics_type==0)
