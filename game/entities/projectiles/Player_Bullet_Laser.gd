extends Node2D

const SPEED:=370
const PIXEL:=4
const RENEW_TIME=0.2

onready var startpos=0
onready var collision=$CollisionShape2D
onready var notifier=$VisibilityNotifier2D
onready var hitbox=$Hitbox/CollisionShape2D
onready var hurtbox=$Hurtbox/CollisionShape2D

const LASER_WIDTH:=54
var laser_max_distance=524
var death_point=0

enum LASER_STATE {STARTING,NORMAL,FINAL,ENDING}
var direction=1	#set by laser weapon
var current_state
var last_state=null
var length
var refresh=true
var rand_dec=0.0
var data=[]
var randy
var max_size=PIXEL
var ready_to_draw=false
var colours_choice
var colour_range
var colour
var current_refresh=true
var draw_from_zero=true
var power=100				#percentage. each level adds 20%. Each hit takes 20%
var is_dead=false

func _ready() -> void:
	randomize()
	set_process(false)
	set_physics_process(false)
	var y=GlobalMethods.get_as_colour(Globals.COLOURS.yellow)
	var yb=GlobalMethods.get_as_colour(Globals.COLOURS.yellow_bright)
	var c=GlobalMethods.get_as_colour(Globals.COLOURS.cyan)
	var cb=GlobalMethods.get_as_colour(Globals.COLOURS.cyan_bright)
	var w=GlobalMethods.get_as_colour(Globals.COLOURS.white)
	var wb=GlobalMethods.get_as_colour(Globals.COLOURS.white_bright)
	if GlobalPersist.graphics_type!=0:
		colours_choice=[
			[yb],
			[cb],
			[y,yb],
			[c,cb],
			[w,wb],
			[w],
			[wb],
			[w],
			[wb]
		]
	else:
		colours_choice=[
			[w],
			[wb],
			[w,wb]
		]

func start(_ignore_one_way):
	_initialise()
	
func _initialise():
	startpos=position.x
	current_state=LASER_STATE.STARTING
	colour_range=colours_choice[randi()%colours_choice.size()]
	colour=0
	randy=0.5
	length=0.0
	$Timer.wait_time=RENEW_TIME
	$Timer.start()
	_create_data(true)
	_set_box_sizes()
	set_process(true)
	set_physics_process(true)
	ready_to_draw=true

func _create_data(new:=false):
	for i in range(LASER_WIDTH):
		if new:
			data.append(0)

		if randf()<randy:
			data[i]=1
		else:
			data[i]=0
		#we don't want more than 4 of anything, or reducing if near/at end
		if i>max_size && i%max_size==0:
			var x=0
			for index in range(max_size):
				x+=data[i-index]
			if x>max_size:
				data[i]=0
			elif x==0 && ![LASER_STATE.ENDING,LASER_STATE.FINAL].has(current_state):
				data[i]=1
			
func _draw():
	#STARTING means laser is not fully out of player, i.e. it is still expanding
	#NORMAL is normal drawing
	#ENDING is hit obstacle or reached life end so reverse of STARTING
	#also at ending, laser starts creating a more normal pattern
	#i.e. max becomes 4 then 3, 2, 1
	if !ready_to_draw:
		return
		
	var pixels_drawn=0
	if refresh:
		_create_data()
		refresh=false
	
	while pixels_drawn<(length):
		if draw_from_zero:
			#normal or end
			if data[pixels_drawn]==1:
				draw_rect(Rect2(pixels_drawn*PIXEL*direction,0.0,PIXEL,PIXEL),colour_range[colour])
		else:
			#starting
			var offset=LASER_WIDTH-length
			if data[offset+pixels_drawn]==1:
				draw_rect(Rect2((pixels_drawn)*PIXEL*direction,0.0,PIXEL,PIXEL),colour_range[colour])
			
		pixels_drawn+=1
	pass

#using _process seems to slow down collision detection so need physics
func _physics_process(delta: float) -> void:
	set_state(delta)
	update()			#calls the draw method
	#$Label.text=str(int(position.x))+","+str(int(collision.position.x))+","+str(int(length))
	#$Label.text=str(int(position.x))+","+str(int(length))+":::"+str(power)
	#get_parent().get_node("Sprite").position.x+=SPEED*delta
		
func _transition(_old,new):
	#one off code
	last_state=current_state
	current_refresh=true
	match new:
		LASER_STATE.STARTING:
			current_refresh=true
			draw_from_zero=false
			max_size=PIXEL
			rand_dec=0.0
		LASER_STATE.NORMAL:
			draw_from_zero=true
			max_size=PIXEL
			length=LASER_WIDTH
			collision.position.x=(length*2)*direction
			collision.shape.extents.x=length*2
		LASER_STATE.FINAL:
			draw_from_zero=true
			max_size=1
			rand_dec=0#0.1
			_renew()
			collision.position.x=length*2*direction
			collision.shape.extents.x=length*2
		LASER_STATE.ENDING:
			draw_from_zero=true if direction==1 else false
			#rand_dec=0.05
			max_size=2
	refresh=current_refresh
	_set_box_sizes()
	death_point=global_position.x+(length*PIXEL*direction)-(16*direction)	#allow for speed
	
func _set_box_sizes():
	var v=Vector2(collision.shape.extents.x,collision.shape.extents.y)
	var leftpos=collision.position.x-(v.x/2)	#v is is top left pos but collision is centred
	var r=Rect2(Vector2(leftpos,notifier.rect.position.y),v*2)
	notifier.rect=r
	hitbox.position.x=collision.position.x
	hurtbox.position.x=collision.position.x
	hitbox.shape.extents.x=collision.shape.extents.x
	hurtbox.shape.extents.x=collision.shape.extents.x
	
func set_state(delta):
	if last_state!=current_state:
		_transition(last_state,current_state)
		
	if length<=0 && current_state!=LASER_STATE.STARTING:
		#length can be adjusted outside of here
		#such as when hit something we lose 20%
		current_state=LASER_STATE.ENDING
	
	match current_state:
		LASER_STATE.STARTING:
			#we need to keep the pattern the same
			#otherwise it looks like it's not moving as it emerges
			collision.position.x=(length*2)*direction
			collision.shape.extents.x=length*2
			position.x+=((SPEED*delta)/(float(PIXEL)+1.0))*direction
			length+=abs((SPEED*delta)/(float(PIXEL)))
			length=min(length,LASER_WIDTH)
			if length>=LASER_WIDTH:
				current_state=LASER_STATE.NORMAL
			_set_box_sizes()
		LASER_STATE.NORMAL:
			position.x+=SPEED*delta*direction
			if abs(position.x-startpos)>laser_max_distance-200:
				current_state=LASER_STATE.FINAL
		LASER_STATE.FINAL:
			length+=(SPEED*delta)
			length=min(length,LASER_WIDTH)
			position.x+=SPEED*delta*direction
			if abs(position.x-startpos)>laser_max_distance:
				current_state=LASER_STATE.ENDING
		LASER_STATE.ENDING:
			_explode()
			position.x+=SPEED*delta*direction
			length-=((SPEED*delta)/PIXEL)
			collision.shape.extents.x=length*2
			collision.position.x=length*2*direction
			_set_box_sizes()
			if length<=0:
				death(false)
				
func death(_with_explosion:=false):
	#called when hit environment, end of life or too many hits
	#explosion is the miss explosion, only called when hit environment
	#death is after end phase
	is_dead=true
	$Timer.stop()
	queue_free()

func _explode():
	#set explosion at collision object not deathpoint
	GlobalMethods.explosion_miss(Vector2(death_point,global_position.y),Globals.COLOURS.white_bright)
	
func _renew():
	refresh=true
	$Timer.stop()
	$Timer.wait_time=RENEW_TIME
	$Timer.start()
	
func _on_Timer_timeout() -> void:
	refresh=current_refresh
	#at the end decrease rand so we get more 0
	randy-=rand_dec
	colour=randi()%colour_range.size()

func _on_Hurtbox_take_hit(_damage_taken) -> void:
	_take_hit()
	
func _take_hit():
	power-=20
	if [LASER_STATE.FINAL,LASER_STATE.NORMAL].has(current_state):
		#shorten laser but only if in normal stage
		length=(LASER_WIDTH/100.0)*power
	if power<=0:
		current_state=LASER_STATE.ENDING
		#death()

func _on_VisibilityNotifier2D_viewport_exited(_viewport: Viewport) -> void:
	if !is_dead:
		if !GlobalTileMap.camera_zoom:
			queue_free()

func _on_PlayerBulletLaser_body_entered(body: Node) -> void:
	#we want to pass through humans so 
	#collision is on environment and enemy and here will block
	#if human then they have separate mask so will pass through
	#but hitbox will still give damage
	#destroyables can still be passed through to allow laser to
	#quickly destroy blocks
	if body.name!="RexTileMapDestroyable":
		current_state=LASER_STATE.ENDING
	else:
		current_state=LASER_STATE.ENDING
		#hitbox is always centre on our bullet
		#position is centre, so we need to offset it
		_set_box_sizes()
		var offset=collision.shape.extents.x*2
		var new_pos=global_position+Vector2(offset*direction,0)
		#definitely hit and tilemap collison maths is shit so trialling to left/right
		#as one will be valid and laser always kills block so quite safe
		var tile1=GlobalTileMap.get_tile_at_position(body,new_pos)
		var tile2=GlobalTileMap.get_tile_at_position(body,new_pos+Vector2(16,0))
		var tile3=GlobalTileMap.get_tile_at_position(body,new_pos+Vector2(-16,0))
		var cellp1=GlobalTileMap.get_cell_from_position(new_pos)
		var cellp2=GlobalTileMap.get_cell_from_position(new_pos+Vector2(16,0))
		var cellp3=GlobalTileMap.get_cell_from_position(new_pos+Vector2(-16,0))
		#laser kills block immediately
		var l1=[6,183,198,199,309,310,311,312,313]
		if l1.has(tile1):
			#tile 3 do explosion
			GlobalTileMap.replace_single_tile(body,cellp1.x,cellp1.y,-1)
		if l1.has(tile2):
			#tile 3 do explosion
			GlobalTileMap.replace_single_tile(body,cellp2.x,cellp2.y,-1)
		if l1.has(tile3):
			#tile 3 do explosion
			GlobalTileMap.replace_single_tile(body,cellp3.x,cellp3.y,-1)
		else:
			#set it to -1 just in case player is stuck
			GlobalTileMap.replace_single_tile(body,cellp1.x,cellp1.y,-1)
		_take_hit()
