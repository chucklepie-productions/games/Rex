extends Missile

#extracts taken from 
#http://kidscancode.org/godot_recipes/ai/homing_missile/

#homing
export var homing_speed:=250
export var homing_steer_force = 75.0
var homing_acceleration=Vector2.ZERO
var homing_target=null
var launch_check=true
var can_move=false
var can_steer=false
var target_is_player:=false
var final_wait_time:=2.0
onready var particle=$Sprite/Particles2D

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	particle.emitting=false
	particle.visible=true
	
func _check_death():
	if !is_instance_valid(homing_target):
		if !launch_check:
			#target gone
			silent_death()
		else:
			#first time but never started
			delayed_death=false
			silent_death()

func _move(delta):
	if !is_instance_valid(homing_target):
		homing_target=GlobalTileMap.get_random_screen_enemy()
		if is_instance_valid(homing_target):
			launch_check=false
		_check_death()
		
	if can_move:
		homing_acceleration+=homing_seek()
		velocity_missile += homing_acceleration * delta
		velocity_missile = velocity_missile.clamped(homing_speed)
		position += velocity_missile * delta * Globals.bullet_time
		
		if Globals.bullet_time!=1.0:
			$DelayedMovementTimer.paused=true
		else:
			if $DelayedMovementTimer.paused:
				$DelayedMovementTimer.paused=false
	if velocity_missile.x!=0.0:
		set_angle()
	else:
		set_angle(true)

func set_target(target):
	homing_target=target

func homing_seek():
	var steer = Vector2.ZERO
	var force=homing_steer_force
	if homing_target:
		#to target, but offset y by 45 to aim above centre mark
		var desired = ((homing_target.global_position) - global_position).normalized() * homing_speed
		steer = (desired - velocity_missile).normalized() * force
		#straight line
		if !can_steer:
			steer=force*Vector2(facing,0)
	return steer
	
func launch(direction:int):
	if target_is_player:
		homing_target=GlobalTileMap.current_player
		final_wait_time=1.5	#shorter range for miniboss
		_set_masks()
	else:
		homing_target=GlobalTileMap.get_random_screen_enemy()
		_set_masks()
	_check_death()

	facing=direction
	launch_check=true
	#positive will always be direction facing as launcher
	#has a scale of -1 on x
	
	velocity_missile=Vector2(0,0)*homing_speed
	_move(0.0)
	set_angle(true)

func _set_masks():
	if target_is_player:
		$Hitbox.set_collision_mask_bit(9,false)
		$Hitbox.set_collision_mask_bit(8,true)
		$Hurtbox.set_collision_layer_bit(5,false)
		$Hurtbox.set_collision_layer_bit(16,true)
	else:
		$Hitbox.set_collision_mask_bit(9,true)
		$Hitbox.set_collision_mask_bit(8,false)
		$Hurtbox.set_collision_layer_bit(5,true)
		$Hurtbox.set_collision_layer_bit(16,false)
		
func _on_DelayedMovementTimer_timeout() -> void:
	#pause before movement
	#then only on straight line for a short time
	#then die after 3 seconds
	if !can_move:
		can_move=true
		$DelayedMovementTimer.wait_time=0.1
		$DelayedMovementTimer.start()
		particle.emitting=true
	elif !can_steer:
		can_steer=true
		$DelayedMovementTimer.wait_time=final_wait_time
		$DelayedMovementTimer.start()
	else:
		silent_death()

func silent_death():
	#so any smoke trail (for player homing missile) can linger
	$Sprite.texture=null
	#set_physics_process(false)
	#set_process(false)
	particle.emitting=false
	.silent_death()
