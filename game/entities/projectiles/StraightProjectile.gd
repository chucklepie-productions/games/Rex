extends Area2D
class_name StraightProjectile
#parent projectile for straight lines. 
#Either inherit or instance and update editable children
#1. put in sprite texture and update externs as required
#2. collision mask is for environment, update if needs changing
#3. enable/disable environmental collision shape or call set_hit_environment
#4. draw hitbox and environmental collision shapes as required
#5. set hitbox mask to where it is looking at (don't set layer)
#6. add
#7. override with inherited script if updating death (override method and/or call .death() to get queue_free for free)
#8. inherit script if doing anything extra like new externs, methods, etc

#typical use (instance)
#add to scene and show inheritable children, draw collision boxes, update externs
#call start
#override death if adding enimation

export(float) var speed = 400 #setget set_speed
export(int, 0,360,1) var firing_angle = 0 setget set_angle
export(bool) var rotate_with_angle = false setget set_rotate_with
export(bool) var hits_environment = false setget set_hit_environment
export(bool) var bullets_destroy_destructible = true
export(float) var self_rotate_speed:float=0.0
export(bool) var has_gravity:=false
export(bool) var sound_on_death:=false

const LOCAL_GRAVITY:=9.8

var velocity_proj=Vector2.ZERO
var dead=false

func _ready() -> void:
	#set_physics_process(false)
	#set_process(false)
	pass
	
func _process(delta: float) -> void:
	if has_gravity:
		velocity_proj.y+=LOCAL_GRAVITY
	position+=(velocity_proj*delta*Globals.bullet_time)
	if self_rotate_speed!=0:
		rotation_degrees+=self_rotate_speed*delta
	
func start(ignore_one_way:=false):
	_update_trajectory()
	set_hit_environment(hits_environment,ignore_one_way)
	#set_process(true)

func _update_trajectory():
	velocity_proj=Vector2(speed,0.0).rotated(deg2rad(firing_angle))
	if rotate_with_angle:
		rotation_degrees=firing_angle

func _on_VisibilityNotifier2D_viewport_exited(_viewport: Viewport) -> void:
	if !dead:
		if !GlobalTileMap.camera_zoom:
			queue_free()

func disable(_display):
	queue_free()
	
func death(with_miss_explosion:=true, with_shard_explosion:=false):
	#print("projectile has left the room")
	dead=true
	if with_miss_explosion:
		GlobalMethods.explosion_miss(global_position,Globals.COLOURS.white_bright)
	if with_shard_explosion:
		GlobalMethods.explosion_shards(
			position,
			"Tile",
			null,
			0.0,
			1.0,	#time
			1)				#gravity
	if sound_on_death:
		AudioManager.play_sfx(Globals.SFX_GAME_IMPACT)
	queue_free()

func override_hitscore(value:int):
	$Hitbox.damage_given=value

#setters
func set_angle(angle_deg):
	firing_angle=angle_deg
	_update_trajectory()
	
func set_speed(new_speed):
	speed=new_speed
	_update_trajectory()
	
func set_rotate_with(rotate:bool):
	rotate_with_angle=rotate
	_update_trajectory()

func set_hit_environment(hit: bool, ignore_oneway:=false):
	hits_environment=hit
	#enable/disable environment hit. normally enabled
	#if drone it sets ignore oneway to be on so we can get past
	#any initial one way platform we are standing on but then
	#become enabled asnormal
	if hits_environment && ignore_oneway:
		set_collision_mask_bit(12,false)
	$EnvironmentalCollisionShape2D.call_deferred("set_disabled",!hit)
	if hits_environment && ignore_oneway:
		yield(get_tree().create_timer(0.1), "timeout")
		set_collision_mask_bit(12,true)

func _on_AbstractProjectile_body_entered(body: Node) -> void:
	#should only be environment
	#only called if enabled
	#override if necessary
	#our collision is the centre point, so offset it based on direction
	
	if body.name!="RexTileMapDestroyable":
		death()
	else:
		if !bullets_destroy_destructible:
			death()
		else:
			#hitbox is always centre on our bullet
			#position is centre, so we need to offset it
			var offsetshape:Shape2D=$Hitbox/CollisionShape2D.shape
			var offset
			if offsetshape.has_method("get_extents"):
				offset=offsetshape.extents.x/2
			elif offsetshape.has_method("get_radius"):
				offset=offsetshape.radius
			else:
				offset=8	#just some number
				
			if velocity_proj.x<0:
				offset=-(offset*2)	#left to right seems to be off a bit, so do not half the extent
			var new_pos=global_position+Vector2(offset,0)
			var tile=GlobalTileMap.get_tile_at_position(body,new_pos)
			var cellp=GlobalTileMap.get_cell_from_position(new_pos)
			var l1=[6,183,198]
			var l2=[199,309,310]
			var l3=[311,312,313]
			#our first destroyable tiles 6,183,198
			#our second destroyable tiles 199,309,310
			#our third destroyable tiles 311,312,313
			#then explosion
			if l1.has(tile):
				#set to level 2
				GlobalTileMap.replace_single_tile(body,cellp.x,cellp.y,l2[randi()%3])
				death()
			elif l2.has(tile):
				#set to level 3
				GlobalTileMap.replace_single_tile(body,cellp.x,cellp.y,l3[randi()%3])
				death()
			elif l3.has(tile):
				#tile 3 do explosion
				GlobalTileMap.replace_single_tile(body,cellp.x,cellp.y,-1)
				death(true,true)
				AudioManager.play_sfx(Globals.SFX_ENEMY_EXPLODE3)
			else:
				#set it to -1 just in case player is stuck
				GlobalTileMap.replace_single_tile(body,cellp.x,cellp.y,-1)
				#print("ERROR. tile found on destroyable not in list (but probably the crappy Godot tile collision returning the entire tilemap and not a tile for a collision...): " +str(tile))
				
				death()

func _on_Hurtbox_take_hit(_damage_taken) -> void:
	death()
