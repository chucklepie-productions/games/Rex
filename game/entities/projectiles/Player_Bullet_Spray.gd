extends StraightProjectile

const ROTATE_SPEED:float=360.0

#cannot access any signals

func _physics_process(delta: float) -> void:
	rotation_degrees+=ROTATE_SPEED*delta

