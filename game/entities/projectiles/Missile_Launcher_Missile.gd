extends Area2D
class_name Missile

#can be a simple launched weapon with no acceleration, i.e. fires an arc
#or a self-propelled with optional guidance

#arc based
export var simple_throw_velocity := Vector2(505,-385)
export var simple_mass:=3.0
export var delayed_death:=false

#all
var velocity_missile :=  Vector2.ZERO
var facing=0

func _ready() -> void:
	set_graphics_sprite_type()
		
func set_graphics_sprite_type():
	var show_material=$Sprite.material
	show_material.set_shader_param("all_white",GlobalPersist.graphics_type==0)
	
func _physics_process(delta: float) -> void:
	_move(delta)
	
func disable(_display):
	queue_free()
	
func _move(delta):
	velocity_missile.y+=Globals.GRAVITY*delta*simple_mass*Globals.bullet_time
	var calced=velocity_missile*delta*Globals.bullet_time
	position+=calced
	set_angle()

func launch(direction:int):
	facing=direction
	#positive will always be direction facing as launcher
	#has a scale of -1 on x
	velocity_missile = simple_throw_velocity
	velocity_missile.x*=facing
	set_angle()
	
	yield(get_tree(),"idle_frame")
	visible=true

func set_angle(ignore_facing:=false):
	var offset = 90 if facing==1 else 270
	var angle
	if ignore_facing:
		angle=1 * deg2rad(offset)
	else:
		angle=facing * deg2rad(offset)
		
	rotation=(velocity_missile.angle()+angle)
	
func _on_VisibilityNotifier2D_viewport_exited(_viewport: Viewport) -> void:
	#silent_death()
	if !GlobalTileMap.camera_zoom:
		queue_free()

func _on_Missile_body_entered(_body: Node) -> void:
	#should only be environment
	#only called if enabled
	#override if necessary
	silent_death()

func death():
	#print("missile has left the room")
	GlobalMethods.explosion_miss(global_position,Globals.COLOURS.yellow_bright)
	queue_free()

func _on_MissileHurtbox_take_hit(_damage_left) -> void:
	#should only be player, but regardless we simply remove
	silent_death()

func _on_DelayedDeathTimer_timeout() -> void:
	death()

func silent_death():
	#so any smoke trail (for player homing missile) can linger
	#$Sprite.texture=null
	#set_physics_process(false)
	#set_process(false)
	if delayed_death:
		GlobalMethods.explosion_miss(global_position,Globals.COLOURS.yellow_bright)
		$CollisionShape2D.set_deferred("disabled",true)
		$Hitbox.disable()
		$Hurtbox.disable()
		$DelayedDeathTimer.start()
	else:
		GlobalMethods.explosion_miss(global_position,Globals.COLOURS.yellow_bright)
		queue_free()
