extends StraightProjectile

var change_direction=true
var can_move=true
export(float) var start_freeze_time=0
export var name_prefix:=""

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	_set_direction()
	set_graphics_sprite_type()
	
func start(ignore_one_way:=false):
	if start_freeze_time>0.0:
		can_move=false
		$StartDelay.wait_time=start_freeze_time
		$StartDelay.start()
	else:
		can_move=true
		$Timer.start()
	.start(ignore_one_way)

func set_graphics_sprite_type():
	var material=$Sprite.material
	material.set_shader_param("all_white",GlobalPersist.graphics_type==0)

func _physics_process(_delta: float) -> void:
	if !can_move:
		velocity_proj=Vector2.ZERO
		return
		
	if change_direction:
		_set_direction()
	
func _set_direction():
	change_direction=false
	var move_right=true
	var move_up=true
	if !is_instance_valid(GlobalTileMap.current_player):
		print("Player not set! default motion")
	else:
		var pos=GlobalTileMap.current_player.position
		if pos.x<position.x:
			move_right=false
		if pos.y>position.y:
			#up/down we want diagonal only
			move_up=false
			
	var angle=0
	if move_right && move_up:
		angle=315
	elif move_right && !move_up:
		angle=45
	elif !move_right && !move_up:
		angle=135
	else:
		angle=225
	set_angle(angle)
	
	var type=""
	if GlobalPersist.graphics_type==0:
		type="_original"
	if move_right:
		$AnimationPlayer.play(name_prefix+"_spinning"+type)
	else:
		$AnimationPlayer.play_backwards(name_prefix+"_spinning"+type)


func _on_Timer_timeout() -> void:
	change_direction=true
	$Timer.start()

func _on_Mine_take_hit(health) -> void:
	#should only be player, but regardless we simply remove
	if health<=0:
		death(true)


func _on_Hurtbox_hurt_started() -> void:
	$FlashAnimationPlayer.play("flash")

func _on_Hurtbox_hurt_ended() -> void:
	$FlashAnimationPlayer.play("default")


func _on_StartDelay_timeout() -> void:
	$Timer.start()
	can_move=true
