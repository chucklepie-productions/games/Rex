extends MapMaster

#map navigation dictionary
#each item is a room number, each item in the exits array is whether can move up,down,left,right with -1 meaning not allowed
#transitions is an array containing 4 arrays for each up,down,left,right direction
#each item contains 2 elements for transition type and duration
#refer to ROOM_TRANSITIONS (but defaults are 0/1/2 none/slide/fade)
#duration not used when 0. The default is what is set in room manager default
#exit_positions structure is same as transitions but contains x/y co-ordinate
#to place player when new room is reached in that direction.
#location is local to the room and default is no change. Typically use for
#warping rooms. this just sends a signal, your game should pick this up
#
#this dictionary in real-world will contain more data, e.g. room enemies, etc
#even though there are room numbers in the array we are simply using -1 for no direction 0+ for a direction
#i.e. we treat -1 as false and any 0+ number as true for navigation allowed
#scrolling camera is achieved via the scrolling camera not here and will
#override anything to do with flip screens like data below
#each room is +-13

const REX_SLIDE_TIME:float=0.75
func _ready() -> void:
	#parent MapMaster performs auto configuration
	scrolling=find_node("ScrollingRegions")

#map master provides the base functionality
#and should work for grid based 2D maps
#auto_configure:	call this from your game to auto configure everything
#					if you set viewport manually in export then no need
#manual_configure:	call this if not calling auto_configure

#room data, we may put this in a tres or external json file...
#"enemy_delay"	: 5,		in seconds before starting wave
#"enemy_interval": 5,		in seconds between each (+-3 so choose mid point)
#"men_max"		: 0,		max number of these
#"ship_max"		: 0,
#"ufo_max"		: 3,
#"drone_max"	: 0,
#"exits"		: [-1,-1,-1,1]	up/down/left/right. 0 cannot exit, 1 can exit
#"transitions": 	 [ [],[],[],[1,1.0] ],	0,1,2 = none,slide,fade
#room_limit		: optional, sets oxygen limit, defaults to 60. -1 is remove limit
#locations of enemies are determined by second tilemap
#your map below needs to implement the following methods:
#get_room_data
#get_room_exits
#get_room_warp_location
#get_room_topleft_position
#get_roomid_from_position
#is_valid_room

func get_room_data(id):
	if _ROOM_DATA.has(str(id)):
		return _ROOM_DATA[str(id)]
	else:
		print("ERROR. Wanted to get room " + str(id) + " but does not exist, returning empty room")
		return _ROOM_DATA["-1"]
		
func get_room_exits(room_id:int) ->Array:
	if !is_valid_room(room_id):
		#room does not exist
		return [-1,-1,-1,-1]
		
	#ensure valid rooms
	for r in _ROOM_DATA[str(room_id)]["exits"]:
		if !is_valid_room(r):
			print("ERROR. Room %s has invalid exit room %d. Setting all exits to false" % [room_id,r])
			return [-1,-1,-1,-1]	#don't rely on -1 being there!
	
	return _ROOM_DATA[str(room_id)]["exits"]
	
func get_room_edges(room_id:int) ->Array:
	if !is_valid_room(room_id):
		#room does not exist
		return [-1,-1,-1,-1]
		
	#ensure valid rooms
	if _ROOM_DATA[str(room_id)].has("player_edges"):
		return _ROOM_DATA[str(room_id)]["player_edges"]
	else:
		return [true,true,true,true]	#presume want player edge signal
	
	
func get_room_warp_location(room_id:int):
		#do we need to reposition player
		if !is_valid_room(room_id):
			return null
			
		var x=_ROOM_DATA[str(room_id)]
		if x.has("exit_positions"):
			return x["exit_positions"]
			
		return null

func get_room_camera_offset(room_id:int) -> Vector2:
		if !is_valid_room(room_id):
			return Vector2.ZERO
			
		var x=_ROOM_DATA[str(room_id)]
		if x.has("camera_offset"):
			var p=x["camera_offset"]
			return Vector2(p[0],p[1])
			
		return Vector2.ZERO
	
func is_valid_room(room_id:int) ->bool:
	return _ROOM_DATA.has(str(room_id))
	
func get_orthoginal_room(room_id:int, direction:Vector2):
	match direction:
		Vector2.LEFT:
			return room_id-1
		Vector2.RIGHT:
			return room_id+1
		Vector2.UP:
			return room_id-map_width_screens
		Vector2.DOWN:
			return room_id+map_width_screens
		_:
			return -1

func get_room_transition(room_id:int) -> Array:
	#is there a transition
	if !is_valid_room(room_id):
		return [-1,-1,-1,-1]
		
	var x=_ROOM_DATA[str(room_id)]
	if x.has("transitions"):
		return x["transitions"]
		
	return [-1,-1,-1,-1]
			
func get_roomid_from_position(location:Vector2) -> int:
	#from a global pixel location get the room id
	if !valid_config():
		return -1
	var screen_x = int(location.x / room_size_pixels.x)
	var screen_y = int(location.y / room_size_pixels.y)
	var room_id = (screen_y * map_width_screens) + screen_x
	return room_id

func get_room_topleft_position(room_id:int) -> Vector2:
	#for a given room return the top left position as a pixel co-ordinate
	#this is to get the top left position of the room to position the camera
	if !valid_config():
		return _INVALID_ROOM
		
	var x=(room_id%map_width_screens)*room_size_pixels.x
	var y=int(room_id/map_width_screens)*room_size_pixels.y
	return Vector2(x,y)

func get_room_bottomright_position(room_id:int) -> Vector2:
	#for a given room return the top left position as a pixel co-ordinate
	#this is to get the top left position of the room to position the camera
	if !valid_config():
		return _INVALID_ROOM
		
	return get_room_topleft_position(room_id)+room_size_pixels
	

func _on_ScrollingRegions_body_exited(body: Node) -> void:
	#inform game/room manager to turn to flip mode
	#game needs to be setup so that the areas and camera limits
	#ensure room exits/scroll position are in place
	#otherwise camera will jump. to avoid this
	#e.g. how Metroid does this, then add a tween
	emit_signal("scroll_end", body.global_position)


func _on_ScrollingRegions_body_shape_entered(_body_id: RID, _body: Node, _body_shape: int, local_shape: int) -> void:
	#inform game/room manager to turn to scrolling mode
	#as scrolling exit signal, presumes correct position is set
	#we pass in the topleft and bottom right of the screens covered
	#by the shape, not the shape, this way the camera will correct
	#itself, as above may need a tween if you do things wrong
	#e.g. room is exited when the camera has not scrolled fully
	
	#get area of shape being entered (i.e. the scroll area)
	if scrolling==null:
		return
		
	var poly=scrolling.get_child(local_shape)
	var shape_size=poly.shape.extents

	#get the position of this shape
	var tl=Vector2(poly.global_position.x-shape_size.x,poly.global_position.y-shape_size.y)
	var br=Vector2(poly.global_position.x+shape_size.x,poly.global_position.y+shape_size.y)
	
	#convert this position to room and get room coordinates
	var st=get_roomid_from_position(tl)
	var se=get_roomid_from_position(br)
	var atl=get_room_topleft_position(st)
	var abr=get_room_bottomright_position(se)
	
	#let game/room manger know we have started scrolling
	emit_signal("scroll_start",atl,abr)

###
#		"enemy_delay"	: 0,
#		"enemy_interval": 0,
#		"men_max"		: 0,
#		"ship_max"		: 0,
#		"ufo_max"		: 0,
#		"drone_max"		: 0,
#		"exits"			: [0,0,0,0]
#		each vertical is +-13
const _ROOM_DATA = {
#underground cavern entrance
	"-1": {
		#this is really for when debugging for the blank room spaces
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,-1],
	},
	"78": {
		#test room
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,-1],
	},
	#start room rex 1
	"52": {
		"enemy_delay"	: 1,
		"enemy_interval": 1,
		"men_max"		: 10,
		"ship_max"		: 1,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,53]
	},
#weapon pod double fire
	"53": {
		"enemy_delay"	: 10,
		"enemy_interval": 4,
		"men_max"		: 3,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,66,-1,54]
	},
#easy path going through walls
	"54": {
		"enemy_delay"	: 4,
		"enemy_interval": 7,
		"men_max"		: 2,
		"ship_max"		: 6,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,55]
	},
	"55": {
		"enemy_delay"	: 3,
		"enemy_interval": 4,
		"men_max"		: 1,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 6,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,68,-1,-1]
	},
	#harder path no walls following train and river
	"66": {
		"enemy_delay"	: 9,
		"enemy_interval": 5,
		"men_max"		: 3,
		"ship_max"		: 7,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,67]
	},
	"67": {
		"enemy_delay"	: 15,
		"enemy_interval": 4,
		"men_max"		: 3,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,68]
	},
	"68": {
		"enemy_delay"	: 15,
		"enemy_interval": 4,
		"men_max"		: 4,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,69]
	},
#split screens for two paths at cavern entrance
	"69": {
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,70]
	},
#weapon laser
#shortest upper path bypasses industrial areas and miss out on weapon upgrade
	"70": {
		"enemy_delay"	: 12,
		"enemy_interval": 9,
		"men_max"		: 2,
		"ship_max"		: 4,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,71]
	},
	"71": {
		"enemy_delay"	: 12,
		"enemy_interval": 10,
		"men_max"		: 0,
		"ship_max"		: 1,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [58,-1,-1,72]
	},
	#shortest path upwards
	"58": {
		"enemy_delay"	: 2,
		"enemy_interval": 2,
		"men_max"		: 4,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [45,-1,-1,-1]
	},
#next split point misses industry and weapon pod
	"72": {
		"enemy_delay"	: 5,
		"enemy_interval": 3,
		"men_max"		: 4,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"transitions": 	 [ [],[],[],[1,2.0] ],
		"exits"			: [59,-1,-1,73]
	},
#43 if choose upwards from 52
	"59": {
		"enemy_delay"	: 8,
		"enemy_interval": 5,
		"men_max"		: 0,
		"ship_max"		: 4,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [46,-1,-1,-1]
	},
#industry and end of the cavern floor, vertical from here
	"73": {
		"enemy_delay"	: 5,
		"enemy_interval": 8,
		"men_max"		: 7,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [60,-1,-1,-1]
	},
	"60": {
		"enemy_delay"	: 10,
		"enemy_interval": 7,
		"men_max"		: 3,
		"ship_max"		: 6,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [47,-1,-1,-1]
	},
#weapon upgrade drone and human farm
	"47": {
		"enemy_delay"	: 0,#1,
		"enemy_interval": 3,
		"men_max"		: 10,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"transitions": 	 [ [],[],[1,2.0],[] ],
		"exits"			: [-1,-1,46,-1]
	},
#first mine layers (i call them ufos)
	"46": {
		"enemy_delay"	: 1,
		"enemy_interval": 3,
		"men_max"		: 4,
		"ship_max"		: 0,
		"ufo_max"		: 5,
		"drone_max"		: 4,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"transitions": 	 [ [],[],[1,2.0],[] ],
		"exits"			: [-1,-1,45,-1]
	},
	"45": {
		"enemy_delay"	: 10,
		"enemy_interval": 5,
		"men_max"		: 2,
		"ship_max"		: 3,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,44,-1],
		"exit_positions":[ 	[],[],[896,454],[] ]
		#"transitions": 	 [ [],[],[2,4.0],[] ]
	},
#rex level 1 canvern mini boss
	"44": {
		"enemy_delay"	: 1,
		"enemy_interval": 1,
		"men_max"		: 4,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,43,-1],
		"room_limit"	: -1
	},
#weapon upgrade spray
#start of upper level with man-made structure
	"43": {
		"enemy_delay"	: 4,
		"enemy_interval": 4,
		"men_max"		: 3,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,42,-1]
	},
	"42": {
		"enemy_delay"	: 9,
		"enemy_interval": 6,
		"men_max"		: 0,
		"ship_max"		: 7,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,41,-1]
	},
#introduction to lifts
	"41": {
		"enemy_delay"	: 18,
		"enemy_interval": 8,
		"men_max"		: 0,
		"ship_max"		: 5,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"transitions": 	 [ [1,2.0],[],[],[] ],
		"exits"			: [28,-1,-1,-1]
	},
	"28": {
		"enemy_delay"	: 3,
		"enemy_interval": 4,
		"men_max"		: 7,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,29]
	},
	"29": {
		"enemy_delay"	: 2,
		"enemy_interval": 4,
		"men_max"		: 7,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,30]
	},
	"30": {
		"enemy_delay"	: 3,
		"enemy_interval": 3,
		"men_max"		: 6,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"transitions": 	 [ [1,2.0],[],[],[] ],
		"exits"			: [17,-1,-1,-1]
	},
#split to undocumented secret levels
	"17": {
		"enemy_delay"	: 1,
		"enemy_interval": 5,
		"men_max"		: 3,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 1,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,16,18]
	},
	#normal path to exit
	"18": {
		"enemy_delay"	: 0.5,
		"enemy_interval": 0.75,
		"men_max"		: 9,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [5,-1,-1,-1]
	},
	"5": {
		"enemy_delay"	: 0.5,
		"enemy_interval": 3,
		"men_max"		: 6,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,4,6],		#6 jsw
		"transitions": 	 [ [],[],[],[1,1.0] ],
	},
	"6": {				#jsw room
		"enemy_delay"	: 0.5,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,19,-1,-1]		#6 jsw
	},
	"19": {				#manic room
		"enemy_delay"	: 0.5,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,18,-1],		#6 manic
		"transitions": 	 [ [],[],[1,1.0],[] ],
	},
	"4": {
		"enemy_delay"	: 1,
		"enemy_interval": 1,
		"men_max"		: 1,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 6,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,3,-1]
	},
	"3": {
		"enemy_delay"	: 5,
		"enemy_interval": 2,
		"men_max"		: 8,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,2,-1]
	},
	#start of secret levels
	"16": {
		"enemy_delay"	: 0.5,
		"enemy_interval": 2,
		"men_max"		: 4,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,15,-1]
	},
	"15": {
		"enemy_delay"	: 0.5,
		"enemy_interval": 3,
		"men_max"		: 4,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,14,-1]
	},
	"14": {
		"enemy_delay"	: 9,
		"enemy_interval": 6,
		"men_max"		: 2,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [1,-1,-1,-1]
	},
	"1": {
		"enemy_delay"	: 0.5,
		"enemy_interval": 6,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 5,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,2]
	},
#final level to exit
	"2": {
		"enemy_delay"	: 2,
		"enemy_interval": 5,
		"men_max"		: 1,
		"ship_max"		: 0,
		"ufo_max"		: 1,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		#"exits"			: [231,-1,-1,-1],
		"exits"			: [270,-1,-1,-1],
		"exit_positions":[ 	[544,664],[],[],[] ],
		"transitions": 	 [ [2,3.0],[],[],[] ]
		#"transitions": 	 [ [2,3.0],[],[],[] ],
	},
#start of rex 2 lift
	"270":{
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [257,-1,-1,-1],
		"activate_rooms": [257,244],
		"transitions": 	 [ [2,3.0],[],[],[] ],
	},
	"257":{
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,270,-1,-1],
		"activate_rooms": [244]
	},
	"244":{
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,-1]
	},
#tutorial rooms. first training room, secret exit to left
	"111": {						#training depart docks
		"enemy_delay"	: 0.5,		#room handles exit
		"enemy_interval": 4,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,-1],
		"room_limit"	: -1
	},
	"91": {							#training start
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,104,92],
		"exit_positions":[ 	[],[],[900,416],[] ],
		"transitions": 	 [ [],[],[2,2.0],[] ],
		"room_limit"	: -1
	},
	"104": {						#monty mole
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,91],
		"exit_positions":[ 	[],[],[],[48,160] ],
		"transitions": 	 [ [],[],[],[2,2.0] ],
		"room_limit"	: -1
	},	
	"92": {							#training hidden and shields
		"enemy_delay"	: 8,
		"enemy_interval": 50,
		"men_max"		: 0,
		"ship_max"		: 1,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,93],
		"room_limit"	: -1
	},
	"93": {							#training smart bombs exit to rex2 secret
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,106,-1,94],
		"room_limit"	: -1
	},
	"94": {						#training rex 2 demo screen
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [81,-1,93,-1],
		"room_limit"	: -1
	},
	"81": {						#training rex2 squidgy not ready
		"enemy_delay"	: 10,
		"enemy_interval": 0.1,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 20,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,94,-1,-1],
		"room_limit"	: -1
	},
	"106": {					#training weapon ships
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,107],
		"room_limit"	: -1
	},
	"107": {					#training rapid fire exit to scroll
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,120,-1,-1],
		"exit_positions":[ 	[],[384,32],[],[] ],
		"transitions": 	 [ [],[2,2.0],[],[] ],
		"room_limit"	: -1
	},
	"120": {					#
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,133,-1,-1],
		"room_limit"	: -1
	},
	"133": {					#training second scroll to start game
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,146,-1,-1],
		"room_limit"	: -1
	},
	"146": {					#training infiltrate (first and second last room)
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,-1,91,147],
		"exit_positions":[ 	[],[],[91,147],[] ],
		"transitions": 	 [ [],[],[2,4.0],[] ],
		"room_limit"	: -1
	},
	"147": {	#final training then move to game, needs down even though
				#not used to avoid a death
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"exits"			: [-1,52,-1,-1],
		"player_edges"	: [false,false,false,false],
		"exit_positions":[ 	[],[200,30],[],[] ],
		"room_limit"	: -1
	},
#REX 2
	"231": {					#start (after lift)
		"enemy_delay"	: 7,
		"enemy_interval": 2,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 3,
		"exits"			: [-1,-1,-1,232],
		"transitions": 	 [ [],[],[],[1,REX_SLIDE_TIME] ]
	},
	"232": {					#floor 1
		"enemy_delay"	: 1,
		"enemy_interval": 3,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 3,
		"exits"			: [-1,-1,-1,233],
		"activate_rooms": [233],
		"transitions": 	 [ [],[],[],[1,REX_SLIDE_TIME] ]
	},
	"233": {					#floor 1
		"enemy_delay"	: 1,
		"enemy_interval": 5,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 1,
		"exits"			: [220,-1,-1,-1],
		"activate_rooms": [232],
		"transitions": 	 [ [1,REX_SLIDE_TIME],[],[],[] ]
	},
	"220": {					#floor 2
		"enemy_delay"	: 1,
		"enemy_interval": 2,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 2,
		"exits"			: [-1,-1,219,-1],
		"activate_rooms": [219],
		"transitions": 	 [ [],[],[1,REX_SLIDE_TIME],[] ]
	},
	"219": {					#floor 2
		"enemy_delay"	: 2,
		"enemy_interval": 0.1,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 1,
		"men2_max"		: 0,
		"exits"			: [-1,-1,218,-1],
		"activate_rooms": [218],
		"transitions": 	 [ [],[],[1,REX_SLIDE_TIME],[] ]
	},
	"218": {					#floor 2
		"enemy_delay"	: 6,
		"enemy_interval": 3,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 2,
		"men2_max"		: 0,
		"exits"			: [205,-1,-1,-1],
		"activate_rooms": [219],
		"transitions": 	 [ [2,REX_SLIDE_TIME],[],[],[] ]
	},
	"205": {					#floor 3 jumps
		"enemy_delay"	: 8,
		"enemy_interval": 1,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 2,
		"men2_max"		: 0,
		"exits"			: [-1,-1,-1,206],
		"activate_rooms": [206]
	},
	"206": {					#floor 3 jumps
		"enemy_delay"	: 7,
		"enemy_interval": 1,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 1,
		"men2_max"		: 0,
		"exits"			: [193,-1,-1,-1],
		"activate_rooms": [205]
	},
	"193": {					#floor 4 platforms R2_2 spawn
		"enemy_delay"	: 8,
		"enemy_interval": 2,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 1,
		"exits"			: [-1,-1,192,-1],
		"activate_rooms": [192],
		"transitions": 	 [ [],[],[1,REX_SLIDE_TIME],[] ]
	},
	"192": {					#floor 4 platforms
		"enemy_delay"	: 2,
		"enemy_interval": 2,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 2,	#3
		"exits"			: [179,-1,-1,-1],
		"activate_rooms": [193],
		"transitions": 	 [ [1,REX_SLIDE_TIME],[],[],[] ]
	},
	"179": {					#floor 5 quick explode
		"enemy_delay"	: 4,
		"enemy_interval": 1.5,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 2,
		"men2_max"		: 1,
		"exits"			: [-1,-1,-1,180],
		"activate_rooms": [180],
	},
	"180": {					#floor 5 quick explode
		"enemy_delay"	: 2,
		"enemy_interval": 1,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 1,
		"exits"			: [167,-1,-1,-1],
		"activate_rooms": [179],
		"transitions": 	 [ [1,REX_SLIDE_TIME],[],[],[] ]
	},
	"167": {					#floor 6 start of vertical shaft.
		"enemy_delay"	: 0.5,
		"enemy_interval": 1.5,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 3,
		"exits"			: [154,-1,-1,-1]
	},
	"154": {					#floor 7 R2_4 spawn. scroll
		"enemy_delay"	: 1,
		"enemy_interval": 1,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 5,
		"men2_max"		: 0,
		"exits"			: [141,-1,-1,-1],
		"activate_rooms": [141]
	},
	"141": {					#floor 8 scroll
		"enemy_delay"	: 0.1,
		"enemy_interval": 0.1,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 6,
		"activate_rooms": [154],
		"exits"			: [128,-1,-1,-1]
	},
	"128": {					#floor 9 double gravity scroll
		"enemy_delay"	: 2,
		"enemy_interval": 2,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 2,
		"men2_max"		: 0,
		"exits"			: [115,-1,-1,-1]
	},
	"115": {					#floor 10 scroll
		"enemy_delay"	: 4,
		"enemy_interval": 2,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 2,
		"men2_max"		: 1,
		"exits"			: [102,-1,-1,-1],
		"activate_rooms": [102]
	},
	"102": {					#floor 11 R2_5 spawn scroll
		"enemy_delay"	: 1,
		"enemy_interval": 1,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 4,
		"men2_max"		: 0,
		"activate_rooms": [115],
		"exits"			: [89,-1,-1,-1]
	},
	"89": {					#floor 12 fixed
		"enemy_delay"	: 8,
		"enemy_interval": 1,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 4,
		"men2_max"		: 0,
		"exits"			: [76,-1,-1,-1]
	},
	"76": {					#floor 13 start long grav jump on left
		"enemy_delay"	: 3,
		"enemy_interval": 0.5,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 2,
		"exits"			: [63,-1,-1,-1],
		"activate_rooms": [63]
	},
	"63": {					#floor 14  R2_6 spawn
		"enemy_delay"	: 3,
		"enemy_interval": 2,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 2,
		"men2_max"		: 1,
		"exits"			: [50,-1,-1,-1],
		"activate_rooms": [76]
	},
	"50": {					#floor 15 (end of long gravity) scroll
		"enemy_delay"	: 2,
		"enemy_interval": 1,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 2,
		"exits"			: [37,-1,-1,-1]
	},
	"37": {					#floor 16 scroll
		"enemy_delay"	: 2,
		"enemy_interval": 1,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 2,
		"exits"			: [24,-1,-1,-1],
		"activate_rooms": [24]
	},
	"24": {					#floor 17 R2_7 spawn
		"enemy_delay"	: 0.5,
		"enemy_interval": 2,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 2,
		"activate_rooms": [37],
		"exits"			: [11,-1,-1,-1]
	},
	"11": {					#final before boss
		"enemy_delay"	: 6,
		"enemy_interval": 3,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 1,
		"men2_max"		: 0,
		"exits"			: [12,-1,-1,-1],
		"exit_positions":[ [544,608],[],[],[] ]
	},
	"12": {					#final boss fake
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"room_limit"	: -1,
		"exits"			: [21,-1,-1,-1],
		"exit_positions":[ [608,512],[],[],[] ]
	},
	"21": {					#final boss
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"room_limit"	: -1,
		"exits"			: [-1,-1,10,-1],		
		"exit_positions":[ [],[],[140,104],[] ]
	},
	"10": {					#top of tower for scrolling section
							#to the left of fake boss but in game
							#spawns after final boss is almost dead
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"room_limit"	: -1,
		"exits"			: [-1,153,-1,-1],
	},
	"153": {				
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"room_limit"	: -1,
		"exits"			: [-1,166,-1,-1],
		"transitions": 	 [ [],[2,0.5],[],[] ],
	},
	"166": {				#bottom of tower, death of queen
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"room_limit"	: -1,
		"exits"			: [-1,-1,165,-1]
	},
	"165": {				#bottom of tower for scrolling section
							#leading left to second part of tower descent
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"room_limit"	: -1,
		"exits"			: [-1,155,-1,-1]
	},
	"155": {				
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"room_limit"	: -1,
		"exits"			: [-1,142,-1,-1],
		"transitions": 	 [ [],[1,3.0],[],[] ]
	},
	"142": {				
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"room_limit"	: -1,
		"exits"			: [-1,230,-1,-1]
	},
	"230": {					#very bottom with train waiting for Rex
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"room_limit"	: -1,
		"exits"			: [-1,-1,222,-1],
		"transitions": 	 [ [],[],[1,6.0],[] ],
		"exit_positions":[ [],[],[940,570],[] ]
	},
	"222": {					#new infiltrate screen 2
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"room_limit"	: -1,
		"exits"			: [-1,-1,221,-1],
	},
	"221": {					#infiltrate 1, end of game
		"enemy_delay"	: 0,
		"enemy_interval": 0,
		"men_max"		: 0,
		"ship_max"		: 0,
		"ufo_max"		: 0,
		"drone_max"		: 0,
		"triangle_max"	: 0,
		"men2_max"		: 0,
		"room_limit"	: -1,
		"exits"			: [-1,-1,111,-1],
		"transitions": 	 [ [],[],[2,3.0],[] ]
	}
}
