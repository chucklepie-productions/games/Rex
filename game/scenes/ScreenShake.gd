extends Node

#https://kidscancode.org/godot_recipes/2d/screen_shake/

onready var camera:Node2D=null
onready var noise=OpenSimplexNoise.new()

var noise_y=0
var decay = 0.7  # How quickly the shaking stops [0, 1].
var max_offset = Vector2(100, 75)  # Maximum hor/ver shake in pixels.
var max_roll = 0.1  # Maximum rotation in radians (use sparingly).
var trauma = 0.0  # Current shake strength.
var trauma_power = 2  # Trauma exponent. Use [2, 3].
var exit_disabled:=false

export(NodePath) var target=null		#object to follow, or null to not follow a node


func add_trauma(amount,trauma_camera:Node2D=null):
	#0 to 1
	#if camera already set just ignore
	trauma=min(trauma+amount,1.0)
	if trauma_camera!=null:
		self.camera=trauma_camera
	if $Timer.is_stopped():
		$Timer.start()
		$Timer.start()
		
func _ready() -> void:
	randomize()
	noise.seed=randi()
	noise.period=4
	noise.octaves=2
	
func _process(delta: float) -> void:
	if camera:
		if target:
			camera.global_position=get_node(target).global_position
		if trauma:
			if camera.has_method("room_edge_detection"):
				GlobalTileMap.set_room_edge_detection(false)
			exit_disabled=true
			trauma=max(trauma-decay*delta,0)
			_shake()
		elif exit_disabled:
			if camera.has_method("room_edge_detection"):
				GlobalTileMap.set_room_edge_detection(true)
			exit_disabled=false
			
	if trauma<=0:
		$Timer.stop()
		
func _shake():
	if camera:
		var amount=pow(trauma,trauma_power)
		noise_y += 1
		camera.rotation = max_roll * amount * noise.get_noise_2d(noise.seed, noise_y)
		camera.offset.x = max_offset.x * amount * noise.get_noise_2d(noise.seed*2, noise_y)
		camera.offset.y = max_offset.y * amount * noise.get_noise_2d(noise.seed*3, noise_y)


func _on_Timer_timeout() -> void:
	AudioManager.play_sfx(Globals.SFX_GAME_SHAKE)
	$Timer.wait_time=rand_range(0.5,1.0)
	$Timer.start()
