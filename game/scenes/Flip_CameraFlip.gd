extends Camera2D

signal room_change(direction)
var block_exits=false			#stop player leaving room. For debugging mainly unless it becomes useful
#offset for the navigation, i.e. how far off screen before
#signalling new room
export(int) var room_navigation_pixel_offset:=16

onready var blockers=[$RoomBlocker/CollisionTop, $RoomBlocker/CollisionBottom, $RoomBlocker/CollisionLeft, $RoomBlocker/CollisionRight]
onready var navi=[$RoomNavigation/NavigateUp, $RoomNavigation/NavigateDown, $RoomNavigation/NavigateLeft, $RoomNavigation/NavigateRight]
onready var player_edges=[$PlayerEdgeDetection/Top, $PlayerEdgeDetection/Bottom, $PlayerEdgeDetection/Left, $PlayerEdgeDetection/Right]

func _ready() -> void:
	print("Camera ready with pre-configured data")
	_show_config()
	lock_room(block_exits)
	_custom_ready()
	
func enable():
	current=true
	
func disable():
	current=false
	
func set_position(location:Vector2,zoom:float=1.0) -> void:
	#very simple method to set position
	#it does not check map
	#as that is responsibility of the map
	position=location
	if zoom!=1.0:
		self.zoom=Vector2(zoom,zoom)
	
func set_exits(block_up:bool,block_down:bool,block_left:bool,block_right:bool):
	#disabled true means does not check so allows player
	#disabled false means does check so disallows player
	#only works for flip
	#if enable_delay>0.0:
		#we need to disable bottom for a short time
		#so that player can jump up and not trigger death
	#	blockers[1].set_deferred("disabled",true)
	#	yield(get_tree().create_timer(enable_delay), "timeout")
		
	blockers[0].set_deferred("disabled",block_up)
	blockers[1].set_deferred("disabled",block_down)
	blockers[2].set_deferred("disabled",block_left)
	blockers[3].set_deferred("disabled",block_right)
	print("Setting exits as follows (u,d,l,r) %s,%s,%s,%s" % [block_up,block_down,block_left,block_right])

func set_player_edge_areas(enable_up:bool,enable_down:bool,enable_left:bool,enable_right:bool):
	#as set_exits but for player edge detection
	player_edges[0].set_deferred("disabled",!enable_up)
	player_edges[1].set_deferred("disabled",!enable_down)
	player_edges[2].set_deferred("disabled",!enable_left)
	player_edges[3].set_deferred("disabled",!enable_right)
	print("Setting edges as follows (u,d,l,r) %s,%s,%s,%s" % [enable_up,enable_down,enable_left,enable_right])
	
func lock_room(is_locked:=true):
	#force all exits to be enabled, thus blocking player
	#simplyer helper function for set_exits
	block_exits=is_locked
	set_exits(!block_exits,!block_exits,!block_exits,!block_exits)
	
func _on_RoomNavigation_body_shape_entered(_body_id: RID, _body: Node, _body_shape: int, local_shape: int) -> void:
#	natural room change:
#	_on_RoomNavigation_body_shape_entered
#	_on_Camera2DFlip_room_change
#	manage_direction
#	change_room
#	_transition_camera
#	_on_changed_new_room
#	changed_new_room
#	map_change_room
#	_level_setup
#	_room_ready
#	_start_room -> set_exits
#collision is with the previous screen because in same frame even though we are now in the new screen

	if local_shape<0 || local_shape>3:
		return
		
	var directions=[Vector2.UP,Vector2.DOWN,Vector2.LEFT,Vector2.RIGHT]
	var change_direction=directions[local_shape]
	emit_signal("room_change",change_direction)
	
	print("Camera changing direction room to direction %s as vector %s" % [local_shape,change_direction])

func _show_config():
	print("Room blocker positions (u,d,l,r): (%s),(%s),(%s),(%s)" % [ [blockers[0].position], 
	[blockers[1].position], 
	[blockers[2].position], 
	[blockers[3].position]
	])
	print("Room navigation positions (u,d,l,r): (%s),(%s),(%s),(%s)" % [ [navi[0].position], 
	[navi[1].position], 
	[navi[2].position], 
	[navi[3].position]
	])
	print("Room blocker sizes (u,d,l,r): (%s),(%s),(%s),(%s)" % [ [blockers[0].shape.extents], 
	[blockers[1].shape.extents], 
	[blockers[2].shape.extents], 
	[blockers[3].shape.extents]
	])
	print("Room navigation sizes (u,d,l,r): (%s),(%s),(%s),(%s)" % [ [navi[0].shape.extents], 
	[navi[1].shape.extents], 
	[navi[2].shape.extents], 
	[navi[3].shape.extents]
	])

func set_navigation_system(viewport_size:Vector2,offscreen_gap:int):
	#either set the RoomBlocker and RoomNavigation manually
	#or call this. 
	#roomblocker (i.e. stopping you leaving) is normally the screen size
	#set with viewport_size
	#room navigation is roomblocker but offset by half player to look as though
	#leaving screen, set via offscreen_gap in pixels
	var newx=viewport_size.x/2
	var newy=viewport_size.y/2
	navi[0].position=Vector2(newx,-offscreen_gap)
	navi[1].position=Vector2(newx,viewport_size.y+offscreen_gap)
	navi[2].position=Vector2(-offscreen_gap,newy)
	navi[3].position=Vector2(viewport_size.x+offscreen_gap,newy)

	blockers[0].position=Vector2(newx,0)
	blockers[1].position=Vector2(newx,viewport_size.y)
	blockers[2].position=Vector2(0,newy)
	blockers[3].position=Vector2(viewport_size.x,newy)

	navi[0].shape.extents.x=newx
	navi[1].shape.extents.x=newx
	navi[2].shape.extents.y=newy
	navi[3].shape.extents.y=newy

	blockers[0].shape.extents.x=newx
	blockers[1].shape.extents.x=newx
	blockers[2].shape.extents.y=newy
	blockers[3].shape.extents.y=newy
	print("--- Navigation system configured manually to")
	_show_config()

# custom code for Rex
signal PlayerEdgeDetection
var current_zoom
var min_zoom
var max_zoom
var zoom_factor = 0.75

func _custom_ready():
	max_zoom = zoom.x
	min_zoom = max_zoom * zoom_factor
	
func _on_PlayerEdgeDetection_body_shape_exited(_body_id: RID, _body: Node, _body_shape: int, local_shape: int) -> void:
	emit_signal("PlayerEdgeDetection",false,local_shape)

func _on_PlayerEdgeDetection_body_shape_entered(_body_id: RID, _body: Node, _body_shape: int, local_shape: int) -> void:
	emit_signal("PlayerEdgeDetection",true,local_shape)

func room_edge_detection(is_on):
	#actually navigation not edge
	#this is required when switching via spawn points
	#or the camera debug
	#because this may override the setting and change room again
	$RoomNavigation.set_deferred("monitoring",is_on)
	

func zoom_in(new_offset, _zoom:=0.75, time:=0.5):
	_transition_camera(Vector2(min_zoom, min_zoom), new_offset,time)

func zoom_out(new_offset, _zoom:=1, time:=0.5):
	_transition_camera(Vector2(max_zoom, max_zoom), new_offset,time)

func _transition_camera(new_zoom, new_offset, time):
	if new_zoom != current_zoom:
		current_zoom = new_zoom
		var tw:SceneTreeTween=create_tween()
		tw.set_parallel()
		tw.tween_property(self,"zoom",current_zoom,time).from(get_zoom()).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)
		tw.tween_property(self,"offset",new_offset,time).from(get_offset()).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)
		
