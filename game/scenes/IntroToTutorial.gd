extends Node2D

func _on_AnimationPlayer_animation_finished(_anim_name: String) -> void:
	#animation auto plays so just go straight to menu
	SceneManager.sequence_shift()



func _on_AnimationPlayer_animation_started(_anim_name: String) -> void:
	#when animation starts, remove options dialog
	OptionsAndAchievements.set_controller_mode(false,true)
