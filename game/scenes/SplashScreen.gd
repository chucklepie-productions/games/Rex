extends Node2D

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("fire") || event.is_action_pressed("ui_accept") || event.is_action_pressed("ui_cancel"):
		if $Label.visible:
			$Label.visible=false
			$ColorRect.visible=true
			$AnimationPlayer.play("play")
		else:
			end_splash()
	
func _ready() -> void:
	if OS.get_name()=="HTML5":
		$Label.visible=true
		$ColorRect.visible=false
	else:
		$ColorRect.visible=true
		$Label.visible=false
		$AnimationPlayer.play("play")
	
func end_splash():
	var time=$AnimationPlayer.current_animation_length
	var now=$AnimationPlayer.current_animation_position
	var cutoff=time-now
	
	if now<1.5:
		#let them see the masterpiece for 1.5 seconds
		return
		
	#skip to 1.5 second before the end or let them quit if 1 second before end
	if cutoff>1.5:
		$AnimationPlayer.seek(time-1.5)
		$AudioStreamPlayer.stop()
	else:
		next_scene()

func next_scene():
	$AudioStreamPlayer.stop()
	$AnimationPlayer.stop()
	SceneManager.changeto_scene("res://scenes/Menu.tscn")
	
func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	next_scene()
