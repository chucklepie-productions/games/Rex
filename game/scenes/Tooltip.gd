extends Node2D

func _ready() -> void:
	visible=false

func open(text:String, pos:Vector2):
	visible=true
	$Label.text=text
	position=pos
	
func close():
	queue_free()
