extends Camera2D
signal scroll_room_update(camera_position)

#if non-zero then timer performs camera position check
#every x seconds and if camera changed by more than
#this amount then signal is raised
export(float) var signal_refresh_time:=0.0
export(int) var min_x_movement:=64
export(int) var min_y_movement:=64

onready var timer=$Timer
var last_position=Vector2.ZERO

func _ready() -> void:
	scroll_check_status(false)

func set_limits(top_left:Vector2, bottom_right:Vector2) ->void:
	limit_left=int(top_left.x)
	limit_right=int(bottom_right.x)
	limit_top=int(top_left.y)
	limit_bottom=int(bottom_right.y)
	
func enable():
	current=true
	scroll_check_status(true)
	
func disable():
	current=false
	scroll_check_status(false)
	
func scroll_check_status(status):
	if !status || signal_refresh_time<=0.0:
		print("Scroller stopping")
		timer.stop()
		return
	
	print("Scroller starting")
	timer.wait_time=signal_refresh_time
	timer.start()


func _on_Timer_timeout() -> void:
	
	var x_off = abs(position.x-last_position.x)
	var y_off = abs(position.y-last_position.y)
	
	if last_position==Vector2.ZERO || x_off>min_x_movement:
		last_position=position
		emit_signal("scroll_room_update",position)
		return
		
	if y_off>min_y_movement || last_position==Vector2.ZERO:
		last_position=position
		emit_signal("scroll_room_update",position)
		return
	
