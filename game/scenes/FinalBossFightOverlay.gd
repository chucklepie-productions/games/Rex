extends Node2D

#this scene is added to an empty node in the main game
#so ready will only be called when we need everything to start

onready var cluster=preload("res://entities/base_classes/ExplosionCluster.tscn")
onready var scorer=preload("res://entities/environment/ScoreRiser.tscn")

onready var human=preload("res://entities/screen_based/Human.tscn")
onready var bouncy=preload("res://entities/moving/BouncyTriangle.tscn")
onready var drone=preload("res://entities/screen_based/ScreenEnemy_Drone.tscn")
onready var ship=preload("res://entities/screen_based/ScreenEnemy_Ship.tscn")
onready var train=preload("res://entities/screen_based/Train.tscn")
onready var floaters=$Floater_Domain
onready var caterpillar=preload("res://entities/moving/Rex2_Caterpillar.tscn")
onready var spore=preload("res://entities/moving/Rex2_Spore.tscn")
onready var triangle=preload("res://entities/screen_based/ScreenEnemy_Rex2Triangle.tscn")
onready var paths=$Paths

var boss_loop:=0
const NICE_GAP=0.7
var is_active=false

onready var queen=$FinalBoss_FakeAndReal
onready var drops=[$DropPoints/Position2D01, $DropPoints/Position2D02, $DropPoints/Position2D03, $DropPoints/Position2D04, $DropPoints/Position2D05, $DropPoints/Position2D06, $DropPoints/Position2D07, $DropPoints/Position2D08, $DropPoints/Position2D09, $DropPoints/Position2D10, $DropPoints/Position2D11, $DropPoints/Position2D12, $DropPoints/Position2D13, $DropPoints/Position2D14]

enum STAGES {
	IDLE,					#set when waiting for another stage to trigger next stage
	STAGE_SETUP,
	STAGE_LOSE_WEAPONS,
	MEET_CAST,
	JETPAC,
	JETPAC_BUILD,
	MEET_FINAL_CAST,
	MINIBOSS,
	BOSS
}

var current_stage=STAGES.IDLE
var player
var terminate_patterns:=false
var boss_patrol_points=null
var boss_patrol_index = 0
var boss_velocity = Vector2.ZERO
var boss_pattern="Basic"
onready var boss_path_start_offset=$Paths.position
const BOSS_MOVE_SPEED:=200
var boss_speed_factor:=1.0

#positions: odd left side, even right side
#position 1,3,5,7  top of screen middle to edge
#position 2,4,6,8  top of screen middle to edge
#position 9,11,13  left side     middle to edge
#position 10,12,14 right side    middle to edge
enum {
	PATTERN_A,	#1,2		top left/right
	PATTERN_B,	#3,4		top left/right
	PATTERN_C,	#5,6 		top left/right
	PATTERN_D,	#7,8		top left/right
	PATTERN_E,	#9,10		side
	PATTERN_F,	#11,12		side
	PATTERN_G,	#13,14		side
}

enum {LEFTRIGHT,LEFT,RIGHT}

enum { HUMAN, BOUNCY, DRONE, SHIP, UFO, TRAIN, FLOATER, MINIBOSS, 
				HUMAN2, CATERPILLAR2, SPORE2, TRIANGLE2}
var container
#for after death
var last_stage=STAGES.IDLE
var last_enemy=HUMAN
var death_latch:=false

func _ready() -> void:
	randomize()
	var b=$MinibossContainer.boss
	b.connect("miniboss_dead",self,"_miniboss_dead")
	GlobalMethods.connect("dialog_finished",self,"_on_dialog_finished")
	queen.enable_wobble(true)
	queen.disable(false)
	$SpritePretendPlayer_Before.visible=false
	$SpritePretendPlayer_Caught.visible=false
	disable(true)
	
func enable(_visible):
	container=GlobalMethods.assign_temp_location()
	player=GlobalTileMap.current_player
	player.weapons.allow_firing(false)
	player.enable_jetpac(true)	#temp
	is_active = true
	if player:
		if !player.is_connected("smart_bomb",self,"_on_smart_bomb"):
			player.connect("smart_bomb",self,"_on_smart_bomb")
	
	current_stage=STAGES.STAGE_SETUP
	#current_stage=STAGES.BOSS
	#current_stage=STAGES.MEET_FINAL_CAST
	#current_stage=STAGES.MINIBOSS
	current_stage=STAGES.JETPAC
	if last_stage!=STAGES.IDLE:
		_kill_screen()
		current_stage=last_stage
		death_latch=true
		_full_strength()
	else:
		#from death so we know there is no need to save
		_set_jetpac_platforms(false)
		GlobalPersist.has_continue=true
		if GlobalPersist.player_spawn_point!="SpawnBossFight":
			GlobalPersist.player_spawn_point="SpawnBossFight"
			GlobalPersist.save_all_world_data(1,"Microdrive write success")
	propagate_call("set_process",[true])
	propagate_call("set_physics_process",[true])
	
func disable(_visible):
	is_active = false
	if player:
		if player.is_connected("smart_bomb",self,"_on_smart_bomb"):
			player.disconnect("smart_bomb",self,"_on_smart_bomb")
	propagate_call("set_process",[false])
	propagate_call("set_physics_process",[false])
	
func _process(_delta: float) -> void:
	#use this as a staging area
	match current_stage:
		STAGES.STAGE_SETUP:
			_stage_setup()
		STAGES.STAGE_LOSE_WEAPONS:
			_stage_lose_weapons()
		STAGES.MEET_CAST:
			last_stage=STAGES.MEET_CAST
			queen.disable(true)
			_stage_meet_cast()
		STAGES.JETPAC:
			last_stage=STAGES.JETPAC
			_stage_jetpac()
		STAGES.JETPAC_BUILD:
			last_stage=STAGES.JETPAC_BUILD
			_stage_jetpac_build()
		STAGES.MEET_FINAL_CAST:
			last_stage=STAGES.MEET_FINAL_CAST
			_stage_final_cast()
		STAGES.MINIBOSS:
			last_stage=STAGES.MINIBOSS
			_stage_miniboss()
		STAGES.BOSS:
			last_stage=STAGES.BOSS
			_stage_boss()

func _physics_process(_delta: float) -> void:
	if !boss_patrol_points:
		return
	var target = boss_patrol_points[boss_patrol_index]#+boss_path_start_offset
	var dist=queen.position.distance_to(target)
	#print(boss_loop)
	if dist < 2:
		boss_patrol_index = wrapi(boss_patrol_index + 1, 0, boss_patrol_points.size())
		target = boss_patrol_points[boss_patrol_index]
		if boss_loop!=0 && boss_patrol_index==0:
			#decide on shooting, maybe a timer
			#also, e.g. certain directions basic on pattern
			#also release something on every infinite
			boss_loop+=1
			if boss_loop%3==0:
				boss_pattern="Infinite"
				_boss_follow_path(boss_pattern)
				boss_speed_factor=1.0
			elif boss_pattern!="Basic":
				boss_pattern="Basic"
				_boss_follow_path(boss_pattern)
				boss_speed_factor=0.7
			if boss_loop==3:
				_change_attack()
				
	boss_velocity = (target - queen.position).normalized() * BOSS_MOVE_SPEED
	boss_velocity = queen.move_and_slide(boss_velocity*boss_speed_factor)
	
func _change_attack():
	$TimerFire.wait_time=rand_range(0.3,1.0)
	$TimerFire.start()
	

func _on_TimerFire_timeout() -> void:
	#if up/down then can fire right, up, down
	#if infinite then can fire up, left, right
	$TimerFire.wait_time=rand_range(0.5,3.0)
	if boss_loop%10==0:	#every ten have a pause
		$TimerFire.wait_time=10
		
	var firing=[true,true,true,true]	#t,b,l,r
	if boss_pattern=="Basic":
		firing[2]=false
		var dist=player.global_position.distance_to(queen.global_position)
		if dist>550 && randi()%10>7:	#should be far enough to not hit unless
										#player moves closer
			queen.fire_missile()
			yield(get_tree().create_timer(0.1), "timeout")
			queen.fire_missile()
			yield(get_tree().create_timer(0.08), "timeout")
			queen.fire_missile()
			yield(get_tree().create_timer(0.15), "timeout")
			queen.fire_missile()
	else:
		firing[1]=false
		
	queen.fire(firing[0],firing[1],firing[2],firing[3])
	
	$TimerFire.start()
	
func _on_smart_bomb():
	floaters.bubble_death()
	
func _set_jetpac_platforms(enable,explosions:=false):
	var tile=462
	if !enable:
		tile=-1
	var locations=[
		[244,28,tile],[245,28,tile],[246,28,tile],[247,28,tile],[248,28,tile],
		[253,31,tile],[254,31,tile],[255,31,tile],[256,31,tile],
		[265,25,tile],[266,25,tile],[267,25,tile],[268,25,tile],
	]
	_set_tile_data(locations,explosions,GlobalTileMap.current_fg_map)
	
	if tile==-1:
		locations=[[262,38,-1], [263,38,-1] ]
		_set_tile_data(locations,explosions,GlobalTileMap.current_map)

func _set_tile_data(locations,explosions,map:TileMap):
	#loop each location and draw it separately
	var tsize=GlobalTileMap.map_tile_size
	for t in locations:
		if explosions:
			var loc=Vector2(t[0]*tsize,t[1]*tsize)
			_create_explosion(2,0.1,0.1,loc)
		yield(get_tree().create_timer(0.2), "timeout")
		GlobalTileMap.set_tiles_from_array(map,t[2],[[t[0],t[1]]])

func _stage_final_cast():
	current_stage=STAGES.IDLE
	_full_strength()
	_start(release_pattern_floaters)
	yield(get_tree().create_timer(30), "timeout")
	current_stage=STAGES.MINIBOSS
	
func _stage_jetpac():
	current_stage=STAGES.IDLE
	yield(get_tree().create_timer(5), "timeout")

	#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"jetpac",		Globals.MINIBOSS_TEXT["jetpac"],true)
	GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["jetpac"],false, true,true,"jetpac")

func _stage_jetpac_after_dialog():
	terminate_patterns=true
	#_kill_screen()

	var locations=[[262,37,157],[263,37,294]]
	_set_tile_data(locations,true,GlobalTileMap.current_map)
	
	if !$ThrustShip.visible:
		player.enable_lock(true)
		yield(get_tree().create_timer(3), "timeout")
		$ThrustShip.enable(true)
		$ThrustShip.drop_from_sky()
		yield(get_tree().create_timer(4), "timeout")
		player.enable_lock(false)
	

func _on_ThrustShip_new_weapon(_w_position, _w_type) -> void:
	player.weapons.acquire_weapon(Weapons_System.WEAPON.LASER)
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)
	player.enable_jetpac(true)
	
	#on collection of jetpac do next message and show platforms
	#player only has bullets now, so give him everything
	queen.disable(false)
	player.enable_lock(true)
	#calls after_Receive_special

func _after_receive_special():
	yield(get_tree().create_timer(2), "timeout")
	GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"jetpac_boss",
			Globals.MINIBOSS_TEXT["jetpac_boss"],true)
	
func _stage_jetpac_build():
	current_stage=STAGES.IDLE
	yield(get_tree().create_timer(1), "timeout")
	#a few explosions then make platforms appear
	#_kill_screen()
	GlobalMethods.screen_shake(0.3)
	GlobalMethods.screen_shake(0.3)
	yield(get_tree().create_timer(1), "timeout")
	GlobalMethods.screen_shake(0.3)
	_set_jetpac_platforms(true,true)
	player.enable_lock(false)
	yield(get_tree().create_timer(2), "timeout")
	current_stage=STAGES.MEET_FINAL_CAST
		
func _stage_miniboss():
	#disappear queen, shake screen, explode jetpac tiles
	AudioManager.play_sfx(Globals.SFX_ENEMY_MINIPULSE)
	current_stage=STAGES.IDLE
	last_enemy=MINIBOSS
	_kill_screen()

	GlobalMethods.screen_shake(0.3)
	yield(get_tree().create_timer(0.1), "timeout")
	GlobalMethods.screen_shake(0.3)
	yield(get_tree().create_timer(0.1), "timeout")
	GlobalMethods.screen_shake(0.3)
	yield(get_tree().create_timer(0.1), "timeout")

	_set_jetpac_platforms(false,true)
	_full_strength()
	
	#send off ship
	$ThrustShip.take_off()

	yield(get_tree().create_timer(2), "timeout")
	queen.disable(true)
	yield(get_tree().create_timer(8), "timeout")
	
	_do_message("My failed experiment")
	yield(get_tree().create_timer(1.5), "timeout")
	queen.disable(false)
	_start(release_pattern_miniboss)
	#miniboss death triggers boss
	
func _stage_lose_weapons():
	#wait a few seconds for player to wander
	#show message about losing weapons
	#fire explosion cluster
	#lose all weapons except single bullet with top level
	current_stage=STAGES.IDLE
	yield(get_tree().create_timer(3), "timeout")
	player.enable_lock(true)
	
	_create_explosion(4,0.1,0.1,player.global_position)
	player.weapons.acquire_weapon(Weapons_System.WEAPON.BULLET_DOUBLE,false)
	player.weapons.acquire_weapon(Weapons_System.WEAPON.LASER,false)
	player.weapons.acquire_weapon(Weapons_System.WEAPON.DRONE,false)
	player.weapons.acquire_weapon(Weapons_System.WEAPON.SPRAY,false)
	GlobalTileMap.current_player.weapons._weapon_downgrade()
	_full_strength()
	
	yield(get_tree().create_timer(2), "timeout")

	#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"lose_weapons",			Globals.MINIBOSS_TEXT["lose_weapons"],true)
	GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["lose_weapons"],false, true,true,"lose_weapons")
	player.weapons.allow_firing(false)

			
func _stage_boss():
	queen.enable(true)
	queen.enable_wobble(false)
	_full_strength()
	current_stage=STAGES.IDLE
	boss_loop=0
	boss_pattern="Basic"
	boss_speed_factor=0.7
	_boss_follow_path(boss_pattern)
	#give a few seconds to get into position
	yield(get_tree().create_timer(2),"timeout")
	AudioManager.play_music(Globals.MUSIC_BOSS_START,true, 3.0)
	boss_loop=1
	
func _stage_lose_weapons_after_dialog():
	player.enable_lock(false)
	current_stage=STAGES.MEET_CAST

func _boss_follow_path(path_name):
	var p=paths.get_node(path_name)
	boss_patrol_points=p.curve.get_baked_points()
	#queen.position=boss_patrol_points[150]
	
func _on_dialog_finished(title):
	#dialog closed so can continue
	if !is_active:
		return
		
	match title:
		"start_final":
			_stage_setup_after_dialog()
		"jetpac":
			_stage_jetpac_after_dialog()
		"jetpac_boss":
			current_stage=STAGES.JETPAC_BUILD
		"lose_weapons":
			_stage_lose_weapons_after_dialog()
		"boss_special":
			_after_receive_special()
		"start_boss":
			current_stage=STAGES.BOSS
		"die_bitch":
			_on_FinalBoss_FakeAndReal_queen_dead_after_message()
		_:
			print("dialog " + title + "not recognised as fight dialog")
			
func _create_explosion(count,dmin,dmax,pos):
	#create explosion
	var e=cluster.instance()
	e.cluster_count=count
	e.min_delay_between_explosions=dmin
	e.max_delay_between_explosions=dmax
	container.add_child(e)
	e.global_position=pos
	e.z_index=100	#ensure on top
	e.start_explosions(100)
		
func _full_strength():
	#TODO: make a noise here
	player._bubble_full_tank()
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)
	player.weapons.increase_current_weapon_energy(6)

func _stage_setup():
	player.weapons.allow_firing(false)
	current_stage=STAGES.IDLE
	yield(get_tree().create_timer(8),"timeout")
	player.weapons.allow_firing(false)
	GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["start_final"],false, true,true,"start_final")
			
func _stage_setup_after_dialog():
	current_stage=STAGES.STAGE_LOSE_WEAPONS

func _miniboss_dead():
	_kill_screen()
	#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"start_boss",Globals.MINIBOSS_TEXT["start_boss"],true)
	yield(get_tree().create_timer(2),"timeout")
	GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["start_boss"],false, true,true,"start_boss")
			
func _stage_meet_cast():
	if death_latch:
		#if death of a player during this loop it will still be running
		#so return out
		current_stage=STAGES.IDLE
		death_latch=false
		return
		
	current_stage=STAGES.IDLE
	queen.disable(true)
	yield(get_tree().create_timer(2), "timeout")
#order
#humans
#drones
#ship
#ufo
#bouncy
#train
#caterpillar
#spore2
#triangle
#human2
#miniboss
#
#jetpac then floaters: wait until all gone

	_do_message("Meet the humans")
	yield(get_tree().create_timer(3.0), "timeout")
	last_enemy=HUMAN
	_start(release_pattern_humans)
	yield(get_tree().create_timer(8), "timeout")
	player.weapons.allow_firing(true)
	
	#yield(get_tree().create_timer(35), "timeout")
	yield(get_tree().create_timer(35), "timeout")

	_do_message("The flying things")
	yield(get_tree().create_timer(2.0), "timeout")
	last_enemy=SHIP
	_start(release_pattern_ships,false)
	yield(get_tree().create_timer(20), "timeout")

	yield(get_tree().create_timer(2.0), "timeout")
	_do_message("The annoying scanners")
	last_enemy=DRONE
	_start(release_pattern_drones,true)
	yield(get_tree().create_timer(10), "timeout")

	_do_message("The weird stompers")
	last_enemy=BOUNCY
	_start(release_pattern_bouncy,true)
	yield(get_tree().create_timer(10), "timeout")

	_full_strength()
	yield(get_tree().create_timer(2.0), "timeout")
	_do_message("My lovely caterpillars")
	last_enemy=CATERPILLAR2
	_start(release_pattern_caterpillar,false)
	yield(get_tree().create_timer(10), "timeout")

	yield(get_tree().create_timer(2.0), "timeout")
	_do_message("Spores and weird things")
	last_enemy=SPORE2
	_start(release_pattern_spore2_triangle,false)
	yield(get_tree().create_timer(25), "timeout")

	_full_strength()
	_do_message("The ore train")
	last_enemy=TRAIN
	_start(release_pattern_train,true)
	yield(get_tree().create_timer(15), "timeout")
	
	_kill_screen()
	_full_strength()
	current_stage=STAGES.JETPAC

func _do_message(text):
	var score1=scorer.instance()
	container.add_child(score1)
	score1.global_position=queen.global_position+Vector2(100,75)
	score1.start(text,GlobalMethods.get_as_colour(Globals.COLOURS.yellow_bright),4,0.25)
	AudioManager.play_sfx(Globals.SFX_QUEEN_CHAT)

func _kill_screen():
	Globals.game_scene.fullscreen_shock(true,Vector2(0.5,0.5),true,3.0)
	GlobalTileMap.rex_room_manager.kill_room(true,0.1,true,false,false,true)
	floaters.bubble_death(false,false,false)
	yield(get_tree().create_timer(2.0), "timeout")

func _start(release_patterns, killscreen:=false):
	if killscreen:
		_kill_screen()
		
	for pattern in release_patterns:
		#if terminate_patterns:
			#terminate_patterns=false
			#continue
			
		if pattern[0]>0.0:
			#this is time to wait until we do NEXT enemy
			yield(get_tree().create_timer(pattern[0]), "timeout")

		_perform_pattern(pattern[1],pattern[2],pattern[3],pattern[4])


func _perform_pattern(type,pattern,meta,direction_include):
	var pos:Array=_get_positions(pattern)
	_make_enemies(type,pos,meta,direction_include)
	
func _get_positions(pattern) -> Array:
	match pattern:
		#a-d: top middle to outside
		PATTERN_A:
			return [drops[0],drops[1]]
		PATTERN_B:
			return [drops[2],drops[3]]
		PATTERN_C:
			return [drops[4],drops[5]]
		PATTERN_D:
			return [drops[6],drops[7]]
		#e-g sides middle to bottom
		PATTERN_E:
			return [drops[8],drops[9]]
		PATTERN_F:
			return [drops[10],drops[11]]
		PATTERN_G:
			return [drops[12],drops[13]]
		_:
			print("ERROR: pattern not in list")
			return []
			
func _make_enemies(type,pos:Array, meta:int,direction_include):
	for location in pos:
		#location is a position2D
		
		#don't want to ignore too close as that would
		#stop the patterns so do not do it
		
		var camera_top=GlobalTileMap.main_camera.get_camera_position()
		var dvector
		var ok=false
		var n=int((location.name.right(len(location.name)-2)))
		if n%2==0:
			dvector=Vector3(0,0,GlobalTileMap.DIRECTION.LEFT)
			if direction_include==RIGHT:
				ok=true
		else:
			dvector=Vector3(0,0,GlobalTileMap.DIRECTION.RIGHT)
			if direction_include==LEFT:
				ok=true

		if direction_include==LEFTRIGHT:
			ok=true
			
		if !ok:
			#here (normally) if not showing odd/even enemies
			continue
			
		match type:
			HUMAN,HUMAN2:
				var h=human.instance()
				
				if type==HUMAN2:
					h.is_rex2=true
				h.screen_based=false
				container.add_child(h)			#this is to go into game scene transient
				h.transient_contained=true
				h.global_position=location.global_position
				
				#firing
				if meta==0:
					h.fire_mode=h.FIRING_MODES.NO
				elif meta==1:
					h.fire_mode=h.FIRING_MODES.YES
				
				#locked mode or random
				if meta!=2:
					h.lock_start_state=h.HUMAN_STATES.idle
					h.lock_state=true
				else:
					h.lock_state=false
					
				#facing
				h.make_ready(dvector)
		
				h.enable(true)
				if h.lock_state:
					_contain_human_after_start(h)

			TRAIN:
				var h=train.instance()
				h.screen_based=false
				h.transient_contained=true
				container.add_child(h)			#this is to go into game scene transient
				h.global_position=location.global_position
				h.make_ready(dvector)
				h.set_no_firing()
				h.override_speed(100)
				h.enable(true)
			BOUNCY,CATERPILLAR2:
				var h
				
				if type==BOUNCY:
					h=bouncy.instance()
				else:
					h=caterpillar.instance()
					h.flip_animation=true
				container.add_child(h)			#this is to go into game scene transient
				h.transient_contained=true
				h.global_position=location.global_position
				h.original_position=h.global_position
				h.start_direction=-1 if dvector.z==RIGHT else 1
				
				h.visibility_disable_startup=4.0
				h.make_ready(dvector)

				h.enable(true)
				
			TRIANGLE2:
				var h=triangle.instance()
				container.add_child(h)
				h.enable(true)
				h.make_ready(Vector3.ZERO)
			DRONE:
				var h=drone.instance()
				container.add_child(h)			#this is to go into game scene transient
				h.transient_contained=true
				h.global_position=location.global_position
				#h.start_direction=-1 if dvector.z==RIGHT else 1
				#h.visibility_disable_startup=4.0
				h.make_ready(dvector)

				h.enable(true)
			SPORE2:
				#only use on top row
				var h=spore.instance()
				container.add_child(h)			#this is to go into game scene transient
				h.ignore_direction_change_startup=3.0
				h.transient_contained=true
				if meta!=0:
					h.speed=randi()%100+10
				h.global_position=location.global_position
				#h.start_direction=-1 if dvector.z==RIGHT else 1
				#h.visibility_disable_startup=4.0
				h.make_ready(dvector)

				h.enable(true)
				
			FLOATER:
				floaters.enable(true)
				if meta>0:
					yield(get_tree().create_timer(meta), "timeout")
					floaters.disable_kill()
				#as a pattern maybe make these appear at random
				#or after a time period remove
				#as simply call disable
				
			SHIP,UFO:
				var h=ship.instance()
				container.add_child(h)			#this is to go into game scene transient
				h.transient_contained=true
				h.global_position=location.global_position
				
				#h.start_direction=-1 if dvector.z==RIGHT else 1
				#h.visibility_disable_startup=4.0
				if h.global_position.y-camera_top.y<50:
					dvector=Vector3(0,0,GlobalTileMap.DIRECTION.TOP)
				else:
					if h.global_position.x-camera_top.x<100:
						dvector=Vector3(0,0,GlobalTileMap.DIRECTION.LEFT)
					else:
						dvector=Vector3(0,0,GlobalTileMap.DIRECTION.RIGHT)
				if type==SHIP:
					h.speed=h.SHIP_TYPE.HUMAN
					h.ship_type=h.SHIP_TYPE.HUMAN
				else:
					h.speed=h.SPEED_UFO
					h.ship_type=h.SHIP_TYPE.UFO

				h.make_ready(dvector)
				h.enable(true)
				
			MINIBOSS:
				#this will restrict everything to a basic
				#one off fight then die
				#also has restricted bullets
				#queen also disappears until boss is dead
				var tw:SceneTreeTween = create_tween()
				tw.tween_property($FinalBoss_FakeAndReal, "modulate", Color(1,1,1,0),1.5).from(Color(1,1,1,1))
				yield(tw,"finished")
				queen.disable(false)
				$FinalBoss_FakeAndReal.modulate=Color(1,1,1,1)
				
				$MinibossContainer.boss.final_mode=true
				$MinibossContainer.boss.activate()

func _contain_human_after_start(h):
	#locked movement so wait until hits ground
	#these are approximate positions of positions available
	var delay:=3.0
	if h.position.y>500:
		delay=1.5
	elif h.position.y>400:
		delay=2.0
	elif h.position.y>300:
		delay=2.5
		
	yield(get_tree().create_timer(delay), "timeout")
	if is_instance_valid(h):
		h.lock_state_after_unlock=h.HUMAN_STATES.walk
		h.lock_state=false
		h._change_state(h.lock_state_after_unlock)

#start delay	type	pattern			meta	which side
var release_pattern_humans=[
	[NICE_GAP*0,	HUMAN,	PATTERN_G,		0,LEFT],
	[NICE_GAP,		HUMAN,	PATTERN_G,		0,LEFT],
	[NICE_GAP,		HUMAN,	PATTERN_G,		0,LEFT],
	[NICE_GAP,		HUMAN,	PATTERN_G,		0,LEFT],
	[NICE_GAP*4,	HUMAN,	PATTERN_G,		0,LEFT],
	[NICE_GAP*0,	HUMAN,	PATTERN_G,		0,RIGHT],
	[NICE_GAP,		HUMAN,	PATTERN_G,		0,RIGHT],
	[NICE_GAP,		HUMAN,	PATTERN_G,		0,RIGHT],
	[NICE_GAP,		HUMAN,	PATTERN_G,		0,RIGHT],
	[NICE_GAP,		HUMAN,	PATTERN_G,		0,RIGHT],
	[NICE_GAP*8,	HUMAN,	PATTERN_G,		0,LEFTRIGHT],
	[0,				HUMAN2,	PATTERN_D,		0,LEFTRIGHT],
	[NICE_GAP*2.6,	HUMAN,	PATTERN_G,		0,LEFTRIGHT],
	[0,				HUMAN2,	PATTERN_D,		0,LEFTRIGHT],
	[NICE_GAP*2.6,	HUMAN,	PATTERN_G,		0,LEFTRIGHT],
	[0,				HUMAN2,	PATTERN_D,		0,LEFTRIGHT],
	[NICE_GAP*2.6,	HUMAN,	PATTERN_G,		0,LEFTRIGHT],
	[0,				HUMAN2,	PATTERN_D,		0,LEFTRIGHT],
	[NICE_GAP*2.6,	HUMAN,	PATTERN_G,		0,LEFTRIGHT],
	[0,				HUMAN2,	PATTERN_D,		0,LEFTRIGHT],
	[NICE_GAP*2.6,	HUMAN,	PATTERN_G,		0,LEFTRIGHT],
	[0,				HUMAN2,	PATTERN_D,		0,LEFTRIGHT],
	[NICE_GAP*2.6,	HUMAN,	PATTERN_G,		0,LEFTRIGHT],
	[NICE_GAP*8,	HUMAN2,	PATTERN_A,		0,LEFTRIGHT],
	[NICE_GAP*0,	HUMAN2,	PATTERN_B,		0,LEFTRIGHT],
	[NICE_GAP*0,	HUMAN2,	PATTERN_C,		0,LEFTRIGHT],
	[NICE_GAP*0,	HUMAN2,	PATTERN_D,		0,LEFTRIGHT],
	[NICE_GAP*4,	HUMAN2,	PATTERN_A,		0,LEFTRIGHT],
	[NICE_GAP*0,	HUMAN2,	PATTERN_B,		0,LEFTRIGHT],
	[NICE_GAP*0,	HUMAN2,	PATTERN_C,		0,LEFTRIGHT],
	[NICE_GAP*0,	HUMAN2,	PATTERN_D,		0,LEFTRIGHT]]

var release_pattern_drones=[
	#drone
	[0,			DRONE,	PATTERN_G,		0,LEFTRIGHT],
]

var release_pattern_ships=[
	#ship
	[0,		SHIP,	PATTERN_E,	0,LEFTRIGHT],
	[5,		UFO,	PATTERN_G,	0,LEFTRIGHT],
]
var release_pattern_bouncy=[
	#BOUNCY
	[0,			BOUNCY,	PATTERN_E,		0,LEFTRIGHT],
]

var release_pattern_train=[
	#train
	[0,			TRAIN,	PATTERN_G,		0,RIGHT],
]
var release_pattern_caterpillar=[
	#caterpillar
	[0,			CATERPILLAR2,	PATTERN_G,		0,LEFTRIGHT],
]

var release_pattern_spore2_triangle=[
	#only time used for triangles
	#spore. only use for a,b,c,d, meta is 1 for different speed
	[0,			SPORE2,		PATTERN_A,	1,LEFT],
	[0,			SPORE2,		PATTERN_A,	1,RIGHT],
	[0,			TRIANGLE2,	PATTERN_A,	1,LEFT],
	[0.3,		TRIANGLE2,	PATTERN_A,	1,LEFT],
	[0,			SPORE2,		PATTERN_D,	0,LEFT],
	[0,			SPORE2,		PATTERN_D,	0,RIGHT],
	[0.9,		TRIANGLE2,	PATTERN_A,	1,LEFT],
	[1.5,		TRIANGLE2,	PATTERN_A,	1,LEFT],
	[6,			TRIANGLE2,	PATTERN_A,	1,LEFT],
	[0,			TRIANGLE2,	PATTERN_A,	1,LEFT],
	[0,			TRIANGLE2,	PATTERN_A,	1,LEFT],
	[0,			TRIANGLE2,	PATTERN_A,	1,LEFT],
]

var release_pattern_miniboss=[
	#pattern ignored
	[0,			MINIBOSS,	PATTERN_A,		1,LEFT]
]

var release_pattern_floaters=[
	#floaters
	#position/pattern is irrelevant, meta is how long before destroying them all
	[0,			FLOATER,	PATTERN_A,	0,RIGHT],
	[5,			HUMAN,	PATTERN_E,		0,LEFT],
	[NICE_GAP*2,HUMAN,	PATTERN_E,		0,LEFT],
	[NICE_GAP*2,HUMAN,	PATTERN_E,		0,LEFT],
	[5,			HUMAN,	PATTERN_E,		0,LEFT],
	[NICE_GAP*2,HUMAN,	PATTERN_E,		0,LEFT],
	[NICE_GAP*2,HUMAN,	PATTERN_E,		0,LEFT],
	[5,			HUMAN,	PATTERN_E,		0,LEFT],
	[NICE_GAP*2,HUMAN,	PATTERN_E,		0,LEFT],
	[NICE_GAP*2,HUMAN,	PATTERN_E,		0,LEFT],
]

func _on_FinalBoss_FakeAndReal_queen_dead() -> void:
	#give queen dead when she is finally dead, not here
	#lock player
	#fade screen
	#at centre and queen in middle
	#explode queen, animate her off the screen
	#rex message
	#tentacle drags rex off screen
	#disable(false)
	AudioManager.stop_music(3.0)
	$TimerFire.stop()
	player.enable_lock(true)
	queen.enable_wobble(true)
	queen.disable(true)
	boss_patrol_points = null
	_kill_screen()

	Globals.game_scene.world_modulate_set(true)
	
	var tw:SceneTreeTween=create_tween()
	tw.tween_method(Globals.game_scene, "world_modulate_change_alpha",1.0,0.0,1.5).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)
	yield(tw,"finished")

	#it's dark, position characters then fade back in
	Globals.game_scene.hud_ui.show_hud(false)	#put back on after queen dies
	player.visible=false
	$SpritePretendPlayer_Caught.visible=false	#used by animation
	$SpritePretendPlayer_Caught.position=$SpritePretendPlayer_Before.position
	$SpritePretendPlayer_Before.visible=true	#only for show, when animation is backwards, this disappears
	queen.position=Vector2(400,250)
	#player.position=Vector2(480,672-(3*32)-64)
	yield(get_tree().create_timer(3), "timeout")
	
	tw=create_tween()
	tw.tween_method(Globals.game_scene, "world_modulate_change_alpha",0.0,1.0,1.5).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)

	Globals.game_scene.world_modulate_set(false)
	yield(tw,"finished")

	#kill queen and shoot her off the building
	queen.final_death_explode()
	yield(get_tree().create_timer(8), "timeout")
	Globals.game_scene.fullscreen_shock(true,Vector2(0.5,0.5),true,1.5)
	yield(get_tree().create_timer(0.5), "timeout")
	$AnimationPlayer.play("queen_fall")
	yield($AnimationPlayer,"animation_finished")
	yield(get_tree().create_timer(1.5), "timeout")
	#rex message
	#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"die_bitch",Globals.MINIBOSS_TEXT["die_bitch"],true)
	GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["die_bitch"],false, true,true,"die_bitch")
	is_active=true


func _on_FinalBoss_FakeAndReal_queen_dead_after_message() -> void:
	yield(get_tree().create_timer(1), "timeout")
	$SpritePretendPlayer_Before.visible=true
	$SpritePretendPlayer_Caught.visible=false
	$AnimationPlayer.play("tentacles_stage1")
	yield($AnimationPlayer,"animation_finished")
	$AnimationPlayer.play("tentacles_stage2")
	yield($AnimationPlayer,"animation_finished")

	$SpritePretendPlayer_Before.visible=false
	$SpritePretendPlayer_Caught.position=$SpritePretendPlayer_Before.position
	$SpritePretendPlayer_Caught.visible=true
	$AnimationPlayer.play_backwards("tentacles_stage2")
	yield($AnimationPlayer,"animation_finished")
	$AnimationPlayer.play("tentacles_stage3")
	yield($AnimationPlayer,"animation_finished")

	#map fall off screen
	$SpritePretendPlayer_Before.visible=false
	$SpritePretendPlayer_Caught.visible=false
	queen.disable(false)
	queen.enable_wobble(false)
	
	#change map to side of tower
	#Globals.game_scene.set_scroll_camera_x_offset(480)
	#disable(false)
	Globals.game_scene.direct_room_change(Vector2.LEFT)
