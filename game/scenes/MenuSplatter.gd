extends Node2D

export(float) var delay:=0.0
export(bool) var is_final:=false
export(bool) var do_sfx:=true

func _ready() -> void:
	reset()
	
func reset():
	$Sprite.visible=false
	$Particles2D.emitting=false
	$Particles2D.one_shot=true

func start():
	if delay>0.0:
		yield(get_tree().create_timer(delay), "timeout")
		
	$Sprite.visible=true
	$Particles2D.emitting=true
	if !do_sfx:
		return
		
	if is_final:
		AudioManager.play_sfx(Globals.SFX_MENU_BULLET2)
	else:
		AudioManager.play_sfx(Globals.SFX_MENU_BULLET)
