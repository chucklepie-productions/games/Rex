extends Area2D

#end of rex1
#ensure the parent node is higher up that the gravity jumps
#so the enable/disable can stop/start them properly

var _found_hint_remaining:=3

func _ready():
	pass
	
func enable(_make_visible:bool):
	set_deferred("monitoring",true)
	$CollisionShape2D.set_deferred("disabled",false)
	GlobalMethods.connect("hint_read",self,"_on_hint_found")
	
func disable(_make_visible:bool):
	set_deferred("monitoring",false)
	$CollisionShape2D.set_deferred("disabled",false)

func _on_hint_found(meta):
	if ["password1","password2","password3","password4"].has(meta):
		_found_hint_remaining-=1
	else:
		pass
		#TODO: some kind of sound effect?
	
func _on_Rex1Code_body_entered(_body: Node) -> void:
	if _found_hint_remaining>0:
		#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"rex1_nopasswords",Globals.MINIBOSS_TEXT["rex1_nopasswords"],true)
		GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["rex1_nopasswords"],false, true,true)
	else:
		#password sequence
		#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"rex1_passwords",Globals.MINIBOSS_TEXT["rex1_passwords"],true)
		GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["rex1_passwords"],false, true,true)
		yield(GlobalMethods,"dialog_finished")
		#dodgy code below
		var grav=get_tree().root.find_node("GravityUpRex1End",true,false)
		grav.is_functioning=true
		grav.enable(true)
		disable(false)
