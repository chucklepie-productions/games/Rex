extends Node

signal room_oxygen_warning
signal room_oxygen_depleted
signal new_room(time_limit)
signal room_killed

onready var spawn_timer=$SpawnTimer

#manages rooms, mainly enemies
const OXYGEN_TIME=50	#plus 10 seconds for warning
const OXYGEN_TIME_FINAL=10
var current_room_id=-1
var previous_room_id=-1
var control_map:TileMap
var original_invincible:=false
var current_enemies={}
var current_room_data={}
var enemy_home:Node=null		#for the transient enemies (i.e. spawned in to player/room)
var no_enemies=false
var enemies_used=[]
var human=preload("res://entities/screen_based/Human.tscn")
var ship=preload("res://entities/screen_based/ScreenEnemy_Ship.tscn")
var drone=preload("res://entities/screen_based/ScreenEnemy_Drone.tscn")
var triangle=preload("res://entities/screen_based/ScreenEnemy_Rex2Triangle.tscn")


var ok_left:=true
var ok_right:=true
var ok_up:=true
var ok_down:=true
var can_randomise_pickup:=false
var last_chance_nomoreenemies=false
var seen_welcome=false
var seen_miniboss_room=false

func changed_new_room(room_id:int,newpos:Rect2, room_data:Dictionary):
	#called after flip room change has occurred
	#DO NOT call this, to change a room call
	GlobalTileMap.map_change_room(room_id,newpos,room_data)
	#GlobalTileMap.current_player.forced_walk_stop()

func set_graphics_sprite_type(container):
	#set colouring for all items
	for c in container.get_children():
		#top level folders
		for s in c.get_children():
			if s.has_method("set_graphics_sprite_type"):
				s.set_graphics_sprite_type()
	
func get_current_room_exits():
	return current_room_data["exits"]
	
func player_at_edge_signal(entering, shape):
	#entering true for entering
	#entering false for leaving
	#shape 0,1,2,3 for top/bottom/left/right
	match shape:
		0:
			ok_up=!entering
		1:
			ok_down=!entering
		2:
			ok_left=!entering
		3:
			ok_right=!entering
		_:
			print("ERROR. invalid player edge shape " + str(shape))
	#redo the enemies
	current_enemies=GlobalTileMap.get_room_enemies(current_room_id, ok_up, ok_down, ok_left, ok_right)

func _ready() -> void:
	#control_map=GlobalTileMap.current_control_map
	var err=GlobalTileMap.connect("room_ready",self,"_room_ready")
	if err:
		print("Error in ready in room manager")
	GlobalTileMap.set_room_manager(self)

func initialise(enemy_container:Node):
	control_map=GlobalTileMap.current_control_map
	enemy_home=enemy_container

func dialog_started():
	$NoMoreEnemies.paused=true
	
func dialog_ended():
	$NoMoreEnemies.paused=false
	
func _start_room(partial_for_scroller:=false):
	#called by _room_ready
	#start the timers, create the enemies, etc
	var oxygen=OXYGEN_TIME+OXYGEN_TIME_FINAL
	
	if partial_for_scroller:
		oxygen=-1
		ok_down=true
		ok_up=true
		ok_left=true
		ok_right=true
	else:
		if current_room_data.has("room_limit"):
			oxygen=current_room_data["room_limit"]

		$NoMoreEnemies.stop()
		$NoMoreEnemies.wait_time=OXYGEN_TIME
			
		if oxygen!=-1:
			$NoMoreEnemies.start()
		last_chance_nomoreenemies=false

	#caught by rex_game code _on_Rex_RoomManager_new_room
	emit_signal("new_room",oxygen)
	
	if !partial_for_scroller:
		#set exit collisions to stop movement
		#if allowed then disable, otherwise enable
		#up/left/right we simply block, bottom we kill
		var rooms=get_current_room_exits()
		var main_camera=GlobalTileMap.main_camera
		if rooms==null || rooms.size()!=4 || main_camera==null:
			print("ERROR. room array is null, camera null or not 4 in size for " + str(current_room_id))
		else:
			var t=(rooms[0]!=-1)
			var b=(rooms[1]!=-1)
			var l=(rooms[2]!=-1)
			var r=(rooms[3]!=-1)
			print("*** start_room "+str(current_room_id)+" "+str(t)+str(b)+str(l)+str(r))
			main_camera.set_exits(t,b,l,r)
			#main_camera.set_player_edge_areas(true,true,true,true)
		
	no_enemies=false
	
	#may as well set enemies to be whatever current room we are in
	if current_room_data["men_max"]<1 && current_room_data["ship_max"]<1 && \
	current_room_data["ufo_max"]<1 &&  current_room_data["drone_max"]<1 && \
	current_room_data["men2_max"]<1 &&  current_room_data["triangle_max"]<1:
			no_enemies=true
			
	if no_enemies:
		return
		
	#get spawn points
	current_enemies=GlobalTileMap.get_room_enemies(current_room_id, ok_up, ok_down, ok_left, ok_right)
		
	#initial wait period
	var delay_start=current_room_data["enemy_delay"]
	var variance=delay_start/3
	var minv=delay_start-variance
	var maxv=delay_start+variance
	var wait=rand_range(minv,maxv)	#+-3 as long as not negative
	if wait<=0.0:
		wait=0.1	#want a timer but cannot have 0
	spawn_timer.wait_time=wait
	spawn_timer.start()
	
func _new_enemy():
	#decide on human, ship, ufo, drone
	#then pick it's start location, or random if many
	#add to transient room
	#add with name to make it easy to count, ensure correct ship is set for ufo/human ship
	#disable room visibility and delay until enemy enters room via timer
	#also need to ensure enemy is going in direction required
	var count
	var choice=[]
	
	_choice_select("men_max","humans",choice)
	#kind of a hack. no such thing as a humanl so do not want to multiply human count
	#i.e. if men max is 6 then if humans/l/r there will be 18 humans
	_choice_select("ship_max","ships",choice)
	_choice_select("ufo_max","ufos",choice)
	_choice_select("drone_max","drones",choice)
	_choice_select("triangle_max","triangles",choice)
	_choice_select("men2_max","rex2_humans",choice)
	count=choice.size()
	if count<1:
		return

	var enemytype=choice[randi()%count-1]
	var enemy
	var location:=Vector2.ZERO
	
	#get start location from a choice
	var arraysize=current_enemies[enemytype].size()
	if arraysize<1:
		return

	var entry=current_enemies[enemytype][randi()%arraysize]
	location=Vector2(entry[0],entry[1])
	var centre_pos=false
	var offset=Vector2.ZERO
	match enemytype:
		"humans", "rex2_humans":
			enemy=human.instance()
			
			#check meta for forced direction
			#this is for human1
			if entry[3]==1:
				enemy.start_direction=1
				entry[2]=GlobalTileMap.DIRECTION.RIGHT
			elif entry[3]==-1:
				entry[2]=GlobalTileMap.DIRECTION.LEFT
				enemy.start_direction=-1
			else:
				#this is for human 1 and human 2
				#human2 is already set, only need to fix human 1
				#direction is facing not side came from, i.e. opposite
				if enemytype=="humans":
					enemy.start_direction=-1 if entry[2]==GlobalTileMap.DIRECTION.RIGHT else 1
					if enemy.start_direction==-1:
						entry[2]=GlobalTileMap.DIRECTION.LEFT
					else:
						entry[2]=GlobalTileMap.DIRECTION.RIGHT
				else:
					if entry[2]==GlobalTileMap.DIRECTION.RIGHT:
						enemy.start_direction=1
					else:
						enemy.start_direction=-1
			enemy.screen_based=false
			offset+=Vector2(0,-4)
			enemy.is_rex2=false
			if enemytype=="rex2_humans":
				enemy.is_rex2=true
				#refresh location every time
				current_enemies=GlobalTileMap.get_room_enemies(current_room_id, ok_up, ok_down, ok_left, ok_right)

		"ufos":
			enemy=ship.instance()
			enemy.screen_based=true
			enemy.speed=enemy.SPEED_UFO
			enemy.ship_type=enemy.SHIP_TYPE.UFO
		"ships":
			enemy=ship.instance()
			enemy.screen_based=true
			enemy.speed=enemy.SPEED_HUMAN
			enemy.ship_type=enemy.SHIP_TYPE.HUMAN
		"drones":
			enemy=drone.instance()
			enemy.screen_based=true
		"triangles":
			enemy=triangle.instance()
			enemy.screen_based=true
			centre_pos=true
		_:
			print("ERROR. got this which is unknown enemy type:" + enemytype)
			return
			
	#tell it to enter a room, thereby triggering its timer to start moving 
	#add to room
	enemy.transient_contained=true
	enemy.position=(location*GlobalTileMap.map_tile_size)+offset
	
	if centre_pos:
		enemy.position+=Vector2(GlobalTileMap.map_tile_size/2,GlobalTileMap.map_tile_size/2)
		
	if enemy.has_method("set_graphics_sprite_type"):
		enemy.set_graphics_sprite_type()
		
	enemy_home.add_child(enemy)	#ready only called when added to scene
	call_deferred("_organise_new_enemy",enemy,Vector3(entry[0],entry[1],entry[2]))

func _organise_new_enemy(enemy:Enemy,entry:Vector3):
	enemy.enable(true)
	enemy.make_ready(entry)
		
func _choice_select(maxname,name,choice:Array, force_max:=0):
	if current_room_data[maxname]>0:
		var count=_get_num_enemies(name)
		if force_max>0:
			if count<force_max:
				choice.append(name)
		else:
			if count<current_room_data[maxname]:
				choice.append(name)
	
func _get_num_enemies(name):
	var count=0
	if enemy_home==null:
		print("ERROR. Transient home is null")
		return count
		
	var found=false
	var itype
	for x in enemy_home.get_children():
		#should only be a few so ok to loop a few times
		found=false
		if x is Enemy:
			itype=x.transient_indicated_type
			match name:
				"humans":
					if itype==Enemy.ENEMY_TYPE.HUMAN:
						found=true
				"rex2_humans":
					if itype==Enemy.ENEMY_TYPE.HUMAN:
						found=true
				"ufos":
					if itype==Enemy.ENEMY_TYPE.SHIP:
						if x.ship_type==1:
							found=true
				"ships":
					if itype==Enemy.ENEMY_TYPE.SHIP:
						if x.ship_type==0:
							found=true
				"drones":
					if itype==Enemy.ENEMY_TYPE.DRONE:
						found=true
				"triangles":
					if itype==Enemy.ENEMY_TYPE.TRIANGLE:
						found=true
				_:
					print("Unknown type for check "+name)
		if found:
		#if x.name.left(len(name))==name:
			count=count+1
	return count	
	
#signals
#MOVE ALL THESE TO GAME MAYBE VIA SIGNAL

func room_about_to_start(room_id):
	#room is not ready, do not do anything that requires room
	#or room objects
	#any room you add here, remember to call show hud with true
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	GlobalTileMap.current_player.weapons.allow_firing(true)	#just in case

	match room_id:
		6:	#jsw/manic miner
			Globals.game_scene.hud_ui.show_hud(false,false)
		19:
			Globals.game_scene.hud_ui.show_hud(false,true)
		18:	#after manic miner room, i.e. ensure back on
			Globals.game_scene.hud_ui.show_hud(true,true)
		44:	#miniboss
			yield(get_tree().create_timer(1.8), "timeout")
			AudioManager.play_sfx(Globals.SFX_ENEMY_MINIPULSE)
		91:
			#start of tutorial
			if !seen_welcome:
				GlobalTileMap.current_player.get_node("Sprite").visible=false
			Globals.game_scene.hud_ui.show_hud(false,true)
			GlobalTileMap.current_player.monty_colours(false)
		104:	#monty mole
			OptionsAndAchievements.increment_achievement("sleuth",1,"Monty")
			GlobalTileMap.current_player.monty_colours(true)
			yield(get_tree().create_timer(2), "timeout")
			Globals.game_scene.hud_ui.show_hud(false,true)
		111:
			Globals.game_scene.hud_ui.show_hud(false, true)
		52:
			#normal screens requiring a hud to be revealed
			Globals.game_scene.hud_ui.show_hud(true)
		107:		#firing screen tutorial (and test screen)
			GlobalTileMap.current_player.weapons.allow_firing(false)
			Globals.game_scene.hud_ui.show_hud(true,true)
		146:
			#infiltrate
			if !Globals.infiltrate_seen:
				Globals.game_scene.hud_ui.show_hud(false,true)
		_:
			#presume normal screen so hud already shown
			#put screen above if require hud to appear
			Globals.game_scene.hud_ui.show_hud(true,true)
		
func room_ready_scroller(room_id, room_data):
	#scrolling has detected new room maybe here
	#so do stuff for new room but not everything
	if current_room_id==room_id:
		return
		
	_room_ready(room_id,null,room_data,true)
	
func _room_ready(room_id, adjacent_rooms,room_data,partial_for_scroller:=false):
	#hack until refactor
	print("doing room ready for " +str(room_id))
	previous_room_id=current_room_id
	current_room_id=room_id
	
	current_room_data=room_data
	can_randomise_pickup=true	#one random per room at mose
	_room_music(room_id)
	_start_room(partial_for_scroller)
	var room="R%03d"%room_id
	OptionsAndAchievements.increment_achievement("rooms",1,str(room))
	GlobalTileMap.current_player.only_gravity(false)
	GlobalTileMap.current_player.forced_walk_stop()	#just in case new room player still forced

	match room_id:
		6:
			OptionsAndAchievements.increment_achievement("sleuth",1,"Manic")
		19:
			OptionsAndAchievements.increment_achievement("sleuth",1,"JSW")
		231:
			GlobalTileMap.current_player.only_gravity(false)
			Globals.game_scene.player.defence.full_tank()
			Globals.game_scene.update_spawnstart_spin_indicator()
			yield(get_tree().create_timer(5), "timeout")
			OptionsAndAchievements.increment_achievement("parttime",1,"")
		44:
			#start of rex  miniboss
			_room_44_miniboss(room_id)
		45:
			pass
			#room before miniboss
			#GlobalPersist.save_all_world_data(1)
		91:#
			#start of tutorial
			_room_91_start_tutorial(room_id)
		81:#
			#queen
			_room_81_training_queen(room_id)
		147:#
			#exit infiltrate room
			_room_147_training_after_infiltrate(room_id)
		146:
			#infiltrate training
			_room_146_training_infiltrate_start(room_id)
		111:#
			#prologue ending and end of game
			if !Globals.game_ended:
				_room_111_training_prologue_start(room_id)
			else:
				_room_111_end_game()
		120:	#scrolling to get back to ship screen
			GlobalTileMap.current_player.only_gravity(true)
		12:
			#fake boss
			_room_boss(room_id)
		10:
			#start of tower descent after boss
			_room_10_start_kill_queen()
		166:
			#kill queen
			_room_166_kill_queen()
		52:	#start of game, first room
			if GlobalPersist.game_mode==GlobalPersist.GAMEMODE.SIMPLE:
				yield(get_tree().create_timer(5), "timeout")
				#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"easy_mode",Globals.DIALOGUE_TEXT["easy_mode"],true)
				if current_room_id==52:
					GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.DIALOGUE_TEXT["easy_mode"],false, true,true)
					yield(GlobalMethods,"dialog_finished")
		2:
			GlobalTileMap.current_player.only_gravity(false)
			_room_2()
		270,257,244:
			GlobalTileMap.current_player.only_gravity(true)
			if current_room_id in [270,257,244]:
				AudioManager.play_music(Globals.MUSIC_LEVEL1,true, 0.0, true)	#continue playing
			if current_room_id==270:
				#rex1 finished, give hims some stuff
				Globals.lives=Globals.START_LIVES
				GlobalPersist.smart_bombs=3
				for x in range(22):
					GlobalTileMap.current_player.weapons.increase_current_weapon_energy(10)
				GlobalTileMap.current_player.weapons.gain_rapid_fire()
				GlobalTileMap.current_player._bubble_full_tank()
				Globals.game_scene.refresh_display()
				GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.DIALOGUE_TEXT["ready_rex2"],false, true,true)
				yield(GlobalMethods,"dialog_finished")


func _room_2():
	yield(get_tree().create_timer(0.5), "timeout")
	#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"rex1_screen", Globals.MINIBOSS_TEXT["rex1_screen"],true)
	GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["rex1_screen"],false, true,true)
	yield(GlobalMethods,"dialog_finished")
	yield(get_tree().create_timer(2), "timeout")
	if current_room_id==2:
		AudioManager.play_music(Globals.MUSIC_LEVEL1,true, 4.0)
	
func _room_10_start_kill_queen():
	var falling_queen=get_tree().root.find_node("Falling_Queen",true,false)
	var queen_sprite=falling_queen.get_node("SpriteQueen")
	var tentacle=queen_sprite.get_node("SpriteTentacle")
	#var player_pos=falling_queen.get_node("Position2D")
	var t = create_tween()
	GlobalTileMap.current_player.enable_lock(true)
	Globals.disallow_options=true
	#t.interpolate_property(tentacle,"modulate",Color(1,1,1,1),Color(1,1,1,0),1)
	#t.start()
	t.tween_property(tentacle,"modulate",Color(1,1,1,0),1).from(Color(1,1,1,1))
	
	GlobalTileMap.current_player.visible=true
	GlobalTileMap.current_player.weapons.disable_current_weapon()
	yield(get_tree().create_timer(0.5), "timeout")
	GlobalTileMap.current_player.enable_lock(false)
	GlobalTileMap.current_player.only_gravity(true)
	yield(get_tree().create_timer(0.25), "timeout")
	falling_queen.start()	#temp until player starts properly, move further up
	tentacle.queue_free()
	GlobalTileMap.current_player.weapons.disable_current_weapon()
	Globals.disallow_options=false
	#GlobalTileMap.current_player.weapons.set_weapons_firing_status()
		
func _room_166_kill_queen():
	#do kill queen animation here
	#player is gravity locked here
	Globals.disallow_options=true
	GlobalTileMap.current_player.velocity_rex=Vector2.ZERO
	yield(get_tree().create_timer(5.0), "timeout")
	var falling_queen=get_tree().root.find_node("Falling_Queen",true,false)
	
	#do dialog 1
	#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"squidgy_kill1", Globals.MINIBOSS_TEXT["squidgy_kill1"],true,null,true,0.05,false,false)
	GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["squidgy_kill1"],false, true,true)
	yield(GlobalMethods,"dialog_finished")
	
	var queen_anim:AnimationPlayer=falling_queen.get_node("AnimationPlayer")
	queen_anim.play("die")
	yield(queen_anim,"animation_finished")
	#unlock achievement
	OptionsAndAchievements.increment_achievement("fulltime",1)
	#save game somewhere here or earlier so can relive final
	
	yield(get_tree().create_timer(2.0), "timeout")
	Globals.game_scene.hud_ui.show_hud(true)
	GlobalTileMap.current_player.only_gravity(false)
	GlobalTileMap.current_player.double_lock(false)
	Globals.disallow_options=false
	
func _room_44_miniboss(_room_id):
	Globals.disallow_options=true
	Globals.game_scene.player.bubble=false
	Globals.game_scene.player.defence.set_defence_bubble(false)
	Globals.game_scene.PauseMiniBossRex1(7)
	#auto save for first time we are here

	GlobalPersist.has_continue=true
	#put in miniboss room so if die then respawn with miniboss
	#at start of game if here then resets to 17
	if GlobalPersist.player_spawn_point!="Spawn2":
		GlobalPersist.player_spawn_point="Spawn2"

	Globals.game_scene.update_spawnstart_spin_indicator()
	#disable screen enemies from spawning
	spawn_timer.stop()
	GlobalTileMap.current_player.enable_lock(true)

	if seen_miniboss_room:
		#replace tiles from backup to new as we've destroyed it
		#GlobalTileMap.CopyTiles(room_id,room_id+13,GlobalTileMap.current_map)
		#GlobalTileMap.CopyTiles(room_id,room_id+13,GlobalTileMap.current_destroyable_map)
		pass
	
	#hide if showing intro, otherwise show
	Globals.game_scene.hud_ui.show_hud(seen_miniboss_room)

	seen_miniboss_room=true
	
	#fade and screenshake
	#set so that the player spawning into room is not shown
	Globals.game_scene.world_modulate_set(true)
	Globals.game_scene.world_modulate_change_alpha(0.0)
	yield(get_tree().create_timer(4.25), "timeout")
	
	#fade to view screen then start
	#var tween:=Tween.new()
	var tween:SceneTreeTween=create_tween()
	tween.tween_method(Globals.game_scene,"world_modulate_change_alpha",0.0,1.0,2.0).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_SINE)
	#add_child(tween)
	#tween.interpolate_method(Globals.game_scene,"world_modulate_change_alpha",
	#	0.0,1.0,2.0,Tween.TRANS_SINE,Tween.EASE_IN)
	#tween.start()
	yield(tween,"finished")
	#tween.queue_free()
	
	Globals.game_scene.world_modulate_set(false)	
	#double lock disabled in miniboss
	Globals.game_scene.StartMiniBossRex1()
	Globals.disallow_options=false

	
	
func _room_91_start_tutorial(_room_id):
	#Globals.game_scene.hud_ui.show_hud(false)
	Globals.invincible=true
	if !seen_welcome:
		GlobalTileMap.current_player.set_bubble(false)
		Globals.game_scene.player.defence.full_tank()
		GlobalPersist.player_spawn_point="Spawn5"
		GlobalTileMap.current_player.fake_birth()
		GlobalTileMap.current_player.double_lock(true)
		Globals.disallow_options=true
		seen_welcome=true
		yield(get_tree().create_timer(5.3), "timeout")
		GlobalMethods.do_hint(Vector2(50,50),"tutorial_movement_basic1")
		Globals.infiltrate_seen=true
		Globals.disallow_options=false
		yield(GlobalMethods,"dialog_finished")
		yield(get_tree().create_timer(0.5), "timeout")
		Globals.game_scene.hud_ui.show_hud(true)
		GlobalTileMap.current_player.double_lock(false)
	else:
		Globals.game_scene.hud_ui.show_hud(true)
		GlobalTileMap.current_player.double_lock(false)
	
func _room_81_training_queen(_room_id):
	Globals.disallow_options=true
	yield(get_tree().create_timer(5.0), "timeout")
	var t=get_tree().root.find_node("TrainingRooms",true,false)
	t.get_node("EndingText2").text="You asked for it. Now feel the wrath of my mineral mining machines converted into lethal killing machines."
	Globals.disallow_options=false

func _room_147_training_after_infiltrate(_room_id):
	#local code in exittutorialtogame node now does this
	#but it calls room_147_auto_trigger
	GlobalPersist.player_spawn_point="Spawn27"
	Globals.game_scene.hud_ui.show_hud(true)
	Globals.invincible=(GlobalPersist.game_mode==GlobalPersist.GAMEMODE.SIMPLE)
	pass
	
func room_147_auto_trigger():
	#called by room 147 local triggers, see _room_147 above
	#training mode we always set as not invincible
	#yield(get_tree().create_timer(20.0), "timeout")
	Globals.disallow_options=true

	var conv=get_tree().root.find_node("CollisionShapeInfiltrate",true,false)
	var tween = create_tween()
	tween.tween_method(GlobalMethods,"screen_shake",0.01,0.08,3.0).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)
	yield(tween,"finished")

	Globals.game_scene.fullscreen_shock(true,Vector2(0.5,0.5),true)
	yield(get_tree().create_timer(1.0), "timeout")

	GlobalTileMap.rex_room_manager.kill_room(true,0.1,true,false,false)
	
	if conv!=null:
		conv.set_deferred("disabled",true)
	#reuse functionality to get tiles from explosion tilemap
	var tiles:Array=GlobalTileMap.get_room_miniboss_tiles(1)
	tiles.shuffle()
	var c=tiles.size()
	var mod=1
	var interval=0.1
	if c<15:
		interval=0.15
	elif c<30:
		mod=2
	else:
		mod=int(c/20)+1
		
	var location:Vector2
	var count=0
	#go through all tiles in miniboss and clear both main map
	#and destroyable map as it's irrelevant
	#then create explosion
	var found
	for ti in tiles:
		count+=1
		found=0
		var t=ti[0]
		#get main tilemaps from explosion tiles
		location=GlobalTileMap.get_global_from_cell(null,t)
		found=GlobalTileMap.replace_single_tile(null,t.x,t.y,-1)
		found+=GlobalTileMap.replace_single_tile(GlobalTileMap.current_destroyable_map,t.x,t.y,-1)
		#no trauma, but turn off animation and then try this
		#only explode if there was a tile there
		if found>0:
			if count%mod==0:
				GlobalMethods.explosion_standard(location,true,true,0.2,0,Globals.SFX_ENEMY_EXPLODE3)
				yield(get_tree().create_timer(interval), "timeout")
	Globals.disallow_options=false

func _room_boss(_room_id):
	yield(get_tree().create_timer(1.5), "timeout")
	GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["boss_real_rex"],false, true,true)
	
func _room_146_training_infiltrate_start(_room_id):
	#infiltrate start
	#entered once on new game
	#re-entered after tutorial
	#SpawnInfiltrate is main starting point, spawn27 is new location on first platform for after death
	var jump_out:=true
	Globals.invincible=false
	if !Globals.infiltrate_seen and (GlobalTileMap.previous_room_id==-1 or GlobalTileMap.previous_room_id==111 or GlobalTileMap.previous_room_id==146):
		#if came in from new game then bump the player, otherwise let the system sort it out
		GlobalTileMap.current_player.position.x+=400
		Globals.invincible=true
		GlobalPersist.player_spawn_point="Spawn27"
	if !Globals.infiltrate_seen and GlobalTileMap.previous_room_id==146:
		#if died in infiltrate screen then continue as normal as must have triggered invincibility
		Globals.invincible=false
	if GlobalTileMap.previous_room_id==111 or GlobalTileMap.previous_room_id==146:
		#came from right or above so when die turn everything to normal
		GlobalPersist.player_spawn_point="Spawn27"
		if GlobalTileMap.previous_room_id!=147:
			GlobalTileMap.current_player.position.x-=400
		Globals.infiltrate_seen=true
		jump_out=false
		Globals.invincible=false
		
	GlobalTileMap.current_player.set_bubble(false)
	Globals.game_scene.player.defence.full_tank()
	Globals.game_scene.player.bubble=false
	Globals.game_scene.player.defence.set_defence_bubble(false)
	GlobalPersist.smart_bombs=3
	
	if Globals.infiltrate_seen:
		Globals.invincible=false
	else:
		#here so first time in or dead after infiltrate
		GlobalPersist.smart_bombs=0
		Globals.disallow_options=true
		if Globals.invincible:
			Globals.invincible=false	#turn it off so we can show it going off after tutorial_full_welcome_rex
			GlobalTileMap.current_player.visible=false
			Globals.game_scene.world_modulate_set(true)
			Globals.game_scene.world_modulate_change_alpha(0.0)
			GlobalTileMap.current_player.double_lock(true)
			Globals.game_scene.hud_ui.show_hud(false,true)
			Globals.birth_invisible_override=false
			yield(get_tree().create_timer(4.0), "timeout")
			#now fade out completely over 8 seconds, 10 in total
			var tw:SceneTreeTween = create_tween()
			tw.set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)
			tw.tween_method(Globals.game_scene,"world_modulate_change_alpha",0.0, 1.0,3.0)
			yield(tw,"finished")
			Globals.game_scene.world_modulate_set(false)	
			yield(get_tree().create_timer(6.4), "timeout")
			#bring player in
			#Globals.game_scene.hud_ui.show_hud(true)
			GlobalTileMap.current_player.visible=false
			GlobalTileMap.current_player.double_lock(false)
			GlobalTileMap.current_player.visible=true
		if jump_out:
			GlobalTileMap.current_player.jump(Player.JUMP_SPEED/2.0,false,1)
		GlobalTileMap.set_room_edge_detection(true)
		Globals.disallow_options=false
		yield(get_tree().create_timer(2.0), "timeout")
		#always keep hud off for infiltrate first, but after tutorial, on
	Globals.game_scene.player.update_player_invincible()
	Globals.game_scene._update_smart_ui()
	
	if Globals.infiltrate_seen:
		Globals.game_scene.hud_ui.show_hud(true)

func _room_111_end_game():
	#room same as 111 but specific to ending
	#then triggers tv80 and back to menu
	#ending
	#put in darkness and set colours back to start
	#ie sky blue, no stars
	Globals.disallow_options=true
	GlobalPersist.graphics_type=0
	GlobalTileMap.current_player.set_bubble(false)
	Globals.game_scene.change_sprite_mode()
	var t=get_tree().root.find_node("TrainingRooms",true,false)
	#var ship2=get_tree().root.find_node("rex1_startship2",true,false)
	
	t.get_node("ColorRect").color=GlobalMethods.get_as_colour(Globals.COLOURS.cyan)
	var sky=get_tree().root.find_node("RexSky",true,false)
	var etext=t.get_node("EndingText")
	etext.text="Having rid the planet of the evil squidgy queen and dispensed with the human parasites, the inhabitants of Zenith rejoiced, and threw a little party for Rex.\n\nAs well as paying him, of course."
	sky.modulate.a=0.0
	GlobalTileMap.current_player.enable_lock(true)
	GlobalTileMap.current_player.global_position=Vector2(6750+100,5734)
	GlobalTileMap.current_player.z_index=20

	yield(get_tree().create_timer(16.0), "timeout")
	etext.text=""

	#wait in darkness 2 seconds then show the above scene then start night fade and music
	var a:AnimationPlayer=t.get_node("AnimationPlayer")
	a.play("game_ending")
	yield(get_tree().create_timer(4.6), "timeout")
	Globals.game_scene.fullscreen_shader_grey_advanced(true,true)
	GlobalTileMap.current_player.visible=true
	yield(get_tree().create_timer(6.0), "timeout")
	var x=load("res://fireworks/PlayFireworks.tscn").instance()
	x.position.y=48
	#var c=GlobalMethods.assign_temp_location()
	var c=get_tree().root.find_node("GameWithHUD",true,false)
	#var main_camera=GlobalTileMap.main_camera
	#x.position=main_camera.position
	c.add_child(x)
	
	yield(get_tree().create_timer(4.0), "timeout")	#42
	#fireworks now finished
	#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"rex_final",Globals.DIALOGUE_TEXT["rex_final"],true)
	GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.DIALOGUE_TEXT["rex_final"],false, true,true)
	yield(GlobalMethods,"dialog_finished")
	Globals.game_scene.fullscreen_shaders(false,false)

	#do tv80
	yield(get_tree().create_timer(4.0), "timeout")
	OptionsAndAchievements.do_intro(false)
	
	#back to menu
	#SceneManager.sequence_shift()
	#Globals.disallow_options=false
	
func _room_111_training_prologue_start(_room_id):
	#ending
	#put in darkness and set colours back to start
	#ie sky blue, no stars
	GlobalTileMap.current_player.double_lock(true)
	Globals.disallow_options=true
	GlobalPersist.graphics_type=0
	GlobalTileMap.current_player.set_bubble(false)
	#GlobalTileMap.current_player.player_invincible(false)
	Globals.game_scene.acquire_oxygen_mask()
	Globals.game_scene.change_sprite_mode()
	var t=get_tree().root.find_node("TrainingRooms",true,false)
	var ship2=get_tree().root.find_node("rex1_startship2",true,false)
	#Globals.game_scene.world_modulate_set(true)
	#Globals.game_scene.world_modulate_change_alpha(0.0)
	Globals.game_scene.hud_ui.show_hud(false,true)
	#set all colours back to default and leave in darkness
	#we have blue sky and water, player locked in position
	t.get_node("ColorRect").color=GlobalMethods.get_as_colour(Globals.COLOURS.cyan)
	var sky=get_tree().root.find_node("RexSky",true,false)
	var etext=t.get_node("EndingText")
	sky.modulate.a=0.0
	GlobalTileMap.current_player.enable_lock(true)
	GlobalTileMap.current_player.weapons._weapon_downgrade()
	GlobalTileMap.current_player.weapons._weapon_downgrade()	#get rid of drone if have it
	GlobalTileMap.current_player.global_position=Vector2(6750,5734)
	GlobalTileMap.current_player.z_index=20
	
	if true:	#to skip section
		#wait in darkness 2 seconds then show the above scene then start night fade and music
		var timegaps=[8.0,12.0,11.5,5.5]
		yield(get_tree().create_timer(timegaps[0]), "timeout")
		var a:AnimationPlayer=t.get_node("AnimationPlayer")
		a.play("ending")
		
		etext.text="Every journey begins with the first step. This is  where it will begin for Rex on his quest to save the planet of Zenith from the evil human overlords."
		yield(get_tree().create_timer(timegaps[1]), "timeout")
		#start walk here
		etext.text="Mission 1:\nInfiltrate the base through the underground river, navigate the caverns to find the base of the tower and leave no prisoners"
		GlobalTileMap.current_player.tutorial_walk()
		yield(get_tree().create_timer(timegaps[2]), "timeout")
		ship2.play_animation("ride_away")
		AudioManager.stop_music(2.0)
		yield(get_tree().create_timer(timegaps[3]), "timeout")
		
	SceneManager.sequence_shift()
	Globals.disallow_options=false
	
func _room_music(room_id):
	if room_id==146:	#infiltrate
		yield(get_tree().create_timer(1.0), "timeout")
		AudioManager.play_music(Globals.MUSIC_INFILTRATE,1.0)
	elif room_id==111:
		pass			#do not want music to stop
	elif room_id==6:
		yield(get_tree().create_timer(2.0), "timeout")
		AudioManager.play_music(Globals.MUSIC_JSW)
	elif room_id==73:
		yield(get_tree().create_timer(5.0), "timeout")
		AudioManager.play_music(Globals.MUSIC_INDUSTRY,false,2.0)
	elif room_id==60:
		yield(get_tree().create_timer(6.0), "timeout")
		AudioManager.play_music(Globals.MUSIC_INDUSTRY,false,0.0,true)
	elif room_id==19:
		yield(get_tree().create_timer(1.0), "timeout")
		AudioManager.play_music(Globals.MUSIC_MANIC)
	elif room_id==104:	#monty mole
		yield(get_tree().create_timer(1.0), "timeout")
		AudioManager.play_music(Globals.MUSIC_MONTY)
	elif room_id==270 or room_id==257 or room_id==244:
		pass	#let music continue
	elif room_id in [155,191,204,230,229,222]:
		AudioManager.play_music(Globals.MUSIC_BOSS_END,true, 4.0, true)
	elif room_id == 221:
		AudioManager.stop_music(8.0)
	else:
		AudioManager.stop_music(2.0)
	
func _on_SpawnTimer_timeout() -> void:
	#here so timer must have started and >0
	#get new time and repeat
	var interval=current_room_data["enemy_interval"]
	if interval<=0:
		return
	var minv=interval-2
	var maxv=interval+2
	var wait=rand_range(max(0.5,minv),max(0.5,maxv))	#+-3 as long as not negative
	spawn_timer.wait_time=wait
	
	_new_enemy()
	spawn_timer.start()

func can_choose_random():
	return can_randomise_pickup
	
func can_choose_random_picked():
	can_randomise_pickup=false
	
func kill_room_bullets():
	var c=GlobalMethods.assign_temp_location()
	for item in c.get_children():
		if item is StraightProjectile:
			item.death()
			
func kill_room(all_enemies, pause_time:=0.0, full_drop:=true, give_score:=true, give_bubbles:=true,real_smartbomb:=false):
	#all enemies or just humans/humans in ships
	#full drop is everything, not is random
	#give bubbles is for those killed create bubbles
	#first remove all bullets that are active
	if real_smartbomb:
		Globals.bullet_time=0.05;
		GlobalTileMap.set_slomo(true)
		AudioManager.play_sfx(Globals.SFX_WEAPON_SMARTSTART)
		#yield(get_tree().create_timer(0.5), "timeout")


	var thiswait=0
	var room_killed=0
	kill_room_bullets()
	#var eroom
	var flip=Globals.game_scene.get_flip_controller()
	
	#now kill all enemies in current room
	if all_enemies:
		for e in GlobalTileMap.active_room_enemies:
			#ignore if not in same room if flipscreen
			#ignore if not within screen offset from player if scrolling
			
			var e_is_valid=is_instance_valid(e)
			if e_is_valid:
				if flip!=null:
					if flip.is_scrolling():
						#calculate position from player in a screen radius
						pass
					elif flip.get_roomid_from_global(e.global_position)!=current_room_id:
						continue
				thiswait+=pause_time
				
			if e_is_valid and e is Enemy and !e.is_dead:
				room_killed+=1
			if e_is_valid and real_smartbomb and e is Enemy and e.smart_bomb_kills and !e.is_dead:
				GlobalMethods.missile_lock(e.global_position)
				
			if !e_is_valid:
				print("ERROR. We have a null static room enemy in kill room. Possibly a deleted item.")
			elif e is Enemy && !e.smart_bomb_kills:
				pass
			elif e.has_method("bubble_death"):
				if pause_time>0.0:
					yield(get_tree().create_timer(pause_time), "timeout")
				if full_drop:
					e.bubble_death(give_bubbles,true,give_score,thiswait)
				else:
				#if timer out, only some give bubbles
					if randf()>0.7:
						e.bubble_death(give_bubbles,true,give_score,thiswait)
			else:
				pass
				#GlobalMethods.debugprint("No bubble death for " + e.name)
			#	print("No bubble death for " + str(e.name))

	#kill everything that is spawned in, i.e. not a room based enemy
	#they are only spawned in same room or scrolling area
	#so wipe them all out regardless as should only be close to player
	var isdeath
	var tokill:Array=[]
	for c in enemy_home.get_children():
		if is_instance_valid(c) and c is Enemy and c.smart_bomb_kills and real_smartbomb:
			if c.has_method("get_real_global_position"):
				GlobalMethods.missile_lock(c.get_real_global_position())
			else:
				GlobalMethods.missile_lock(c.global_position)

			if c is Enemy:
				tokill.append(c)
			else:
				pass
		elif ("HomingMine" in c.name || "Bullet" in c.name):
			c.death()
	
	#we have our list of valid items to kill
	#check if too many to pause
	if pause_time*float(tokill.size())>1.5:
		pause_time=max(0.05,pause_time/tokill.size())
		
	for enemy in tokill:
		if enemy!=null && is_instance_valid(enemy):
			var itype=enemy.transient_indicated_type
			if !all_enemies:
				if itype==Enemy.ENEMY_TYPE.HUMAN:
					isdeath=true
				if itype==Enemy.ENEMY_TYPE.SHIP && enemy.ship_type==0:
					isdeath=true
			else:
				if enemy.has_method("bubble_death"):
					isdeath=true

			if isdeath:
				if full_drop:
					enemy.bubble_death(give_bubbles,true,give_score)
				else:
					#if timer out, only some give bubbles
					if randf()>0.7:
						enemy.bubble_death(give_bubbles,true,give_score)
					else:
						enemy.bubble_death(false,true,give_score)

			if pause_time>0.0:
				yield(get_tree().create_timer(pause_time), "timeout")

	if real_smartbomb and (room_killed>0 or tokill.size()>0):
		yield(get_tree().create_timer(1.5), "timeout")
	elif real_smartbomb:
		yield(get_tree().create_timer(0.5), "timeout")

	Globals.bullet_time=1.0;
	GlobalTileMap.set_slomo(false)
		
	emit_signal("room_killed")
	
func _on_NoMoreEnemies_timeout() -> void:
	if last_chance_nomoreenemies:
		#play sound?
		#flash screen?
		#kill all walking humans?
		spawn_timer.stop()
		kill_room(false,0.0,false)
		emit_signal("room_oxygen_depleted")
	else:
		$NoMoreEnemies.wait_time=OXYGEN_TIME_FINAL
		$NoMoreEnemies.stop()
		$NoMoreEnemies.start()
		last_chance_nomoreenemies=true
		emit_signal("room_oxygen_warning", $NoMoreEnemies.wait_time)
