extends Node2D
#Godot 2D Flipscreen Template V1.3.0
#for getting started with the template
#Visit https://gitlab.com/chucklepie-productions/flipscreen-template
#
#For the tutorial in how the template was made
#https://gitlab.com/chucklepie-productions/tutorials/flipscreen-camera
#
#For youtube 
#https://www.youtube.com/playlist?list=PLtc9v8wsy_BY8l7ViNJamL6ao1SKABM5T
#

export(bool) var use_override_settings:=true
export(Vector2) var viewport_size_override=Vector2(-1,-1)
export(Vector2) var viewport_start_location=Vector2(-1,-1)
onready var container=$ViewportContainer
onready var viewport=$ViewportContainer/Viewport
onready var game=$ViewportContainer/Viewport/Game
var viewport_top:=0	#for cinematic

func _ready() -> void:
	#we do not know the size of the viewport
	#as hud may be any size
	#either manually set them or set the size in the export
	Globals.in_menu=false
	game.set_hud_ui($UI/PlayerUI)
	_on_Game_modulate_set()
	GlobalMethods.connect("dialog_started",self,"_rex_dialog_start")
	GlobalMethods.connect("dialog_finished",self,"_rex_dialog_end")
	GlobalMethods.connect("dialog_tag",self,"_rex_dialog_tag")
	if use_override_settings && (
		viewport_size_override.x<=0 ||viewport_size_override.y<=0):
			print("ERROR. Override set but values are negative. Game may not size or position correctly")
			#just pass game whatever is set
			#we assume negative is ok for start location
			viewport_top=$ViewportContainer.margin_top
			$UI/PlayerUI.connect("ui_shown",self,"_on_PlayerUI_ui_shown")
			game.configure_game(viewport.size)
			return
			
	if use_override_settings:
		#using overrides. This is usually the easiest way
		#update the position and size
		viewport.size=viewport_size_override
		container.rect_size=viewport_size_override
		container.rect_position=viewport_start_location
		viewport_top=$ViewportContainer.margin_top
		$UI/PlayerUI.connect("ui_shown",self,"_on_PlayerUI_ui_shown")
		game.configure_game(viewport.size)
		return
		
	#we are here meaning does not want to use overrides
	#ie the settings in viewport and container are to be kept
	game.configure_game(viewport.size)
	GlobalTileMap.set_rex2_backdrop(GlobalPersist.rex2_backdrop)


#################################
# specific game code
##################################
var mouse_move:=false
		
func _on_PlayerUI_ui_shown(enable_flag,immediate) -> void:
	#hides or shows the HUD panel, will pan main game
	#to centre if off or pan to bottom if on
	#hud is done show_hud method that calls this
	# this method emits the signal that calls this
	
	if enable_flag:
		if !immediate:
			$ViewportContainer.margin_top=viewport_top/2.0
			var tw = create_tween()
			var pt = tw.tween_property($ViewportContainer,"margin_top",float(100),0.5)
		else:
			$ViewportContainer.margin_top=viewport_top
	else:
		if !immediate:
			$ViewportContainer.margin_top=viewport_top
			var tw = create_tween()
			tw.tween_property($ViewportContainer,"margin_top",viewport_top/2.0,0.5)

		else:
			$ViewportContainer.margin_top=viewport_top/2.0

func _on_Game_modulate_change(value) -> void:
	#if visible then 0 means no change to colour, 1 means full change
	#so if colour is white then 0 is normal, 1 is invisible
	$CanvasModulate.color.a=value

func _on_Game_modulate_set(status:=false, colour:=Color.white) -> void:
	#white means colours as they are if visible true
	#i.e. same as visible false. see change alpha
	$CanvasModulate.visible=status
	$CanvasModulate.color=colour

func _rex_dialog_tag(tag):
	match tag:
		"tutor_weapons_colours":
			#weapons in hud
			$UI/PlayerUI.tutorial("weapons")
		"tutor_shield","tutor_lives":
			#shield, lives, score in hud
			$UI/PlayerUI.tutorial("shield")
		"tutor_smart":
			#smart bomb in hud
			$UI/PlayerUI.tutorial("smart")
		"tutor_beam":
			#room meter in hud
			$UI/PlayerUI.tutorial("meter")
			
func _rex_dialog_start(title):
	#alert room manager (handles room time death) and gauge (handles display)
	$UI/PlayerUI.dialog_started()
	game.dialog_started()

	

func _rex_dialog_end(_title):
	$UI/PlayerUI.tutorial("off")
	#alert room manager and gauge
	game.dialog_ended()
	$UI/PlayerUI.dialog_ended()
	
