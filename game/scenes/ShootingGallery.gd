extends Node2D

# 1. 1 man walks left bottom, stops, when dead next triggers
# 2. 6 men walk left bottom, stops, when dead next triggers
# 3. 1 man bottom 1 man mid walk left, stops, when dead next triggers
# 4. 1 man bottom/mid/top walk, stops, when dead next triggers

onready var human=preload("res://entities/screen_based/Human.tscn")
onready var ship=preload("res://entities/screen_based/ScreenEnemy_Ship.tscn")

onready var container = $HumanContainers
enum {PTOP,PMID,PBOT, F1, F2, F3}
const WALK_TIME = 4.5
var phase:=0
var player
var next_phase_ready=true

func _ready() -> void:
	set_process(false)
	pass

func enable(_display):
	set_process(false)
	_set_player()
	player.weapons.acquire_weapon(Weapons_System.WEAPON.BULLET_DOUBLE,false)
	player.weapons.acquire_weapon(Weapons_System.WEAPON.LASER,false)
	player.weapons.acquire_weapon(Weapons_System.WEAPON.DRONE,false)
	player.weapons.acquire_weapon(Weapons_System.WEAPON.SPRAY,false)
	GlobalTileMap.current_player.weapons._weapon_downgrade()

	if $StartTrigger:
		$StartTrigger.enable(true)
	
func disable(_display):
	set_process(false)
	
#2. add 'set specific' level for weapons as might change
#3. set a block on floor to show player where to stop
func _process(delta: float) -> void:
	if container.get_child_count()==0 and next_phase_ready:
		_set_player()
		player.weapons.allow_firing(false)
		$ResetTimer.stop()
		next_phase_ready=false
		yield(get_tree().create_timer(3), "timeout")
		next_phase_ready=true
		phase+=1
		_do_phase(phase)

func _set_player():
	if player==null:
		player=GlobalTileMap.current_player

func _do_phase(phase):
	$ResetTimer.wait_time=15
	$ResetTimer.start()
	_set_player()
	player.weapons.allow_firing(false)
	var fire_wait_time = 4.0
	match phase:
		1:
			_get_weapon(Weapons_System.WEAPON.BULLET_SINGLE,5)
			_create_recoil()
		2:
			_get_weapon(Weapons_System.WEAPON.BULLET_DOUBLE,5)
			GlobalMethods.do_hint(null,"tutor_rapid2")
			yield(GlobalMethods,"dialog_finished")
			_create_recoil()
		3:
			_get_weapon(Weapons_System.WEAPON.LASER,5)
			GlobalMethods.do_hint(null,"tutor_laser")
			yield(GlobalMethods,"dialog_finished")
			_create_laser()
		4:
			_get_weapon(Weapons_System.WEAPON.DRONE,5)
			GlobalMethods.do_hint(null,"tutor_drone")
			yield(GlobalMethods,"dialog_finished")
			_create_drone()
		5:
			_get_weapon(Weapons_System.WEAPON.SPRAY,2)
			GlobalMethods.do_hint(null,"tutor_scatter")
			yield(GlobalMethods,"dialog_finished")
			_create_cluster()
		6:
			_get_weapon(Weapons_System.WEAPON.SPRAY,3)
			GlobalMethods.do_hint(null,"tutor_scatter")
			yield(GlobalMethods,"dialog_finished")
			_create_cluster(true)
		7:
			_get_weapon(Weapons_System.WEAPON.SPRAY,4)
			GlobalMethods.do_hint(null,"tutor_scatter")
			yield(GlobalMethods,"dialog_finished")
			_create_cluster(false,true)
		8:
			fire_wait_time = 1.0
			$ResetTimer.wait_time=25
			$ResetTimer.stop()
			$ResetTimer.start()
			_create_mass()
		9:
			$ResetTimer.stop()
			var a = get_tree().root.find_node("ArrowRightWeaponTut",true,false)
			$BlockPlayer.collision_mask=0
			a.start_disabled=false
			a.enable(true)
			next_phase_ready=false
			return
			
	yield(get_tree().create_timer(fire_wait_time), "timeout")
	player.weapons.allow_firing(true)

	
func _get_weapon(type,level):
	player.weapons.acquire_weapon(type)
	player.weapons.set_weapon_level(type,level)
	yield(get_tree().create_timer(2.75), "timeout")
	Globals.game_scene.weapons_titilate()
		
func _create_recoil():
	_create_human(PBOT,WALK_TIME)

func _create_laser():
	for i in range(6):
		_create_human(PBOT,0.5+WALK_TIME-(i*0.75))
		yield(get_tree().create_timer(0.75), "timeout")
		
func _create_drone():
	_create_human(PBOT,WALK_TIME)
	_create_human(PMID,WALK_TIME-0.25)
	
func _create_cluster(want_top:=false,want_ship:=false):
	_create_human(PBOT,WALK_TIME)
	_create_human(PMID,WALK_TIME)
	if want_top:
		_create_human(PTOP,WALK_TIME)
	if want_ship:
		_create_ship()

func _create_mass():
	for i in range(3):
		_create_human(F1,0)
		_create_human(F2,0)
		_create_human(F3,0)
		_create_human(PMID,0)
		yield(get_tree().create_timer(2), "timeout")
	for i in range(10):
		_create_human(PBOT,0)
		_create_human(PMID,0)
		_create_human(PTOP,0)
		yield(get_tree().create_timer(0.75), "timeout")
	
func _create_ship():
	var h=ship.instance()
	var container=GlobalMethods.assign_temp_location()
	container.add_child(h)			#this is to go into game scene transient
	h.transient_contained=true
	#h.screen_based=true
	h.global_position=get_node("ShipPosition").global_position
	
	h.speed=h.SHIP_TYPE.HUMAN
	h.ship_type=h.SHIP_TYPE.HUMAN

	h.make_ready(Vector3(0,0,GlobalTileMap.DIRECTION.LEFT))
	h.enable(true)

func _create_human(platform,walk_time):
	var h=human.instance()
	
	#h.is_rex2=is_rex2
	h.screen_based=false
	h.special_training_mode=true
	h.gives_energy_on_death=false
	container.add_child(h)
	h.transient_contained=true
	h.fire_mode=h.FIRING_MODES.NO
	h.lock_start_state=h.HUMAN_STATES.walk
	h.lock_state_after_unlock=h.HUMAN_STATES.walk
	h.lock_state=true
	var dvector = Vector3(0,0,GlobalTileMap.DIRECTION.LEFT)
	
	if platform==PTOP:
		h.global_position=get_node("TopPosition").global_position
	elif platform==PMID:
		h.global_position=get_node("MidPosition").global_position
	elif platform==F1:
		h.global_position=get_node("Falling1").global_position
	elif platform==F2:
		h.global_position=get_node("Falling2").global_position
	elif platform==F3:
		h.global_position=get_node("Falling3").global_position
	else:
		h.global_position=get_node("LowerPosition").global_position
		
	#facing
	h.make_ready(dvector)
	h.enable(true)
	
	if walk_time>0.0:	#don't make invincible or pause of walk time not set
		h.invincible(true)
		yield(get_tree().create_timer(walk_time), "timeout")
		if is_instance_valid(h):
			h.invincible(false)
			h.lock_state=h.HUMAN_STATES.idle
			h.lock_state_after_unlock=h.HUMAN_STATES.idle
			h._change_state(h.lock_state_after_unlock)


func _on_StartTrigger_picked_up(value, location, meta) -> void:
	GlobalMethods.do_hint(null,"tutor_rapid1")
	set_process(true)



func _on_ResetTimer_timeout() -> void:
	#in case something happens, end current phase after 15 seconds
	for c in container.get_children():
		c.queue_free()
		
