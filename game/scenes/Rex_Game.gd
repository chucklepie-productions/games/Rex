extends Node2D
#Godot 2D Flipscreen Template V1.3.0
#for getting started with the template
#Visit https://gitlab.com/chucklepie-productions/flipscreen-template
#
#For the tutorial in how the template was made
#https://gitlab.com/chucklepie-productions/tutorials/flipscreen-camera
#
#For youtube 
#https://www.youtube.com/playlist?list=PLtc9v8wsy_BY8l7ViNJamL6ao1SKABM5T
#

#if false then _ready does not configure game, i.e. relies on something else
#typically set this when using hud as hud will configure game
#doesn't matter, it just avoids setting things twice
signal modulate_set(status,colour)
signal modulate_change(value)

const BUBBLE_ACHIEVEMENT_TIME:float=20.0
const BUBBLE_ACHIEVEMENT_AMOUNT:=10
const GAMESIZE=Vector2(960,672)	#of game, not the hud, i.e. tilemap

export(bool) var configure_on_ready=false
export(bool) var camera_debug=false

onready var _camera_controller:Camera2D=$Cameras/Camera2DFlip
onready var _camera_scrolling:Camera2D=$Cameras/Camera2DScroller
onready var _screen_shake=$Cameras/ScreenShake

onready var _map_controller:Node2D=$Map
onready var _flip_room_manager:Node2D=$Flip_RoomManager
onready var _rex_room_manager:Node2D=$Rex_RoomManager

onready var defence_bubble=$Player/Defence
onready var view_shader_shock=$CanvasLayerShockwave/ColorRect
onready var view_shader_interlace=$CanvasLayerInterlaced/ColorRect
onready var view_shader_grey=$CanvasLayerGreyscale/ColorRect
onready var player=$Player

onready var tesla=preload("res://entities/environment/Tesla.tscn")
onready var tesla_rex2=preload("res://entities/environment/Rex2_Tesla.tscn")
onready var extra=preload("res://ui/ExtraItem.tscn")

onready var view_map=$Map/Area1
onready var view_map_main=$Map/Area1/RexTileMap
onready var view_map_control=$Map/Area1/ControlTileMap
onready var destroyable_map=$Map/Area1/RexTileMapDestroyable
onready var instantkill_map=$Map/Area1/RexTileMapInstantKill
onready var fg_map=$Map/Area1/RexTileMapIgnoreShipsFG
onready var rex2_backdrop_map=$Map/Area1/Rex2Background
onready var miniboss_map=$Map/Area1/MinibossTimedExplosions
onready var container_disposable=$Map/Area1/Map_Containers_Disableable
onready var container_pods=$Map/Area1/Map_Containers_Disableable/Container_EnergyPods
onready var container_rockets=$Map/Area1/Map_Containers_Disableable/Container_Spawn_Rockets
onready var container_pickups=$Map/Area1/Map_Containers_Disableable/Container_FixedPickups
onready var debug_overlay=$DebugOverlay

var bubble_count:=0
var spawning:bool = false
var new_tesla = null
var tesla_old_tiles=[]
var full_ui:=true
var hud_ui=null 		#formerly weapon_ui_full

func _ready() -> void:
	#if using HUD then configure_game needs to be called
	#in game_with_hud ready function
	#and not this
	_flip_room_manager.connect("room_transistion_started",self,"_room_about_to_start")
	if configure_on_ready:
		var vp=get_viewport().size
		configure_game(vp)

func _room_about_to_start(room_id):
	_rex_room_manager.room_about_to_start(room_id)
	
func _process(_delta: float) -> void:
	#temp
	if Globals.debug || Globals.training_mode:
		debug_aids()

	if Input.is_action_just_pressed("temp_w7"):
		if GlobalPersist.graphics_type==0:
			GlobalPersist.graphics_type=1
		elif GlobalPersist.graphics_type==1:
			GlobalPersist.graphics_type=0
		refresh_weapons()
		change_sprite_mode()

func debug_aids():
	$LabelFPS.text=str(Engine.get_frames_per_second())
	if Input.is_action_just_pressed("temp_w2"):
		player.weapons.acquire_weapon(Weapons_System.WEAPON.BULLET_DOUBLE)
		_update_weapon_ui()
	if Input.is_action_just_pressed("temp_w3"):
		player.weapons.acquire_weapon(Weapons_System.WEAPON.LASER)
		_update_weapon_ui()
	if Input.is_action_just_pressed("temp_w4"):
		player.weapons.acquire_weapon(Weapons_System.WEAPON.DRONE)
		_update_weapon_ui()
	if Input.is_action_just_pressed("temp_w5"):
		player.weapons.acquire_weapon(Weapons_System.WEAPON.SPRAY)
		_update_weapon_ui()
	if Input.is_action_just_pressed("temp_w6"):
		player.weapons.increase_current_weapon_energy(6)
		_update_weapon_ui()
		
func set_hud_ui(ui:Node):
	hud_ui=ui
	
func configure_game(viewport_size:Vector2):
	#initialise the room manager
	#if using HUD, this needs to be called in addition to _ready
	if !_flip_room_manager.setup_rooms(_camera_controller, _camera_scrolling, _map_controller,viewport_size):
		print("ERROR. Setup rooms failed")
		return
	_initialise()

	#sample of calling set config manually
	#_map_controller.manual_config(Vector2(32,32),Vector2(16,15))
	#sample setting map sizes manually
	#_camera_controller.set_navigation_system(get_viewport().size)
	
	#set us at the start room
	#player does this now
	#_flip_room_manager.change_room(-1,_flip_room_manager.ROOM_TRANSITION.NONE,0.0)

func _initialise():
	OptionsAndAchievements.reset_achievement("onslaught",false)
	var found_screen_size=Rect2(Vector2(0,0),GAMESIZE)
	fullscreen_shaders(false,false);
	fullscreen_shock(false);
	change_sprite_mode()
	GlobalMethods.register_game_owner(self)

	hud_ui.show_oxygen(false)
	
	#configure tilemap
	var map:TileMap
	map=view_map_main
	GlobalTileMap.set_map_details(map,view_map_control,destroyable_map,miniboss_map, instantkill_map,fg_map,rex2_backdrop_map, found_screen_size.size,int(map.cell_size.x),_camera_controller,player)

	#refresh miniboss
	GlobalTileMap.copy_room_tiles(44+13,44)

	_set_weapons_from_load()
	if GlobalPersist.has_oxygen:
		acquire_oxygen_mask()
		
	debug_overlay.set_visible(false)
	
	set_game_connects()
	var b=get_tree().get_root()
	debug_overlay.add_monitor("Current Room",b,"GlobalTileMap:current_room_id")
	debug_overlay.add_monitor("Previous Room",b,"GlobalTileMap:previous_room_id")
	debug_overlay.add_monitor("Camera",_camera_controller,":position")
	debug_overlay.add_monitor("Scrolling",_flip_room_manager,":_is_scrolling")
	debug_overlay.add_monitor("Rooms",b,"GlobalTileMap:adjacent_rooms")
	debug_overlay.add_monitor("Player",player,":global_position")
	debug_overlay.add_monitor("Velocity",player,":velocity_rex")
	debug_overlay.add_monitor("Invincible",b,"Globals:invincible")
	#debug_overlay.add_monitor("Debug Mode",b,"Globals:debug")
	debug_overlay.add_monitor("Spawning",b,"GlobalPersist:player_spawn_point")
	debug_overlay.add_monitor("Room Enemy u ok",_rex_room_manager,":ok_up")
	debug_overlay.add_monitor("Room Enemy d ok",_rex_room_manager,":ok_down")
	debug_overlay.add_monitor("Room Enemy l ok",_rex_room_manager,":ok_left")
	debug_overlay.add_monitor("Room Enemy r ok",_rex_room_manager,":ok_right")
	
	_rex_room_manager.initialise(find_node("Container_Level_Transient",true,true))
	change_sprite_mode()

	_update_lives_ui()
	_update_smart_ui()
	_update_score_ui()
	_update_weapon_ui()
		
	player.birth()
	update_spawnstart_new_game()
	
	call_deferred("backbuffer_setup")

func dialog_started():
	if _rex_room_manager:
		_rex_room_manager.dialog_started()
	if _flip_room_manager:
		_flip_room_manager.dialog_started()
	
func dialog_ended():
	if _rex_room_manager:
		_rex_room_manager.dialog_ended()
	if _flip_room_manager:
		_flip_room_manager.dialog_ended()
	
func set_game_connects():
	
	OptionsAndAchievements.connect("reset_to_checkpoint",self,"_on_reset_last_checkpoint")
	OptionsAndAchievements.connect("quit_active_game",self,"_on_quit")
	_rex_room_manager.connect("room_killed",player,"_on_room_killed")
	player.connect("spawn_point_enabled",self,"_on_SpawnStart_spawn_point_enabled")
	player.connect("spawn_point_exit",self,"_on_SpawnStart_spawn_point_exit")
	player.connect("spawn_point_centre",self,"_on_SpawnStart_centre")
	player.connect("smart_bomb",self,"_on_smart_bomb")
	player.connect("life_lost",self,"_on_life_lost")
	_camera_controller.connect("PlayerEdgeDetection",self,"_on_PlayerAtEdge")
	player.weapons.connect("new_weapon_acquired",self,"_on_acquired_weapon")
	#ui
	player.weapons.connect("weapon_status_change",self,"_on_weapon_status")
	defence_bubble.connect("new_bubble_level",hud_ui,"_on_new_defence_level")
	_flip_room_manager.connect("flip_room_transition_completed",self,"_on_changed_new_room")

	for child in container_pods.get_children():
		#raised by pod
		child.connect("shield_new_level",player.defence,"_on_new_shield_level")
		
	for child in container_rockets.get_children():
		child.connect("new_weapon",player.weapons,"_on_new_weapon")
		child.connect("new_weapon",self,"_on_new_weapon")
		child.connect("new_weapon_ended",self,"_on_new_weapon_ended")
		child.connect("new_weapon_ship_move",self,"_on_new_weapon_shipmove")
	for child in container_pickups.get_children():
		#new instances from dead enemies created by bubble
		child.connect("picked_up",self,"_on_pickup")
		#print("dead: " + child.name + ", "+str(child.pickup_type))

func set_scroll_camera_x_offset(x_offset):
	_camera_scrolling.offset.x=x_offset
	
#events raised by game

func _on_RoomManager_map_reposition_player(new_global_position) -> void:
	#the room data wants the player to change position
	$Player.global_position=new_global_position


func _on_Map_scroll_start(tl:Vector2, br:Vector2) -> void:
	_flip_room_manager.set_camera_scrolling(tl,br,false)


func _on_Map_scroll_end(position:Vector2) -> void:
	_flip_room_manager.set_camera_flip(position,true)

func _on_Camera2DScroller_scroll_room_update(camera_position) -> void:
	#scroll camera is active and detected a minimum positional movement
	#we activate the current and next room room objects by a room change
	#so here we want to do 2 things
	#1. change the room so the activations occur as if moving rooms
	#2. update the room enemies list based on screen area
	#do not want a full room change

	var room=_flip_room_manager.get_roomid_from_global(camera_position)
	#var camera_top_left=_flip_room_manager.get_room_topleft_global(room)
	var room_data=_map_controller.get_room_data(room)

	print("from "+str(GlobalTileMap.current_room_id)+" to "+str(room))
	#note, global tilemap and room manager has room id
	#flip room has but think no need to set it
	GlobalTileMap.current_room_id=room
	_rex_room_manager.room_ready_scroller(room,room_data)


func _on_Camera2DFlip_room_change(direction) -> void:
	#raised by the camera
	#this is the entry point for a room change
	#everything should come from here
	#at the end it raised change signal so rex game can work
	if typeof(direction)!=TYPE_VECTOR2:
		return
	
	#use default direction or direction specified in the array
	var _new_room
	_new_room=_flip_room_manager.manage_direction(direction)
	
	#use this to override
	#_room_manager.manage_direction(direction,_room_manager.ROOM_TRANSITION.SLIDE,0.5)
	#_room_manager.manage_direction(direction,_room_manager.ROOM_TRANSITION.FADE,1.0)
	
func direct_room_change(direction:Vector2):
	_flip_room_manager.manage_direction(direction)
	
#################################
# specific game code
##################################
func _on_BubbleAchievementTimer_timeout() -> void:
	bubble_achievement_reset()

func bubble_achievement_reset():
	$BubbleAchievementTimer.stop()
	$BubbleAchievementTimer.wait_time=BUBBLE_ACHIEVEMENT_TIME
	bubble_count=0

func bubble_achievement_start():
	if $BubbleAchievementTimer.is_stopped():
		$BubbleAchievementTimer.start()
		
func bubble_achievement_increment():
	if !$BubbleAchievementTimer.is_stopped():
		bubble_count+=1
	if bubble_count>=BUBBLE_ACHIEVEMENT_AMOUNT:
		bubble_achievement_reset()
		OptionsAndAchievements.increment_achievement("bubbles",1)

func fullscreen_shock(enable, centre=Vector2(0.5,0.5),original_override:=false,_thickness=0.0):
	
	if !enable:
		view_shader_shock.visible=false
		return
	#do a shock
	if $ScreenEffects.is_playing():
		return
	view_shader_shock.material.set_shader_param("thickness",0.3)
	view_shader_shock.material.set_shader_param("size",0.5)
	view_shader_shock.visible=true
	view_shader_shock.material.set_shader_param("centre",centre)
	$ScreenEffects.play("ScreenShockwave")

	yield($ScreenEffects,"animation_finished")
	view_shader_shock.material.set_shader_param("thickness",0)
	view_shader_shock.material.set_shader_param("size",0)
	view_shader_shock.visible=false	
	
func fullscreen_grey_fade(fade_range):
	#0.0 is full black and white - 0 turns off so change to 0.00001 or something
	#1.0 is full colour
	#ie call with a value to set saturation level
	#used by tween to gradually fade
	fullscreen_shader_grey_advanced(true,false,fade_range)
	
func fullscreen_shaders(greyscale,scanlines):
	fullscreen_shader_grey_advanced(greyscale,false)
	view_shader_interlace.visible=scanlines
	

func fullscreen_shader_grey_advanced(enable,fade_not_grey:=false,grey_amount:=0.0, colour_pos:Vector2=Vector2.ZERO,colour_radius:int=0):
	#grey amount is for when calling from tween usually
	#but will set a saturation level, i.e. 0.0 is full colour, 1.0 is full grey
	view_shader_grey.visible=enable
	if enable:
		#greyscale but put a circle of colour somewhere
		var frag=view_shader_grey
		frag.material.set_shader_param("ignore_centre_point",colour_pos)
		frag.material.set_shader_param("ignore_centre_point_radius",colour_radius)
		frag.material.set_shader_param("fade_not_grey",fade_not_grey)
		frag.material.set_shader_param("factor",grey_amount)

func backbuffer_setup():
	print("BACKBUFFER ANYONE?")	
	pass
	
func _do_random(location):
	var loc=GlobalTileMap.get_local_from_global(location)
	OptionsAndAchievements.increment_log("random",1)
	GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.PICKUP,"random",Globals.DIALOGUE_RANDOM,true,loc,true,0,true,true,false,true)


func PauseMiniBossRex1(time:float):
	var mini=find_node("MinibossContainer",true,false)
	if mini:
		mini.get_node("Miniboss").boss_pause_firing(time)
		
func StartMiniBossRex1():
	var mini=find_node("MinibossContainer",true,false)
	if mini==null:
		player.double_lock(false)
		print("ERROR miniboss not found!")
		return
	
	#var ret = SceneManager.replace_node("res://entities/screen_based/Miniboss.tscn", "Container_Miniboss",Vector2(5030,1861),false)
	mini.get_node("Miniboss").activate()
	#if ret:
	#	ret.get_node("Miniboss").activate()
	#else:
	#	print("ERROR. Miniboss not created")
	
func update_spawnstart_spin_indicator():
	#turn all off as we only ever want one active as when we die
	#and go back we don't want animation showing
	GlobalTileMap.replace_all_tiles(view_map_main,GlobalTileMap.SPAWN_POINT_ACTIVE,GlobalTileMap.SPAWN_POINT_INACTIVE)
	GlobalTileMap.replace_all_tiles(view_map_main,GlobalTileMap.SPAWN_POINT_ACTIVE2,GlobalTileMap.SPAWN_POINT_INACTIVE2)
	#set current
	GlobalTileMap.replace_current_screen_tile(view_map_main,GlobalTileMap.SPAWN_POINT_INACTIVE,GlobalTileMap.SPAWN_POINT_ACTIVE)
	GlobalTileMap.replace_current_screen_tile(view_map_main,GlobalTileMap.SPAWN_POINT_INACTIVE2,GlobalTileMap.SPAWN_POINT_ACTIVE2)

func update_spawnstart_new_game():
	GlobalTileMap.replace_current_screen_tile(view_map_main,GlobalTileMap.SPAWN_POINT_INACTIVE,GlobalTileMap.SPAWN_POINT_ACTIVE)
	GlobalTileMap.replace_current_screen_tile(view_map_main,GlobalTileMap.SPAWN_POINT_INACTIVE2,GlobalTileMap.SPAWN_POINT_ACTIVE2)
	var spawn=find_node(GlobalPersist.player_spawn_point,true,false)
	if spawn!=null:
		spawn.disable(true)
		
func game_over():
	var code=GlobalMethods.get_highscore_code_string()
	var code2=GlobalMethods.get_highscore_code_string2()
	
	var s1={"text":"\n[center][color=#C000C0]Game Over[/color][/center]\n\n[color=#00C0C0]Score -[/color][color=#C0C0C0]" + str(GlobalPersist.score)+ "\n[/color][color=#00C0C0]Humans Killed -[/color][color=#C0C0C0]" + str(Globals.humans_killed)+	"[/color][color=#00C0C0]\nBio Mutants Killed -[/color][color=#C0C0C0]" + str(Globals.biogrowths_killed)+"[/color]" + "[color=#00C0C0]\n\n"+code+"[/color]"}
	
	var string=[s1,{"text":"\n"+code2}]
		
	Globals.is_game_over=true
	yield(get_tree().create_timer(4), "timeout")
	GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.TUTORIAL,"gameover",string,true)
	
func _on_smart_bomb():
	#should really just use the signal rather than hard linking them
	_update_smart_ui()
	#yield(get_tree().create_timer(0.3), "timeout")
	var x=GlobalTileMap.get_local_from_global(player.global_position)/GlobalTileMap.found_screen_size
	fullscreen_shock(true,Vector2(x.x,1.0-x.y))

func _on_SpawnStart_tesla_finished():
	if tesla_old_tiles!=null && tesla_old_tiles.size()>0:
		for t in tesla_old_tiles:
			GlobalTileMap.replace_single_tile(view_map_main,t[0],t[1],t[2])
	update_spawnstart_spin_indicator()
	_player_normal_after_spawnpoint()

func _on_SpawnStart_spawn_point_enabled(_point_position) -> void:
	Globals.bullet_time=0.05;
	GlobalTileMap.set_slomo(true)
	var lpos=GlobalTileMap.get_local_from_global(_point_position)
	fullscreen_shader_grey_advanced(true,true,0.0,Vector2(lpos.x,672.0-lpos.y),125)

func _on_SpawnStart_centre(_point_position) -> void:
	#hit the centre
	#if locked then birth/death animation is playing
	#if null then animation has finished or never started as animation deletes item
	#if already have active tile then no need for tesla again
	var spawn=find_node(GlobalPersist.player_spawn_point,true,false)
	#player.lock_player=true
	Globals.bullet_time=0.05
	GlobalTileMap.set_slomo(true)
	fullscreen_shaders(false,false)

	#if we are in centre then save it, otherwise just let bullet time happen
	#only need x position
	if !spawn.is_rex2:
		tesla_old_tiles=[]
		new_tesla=tesla.instance()
		#our player is offset
		new_tesla.global_position=spawn.global_position-Vector2(47,42)
		
		new_tesla.connect("tesla_finished",self,"_on_SpawnStart_tesla_finished")
	else:
		tesla_old_tiles=[]
		new_tesla=tesla_rex2.instance()
		#our player is offset
		new_tesla.global_position=spawn.global_position+Vector2(0,42)
		
		new_tesla.connect("tesla_rex2_finished",self,"_on_SpawnStart_tesla_finished")
		
		tesla_old_tiles=GlobalTileMap.replace_current_screen_tile(view_map_main,GlobalTileMap.SPAWN_POINT_INACTIVE2,-1)
	
	var loc=GlobalMethods.assign_temp_location()
	loc.add_child(new_tesla)
	if GlobalPersist.graphics_type!=0 && spawn.zoom_on_save:
		GlobalTileMap.zoom_camera(true,2.0,_point_position)
	new_tesla.start()
	player.enable_lock(true)
		
func _on_SpawnStart_spawn_point_exit(_point_position) -> void:
	if new_tesla==null:
		_player_normal_after_spawnpoint()
	
func _player_normal_after_spawnpoint():
	player.enable_lock(false)
	Globals.bullet_time=1.0
	GlobalTileMap.set_slomo(false)
	fullscreen_shader_grey_advanced(false,false)
	GlobalTileMap.zoom_camera(false)
	
func _on_new_weapon_shipmove(_point_position) ->void:
	var lpos=GlobalTileMap.get_local_from_global(_point_position)
	fullscreen_shader_grey_advanced(true,true,0.0,Vector2(lpos.x,672.0-lpos.y),125)
	
func _on_new_weapon_ended():
	_player_normal_after_spawnpoint()
	
func _start_animate_pickup():
	var item=extra.instance()
	var loc=GlobalMethods.assign_temp_location()
	loc.add_child(item)
	item.global_position=player.global_position
	return item
	
func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("debug") && Globals.debug:
		camera_debug=!camera_debug
		
		if debug_overlay!=null:
			debug_overlay.set_visible(camera_debug)

# these should all be in hud, 
# or the hud parts signalled out
# but it's too late now
func refresh_weapons():
	_update_smart_ui()
	_update_weapon_ui()

func refresh_display():
	refresh_weapons()
	hud_ui.new_lives_left(Globals.lives,false)
	
func extra_life():
	Globals.lives+=1
	var x=extra.instance()
	var loc=GlobalMethods.assign_temp_location()
	loc.add_child(x)
	x.global_position=player.global_position
	var to:Vector2=_camera_controller.global_position+Vector2(670,16) #just somewhere near top right
	x.start_life(to)
	yield(get_tree().create_timer(0.5), "timeout")
	hud_ui.new_lives_left(Globals.lives,true)

func _on_life_lost():
	if Globals.is_game_over && Globals.training_mode:
		#if already died don't show this again
		return
		
	Globals.lives-=1
	if Globals.lives<0:
		game_over()
	else:
		_update_lives_ui()
		
func _update_weapon_ui():
	var weapon_list=player.weapons.get_weapons_status()	
	hud_ui.update_weapons_display(weapon_list)
	
func weapons_titilate():
	#this is really for the tutorial/weapons demo
	hud_ui.on_energy_pickup()
	yield(get_tree().create_timer(1.5), "timeout")
	hud_ui.tutorial("weapons")
	yield(get_tree().create_timer(2), "timeout")
	hud_ui.tutorial("off")

func _on_weapon_status():
	refresh_weapons()
	
func _update_lives_ui():
	#should really just use the signal rather than hard linking them
	hud_ui.new_lives_left(Globals.lives)
	
func _update_smart_ui():
	hud_ui.new_smarts_left(GlobalPersist.smart_bombs)
	
func new_score():
	_update_score_ui()
	
func _update_score_ui():
	hud_ui.new_score(GlobalPersist.score)

func get_wl1(index:int):
	var s= str(GlobalPersist.weapon_level[index])
	return s
	
func _on_new_weapon(_point_position, _weapon) ->void:
	player.enable_lock(true)
	Globals.bullet_time=0.05
	GlobalTileMap.set_slomo(true)
	var lpos=GlobalTileMap.get_local_from_global(_point_position)
	fullscreen_shader_grey_advanced(true,true,0.0,Vector2(lpos.x,672.0-lpos.y),125)
	#this is sometimes prior as called in parallel, so use below method from signal
	_update_weapon_ui()

func _on_acquired_weapon(_position, _weapon):
	#weapon actually acquired
	_update_weapon_ui()
	
func change_sprite_mode():
	player.set_graphics_sprite_type()
	_rex_room_manager.set_graphics_sprite_type(container_disposable)
	
func _on_PlayerAtEdge(entering,direction_shape):
	#print("Player received signal is entering: "+str(entering) + " shape "+str(direction_shape))
	_rex_room_manager.player_at_edge_signal(entering,direction_shape)

func _on_Rex_RoomManager_room_oxygen_warning(_seconds:float=10.0) -> void:
	#warning is 10 seconds left
	#play sound, sound claxon, show timer, greyscale room or flash red
	#maybe...
	#depleted will kill everything and revert to normal
	if GlobalPersist.has_oxygen:
		hud_ui.flash(Globals.COLOURS.green)
	else:
		hud_ui.flash()
		AudioManager.play_sfx(Globals.SFX_GAME_WARNING)

func _on_Rex_RoomManager_room_oxygen_depleted() -> void:
	#the room manager has killed every room enemy
	#here we let player know
	#if has oxygen mask then all is well, otherwise death
	#reset screen back to normal
	var dead=player.room_oxygen_depleted()
	if !dead:
		fullscreen_shaders(false,false)

func _on_Rex_RoomManager_new_room(time_limit) -> void:
	#currently only used by room manager for signalling to remove any screen filter
	#called by room manager _start_room
	bubble_achievement_reset()
	hud_ui.new_room(time_limit)
	fullscreen_shaders(false,false)

func _on_pickup(pickup_type,location, meta):
	match pickup_type:
		Globals.PICKUP.LARGE_ENERGY:
			AudioManager.play_sfx(Globals.SFX_GAME_PICKUP)
			bubble_achievement_increment()
			hud_ui.on_energy_pickup()
			player.weapons.increase_current_weapon_energy(Globals.ENERGY_LARGE)
			_update_weapon_ui()
		Globals.PICKUP.SMALL_ENERGY:
			AudioManager.play_sfx(Globals.SFX_GAME_PICKUP)
			bubble_achievement_increment()
			hud_ui.on_energy_pickup()
			player.weapons.increase_current_weapon_energy(Globals.ENERGY_SMALL)
			_update_weapon_ui()
		Globals.PICKUP.OXYGEN:
			AudioManager.play_sfx(Globals.SFX_GAME_PICKUPGOOD)
			var item=_start_animate_pickup()
			#var to:Vector2=_camera_controller.global_position+Vector2(500,0) #just somewhere near top right
			var to=_calc_canvas_offset(player,Vector2(545,0))
			item.start_oxygen(player.position+to)
			yield(get_tree().create_timer(0.75), "timeout")
			acquire_oxygen_mask()
		Globals.PICKUP.RANDOMITEM:
			_do_random(location)
		Globals.PICKUP.SMARTBOMB:
			AudioManager.play_sfx(Globals.SFX_GAME_PICKUPGOOD)
			GlobalPersist.smart_bombs+=1
			if GlobalPersist.smart_bombs>3:
				GlobalPersist.smart_bombs=3
			else:
				var item=_start_animate_pickup()
				#var to:Vector2=_camera_controller.global_position+Vector2(900,0) #just somewhere near top right
				#var to=_flip_room_manager.get_room_topleft_global(GlobalTileMap.current_room_id)+Vector2(900,0)
				var to=_calc_canvas_offset(player,Vector2(875,0))
				item.start_smart(player.position+to)
				yield(get_tree().create_timer(0.75), "timeout")
				_update_smart_ui()
		Globals.PICKUP.HINT:
			GlobalMethods.do_hint(location, meta)
		_:
			print("ERROR. pickup type unknown "+str(pickup_type))

func acquire_oxygen_mask():
	player.has_oxygen_mask(true)
	hud_ui.show_oxygen(true)

func get_flip_controller():
	return _flip_room_manager
	
func _calc_canvas_offset(start_object:Node2D,screen_pos:Vector2):
	var pos = start_object.get_global_transform_with_canvas()
	var origin = pos.get_origin()
	var new_offset = screen_pos-origin
	return new_offset
	
func _on_Player_new_room_warp(location) -> void:
	#player wishes to go to a room
	#main_camera.scale=Vector2(1,1)
	var room_id=_flip_room_manager.get_roomid_from_global(location)
	_flip_room_manager.change_room(room_id)

func _on_changed_new_room(room_id,newpos):
	new_room_changed(room_id,newpos)
	
func new_room_changed(room_id,newpos,_include_adjacent_rooms:=false):
	var room_data=_map_controller.get_room_data(room_id)
	
	var tl=newpos
	var br=_flip_room_manager.get_room_bottomright_global(room_id)
	#var temp_room
	var tl1
	var br1
	#scrolling rooms should activate all rooms
	#ok for rex as small areas (2 rooms)
	#will need to change if many rooms
	if room_data.has("activate_rooms"):
		for r in room_data["activate_rooms"]:
			tl1=_flip_room_manager.get_room_topleft_global(r)
			br1=_flip_room_manager.get_room_bottomright_global(r)
			if tl1.x<tl.x:
				tl.x=tl1.x
			if tl1.y<tl.y:
				tl.y=tl1.y
			if br1.x>br.x:
				br.x=br1.x
			if br1.y>br.y:
				br.y=br1.y
	var size=Vector2(br.x-tl.x,br.y-tl.y)
	_rex_room_manager.changed_new_room(room_id,Rect2(tl,size),room_data)
	_camera_controller.scale=Vector2(1,1)


func screen_shake(trauma_amount:float):

	var camera=_flip_room_manager.get_active_camera()
	if _screen_shake==null || camera==null:
		print("ERROR. camera does not have ScreenShake node for screen shake")
	else:
		_screen_shake.add_trauma(trauma_amount,camera)
		#shake.start(main_camera, priority, total_shake_time, shakes_per_second, shake_half_height)


func world_modulate_set(status:bool=false,colour:Color=Color.white):
	emit_signal("modulate_set",status,colour)

func world_modulate_change_alpha(value:float):
	emit_signal("modulate_change",value)

func _on_reset_last_checkpoint():
	player.birth()
	
func _on_quit():
	get_tree().paused=true
	yield(get_tree(),"idle_frame")
	yield(get_tree(),"idle_frame")
	SceneManager.changeto_scene("res://scenes/Menu.tscn","basicfade",true)
#change_room(room_id:int, transition:int=-1, transition_length:float=-1, force_transition:bool=false)

func _set_weapons_from_load():
	#sync the weapons from the loaded data, i.e. on loading world data
	#var current=GlobalPersist.current_weapon
	var weapon:Weapon
	var level
	var last_level=1
	var count=GlobalPersist.weapon_level.size()
	if count!=5:
		GlobalPersist.weapon_level=[0,0,0,0,0]
	for i in range(5):
		level=GlobalPersist.weapon_level[i]
		if i==0:
			#alway acquire single bullet
			weapon=player.weapons.acquire_weapon(i+1,true,true)
			level=max(1,level)
		else:
			if level>0:
				weapon=player.weapons.acquire_weapon(i+1)
		
		if level>0:
			last_level=level
			
	player.weapons.current_weapon=weapon
	player.weapons.set_weapons_firing_status()
	
	for _v in range(1,last_level):
		weapon.level_up(5)		#give max allowed before going to next level
	pass
