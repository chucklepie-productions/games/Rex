extends KinematicBody2D

var velocity_queen:=Vector2.ZERO

func _ready() -> void:
	stop()

func _physics_process(delta: float) -> void:
	velocity_queen.y+=Globals.GRAVITY*delta
	velocity_queen=move_and_slide(velocity_queen,Vector2.UP)

func stop():
	propagate_call("set_process",[false])
	propagate_call("set_physics_process",[false])
		
func start():
	propagate_call("set_process",[true])
	propagate_call("set_physics_process",[true])



