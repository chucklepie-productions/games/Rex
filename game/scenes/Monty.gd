extends Node2D


func _ready() -> void:
	pass # Replace with function body.

func enable(_flag):
	$Particles2D.emitting=true
	$Particles2D2.emitting=true
	
func disable(_flag):
	$Particles2D.emitting=true
	$Particles2D2.emitting=true
	
func _on_Area2D_body_entered(_body: Node) -> void:
	AudioManager.play_sfx(Globals.SFX_GAME_WARNING)
	find_node("AnimationPlayer").play("miner")
