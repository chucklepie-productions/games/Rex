extends Node2D

const OFFSET=14
var help_shown=false

onready var menu=$Menu/VBoxContainer
onready var menu2=$MenuNew/VBoxContainer
onready var menu_game=$MenuNew/VBoxContainer
onready var splat=$Splatter
onready var rex=$SpriteRex
onready var wink=$SpriteRex/AnimatedSprite

#godot shiteness on focus and keyboard/mouse dictates 
#i have all this stupid code
var menu_cont=null
var menu_newgame=null
var menu_tutorial=null
var menu_options=null
var menu_exit=null
var menu_normal=null
var menu_easy=null
var menu_simple=null
var menu_back=null
var play_focus_sound:=false		#whether to play focus sfx, not on load and not when going backwards

func _ready() -> void:
	Globals.init()
	_reset_splats()
	Globals.in_menu=true
	$Menu.visible=false
	$MenuNew.visible=false
	wink.visible=false
	_update_wink()
	#rex.modulate=Color(1,1,1,0)
	
	#items that may change
	menu_tutorial=menu.find_node("ButtonTutorial")
	menu_cont=menu.find_node("ButtonContinue")
	menu_newgame=menu.find_node("ButtonNew")
	menu_normal=menu_game.find_node("ButtonNormal")
	menu_options=menu.find_node("ButtonOptions")
	menu_exit=menu.find_node("ButtonExit")
	menu_easy=menu_game.find_node("ButtonEasy")
	menu_simple=menu_game.find_node("ButtonSimple")
	menu_back=menu_game.find_node("ButtonBack")
	
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	_load_config()

	yield(get_tree().create_timer(3.0), "timeout")
	_do_menu_splatter()
	yield(get_tree().create_timer(3), "timeout")

	AudioManager.play_music(Globals.MUSIC_MENU,true,2)
	_do_rex()
	_do_abb(8,10)
	
	#try to fix bug with mouse not moving causing
	#mouse pointer to not set as custom in options gui
	var custom= "display/mouse_cursor/custom_image"
	var icon=ProjectSettings.get_setting(custom)
	ProjectSettings.set_setting(custom,null)
	#get_viewport().warp_mouse(Vector2(50,50))
	#get_viewport().warp_mouse(Vector2(150,150))
	ProjectSettings.set_setting(custom,icon)

	
func _load_config():
	menu_cont.visible=GlobalPersist.has_continue
	menu_tutorial.visible=GlobalPersist.has_continue
	GlobalMethods.screen_change()
	
func _do_menu_splatter():
	#splatmode is normal, otherwise it just resets
	_reset_splats()
	AudioManager.play_sfx(Globals.SFX_MENU_PUMP)
	yield(get_tree().create_timer(1.0), "timeout")
	for s in splat.get_children():
		s.start()
	yield(get_tree().create_timer(1.5), "timeout")
	
func _reset_splats():
	for s in splat.get_children():
		s.reset()
	
func _do_menu():
	#reset some stuff
	Globals.game_ended=false
	Globals.highscore_allowed=false
	$MenuNew.visible=false
	$Menu.visible=true
		
	if GlobalPersist.has_continue:
		menu_cont.grab_focus()
	else:
		menu_newgame.grab_focus()
		
	OptionsAndAchievements.increment_log("single",1)
	OptionsAndAchievements.increment_achievement("wmd",1,"BULLET_SINGLE")
	OptionsAndAchievements.reset_achievement("onslaught",false)

func _update_hint(info):
	$hint.text=info

#internal code
func _new_game(mode):
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	_do_confirm_sound()
	GlobalPersist.init(true,false,false,true)
	GlobalPersist.game_mode=mode
	
	Globals.highscore_allowed=false
	if mode==GlobalPersist.GAMEMODE.NORMAL:
		Globals.highscore_allowed=true
		
	#if has_continue then we've completed training as only set at end of training
	if !GlobalPersist.has_continue:
		#start training
		#GlobalPersist.training_complete=false
		GlobalMethods.do_hint(null,"tutorial_welcome",false,false)
		yield(GlobalMethods,"dialog_finished")

		SceneManager.game_scene_sequence=SceneManager.SCENE_GAME_MODE.GAME_INTRO
		OptionsAndAchievements.do_intro(true)
	else:
		#has continue so saved after tutorial finished
		#so new games start from scratch
		#and access to intro/tutorial available
		GlobalPersist.player_spawn_point="SpawnStart"
		Globals.ingame_time_start=Time.get_ticks_msec()
		SceneManager.game_scene_sequence=SceneManager.SCENE_GAME_MODE.GAME
		SceneManager.ready_fullgame()
		SceneManager.changeto_scene("res://scenes/Rex_Game_HUD.tscn")


#timer based
func _do_abb(mina:=12,maxa:=18):
	$TimerAbb.wait_time=rand_range(mina,maxa)
	$TimerAbb.start()


func _do_rex():
	yield(get_tree().create_timer(1.8),"timeout") 
	wink.visible=true
	$TimerWink.wait_time=0.3
	$TimerWink.start()
	_do_menu()

func _update_wink(mode:=-1):
	wink.modulate=GlobalMethods.get_game_mode_colour(mode)


#buttons and events
#events
func _on_Timer_timeout() -> void:
	#eye wink
	wink.play("wink")
	$TimerWink.wait_time=rand_range(12.0,20.0)
	wink.frame=0
	$TimerWink.start()

func _on_TimerMission_timeout() -> void:
	$AnimationPlayerR.play("reveal")
	$TimerMission.wait_time=14

func _on_TimerAbb_timeout() -> void:
	$AnimationPlayerA.play("abb")
	AudioManager.play_sfx(Globals.SFX_MENU_REX)

	_do_abb()

#top level buttons
#continue
func _on_ButtonContinue_pressed() -> void:
	_do_confirm_sound()
	yield(get_tree().create_timer(0.5), "timeout")	#below will reinitialise sound so wait
	Globals.highscore_allowed=false
	GlobalPersist.load_world_data()
	SceneManager.ready_fullgame()
	Globals.ingame_time_start=0
	SceneManager.game_scene_sequence=SceneManager.SCENE_GAME_MODE.GAME
	SceneManager.changeto_scene("res://scenes/Rex_Game_HUD.tscn")

func _on_ButtonNew_pressed() -> void:
	#new game menu
	_do_confirm_sound()
	$Menu.visible=false
	$MenuNew.visible=true
	menu_normal.grab_focus()

func _on_ButtonTutorial_pressed() -> void:
	#play tutorial, only visible after a new game has started
	_do_confirm_sound()
	AudioManager.play_music(Globals.MUSIC_INTRO,true)
	SceneManager.game_scene_sequence=SceneManager.SCENE_GAME_MODE.JUST_TUTORIAL
	SceneManager.ready_tutorial()
	SceneManager.changeto_scene("res://scenes/Rex_Game_HUD.tscn")

func _on_ButtonOptions_pressed() -> void:
	#options
	OptionsAndAchievements.show_options()

func _on_ButtonExit_pressed() -> void:
	_do_cancel_sound()
	get_tree().quit()

#second level buttons
#new game
func _on_ButtonNormal_pressed() -> void:
	_new_game(GlobalPersist.GAMEMODE.NORMAL)

func _on_ButtonEasy_pressed() -> void:
	_new_game(GlobalPersist.GAMEMODE.EASY)

func _on_ButtonSimple_pressed() -> void:
	_new_game(GlobalPersist.GAMEMODE.SIMPLE)

func _on_ButtonBack_pressed() -> void:
	_do_cancel_sound()
	_do_menu()

#hint text

func _do_confirm_sound():
	play_focus_sound=false	#don't play focus after this sound
	AudioManager.play_sfx(Globals.SFX_MENU_CONFIRM)

func _do_cancel_sound():
	play_focus_sound=false	#don't play focus after this sound
	AudioManager.play_sfx(Globals.SFX_MENU_CANCEL)
	
func _do_focus_sound():
	if !$NavigateTimerIgnore.is_stopped():
		#timer is playing so we want to ignore any more focus sounds for 0.1 second
		#to avoid mouse triggering too many times
		return
		
	if !play_focus_sound:
		play_focus_sound=true	#we've not played it so now allow it
		return
		
	#here so not coming back from a menu and not already playing
	$NavigateTimerIgnore.start()
	AudioManager.play_sfx(Globals.SFX_MENU_NAVIGATE)
	
#floating text
func _on_ButtonContinue_focus_entered() -> void:
	_do_focus_sound()
	_update_hint("Continuing a game disallows high-score entry, it may be added  if I secure the save data")
func _on_ButtonContinue_focus_exited() -> void:
	_update_hint("")

func _on_ButtonTutorial_focus_entered() -> void:
	_do_focus_sound()
	_update_hint("Revisit training, or perhaps hunt for things you may have missed ;-)")
func _on_ButtonTutorial_focus_exited() -> void:
	_update_hint("")

func _on_ButtonNormal_focus_entered() -> void:
	_do_focus_sound()
	_update_hint("Gameplay as presented in 1989. Note: progress (not achievements) will be lost on starting a new game. Finishing the game will put you in the high score table.")
	_update_wink(GlobalPersist.GAMEMODE.NORMAL)
func _on_ButtonNormal_focus_exited() -> void:
		_update_hint("")

func _on_ButtonEasy_focus_entered() -> void:
	_do_focus_sound()
	_update_hint("As Normal game, but with 99 lives and entry to the hi-score table on completing the game is disallowed. Try this mode instead of uninstalling!")
	_update_wink(GlobalPersist.GAMEMODE.EASY)
func _on_ButtonEasy_focus_exited() -> void:
	_update_hint("")

func _on_ButtonSimple_focus_entered() -> void:
	_do_focus_sound()

	_update_hint("As Easy game, but you cannot lose a life unless you are really, really bad ;-)\n\nMay help with those tricky achievements")
	_update_wink(GlobalPersist.GAMEMODE.SIMPLE)
func _on_ButtonSimple_focus_exited() -> void:
	_update_hint("")
func _on_ButtonNew_focus_entered() -> void:
	_do_focus_sound()
	_update_hint("Be the hero you've always wanted to be, choose Normal difficulty. Pick easy or simple for those after the 'experience' and get nothing but a fear of missing out")
func _on_ButtonNew_focus_exited() -> void:
	_update_hint("")
func _on_ButtonOptions_focus_entered() -> void:
	_do_focus_sound()
	_update_hint("Adjust settings and fix the annoying filters, watch the cut-scenes, view achievements and progress, see the live high-scores, and what is that 'B' channel")
func _on_ButtonOptions_focus_exited() -> void:
	_update_hint("")

func _on_ButtonBack_focus_entered() -> void:
	_do_focus_sound()
	wink.modulate=GlobalMethods.get_game_mode_colour()

#replicate keyboard from mouse
func _get_focus(menu_button:TextureButton,exiting:=false):
	if !exiting:
		_do_focus_sound()
	for c in menu.get_children():
		if c is TextureButton:
			c.release_focus()
	for c in menu2.get_children():
		if c is TextureButton:
			c.release_focus()
	yield(get_tree().create_timer(0.01), "timeout")
	if menu_button is TextureButton:
		menu_button.grab_focus()
		pass
	
func _on_ButtonContinue_mouse_entered() -> void:
	_get_focus(menu_cont)
func _on_ButtonNew_mouse_entered() -> void:
	_get_focus(menu_newgame)
func _on_ButtonTutorial_mouse_entered() -> void:
	_get_focus(menu_tutorial)
func _on_ButtonOptions_mouse_entered() -> void:
	_get_focus(menu_options)
func _on_ButtonExit_mouse_entered() -> void:
	_get_focus(menu_exit)
func _on_ButtonNormal_mouse_entered() -> void:
	_get_focus(menu_normal)
func _on_ButtonEasy_mouse_entered() -> void:
	_get_focus(menu_easy)
func _on_ButtonSimple_mouse_entered() -> void:
	_get_focus(menu_simple)
func _on_ButtonBack_mouse_entered() -> void:
	_get_focus(menu_back)


func _on_ButtonContinue_mouse_exited() -> void:
	_get_focus(menu_cont,true)
func _on_ButtonNew_mouse_exited() -> void:
	_get_focus(menu_newgame,true)
func _on_ButtonTutorial_mouse_exited() -> void:
	_get_focus(menu_tutorial,true)
func _on_ButtonOptions_mouse_exited() -> void:
	_get_focus(menu_options,true)
func _on_ButtonExit_mouse_exited() -> void:
	_get_focus(menu_exit,true)
func _on_ButtonNormal_mouse_exited() -> void:
	_get_focus(menu_normal,true)
func _on_ButtonEasy_mouse_exited() -> void:
	_get_focus(menu_easy,true)
func _on_ButtonSimple_mouse_exited() -> void:
	_get_focus(menu_simple,true)
func _on_ButtonBack_mouse_exited() -> void:
	_get_focus(menu_back,true)



func _on_ButtonExit_focus_entered() -> void:
	_do_focus_sound()
