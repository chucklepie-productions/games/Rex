extends Area2D
class_name bunny_item

export(bool) var is_duck=false
export(bool) var is_egg=false
export(bool) var is_deadduck=false
export(bool) var is_house=false
export(bool) var is_points=false
export(bool) var is_exit=false

signal exit_ended

func enable() -> void:
	if is_exit:
		$Timer.wait_time=rand_range(2,10)
		$Timer.start()

	var ani=""
	if is_duck:
		ani="duck"
	elif is_egg:
		ani="egg"
	elif is_deadduck:
		ani="dead"
	elif is_house:
		ani="house"
	elif is_points:
		ani="points"
	elif is_exit:
		ani="exit"
	else:
		print("ERROR. ANI.")
		
	if ani!="":
		$AnimatedSprite.play(ani)
		
func _on_Timer_timeout() -> void:
	emit_signal("exit_ended")
	queue_free()
