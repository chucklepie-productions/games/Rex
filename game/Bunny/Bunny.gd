extends Node2D

const START=Vector2(7,2)
const CELL=Vector2(16,16)
const MAP_SIZE=Vector2(32,21)
signal bunny_quit

enum MODE {MENU,GAME,SCORE}

#onready var map_b=$WindowDialogGame/Bunnies/TileMapBeginner
#onready var map_e=$WindowDialogGame/Bunnies/TileMapExpert
#onready var container=$WindowDialogGame/Bunnies/Items
#onready var player=$WindowDialogGame/Bunnies/Player
#onready var wmenu=$WindowDialogMenu
#onready var wgame=$WindowDialogGame
#onready var wscore=$WindowDialogScore

onready var map_b=$Game/Bunnies/TileMapBeginner
onready var map_e=$Game/Bunnies/TileMapExpert
onready var container=$Game/Bunnies/Items
onready var player=$Game/Bunnies/Player
onready var wmenu=$Menu
onready var wgame=$Game
onready var wscore=$Score

onready var new_item=preload("res://Bunny/Item.tscn")

var map:TileMap=null
var exit_available=true
var moving=false
var direction=Vector2.ZERO
var old_direction=direction
var lives=3 
var tot_score=0
var hiscore=0
var score=0
var ducks=0		#chick -150
var eggs=0		#egg 150 
var tot_eggs=0
var dead=0		#bad egg -50
var current_mode=MODE.MENU
var mbeginner=true
var level=1
var temp=0
func _on_MovePlayer_timeout() -> void:
	#move every second
	#print("moving: "+str(direction))
	player.position+=(direction*CELL)
	$MovePlayer.start()
	
func _ready() -> void:
	randomize()
	_menu()
	
func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		return
	#if !event.pressed:
	#	return
		
	var x=0
	var y=0
	var handled=false
	old_direction=direction
	if event.is_action_pressed("bunny_quit"):
		_clear_enemies()
		if current_mode==MODE.MENU:
			emit_signal("bunny_quit")
			set_process_unhandled_input(false)
		else:
			_menu()
		
	if current_mode==MODE.MENU:
		level=1
		exit_available=false
		lives=3
		tot_score=0
		tot_eggs=0
		if event.is_action_pressed("bunny_beginner"):
			handled=true
			start_beginner()
		if event.is_action_pressed("bunny_expert"):
			handled=true
			start_expert()
			
	if current_mode==MODE.SCORE:
		if event.is_action_pressed("bunny_continue"):
			handled=true
			_next()
				
	if current_mode!=MODE.GAME:
		pass
	if current_mode==MODE.GAME:
		
		if event.is_action_pressed("ui_left"):
			x=-1
		elif event.is_action_pressed("ui_right"):
			x=1
		elif event.is_action_pressed("ui_up"):
			y=-1
		elif event.is_action_pressed("ui_down"):
			y=1
		else:
			temp+=1
			
		if x!=0 || y!=0:
			temp+=1
			handled=true
			if !moving:
				moving=true
				player.get_node("Sprite").play()
				$MovePlayer.start()
				_new_timer()
				
			direction=Vector2(x,y)
	
	if handled:
		get_tree().set_input_as_handled()
	#else:
	#	print("not handled "+event.as_text())
	
func _on_Timer_timeout() -> void:
	_new_enemy()
	_new_timer()

func _menu():
	current_mode=MODE.MENU
	wgame.hide()
	wscore.hide()
	wmenu.show()

func start_beginner():
	map=map_b
	mbeginner=true
	_set_map(map_b,true)
	_set_map(map_e,false)
	_start()
	
func start_expert():
	map=map_e
	mbeginner=false
	_set_map(map_e,true)
	_set_map(map_b,false)
	_start()

func _start():
	current_mode=MODE.GAME
	ducks=0
	eggs=0
	dead=0
	score=0
	wgame.show()
	wscore.hide()
	wmenu.hide()
	wgame.get_node("Bunnies/Carrots").region_rect.size.x=16*level
	
	player.get_node("Death").visible=false
	var start=map.global_position
	direction=Vector2(0,0)
	old_direction=direction
	moving=false
	player.get_node("Sprite").stop()
	player.get_node("Sprite").frame=0


	old_direction=direction
	player.global_position=start+(START*CELL)+(CELL/2.0)
	
func _new_timer():
	$Timer.stop()
	var minv=1
	var maxv=max(7-level,1)
	if maxv<2:
		minv=0.25
	$Timer.wait_time=rand_range(minv,maxv)
	$Timer.start()
	
func _new_enemy():
	var pos=_get_empty_slot()
	
	if pos==Vector2.ZERO:
		print("ERROR no cell")
		return
	
	var item=randi()%6
	var pickup=new_item.instance()
	var add=true
	match item:
		0:
			pickup.is_duck=true
		1:
			pickup.is_egg=true
		2:
			pickup.is_deadduck=true
		3:
			pickup.is_house=true
		4:
			pickup.is_points=true
		5:
			if exit_available:
				pickup.is_exit=true
				exit_available=false
				pickup.connect("exit_ended",self,"_exit_ended")
			else:
				pickup.queue_free()
				add=false
	
	if add:
		container.add_child(pickup)
		pickup.global_position=pos
		pickup.enable()

func _exit_ended():
	exit_available=true
	
func _set_map(map:TileMap,enable:bool):
	map.visible=enable
	map.set_collision_layer_bit(1,enable)
	map.set_collision_mask_bit(0,enable)
	
func _get_empty_slot():
	#map.get_used_cells()
	var found=false
	var max_tries=MAP_SIZE.x*MAP_SIZE.y
	var pos=Vector2.ZERO
	while !found:
		pos=Vector2(randi()%int(MAP_SIZE.x),randi()%int(MAP_SIZE.y))
		if map.get_cellv(pos)==TileMap.INVALID_CELL:
			var start=map.global_position
			pos=(pos*CELL)+start+(CELL/2.0)
			if !_find_object(pos):
				found=true
		else:
			max_tries-=1
			if max_tries<1:
				break
	
	if found:
		return pos
	else:
		return Vector2.ZERO

func _find_object(pos):
	for item in container.get_children():
		if item.global_position==pos:
			return true
		if item.global_position==player.global_position:
			return true
	return false


func _on_Player_area_entered(area: Area2D) -> void:
	#a pickup
	if current_mode==MODE.GAME:
		if area.is_deadduck:
			dead+=1
		elif area.is_duck:
			ducks+=1
		elif area.is_egg:
			eggs+=1
		elif area.is_points:
			score+=100
		elif area.is_house:
			_end_level(true)
		elif area.is_exit:
			level+=1
			_end_level(false)
		else:
			print("ERROR UNKNOWN")
		area.queue_free()

func _on_Player_body_entered(body: Node) -> void:
	#the map
	if current_mode==MODE.GAME:
		lives-=1
		_end_level(true)
	
func set_hiscore(score):
	hiscore=hiscore if hiscore<9999 else 9999

func _clear_enemies():
	for item in container.get_children():
		item.queue_free()
	
func _end_level(is_dead):
	$Timer.stop()
	$MovePlayer.stop()
	_clear_enemies()
	if is_dead:
		var a=player.get_node("Death")
		a.visible=true
		a.play("default")
		yield(get_tree().create_timer(5), "timeout")
		a.visible=false
	
	score+=(eggs*150)
	score-=(ducks*150)
	score-=(dead*50)
	tot_score+=score
	tot_score=tot_score if tot_score<9999 else 9999
	
	tot_eggs+=eggs
	var s=find_node("score")
	var e=find_node("eggs")
	var b=find_node("bad")
	var c=find_node("chicks")
	var te=find_node("tot_eggs")
	var ts=find_node("tot_score")
	var l=find_node("lives")
	var m=find_node("continue")
	var h=find_node("hiscore")
	s.text="Score for this round: " + str(score)
	e.text="Good eggs collected:" + str(eggs)
	b.text="Bad eggs collected:" + str(dead)
	c.text="Chicks stood on:" + str(ducks)
	te.text="TOTAL GOOD EGGS COLLECTED " + str(tot_eggs)
	ts.text="TOTAL SCORE " + str(tot_score)
	l.text="BUNNIES LEFT " + str(max(0,lives))
	if tot_score>hiscore:
		h.text="NEW HIGH SCORE!"
		OptionsAndAchievements.increment_achievement("retro",1)
		var hs=find_node("score3")
		if hs!=null:
			hs.text="Hi-Score " + str(hiscore).pad_zeros(4)
		else:
			pass
	else:
		h.text=""
		
	if lives<0:
		m.text="GAME OVER. SCORE: " + str(tot_score) + ". PRESS ENTER"
	else:
		m.text="PRESS ENTER. Q QUITS"
	
	wgame.hide()
	wmenu.hide()
	current_mode=MODE.SCORE
	#wscore.popup_centered()
	wscore.show()

func _next():
	_clear_enemies()
	if lives<0:
		_menu()
	else:
		if mbeginner:
			start_beginner()
		else:
			start_expert()
