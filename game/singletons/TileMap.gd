extends Node

signal room_ready(room_id,pos,room_Data)

#spinning or unspinning for rex1 and rex2
const SPAWN_POINT_INACTIVE = 152
const SPAWN_POINT_INACTIVE2 = 401
const SPAWN_POINT_ACTIVE=295
const SPAWN_POINT_ACTIVE2= 495
const LEVEL_WIDTH_SCREENS=13	#doesn't matter if there's less, just a number that is at least size of map
							#if we do rex2 then that is only 4 or 5 wide
enum DIRECTION {TOP,BOTTOM,LEFT,RIGHT}

#need to adjust tilesheet tiles speed animation for slomo, so this is the only way
onready var conveyor_anim:AnimatedTexture=preload("res://assets/graphics/tilesets/gree_conveyor/conveyor.tres")
onready var bucket1_anim:AnimatedTexture=preload("res://assets/graphics/tilesets/blue_bucket_up/bucket_down.tres")
onready var bucket2_anim:AnimatedTexture=preload("res://assets/graphics/tilesets/blue_bucket_up/bucket_up.tres")
onready var gravity_anim:AnimatedTexture=preload("res://assets/graphics/tilesets/gravityjump/gravityjump.tres")
onready var spawn_anim:AnimatedTexture=preload("res://assets/graphics/tilesets/spinning_spawn/spinning_animatedtexture.tres")
onready var water_anim:AnimatedTexture=preload("res://assets/graphics/tilesets/sea/sea.tres")
onready var pod_anim:AnimatedTexture=preload("res://assets/graphics/tilesets/shield_charge/shield_Charge.tres")

#nasty globals to make life easier
var current_player:KinematicBody2D
#pixels
var map_tile_size:int					#size in pixels (32,32)
var found_screen_size:Vector2			#size of viewport in pixels
var screen_tiles:Vector2				#how many tiles per screen
var main_camera:Camera2D				#the camera
var current_map:TileMap					#in case required outside of a collision
var current_control_map:TileMap
var current_destroyable_map:TileMap
var current_miniboss_map:TileMap
var current_intantkill_map:TileMap
var current_fg_map:TileMap
var current_rex2_backdrop:TileMap
var standard_canim_speed
var standard_b1anim_speed
var standard_b2anim_speed
var standard_gravityanim_speed
var standard_spawnanim_speed
var standard_wateranim_speed
var standard_podanim_speed
var current_room_id:=-1
var previous_room_id:=-1
var adjacent_rooms={}
var active_room_enemies=[]		#for smart bombs/room kill mainly
var camera_zoom
var rex_room_manager

func _init() -> void:
	initialise()
	
func _ready() -> void:
	standard_canim_speed=conveyor_anim.fps
	standard_b1anim_speed=bucket1_anim.fps
	standard_b2anim_speed=bucket2_anim.fps
	standard_gravityanim_speed=pod_anim.fps
	standard_spawnanim_speed=spawn_anim.fps
	standard_podanim_speed=pod_anim.fps
	standard_wateranim_speed=water_anim.fps
	
func initialise():
	#call this on starting a new game
	current_player=null
	main_camera=null
	current_map=null
	current_control_map=null
	current_room_id=-1
	adjacent_rooms={}
	active_room_enemies=[]
	camera_zoom=false
	rex_room_manager=null
	
func set_rex2_backdrop(is_visible:bool):
	#may be set from main menu so need check
	if current_rex2_backdrop:
		current_rex2_backdrop.visible=is_visible
	
func is_player_in_spawn_room():
	pass

func set_room_manager(mgr):
	#might not be ready for set_map_details
	rex_room_manager=mgr
		
func set_slomo(is_enabled,speed:=1):
	if is_enabled:
		conveyor_anim.fps=speed
		bucket1_anim.fps=speed
		bucket1_anim.fps=speed
		bucket2_anim.fps=speed
		gravity_anim.fps=speed
		spawn_anim.fps=speed
		water_anim.fps=speed
		pod_anim.fps=speed
	else:
		conveyor_anim.fps=standard_canim_speed
		bucket1_anim.fps=standard_b1anim_speed
		bucket2_anim.fps=standard_b2anim_speed
		gravity_anim.fps=standard_gravityanim_speed
		spawn_anim.fps=standard_spawnanim_speed
		water_anim.fps=standard_wateranim_speed
		pod_anim.fps=standard_podanim_speed
	
func get_empty_room_location(player_min_gap:=10):
	#mainly used for rex2 triangle
	#return an empty cell (main rex and instant kill only)
	#and not near player
	randomize()
	var tl=main_camera.global_position+Vector2(map_tile_size*3,map_tile_size*3)

	var startx=tl.x/map_tile_size
	var starty=tl.y/map_tile_size
	var player_pos:Vector2=get_cell_from_position(current_player.global_position)
	
	var items=[]
	for x in screen_tiles.x-6:
		for y in screen_tiles.y-6:
			var diff=(player_pos.distance_to(Vector2(startx+x,starty+y)))
			var item1=current_map.get_cell(startx+x,starty+y)
			var item2=current_intantkill_map.get_cell(startx+x,starty+y)
			#print(str(startx) + ","+str(starty)+":"+str(item))
			if item1 <=0 && diff>player_min_gap:
				items.append(Vector2(x,y))
			elif item2<=0  && diff>player_min_gap:
				items.append(Vector2(x,y))
	
	if items.size()==0:
		return Vector2.ZERO
		
	var choice=items[randi()%items.size()]
	return tl+(choice*map_tile_size)

func set_map_details(map:TileMap,ctrl_map:TileMap,destroyable_map:TileMap,miniboss_map:TileMap,instant_map:TileMap,fg_map:TileMap,rex2_backdrop:TileMap,screen_size_pixels:Vector2,tile_size:int, camera:Camera2D, player:KinematicBody2D):
	#set the size of our tiles (presume square) and 
	#calculate number per screen
	current_map=map
	current_rex2_backdrop=rex2_backdrop
	current_control_map=ctrl_map
	current_destroyable_map=destroyable_map
	current_miniboss_map=miniboss_map
	current_intantkill_map=instant_map
	current_fg_map=fg_map
	found_screen_size=screen_size_pixels
	map_tile_size = tile_size
	main_camera=camera
	current_player=player
	screen_tiles.x=found_screen_size.x/map_tile_size
	screen_tiles.y=found_screen_size.y/map_tile_size
	
	print("tile size: " + str(map_tile_size))
	print("tiles per screen: " + str(screen_tiles))

func set_room_edge_detection(is_on):
	if main_camera!=null:
		main_camera.room_edge_detection(is_on)
	else:
		print("ERROR. camera is null, are you in WIP demo screen?")
	
func get_room_id_from_position(location:Vector2):
	#with any given pixel location tell us the room number
	#top left is 0
	var roomid=-1
	var posx=int(location.x/found_screen_size.x)
	var posy=int(location.y/found_screen_size.y)
	
	roomid=posy*LEVEL_WIDTH_SCREENS+posx	
	return roomid
	
func get_orthogonal_rooms(id:int):
	var rooms={
		"up":null,
		"down":null,
		"left":null,
		"right":null
	}
	var maxleft=id%LEVEL_WIDTH_SCREENS
	var left=id-1
	var right=id+1
	var up=id-LEVEL_WIDTH_SCREENS
	var down=id+LEVEL_WIDTH_SCREENS
	rooms["up"]=up if up>=0 else null
	rooms["down"]=down
	rooms["left"]=left if maxleft>0 else null
	rooms["right"]=right if maxleft!=LEVEL_WIDTH_SCREENS-1 else null
	return rooms
	
func get_local_from_global(location:Vector2):
	#get screen coordinate from global
	var size=screen_tiles*map_tile_size
	var x = fmod(location.x,size.x)
	var y=fmod(location.y,size.y)
	return Vector2(x,y)

#move all this to rex room manager
#so only room manager manages rooms
#tilemap just deals with stuff in the room
func map_change_room(new_room_id:int,newpos:Rect2,room_data:Dictionary) -> void:
	#get the area to activate (may be more than one room)
	#then set it up
	#two parts: enabling/disabling objects and setting transient room enemies
	#first is done only on a new screen, second is done at same time if flip
	#but if scrolling need to adjust to poll via scroll camera

	_level_setup(new_room_id,newpos,room_data)
	
func _level_setup(new_room_id:int,newpos:Rect2,room_data):
	#level is passed in camera, which is top left
	active_room_enemies=[]
	#clear out transient items
	previous_room_id=current_room_id
	current_room_id=new_room_id
	adjacent_rooms=get_orthogonal_rooms(current_room_id)
	refresh_map_objects("Container_Level_Transient",true,false,Rect2(-1,-1,-1,-1),false)
	
	#disable all items and enable for current room
	var root=get_tree().get_root()
	var children
	#Globals.debugprint("Disabling all items of Map_Containers_Disableable")
	children=root.find_node("Map_Containers_Disableable",true,false)
	if children!=null:
		for container in children.get_children():
			#Globals.debugprint("Disabling items in container " + container.name)
			refresh_map_objects(container.name,false,true,newpos,true)
	else:
		print("Error. No containers found")

	emit_signal("room_ready",current_room_id,adjacent_rooms,room_data)
		
func refresh_map_objects(node_name,free_item:bool, disable_item:bool, exclude_area:Rect2, enable_if_current:bool):
	var root=get_tree().get_root()
	var start=root.find_node("Map_Containers_Disableable",true,false)
	var child

	var container=start.find_node(node_name,true,false)
	
	if container.name=="TestRoom":
		pass
		
	#exclude area is the current room (or rooms for scrolling)
	#we want to enable these and disable others
	if container!=null && container.get_child_count()>0:
		for i in range(container.get_child_count(),0,-1):
			child=container.get_child(i-1)
			if not _is_in_exclude_area(child.global_position,exclude_area):
				if disable_item:
					if child.has_method("disable"):
						#node_disabled+=(child.name + ", ")
						child.call("disable",false)
					else:
						print(child.name + " Error. disable method does not exist")
				if free_item:
					child.queue_free()
			else:
				if enable_if_current:
					if child is Enemy && child.has_died:
						print(child.name + " has been killed already, not spawning")
					else:
						#node_enabled+=(child.name + ", ")
						if child.has_method("enable"):
							#Globals.debugprint(child.name + " enable")
							child.call("enable",true)
							active_room_enemies.append(child)
						else:
							print(child.name+" Error. enable method does not exist")
		
	else:
		if container==null:
			print("Error. Node name does not exist")
		else:
			print("No children "+container.name + ". If says transient ok as this is transient data :)")

func _is_in_exclude_area(child_global_position,exclude_area:Rect2):
	#find if this position is in the room area specified
	#-1 means always, this is currently done for transient areas to always 
	#ask to delete
	if exclude_area.position.x<0:
		return false
	
	#var width=screen_tiles.x*map_tile_size
	#var height=screen_tiles.y*map_tile_size
	#var screen=Rect2(exclude_room_start,Vector2(width,height))
	if exclude_area.encloses(Rect2(child_global_position.x,child_global_position.y,1,1)):
		return true
	else:
		return false

func is_tile_in_current_screen(map:TileMap,tile:int):
	var startx=main_camera.global_position.x/map_tile_size
	var starty=main_camera.global_position.y/map_tile_size
	for x in screen_tiles.x:
		for y in screen_tiles.y:
			var item=map.get_cell(startx+x,starty+y)
			#print(str(startx) + ","+str(starty)+":"+str(item))
			if item == tile:
				return true
	return false
	
func replace_single_tile(map:TileMap,cellx,celly,new_tile_id):
	var ret=0
	if map!=null:
		ret=map.get_cell(cellx,celly)
		map.set_cell(cellx,celly,new_tile_id)
	else:
		if current_map!=null:
			ret=current_map.get_cell(cellx,celly)
			current_map.set_cell(cellx,celly,new_tile_id)
		else:
			print("ERROR. current_map is null")
	return 1 if ret>=0 else 0
	
func set_tiles_from_array(map:TileMap,tile:int,cell_positions:Array):
	#replace tiles from array
	#-1 to delete
	if !map:
		return
	if !cell_positions:
		return
	
	for t in cell_positions:
		map.set_cell(t[0],t[1],tile)
		
func replace_all_tiles(map:TileMap,from:int,to:int):
	var all_tiles=map.get_used_cells_by_id(from)
	if all_tiles!=null:
		for tile in all_tiles:
			map.set_cell(tile.x,tile.y,to)

func replace_current_screen_tile(map:TileMap,from:int, to:int):
	#using current screen find tiles 'from' and change to 'to'
	#in the supplied tilemap
	#returns an array of the originals (location+tile)
	var found=[]
	var startx=main_camera.global_position.x/map_tile_size
	var starty=main_camera.global_position.y/map_tile_size
	for x in screen_tiles.x:
		for y in screen_tiles.y:
			var item=map.get_cell(startx+x,starty+y)
			#print(str(startx) + ","+str(starty)+":"+str(item))
			if item == from:
				map.set_cell(startx+x,starty+y,to)
				found.append(Vector3(startx+x,starty+y,from))
				#print("replaced")
				
	return found

func get_tile_from_collision(col:KinematicCollision2D, position:Vector2):
	if col==null:
		return 0
		
	var tile_pos = col.collider.world_to_map(position)
	# Find the colliding tile position
	tile_pos -= col.normal
	# Get the tile id
	var tile_id = col.collider.get_cellv(tile_pos)
	return tile_id
	
func get_cell_from_position(position:Vector2):
	#the map does not matter as all are the same size and position
	return current_map.world_to_map(position)
	
func get_tile_at_position(map:TileMap,position:Vector2, offset=Vector2.ZERO):
	var pos=map.world_to_map(position)
	pos-=offset
	var id=map.get_cellv(pos)
	return id
	
func get_global_from_cell(map:TileMap,location:Vector2):
	if map!=null:
		return map.map_to_world(location)
	else:
		if current_map!=null:
			return current_map.map_to_world(location)
		else:
			print("ERROR. current map null")
			
func get_tile_at_position_is_one_way(map:TileMap,position:Vector2, norm:Vector2=Vector2.ZERO):
	var pos=map.world_to_map(position)
	pos-=norm
	var id=map.get_cellv(pos)

	var tileset=map.tile_set
	var shapes=tileset.tile_get_shape_count(id)
	var one_way=false
	if shapes>=0:
		one_way=tileset.tile_get_shape_one_way(id,0)
	#var one_way=map.tile_set.tile_get_shape_one_way (id,0) 
	return one_way

func get_room_enemies(room_id:int, ok_top, ok_bottom, ok_left, ok_right):
	#in the current room, get control tiles along the edges
	#translate to start points just outside the room
	#return positions
	
	#human=0
	#drone=1
	#ufo=2
	#shipman=3
	var toppos=int(room_id/LEVEL_WIDTH_SCREENS)*screen_tiles.y
	var leftpos=(room_id%LEVEL_WIDTH_SCREENS)*screen_tiles.x
	var enemies={}
	var humans=[]
	var humansL=[]
	var humansR=[]
	var human_rex2=[]	#spawn anywhere
	var ufos=[]
	var drones=[]
	var spaceships=[]
	var triangles=[]
	
	humans=_get_room_enemies_edges(0,leftpos,toppos, ok_top, ok_bottom, ok_left, ok_right)
	humansL=_get_room_enemies_edges(5,leftpos,toppos, ok_top, ok_bottom, ok_left, ok_right,0,0,-1)
	humansR=_get_room_enemies_edges(6,leftpos,toppos, ok_top, ok_bottom, ok_left, ok_right,0,0,1)
	humans=humans+humansL+humansR
	drones=_get_room_enemies_edges(1,leftpos,toppos, ok_top, ok_bottom, ok_left, ok_right)
	ufos=_get_room_enemies_edges(2,leftpos,toppos, ok_top, ok_bottom, ok_left, ok_right)
	spaceships=_get_room_enemies_edges(3,leftpos,toppos, ok_top, ok_bottom, ok_left, ok_right)
	triangles=_get_room_enemies_edges(7,leftpos,toppos, ok_top, ok_bottom, ok_left, ok_right)
	
	#currently only rex2 humans supported, they are always ok unless too close to player
	human_rex2=_get_room_enemies_inside(5,6,leftpos,toppos)
	
	enemies["humans"]=humans
	enemies["drones"]=drones
	enemies["ships"]=spaceships
	enemies["ufos"]=ufos
	enemies["triangles"]=triangles
	enemies["rex2_humans"]=human_rex2
	return enemies

func get_random_screen_enemy():
	var container=GlobalMethods.assign_temp_location()
	var x=[]
	#only target disposable screen enemies
	#only if they are enabled
	#only if they are in the same room (i.e. not spawning)
	for e in container.get_children():
		if e is Enemy:
			var n=e.find_node("Hitbox")
			if n!=null && !n.get_node("CollisionShape2D").disabled:
				var r=get_room_id_from_position(e.global_position)
				if r==current_room_id:
					x.append(e)
	if x.size()>0:
		var target=x[randi()%x.size()]
		return target
	else:
		if current_room_id==44:
			var mini=get_tree().root.find_node("MinibossContainer",true,false)
			if mini!=null:
				return mini.get_node("Miniboss")
			
	return null
	
func get_room_destroyable_tiles(tile_id:int,room_id:=-1):
	#for room find all tiles with tile_id (-2 for all tiles)
	#for destroyable map
	return _get_room_tiles(current_destroyable_map,tile_id,room_id)

func get_room_miniboss_tiles(tile_id:int,room_id:=-1):
	#for room find all tiles with tile_id (-2 for all tiles)
	#for destroyable map
	var data_array=_get_room_tiles(current_miniboss_map,tile_id,room_id)
	return data_array

func get_room_main_tiles(tile_id:int,room_id:=-1):
	#for room find all tiles with tile_id (-2 for all tiles)
	#for main map
	return _get_room_tiles(current_map,tile_id,room_id)

func copy_room_tiles(from_room_id,to_room_id:=-1,override_when_from_empty:=false):
	#main and destroyable only
	#does not overwrite where from tile is -1 if flag false
	_copy_room_tiles(current_map,				from_room_id,to_room_id, override_when_from_empty)
	_copy_room_tiles(current_destroyable_map,	from_room_id,to_room_id, override_when_from_empty)
	
func _copy_room_tiles(map:TileMap,from_room_id,to_room_id:=-1,override_when_from_empty:=false):
	var room:=to_room_id
	if room==-1:
		room=current_room_id
		
	var from:=_get_room_tiles(map,-2,from_room_id)
	var offset_y_to=int(room/LEVEL_WIDTH_SCREENS)*screen_tiles.y
	var offset_x_to=(room%LEVEL_WIDTH_SCREENS)*screen_tiles.x
	var offset_y_from=int(from_room_id/LEVEL_WIDTH_SCREENS)*screen_tiles.y
	var offset_x_from=(from_room_id%LEVEL_WIDTH_SCREENS)*screen_tiles.x
	var xdiff=offset_x_from-offset_x_to
	var ydiff=offset_y_from-offset_y_to
	var vdiff=Vector2(xdiff,ydiff)
	var newtile
	var newloc
	for loc in from:
		newtile=loc[1]
		if newtile!=-1 || override_when_from_empty:
			newloc=loc[0]-vdiff
			map.set_cellv(newloc,newtile)
	
func _get_room_tiles(map:TileMap,tile_id:int,roomid:=-1) -> Array:
	#for room find all tiles with tile_id
	var room:=current_room_id
	if roomid!=-1:
		room=roomid	
		
	var data_array=[]
	var ct
	var offset_y=int(room/LEVEL_WIDTH_SCREENS)*screen_tiles.y
	var offset_x=(room%LEVEL_WIDTH_SCREENS)*screen_tiles.x
	for y in range(screen_tiles.y):
		for x in range(screen_tiles.x):
			ct=map.get_cell(x+offset_x,y+offset_y)
			if ct==tile_id || tile_id==-2:
				data_array.append([Vector2(x+offset_x,y+offset_y),ct])
				
	return data_array
	
func _get_room_enemies_inside(tile_id_left:int,tile_id_right:int,leftpos,toppos,min_player_dist:=5):
	#get tiles not around edges
	#these currently only work for humans and set 
	#flag to spawn rex2 humans
	var data_array=[]
	var current_tile
	
	var player_pos:Vector2=get_cell_from_position(current_player.global_position)

	for x in range(1, screen_tiles.x-1):
		for y in range(1,screen_tiles.y-1):
			current_tile=current_control_map.get_cell(leftpos+x,toppos+y)
			
			var ctile=Vector2(leftpos+x,toppos+y)
			var diff=(player_pos.distance_to(ctile))
			
			if diff>=min_player_dist:
				if current_tile==tile_id_left:
					data_array.append([leftpos+x,toppos+y,DIRECTION.LEFT,0])
				if current_tile==tile_id_right:
					data_array.append([leftpos+x,toppos+y,DIRECTION.RIGHT,0])
				if current_tile>0:
					pass

	return data_array
	
func _get_room_enemies_edges(tile_id:int,leftpos,toppos, ok_top, ok_bottom, ok_left, ok_right,offset_x:=0, offset_y:=0, meta:=0):
	#find tiles on each edge specified
	#offset is to offset position, we do this to allow other objects to enter at same place
	#data_array: 0,1 position, 2 edge, 3 meta int (used ony for humans)
	var data_array=[]
	var x1
	var y1
	var current_tile

	if ok_top:
		#top
		for x in range(screen_tiles.x):
			x1=leftpos+x
			y1=toppos
			current_tile=current_control_map.get_cell(x1,y1)
			if current_tile==tile_id:
				data_array.append([x1+offset_x,y1-1+offset_y,DIRECTION.TOP,meta])
				#data_array.append([x1+offset_y,y1-1+offset_y,DIRECTION.TOP,meta])
	else:
		#print("returning no top enemies as player at top")
		pass
		
	#bottom
	if ok_bottom:
		for x in range(screen_tiles.x):
			x1=leftpos+x
			y1=toppos+screen_tiles.y-1
			current_tile=current_control_map.get_cell(x1,y1)
			if current_tile==tile_id:
				data_array.append([x1+offset_x,y1+1+offset_y,DIRECTION.BOTTOM,meta])
	else:
		#print("returning no bottom enemies as player at bottom")
		pass
		
	#left
	if ok_left:
		for y in range(screen_tiles.y):
			x1=leftpos
			y1=toppos+y
			current_tile=current_control_map.get_cell(x1,y1)
			if current_tile==tile_id:
				data_array.append([x1-1+offset_x,y1+offset_y,DIRECTION.LEFT,meta])
	else:
		#print("returning no left enemies as player at left")
		pass

	#right
	if ok_right:
		for y in range(screen_tiles.y):
			x1=leftpos+screen_tiles.x-1
			y1=toppos+y
			current_tile=current_control_map.get_cell(x1,y1)
			if current_tile==tile_id:
				data_array.append([x1+1+offset_x,y1+offset_y,DIRECTION.RIGHT,meta])
	else:
		#print("returning no right enemies as player at right")
		pass

	return data_array

func zoom_camera(is_zoomed:bool, _scale_factor:float=1.0, centre_point:Vector2=Vector2.ZERO):
	camera_zoom=is_zoomed
	if !is_zoomed:
		main_camera.zoom_out(Vector2(0,0))
	else:
		var centre:=main_camera.global_position+Vector2(480,336)
		var offset:=centre-centre_point
		main_camera.zoom_in(-offset,1)
	
