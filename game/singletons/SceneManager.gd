extends Node

var scene_to=""

#const WORLD:String="/root/World"
const WORLD:String="World"

const MENU:="res://scenes/Menu.tscn"
const GAME:="res://scenes/Rex_Game_HUD.tscn"
const ZOOM:="res://scenes/IntroToTutorial.tscn"

#just_ exits to menu when finished
#game skips to the next until end, intro>tut>game, tut>game, game
enum SCENE_GAME_MODE {NONE,JUST_INTRO, JUST_TUTORIAL, JUST_INFILTRATE, JUST_OUTRO, GAME_INTRO, GAME_ZOOM, GAME_TUTORIAL, GAME_INFILTRATE,GAME}
var game_scene_sequence=SCENE_GAME_MODE.NONE

func _ready() -> void:
	$Control.visible=false	#required to allow mouse

func sequence_shift():
	#this is using the game_scene_sequence to determine if we need to switch scenes
	if game_scene_sequence==null:
		changeto_scene(MENU)
		return
		
	match game_scene_sequence:
		SCENE_GAME_MODE.NONE:
			changeto_scene(MENU)
		SCENE_GAME_MODE.JUST_INTRO, SCENE_GAME_MODE.JUST_OUTRO:
			#the menu is already there and options closes itself
			pass
		SCENE_GAME_MODE.JUST_TUTORIAL:
			ready_tutorial()
			GlobalPersist.player_spawn_point="SpawnTrainingInfiltrate"
			game_scene_sequence=SCENE_GAME_MODE.JUST_INFILTRATE
			changeto_scene(GAME)
		SCENE_GAME_MODE.JUST_INFILTRATE:
			changeto_scene(MENU)
		SCENE_GAME_MODE.GAME_INTRO:
			#zoom in
			game_scene_sequence=SCENE_GAME_MODE.GAME_ZOOM
			changeto_scene(ZOOM,"immediate")
		SCENE_GAME_MODE.GAME_ZOOM:
			#setup tutorial and switch scene
			ready_tutorial()
			game_scene_sequence=SCENE_GAME_MODE.GAME_TUTORIAL
			changeto_scene(GAME,"immediate")
		SCENE_GAME_MODE.GAME_TUTORIAL:
			#tutorial not completed, infiltrate is next
			ready_tutorial()
			GlobalPersist.player_spawn_point="SpawnTrainingInfiltrate"
			game_scene_sequence=SCENE_GAME_MODE.GAME_INFILTRATE
			Globals.birth_invisible_override=true
			changeto_scene(GAME)
		SCENE_GAME_MODE.GAME_INFILTRATE:
			#setup game and call main game
			GlobalPersist.init(true,false,false,false)
			ready_fullgame()
			Globals.ingame_time_start=Time.get_ticks_msec()
			game_scene_sequence=SCENE_GAME_MODE.GAME
			#game from infiltrate will do the below
			#but if we abort then do it manually
			GlobalPersist.player_spawn_point="SpawnStart3"
			if !GlobalPersist.has_continue:
				GlobalPersist.has_continue=true
				GlobalPersist.save_all_world_data(1,"Microdrive write success")

			changeto_scene(GAME)
		
		SCENE_GAME_MODE.GAME:
			#finished game go back to menu
			changeto_scene(MENU)
	

func ready_tutorial():
	Globals.init()
	GlobalTileMap.initialise()
	Globals.humans_killed=0
	Globals.biogrowths_killed=0
	GlobalPersist.player_spawn_point="SpawnJetty"
	Globals.lives=2
	Globals.training_mode=true
	GlobalPersist.score=9999
	GlobalPersist.smart_bombs=0
	Globals.invincible=(GlobalPersist.game_mode==GlobalPersist.GAMEMODE.SIMPLE)
	
func ready_fullgame():
	Globals.init()
	GlobalTileMap.initialise()
	Globals.humans_killed=0
	Globals.biogrowths_killed=0
	Globals.training_mode=false
	#start game
	if GlobalPersist.game_mode==GlobalPersist.GAMEMODE.NORMAL:
		Globals.lives=Globals.START_LIVES
	else:
		Globals.lives=99
	
	Globals.invincible=(GlobalPersist.game_mode==GlobalPersist.GAMEMODE.SIMPLE)
	
func changeto_scene(to, animation:="basicfade", unpause_game:=false):
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	scene_to=to
	if unpause_game:
		get_tree().paused=false
		
	$Control.visible=true	#we disable by default to allow mouse
	$Control/AnimationPlayer.play(animation)
	yield($Control/AnimationPlayer,"animation_finished")
	$Control.visible=false
	
func _switch_scene():
	#called from change_scene
	if scene_to!="":
		get_tree().change_scene(scene_to)
	
func get_node_from_game(name:String, exact_path:=true):
	var r=get_tree().root
	
	var w = r.find_node(WORLD,true,false)
	if !w:
		return
	
	if exact_path:
		return w.get_node(name)
	
	return w.find_node(name,true)

func remove_node(name,exact_path:=true):
	var n=get_node_from_game(name,exact_path)
	if n!=null:
		n.queue_free()
	else:
		print("ERROR. failed to remove node "+name)
		
func replace_node(scene_file:String, node_path_parent,location:Vector2,exact_parent_path:=true):
	
	#get parent item
	var item=get_node_from_game(node_path_parent,exact_parent_path)
	if item==null:
		print("ERROR. Parent does not exist. "+node_path_parent)
		return
		
	#instantiate item
	var n=load(scene_file)
	if !n:
		print("ERROR. Scene does not exist. "+scene_file)
		return
	var i=n.instance()
	
	#remove node if already in parent container
	var c=item.get_node(i.name)
	if c!=null:
		c.queue_free()
		
	#add the item
	#yield(get_tree(), "idle_frame")
	i.position=location
	item.add_child(i)
	return i
