extends Node

#music is always played here
#audio can either be per node or here
# - per node to ensure that node always manages it's sound and always plays
# - use this to allow max number of voices playing, e.g. do not want 100 effects all playing togeter

#we'll keep master at 0.0 (max)
#music will use the single audiostreamplayer here for volume
#sfx we'll have a new bus with it's own volume that way nodes or this can use that

onready var music_player=$AudioStreamPlayerMusic
onready var sfx_container=$ContainerSFX

const FADE_TIME = 2.0
var current_music:AudioStream = null
var music_level:=1.0	#100%
var sfx_level:=1.0		#100%
var sfx_available = []  # The available players.
var sfx_queue = []  # The queue of sounds to play
var sfx_default_bus = "master"		#should be a separate bus so audio streams can directly use sounds
									#but still use singleton to set volume
var music_default_bus = "master"	#as sfx
var initialised = false

#only ever one music stream player so set that volume
#for sfx we will set the bus not each item - 
# change if ever need to set individual volumes
# this way we can add audio streams to nodes as well as play here
var bus_sfx=null
var sfx_allowed:=true

func _ready() -> void:
	pass

func sfx_mute_override(mute_sfx):
	#for sfx to disable temporarily, mainly for when global time is slowed down
	for x in sfx_available:
		if x.playing:
			x.stream_paused=mute_sfx
				
func _process(delta: float) -> void:
	# Play a queued sound if any players are available.
	#if sfx volume is zero then process is stopped
	#process only used with sfx, not music
	if not sfx_queue.empty() and not sfx_available.empty():
		sfx_available[0].stream = load(sfx_queue.pop_front())
		#sfx_available[0].volume_db=linear2db(sfx_level)
		if sfx_available[0].stream:
			if "loop_mode" in sfx_available[0].stream:
				sfx_available[0].stream.loop_mode=0
		sfx_available[0].play()
		sfx_available.pop_front()
		
func initialise_audio(max_players:=10, default_music_bus="master",default_sfx_bus="master"):
	#normally music uses 'music' bus and sfx uses 'sfx' bus
	#so we can control volume
	#but set as master for when we don't have buses
	bus_sfx=AudioServer.get_bus_index(default_sfx_bus)
	_initialise_sfx_channels(max_players, default_sfx_bus)
	music_default_bus = default_music_bus
	music_player.bus = music_default_bus
	
func set_music_volume(volume:float):
	music_level = clamp(volume,0.0,1.0)
	music_player.volume_db=linear2db(music_level)
	
	
func set_sfx_volume(volume:float):
	sfx_level = clamp(volume,0.0,1.0)
	if volume<=0.0:
		set_process(false)
		sfx_allowed=false
	else:
		set_process(true)
		sfx_allowed=true
		AudioServer.set_bus_volume_db(bus_sfx,linear2db(sfx_level))
func play_music(music_file:String,fade_existing:=true, force_fade_in:=0.0,skip_if_playing:=false):
	
	set_music_volume(music_level)
	if music_level==0.0:
		#if playing then will stop and setting new volume
		#will mean existing music prior will not be playing
		music_player.stop()
		return
		
	if !music_player.playing:
		fade_existing=false
	elif skip_if_playing:
		return
		
	if music_file == "":
		return
		
	var start = music_player.volume_db
	if force_fade_in<=0.0 and (!fade_existing or start<=-30.0):
		#-30 is too quiet to hear so just stop here instead of going all the way to -80
		#so if already less then don't bother fading
		#force is for when we want to fade music in
		_play_music_now(music_file)
		return
		
	#fade out then new one back in
		
	var tw:SceneTreeTween=get_tree().create_tween()
	if fade_existing:
		tw.tween_property(music_player, "volume_db",-30.0,FADE_TIME).from(start).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)
		force_fade_in = FADE_TIME #faded out so use default fade in
		
	tw.tween_callback(self,"_play_music_now",[music_file])
	tw.tween_property(music_player, "volume_db",start,force_fade_in).from(-30.0).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)
	
func pause_music():
	if music_player.playing:
		music_player.stream_paused=true

func resume_music():
	if music_player.stream_paused:
		music_player.stream_paused=false

func stop_music(fade_time:=0.0):
	#if tw and tw.is_running():
	#	tw.stop()
	#tween_fade.stop_all()

	if !music_player.playing:
		return

	if music_level==0.0 or fade_time<=0.0 or music_player.volume_db<=-30.0:
		music_player.stop()
		return
		
	var tw=get_tree().create_tween()
	var twp = tw.tween_property(music_player,"volume_db",-30.0,fade_time)
	twp.set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR).from(music_player.volume_db)
	yield(tw,"finished")
	
	music_player.stop()
	set_music_volume(music_level)

func play_sfx(sound_path):
	sfx_queue.append(sound_path)
	
func _on_sfx_stream_finished(stream):
	# When finished playing a stream, make the player available again.
	sfx_available.append(stream)

func _play_music_now(music_file:String):
	music_player.stop()
	current_music = null
	current_music = load(music_file)
	if current_music:
		music_player.stream = current_music
		music_player.play()

func _initialise_sfx_channels(num_players:int, sfx_bus:String):
	sfx_default_bus = sfx_bus
	
	for i in sfx_available:
		i.stop()
		i.stream = null
		
	sfx_available = []
	sfx_queue = []
	
	#Create the pool of AudioStreamPlayer nodes.
	for i in num_players:
		var p = AudioStreamPlayer.new()
		sfx_container.add_child(p)
		sfx_available.append(p)
		p.connect("finished", self, "_on_sfx_stream_finished", [p])
		p.bus = sfx_default_bus
