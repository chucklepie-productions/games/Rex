extends Node
#contains everything saved to file

#const GAMESAVE="user://gamesave"	#change to res for testing
const GAMESAVE="res://gamesave"	#change to res for testing
const GAMESAVE_EXT=".save"	#change to res

enum GAMEMODE {NORMAL,SIMPLE,EASY}

#player details
var player_spawn_point:String
var score:int = 0 setget set_score, get_score
var next_life:int
var smart_bombs:int
var current_weapon:int
var has_oxygen:bool
var weapon_level:Array
var help_energy_seen:bool
var help_pod_seen:bool
var help_smart_seen:bool
var help_oxygen_seen:bool
var graphics_type:int		#0 for original, 1 for coloured, 2 for new
var windowed_mode:int
var volume_music:int
var volume_sound:int
var mouse_mode:int
var has_continue:bool		#if continue available, after training complete will always be true
var rex2_backdrop:bool
var crt_enabled:bool
var tooltips:bool

#gameplay
#we do not want lives because only provide a checkpoint for when player dies
#they get full lives back
#var training_complete:bool=false	#to determine whether to show training in menu
var bunny_hiscore:int=0
var bunny_unlocked:bool
var game_mode:int
var has_finished:bool
#environment

func _ready() -> void:
	#new save will be created if does not exist
	load_world_data(1)
	GlobalMethods.screen_change()
	
func set_score(new_score):
	#new life every 10000
	score=new_score
	if Globals.game_scene!=null:
		Globals.game_scene.new_score()
		
	if score>=next_life:
		next_life+=10000
		if Globals.game_scene!=null:
			Globals.game_scene.extra_life()
		
func get_score():
	return score
	

#resource saving from gonkee
func save_bunny_world_data():
	#get last saved data then merge
	var save_data=load_world_data(1,false)
	save_data["bunny_hiscore"]=GlobalPersist.bunny_hiscore
	save_data["bunny_unlocked"]=GlobalPersist.bunny_unlocked
	save_all_world_data(1,"Bunny found. Saving!",0,save_data)
	
func save_config_world_data(show_save_popup:=true):
	#get last saved data then merge
	var save_data=load_world_data(1,false)

	save_data["graphics_type"]=GlobalPersist.graphics_type
	save_data["rex2_backdrop"]=GlobalPersist.rex2_backdrop
	save_data["CRT"]=GlobalPersist.crt_enabled
	save_data["tooltips"]=GlobalPersist.tooltips
	save_data["windowed_mode"]=GlobalPersist.windowed_mode
	save_data["volume_music"]=GlobalPersist.volume_music
	save_data["volume_sound"]=GlobalPersist.volume_sound
	save_data["mouse_mode"]=GlobalPersist.mouse_mode

	var message="Config Saved"
	if !show_save_popup:
		message=""
		
	save_all_world_data(1,message,0,save_data)
	
func save_all_world_data(slot,message="Checkpoint Reached!",x_offset:=0,save_data:Dictionary={}) ->Dictionary:
	
	var save
	if save_data!=null && save_data.size()>0:
		save=save_data
	if save_data==null || save_data.size()==0:
		save = {
			"bunny_hiscore" 	: bunny_hiscore,
			"bunny_unlocked"	: bunny_unlocked,
			"current_weapon" 	: current_weapon,
			"game_mode"			: game_mode,
			"graphics_type"		: graphics_type,
			"has_continue"		: has_continue,
			"help_energy_seen" 	: help_energy_seen,
			"help_oxygen_seen" 	: help_oxygen_seen,
			"help_pod_seen" 	: help_pod_seen,
			"help_smart_seen" 	: help_smart_seen,
			"mouse_mode"		: mouse_mode,
			"next_life" 		: next_life,
			"player_spawn_point": player_spawn_point,
			"score" 			: score,
			"smart_bombs" 		: smart_bombs,
			"rex2_backdrop"		: rex2_backdrop,
			#"training_complete" : training_complete,
			"weapon_level" 		: weapon_level,
			"windowed_mode"		: windowed_mode,
			"volume_music"		: volume_music,
			"volume_sound"		: volume_sound,
			"has_oxygen"		: has_oxygen,
			"CRT"				: crt_enabled,
			"has_finished"		: has_finished,
			"tooltips"			: tooltips
		}
	
	var new_save=File.new()
	new_save.open(GAMESAVE+str(slot)+GAMESAVE_EXT,File.WRITE)

	var jdata=to_json(save)
	var err2=new_save.store_string(jdata)
	new_save.close()
	#new_save=null
	if message!="":
		OptionsAndAchievements.show_checkpoint_saved(message,x_offset)
	
	return save
	
func load_world_data(slot:=1,update_globals:=true) -> Dictionary:
	var data={}
	var file = File.new()
	var filename=GAMESAVE+str(slot)+GAMESAVE_EXT
	var success=true
	if not file.file_exists(filename):
		#show a message?
		print("Load fail, file not found: " + filename+". Creating new.")
		success=false
	else:
		file.open(filename,File.READ)
		data=parse_json(file.get_as_text())
		if data==null:
			success=false
		elif update_globals:
			success=verify_save(data)
		if !success:
			OS.alert("Error loading game save. Reverting to new save file.","File Error")

	if !success:
		init(true,true,true,true)
		data=save_all_world_data(1,"Microdrive write success")
		
	return data
		
func verify_save(data):
	has_continue=bool(data["has_continue"])
	player_spawn_point = str(data["player_spawn_point"])
	score = int(data["score"])
	next_life=int(data["next_life"])
	smart_bombs = int(data["smart_bombs"])
	current_weapon=int(data["current_weapon"])
	weapon_level = data["weapon_level"]
	rex2_backdrop=bool(data["rex2_backdrop"])
	help_energy_seen=bool(data["help_energy_seen"])
	help_pod_seen=bool(data["help_pod_seen"])
	help_smart_seen=bool(data["help_smart_seen"])
	help_oxygen_seen=bool(data["help_oxygen_seen"])
	graphics_type=int(data["graphics_type"])
	#training_complete=bool(data["training_complete"])
	bunny_hiscore=int(data["bunny_hiscore"])
	bunny_unlocked=bool(data["bunny_unlocked"])
	windowed_mode=int(data["windowed_mode"])
	volume_music=int(data["volume_music"])
	volume_sound=int(data["volume_sound"])
	mouse_mode=int(data["mouse_mode"])
	game_mode=int(data["game_mode"])
	has_oxygen=bool(data["has_oxygen"])
	crt_enabled=bool(data["CRT"])
	tooltips=bool(data["tooltips"])
	has_finished=bool(data["has_finished"])
	if game_mode==GAMEMODE.SIMPLE:
		Globals.invincible=true
	else:
		Globals.invincible=false

	if has_continue==null: 
		return false
	if player_spawn_point == null || player_spawn_point=="" : 
		return false
	if next_life==null || next_life<0: 
		return false
	if score == null || score<0: 
		return false
	if smart_bombs == null || smart_bombs<0: 
		return false
	if current_weapon == null || current_weapon<0: 
		return false
	if weapon_level == null || !weapon_level is Array: 
		return false
	if help_pod_seen==null: 
		return false
	if help_energy_seen==null:
		return false
	if help_smart_seen==null: 
		return false
	if help_oxygen_seen==null: 
		return false
	if rex2_backdrop==null:
		return false
	if has_oxygen==null:
		return false
	if graphics_type == null || graphics_type<0: 
		return false
	if bunny_hiscore == null || bunny_hiscore<0: 
		return false
	if bunny_unlocked==null:
		return false
	if crt_enabled==null:
		return false
	if tooltips==null:
		return false
	if has_finished==null:
		return false
	#if training_complete==null: 
	#	return false
	if windowed_mode==null || windowed_mode<0 || windowed_mode>2:
		return false
	if mouse_mode==null || mouse_mode<0 || mouse_mode>2:
		return false
	if volume_music==null || volume_music<0 || volume_music>100:
		volume_music=100
	if volume_sound==null || volume_sound<0 || volume_sound>100:
		volume_sound=100
		
	AudioManager.initialise_audio(10,"master","sfx")
	AudioManager.set_music_volume(float(GlobalPersist.volume_music)/100.0)
	AudioManager.set_sfx_volume(float(GlobalPersist.volume_sound)/100.0)

	#if in miniboss room, put in previous room so we can set it all up
	if GlobalPersist.player_spawn_point=="Spawn2":
		GlobalPersist.player_spawn_point="Spawn17"

	return true
	
func init(reset_game:bool, reset_gameplay:bool, reset_config:bool,reset_spawn:bool) -> void:
	#normally reset_game when starting a new game or no save file
	#normally reset_config when no save file exists
	#normally reset_gameplay during a game (always true, never false) or no save file
	#normally reset_spawn for new game (first normal screen)

	#reset every game
	if reset_game:
		score = 0
		next_life=10000
		smart_bombs = 3
		current_weapon=0
		weapon_level = [0,0,0,0,0]
		has_oxygen=false

	#set through gameplay only
	if reset_gameplay:
		has_continue=false
		help_energy_seen=false
		help_pod_seen=false
		help_smart_seen=false
		help_oxygen_seen=false
		#training_complete=false
		bunny_hiscore=0
		bunny_unlocked=false
		has_finished=false

	#part of config
	if reset_config:
		graphics_type=0
		rex2_backdrop=true
		windowed_mode=0
		volume_music=100
		volume_sound=100
		AudioManager.initialise_audio(10,"music","sfx")
		AudioManager.set_music_volume(1.0)
		AudioManager.set_sfx_volume(1.0)
		mouse_mode=0
		game_mode=0
		crt_enabled=true
		tooltips=true

	if reset_spawn:
		player_spawn_point = "SpawnStart"
	
