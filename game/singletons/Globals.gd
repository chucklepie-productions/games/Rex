#THIS IS DATA ONLY
#start use new screen
#'create zentian lake' start circle fraction earlier
#grey fade if non speccy mode brighten a bit
#add particles permanent on charge
#80s -> 80's
#save station only freeze/slow on actually doing it

extends Node

const GRAVITY = 310
const START_LIVES=4	#full lives after every loss

const MUSIC_INTRO="res://assets/audio/music/Dance-of-the-Satellites.soundimage.org.ogg"
const MUSIC_MENU="res://assets/audio/music/Deep-Space-Outpost.soundimage.org.ogg"
const MUSIC_OUTRO="res://assets/audio/music/DOS-88 - Wasting Time-HQ.ogg"
const MUSIC_MONTY="res://assets/audio/music/PeterM Harrap Wanted Monty Mole.ogg"
const MUSIC_MANIC="res://assets/audio/music/Matthew Smith Manic Miner Title Music.ogg"
const MUSIC_JSW="res://assets/audio/music/Matthew Smith Jet Set Willy 2.ogg"
const MUSIC_INFILTRATE="res://assets/audio/music/TechTalk.cut.audionautix.com.ogg"
const MUSIC_MINI="res://assets/audio/music/Twin Musicom - NES Boss.ogg"
const MUSIC_LEVEL1="res://assets/audio/music/Fiberitron Loopdl-sounds.com.ogg"
const MUSIC_INDUSTRY="res://assets/audio/music/Creepy-Sci-Fi-Ambience-2.soundimage.org.ogg"
const MUSIC_BOSS_START="res://assets/audio/music/eric matayas Arcade-Heroes.ogg"
const MUSIC_BOSS_END="res://assets/audio/music/Jetpac_slow.ogg"
const SFX_MENU_NAVIGATE="res://assets/audio/game/ui/kenny bong_001.wav"
const SFX_MENU_CONFIRM="res://assets/audio/game/ui/kenny confirmation_001.wav"
const SFX_MENU_CANCEL="res://assets/audio/game/enemy/kenny pepSound1.wav"
const SFX_TV80_OPEN="res://assets/audio/game/ui/kenny question_003 tv80 open.wav"
const SFX_TV80_CLOSE="res://assets/audio/game/ui/kenny question_004 tv80 close.wav"
const SFX_TV80_CLICK="res://assets/audio/game/ui/kenny click3 TV80 click.wav"
const SFX_MENU_BULLET="res://assets/audio/game/ui/mixkit-game-gun-shot-1662 - standard.wav"
const SFX_MENU_BULLET2="res://assets/audio/game/ui/mixkit-game-gun-shot-1662 - more bass.wav"
const SFX_MENU_PUMP="res://assets/audio/game/ui/mixkit-shotgun-hard-pump-1665.wav"
const SFX_MENU_REX="res://assets/audio/game/ui/harvey656.itch.io tele_149 menu rex appear.wav"
const SFX_TV80_NOISE="res://assets/audio/game/ui/pixabay.com pink_noise_300_sec-18206 short.wav"
const SFX_TV80_NOISE2="res://assets/audio/game/ui/pixabay.com pink_noise_300_sec-18206 shortest.wav"
const SFX_GAME_NOCHECKPOINT="res://assets/audio/game/environment/kenny minimize_006 no checkpoint.wav"
const SFX_GAME_NORABBIT="res://assets/audio/game/environment/dreamstime 175904266 rabbit.wav"
const SFX_GAME_DEFENCE_GONE="res://assets/audio/game/environment/mixkit-game-blood-pop-slide-2363 shield explode.wav"
const SFX_GAME_DEFENCE_HIT="res://assets/audio/game/environment/kenny impactMetal_004 shield hit.wav"
const SFX_GAME_CHECKPOINTDOWN="res://assets/audio/game/environment/kenny maximize_006 checkpoint down.wav"
const SFX_GAME_CHECKPOINTSTART="res://assets/audio/game/environment/kenny engineCircular_000 checkpoint start.wav"
const SFX_GAME_FIREWORK1="res://fireworks/sfx/firework_explosion_001.wav"
const SFX_GAME_FIREWORK2="res://fireworks/sfx/firework_explosion_002.wav"
const SFX_GAME_FIREWORKSH="res://fireworks/sfx/firework_shower.wav"
const SFX_GAME_FIREWORKROCKET="res://fireworks/sfx/firework_rocket_launch.wav"
const SFX_GAME_SHOOTINGSTAR="res://assets/audio/game/environment/soundbible.com shooting_star-Mike_Koenig-1132888100.wav"
const SFX_GAME_ACHIEVEMENT="res://Achievements/Audio/manic.wav"
const SFX_GAME_PICKUP_DEFAULT="res://assets/audio/game/environment/kenny phaseJump4 bubble.wav"
const SFX_GAME_GOTSOMETHING="res://assets/audio/game/environment/mixkit-winning-a-coin-video-game-2069 Got Something.wav"
const SFX_GAME_PICKUP="res://assets/audio/game/environment/kenny impactGeneric_light_003 pickup.wav"
const SFX_GAME_PICKUPGOOD="res://assets/audio/game/environment/maximize_006 pickup good.wav"
const SFX_GAME_WARNING="res://assets/audio/game/environment/pixabay.com alert-1-86270 warning.wav"
const SFX_GAME_LIFTJUMP="res://assets/audio/game/environment/mixkit-electric-whoosh-2596.wav"
const SFX_GAME_WSHIP_IDLE="res://assets/audio/game/environment/kenny thrusterFire_004.ogg"	#on repeat
const SFX_GAME_WSHIP_LIFTOFF="res://assets/audio/game/environment/kenny spaceEngineLarge_002.ogg"
const SFX_GAME_TRAIN="res://assets/audio/game/environment/opengameart macro Alarm.wav"
const SFX_GAME_IMPACT="res://assets/audio/game/environment/kenny footstep_snow_001.wav"
const SFX_GAME_SHAKE="res://assets/audio/game/environment/opengameartdeva Explosion 2.wav"
const SFX_UI_HUDOPEN="res://assets/audio/game/ui/kenny maximize_008.wav"
const SFX_UI_HUDCLOSE="res://assets/audio/game/ui/kenny minimize_008.wav"
const SFX_UI_HIGHLIGHT="res://assets/audio/game/ui/kenny powerUp1.wav"
const SFX_UI_FLASH="res://assets/audio/game/ui/pixabay.com notification-37858.wav"
const SFX_PLAYER_WARP1="res://assets/audio/game/player/pixabay teleport-90323.wav"
const SFX_PLAYER_WARP2="res://assets/audio/game/player/pixabay powering_up_funny_sci-fi_dry-102263.wav"
const SFX_PLAYER_GOTUP="res://assets/audio/game/player/99soundeffects Retro - Chip Power.wav"
const SFX_WEAPON_BULLET="res://assets/audio/game/weapons/opengameart-deva-Shoot 3.wav"
const SFX_WEAPON_LASER="res://assets/audio/game/weapons/kenny laserRetro_000.wav"
const SFX_WEAPON_SPRAY="res://assets/audio/game/weapons/opengameart munchybobo Fire 6.wav"
const SFX_WEAPON_NOBULLETS="res://assets/audio/game/weapons/opengameart deva Hit 2.wav"
const SFX_WEAPON_SPRAYMISSILE="res://assets/audio/game/weapons/opengameart deva Explosion 3.wav"
const SFX_PLAYER_WALK="res://assets/audio/game/player/kenny footstep_concrete_001.wav"	#and ship close
const SFX_PLAYER_JUMP="res://assets/audio/game/player/opengameart deva Exit 2.wav"
const SFX_WEAPON_SMARTSTART="res://assets/audio/game/weapons/99soundeffects Punch - Sploop.wav"
const SFX_WEAPON_SMARTEND="res://assets/audio/game/weapons/opengameart deva Explosion 5.wav"
const SFX_PLAYER_SHIELDON="res://assets/audio/game/player/kenny forceField_000.wav"
const SFX_PLAYER_SHIELDOFF="res://assets/audio/game/player/kenny forceField_001.wav"
const SFX_PLAYER_SHIELDERROR="res://assets/audio/game/player/opengameart deva Wrong 3.wav"
const SFX_PLAYER_INVINCIBLE="res://assets/audio/game/player/mixkit-video-game-health-recharge-2837.wav"
const SFX_ENEMY_EXPLODE1="res://assets/audio/game/weapons/harvey656 explosion_03.wav"		#bigger
const SFX_ENEMY_EXPLODE2="res://assets/audio/game/weapons/harvey656 explosion_04.wav"		#smaller
const SFX_ENEMY_EXPLODE3="res://assets/audio/game/weapons/harvey656 explosion_10.wav"		#smallest
const SFX_ENEMY2_EXPLODE1="res://assets/audio/game/weapons/99soundeffects Punch - Splok.wav" #medium
const SFX_ENEMY2_EXPLODE2="res://assets/audio/game/weapons/99soundeffects Punch - Pop.wav"	#small
const SFX_ENEMY2_EXPLODE3="res://assets/audio/game/weapons/99soundeffects Short - Ploppy Plop.wav"	#pop
const SFX_ENEMY2_EXPLODE4="res://assets/audio/game/weapons/99soundeffects Whoosh - Trapped Firecracker.wav"	#triangle
const SFX_ENEMY_DROID="res://assets/audio/game/enemy/harvey666 Alarm_10.wav"
const SFX_ENEMY_RECOIL1="res://assets/audio/game/enemy/kenny impactPlank_medium_000.wav"
const SFX_ENEMY_RECOIL2="res://assets/audio/game/enemy/mixkit-winning-a-coin-video-game-2069.wav"
const SFX_ENEMY_MINIPULSE="res://assets/audio/game/environment/99sound effects Braam - Retro Pulse.wav"
const SFX_ENEMY_MINISTRIPPED="res://assets/audio/game/enemy/99soundeffects Impact - Robot Punch.wav"
const SFX_ENEMY_MINJUMP="res://assets/audio/game/enemy/opengameart deva Exit 3.wav"
const SFX_ENEMY_CHAT="res://assets/audio/game/enemy/kenny pepSound1.wav"
const SFX_QUEEN_CHAT="res://assets/audio/game/ui/kenny slime_000 - sqidgy dialog start.wav"
#these are set once and should never be reset
var game_scene=null
var debug:bool
var dialog_parent
var in_menu:bool

#everything should be set in reset function below
var lives:int=START_LIVES
var bullet_time:float setget set_bullet_time				#we slow things down globally
									#e.g. when spawning. this is our multiplier
var training_mode:bool
var temp_location				#for storing explosions mainly (the transient area)
var is_game_over:bool
var quick_spawn:bool			#really only for first screen after infiltrate
var birth_invisible_override:bool
var invincible:bool		#only works if easy mode
var ingame_time_start:int
var highscore_allowed:bool
var popup_allowed:bool
var humans_killed:int
var biogrowths_killed:int
var game_ended:bool	#transient, only set when finishing game to show alternate screens
var infiltrate_seen:bool
var player_being_born:bool

var used_shield_this_game:bool	#just to speed up achievement check
var disallow_options:bool

enum HUMAN_DEATH_TYPE{REPEAT,AIR,IMMEDIATE}
enum DIALOGUE_SIZE {PICKUP,TUTORIAL,DIALOG}
enum PICKUP {LARGE_ENERGY, SMALL_ENERGY, SMARTBOMB, RANDOMITEM,OXYGEN,HINT}

#points gained - to be done
const VALUE_LARGE:=50
const VALUE_SMALL:=25
const VALUE_HUMAN:=100
#energy gain - every 6 gains a level up, 5 levels available
const ENERGY_LARGE:=2
const ENERGY_SMALL:=1
const BUBBLE_DELAY=0.4	#how long to wait before bubble appears
const BUBBLE_DISPLAY_DELAY=0.7	#how long before showing

const DIALOGUE_TEXT = {
	"ready_rex2":
		[{
			"character": "rex",
			"position":"right",
			"text":"Energy restored, weapons restore. I am ready.\n\n[color=#C000C0][wave amp=30 freq=10]Power. I have it, they don’t.[/wave][/color]"
		}
		],		
	"rex_final":
		[{
			"character": "rex",
			"position":"right",
			"text":"\n\nYou know what, I think I will go home, put my feet up and have that nice cup of tea"
		}
		],		
	"easy_mode": 
		[{
			"character": "rex",
			"position":"right",
			"text":"\n\nLet's get this right..."
		},
		{
			"character": "rex",
			"position":"right",
			"text":"\n[color=#00C0C0]POKE 40057,0[/color]  Cavern Infinite Lives\n[color=#00C0C0]POKE 40303,0[/color]  Tower  Infinite Lives\n[color=#00C0C0]POKE 39396,0[/color]  Cavern Infinite Shield\n[color=#00C0C0]POKE 39170,0[/color]  Tower  Infinite Shield\n"
		}
		],
	"squidgy": 
		[{"text":"\n\n\n   With the big squidgy\n   thing destroyed, rex\n   can go home, have\n   a nice cup of tea,\n   and put his feet up."}],
	"energy"		: 
		[{
			"character": "energy",
			"position":"center",
			"text":"[color=#00C0C0]Energy Boost[/color]\nPower surges through you, giving increased vigour and renewed confidence"
		}
		],
	"smart"			: 
		[{"character": "smart", "position":"center","text" :"[color=#C0C000]Smart Bomb[/color]\nNow I am become Death, the destroyer of worlds"}],
	"oxygen"		: 
		[{"character": "oxygen","position":"center","text":"[color=#C0C000]Air Circulation Device[/color]\nThe feeling of dread dissipates and a sense of adventure ripples through your veins."}],
	"single-bullet"	: 
		[{"character": "single","position":"center","text":"[color=#C0C000]apa-150 mkI machine gun[/color]Standard issue single bullet rounds"}],
	"double-bullet"	: 
		[{"character": "double","position":"center","text":"[color=#C0C000]apa-150 mkii[/color]\na dual shot version of the standard issue apa mki machine gun"}],
	"laser"	: 		  
		[{"character": "laser", "position":"center","text":"[color=#C0C000]tusk laser mkiv[/color]\nAs used in the great life wars"}],
	"drone"	: 		  
		[{"character": "drone", "position":"center","text":"[color=#C0C000]apa-250 Scythe[/color]\nremote drone offensive system incorporating bi-directional machine gun fire"}],
	"spray"	: 		  
		[{"character": "spray", "position":"center","text":"[color=#C0C000]Amara multi-gun[/color]multi-directional projectile with homing missile.The ultimate in personal defence"}],
	"boss_special":
		[{"character": "spray", "position":"center","text":"[color=#C0C000]Ultimate Jetpac[/color]Provides a short vertical thrust with an added laser weapon system"}],
	"pod" :			  
		[{"character": "pod",   "position":"center","text":"[color=#C0C000]shield recharge station[/color]\nHe will win who knows when to fight and when not to fight"}],
	"rnd" :	 		  
		[{"character": "rnd",   "position":"center","text":"Random item\n[color=#C0C000]Press fire to stop and get a prize. Hurry as it speeds up then stops randomly[/color]"}]
}
#			
#			"position":"center",

const MINIBOSS_TEXT = {
	"boss_real_rex":
		[{
			"character":"rex",
			"position" :"right",
			"text":"\nSo if I’m going out, it's here. If you wanna kill the world, then start with me.\n[color=#C000C0][wave amp=30 freq=10]I'm the thing that monsters have nightmares about[/wave][/color]",
		}],
	"boss_fake" :
		[
		{
			"character":"squidgy",
			"position" :"left",
			"text":"\n\nReally....\n\nYou thought that was it",
		},
		{
			"character":"squidgy",
			"position" :"left",
			"text":"\n[color=#C000C0][wave amp=30 freq=10]\n          mwahahaha[/wave][/color]",
		},
		{			
			"action":"clear_portraits",
		},
		{
			"character":"rex",
			"position" :"right",
			"text":"\n ...",
		},
		{			
			"action":"clear_portraits",
		},
		{
			"character":"squidgy",
			"position" :"left",
			"text":"\nTell you what, come upstairs and we'll have a little dance, you've become quite the little adversary",
		}
		],		
	"training_end1": 
		[{
			"character": "rex",
			"position":"right",
			"text":"This tunnel is blocked. Can I hold them off with the ammo I have left?..\n\n [color=#00C0C0][wave amp=30 freq=10]...or is there another way?[/wave][/color]"
		}
		],
		"training_end2": 
		[{
			"character": "rex",
			"position":"right",
			"text":"...sometimes when it's dark, and I'm all alone I think, 'What would Rex do?'\n\n[color=#C000C0][shake rate=5 level=10]Blow stuff up, that's what I'd do!...[/shake][/color]"
		}
		],
	"phew":[{"text":"Phew!!!"}],
	"phew2":[{"text":"Lucky Escape!!!"}],
	"squidgy_kill1":
		[
			{
				"text":"\nYou can't kill me\n\n[color=#c000c0][shake rate=5 level=10]     I will live forever[/shake][/color]"
			},
			{
				"text":"\n\n\n    You are ..."
			},
			{
				"text":"\n\n\n    ..."
			},
			{
				"text":"\n\n\n    ..."
			},
			{
				"character":"rex",
				"position" :"right",
				"text":"Is this the little dance you were talking about? Remember what I said,\n\n[color=#c000c0][shake rate=5 level=10]I'm the thing that monsters have nightmares about[/shake][/color]"
			},
			{
				"character":"rex",
				"position" :"right",
				"text":"\n\n\nNow it's time to leave this planet and return home"
			},
			{			
				"action":"clear_portraits",
			},
			{
				"text":"\n\n\n[color=#C000C0][wave amp=30 freq=10] I may be dead, but I'm still pretty[/wave][/color]"
			},
		],
	"start_boss":
		[
			{
				"character":"squidgy",
				"position":"left",
				"text":"\nThat's enough now. If you won't join me, then I cannot let you destroy my tower and my children."
			}
		],
	"jetpac":
		[
			{
				"character":"rex",
				"position":"right",
				"text":"\nEnough of this nonsense, if you will not come to me...\n\n[color=#c000c0][shake rate=5 level=10]     Then I will come to you[/shake][/color]"
			}
		],
	"rex1_nopasswords":
		[
			{
				"character":"squidgy",
				"position":"left",
				"text":"[color=#c000c0][shake rate=5 level=10]\nError, password collection not found[/shake][/color]"
			}
		],
	"rex1_passwords":
		[
			{
				"character":"squidgy",
				"position":"left",
				"text":"[color=#00c0c0]\nEnter Password[/color]\n\nYou have 10 attempts remaining"
			},
			{
				"character":"rex",
				"position":"right",
				"text":"\n\n     '6031769'"
			},
			{
				"character":"squidgy",
				"position":"left",
				"text":"[color=#00c0c0]Thank you user![/color]\n\n[color=#c0c0c0]But our queen is in a different Cavern. This is not the central cavern[/color]"
			},
			{
				"text":"[color=#00c0c0]Enter Password[/color]\n\n[color=#c0c0c0]You have 9 attempts remaining[/color]"
			},
			{
				"character":"rex",
				"position":"right",
				"text":"\n\n     POKE 56287,58\n     POKE 56405,201"
			},
			{
				"character":"squidgy",
				"position":"left",
				"text":"[color=#c000c0]Error![/color]\n\n[color=#00c0c0]Mitre T1059.008 intrusion threat detected, M1038 invoked[/color]"
			},
			{
				"character":"squidgy",
				"position":"left",
				"text":"[color=#00c0c0]Enter Password[/color]\n\n[color=#c0c0c0]You have 1 attempt remaining before termination[/color]"
			},
			{
				"character":"rex",
				"position":"right",
				"text":"\n\n     '9985998207709208'"
			},
			{
				"character":"squidgy",
				"position":"left",
				"text":"[color=#00c0c0]Password accepted[/color]\n\n[color=#C000C0][wave amp=30 freq=10]User Rino-Sapian is cleared for gravity lift[/wave][/color]"
			}
		],
	"rex1_screen":
		[
			{
				"character":"squidgy",
				"position":"left",
				"text":"[color=#00c0c0]All personnel: [/color][color=#c0c0c0]Access to the Tower through the gravity lift is password protected[/color]\n\n[color=#c000c0][shake rate=10 level=10]Failed attempts will result in death[/shake][/color]"
			}
		],

	"die_bitch":
		[
			{
				"character":"rex",
				"position":"right",
				"text":"\n\n[color=#c000c0][shake rate=5 level=10]     Die, evil hag![/shake][/color]"
			}
		],
	"jetpac_boss":
		[
			{
				"character":"squidgy",
				"position":"left",
				"text":"I like it! But surely with a Jetpac like that you need to test it out?\n\nHow about meeting some deadly gas pockets..."
			},
			{
				"character":"squidgy",
				"position":"left",
				"text":"\n\n[color=#C000C0][wave amp=30 freq=10]But rest assured, the big moments are going to come, you will get your time...[/wave][/color]"
			}
		],
	"start_final":
		[
			{
				"character":"squidgy",
				"position":"left",
				"text":"\n\nWelcome to the penthouse Rino, such a lovely view isn't it?"
			},
			{			
				"action":"clear_portraits",
			},
			{
				"character":"rex",
				"position":"right",
				"text":"\nI only care for your death, spawn of the human evil.\n\n[color=#c000c0][shake rate=5 level=10]You will fall just like them[/shake][/color]"
			},
			{
				"character":"squidgy",
				"position":"left",
				"text":"Evil you say? There is only one thing on this planet more powerful than evil, and that's us.\n[color=#C000C0][wave amp=30 freq=10]\nI look forward to our battle[/wave][/color]"
			}
		],
	"lose_weapons":
		[
			{
				"character":"squidgy",
				"position":"left",
				"text":"What was that, you've lost all your weapons. Just when all the occupants of this lovely tower wanted to give you a send off to remember...\n\n[color=#C000C0][wave amp=30 freq=10]mwahahaha[/wave][/color]"
			},
			{
				"character":"rex",
				"position":"right",
				"text":"\n\n[color=#c000c0][shake rate=5 level=10]      Weapons do not define me[/shake][/color]"
			}
		],
	"start_battle" :
		[
		{
			"character":"squidgy",
			"position" :"left",
			"text":"\nI don't know who thought to mate a thick skinned ungulate with the human parasite, but I congratulate them!",
		},
		{			
			"action":"clear_portraits",
		},
		{
			"character":"rex",
			"position" :"right",
			"text":"\nYour words mean nothing evil hag, I will find and destroy you"
		},	
		{			
			"action":"clear_portraits",
		},
		{
			"character":"squidgy",
			"position" :"left",
			"text":"Do you not realise that killing the humans does nothing but amuse me?\n\n[color=#C000C0]Once the Tower is ready,and I bore of them,I shall remove your petulant species from existance too![/color]",
		},
		{
			"character":"squidgy",
			"position" :"left",
			"text":"Anyway I have things to do. But, why don't you stay a little longer and say hello to a little something I made earlier\n\n[color=#C000C0][wave amp=30 freq=10]mwahahaha[/wave][/color]",
		},
		{			
			"action":"clear_portraits",
		}
		],
	"end_battle" :
		[
		{
			"character":"squidgy",
			"position" :"left",
			"text":"Bravo rino-sapian! Seems I underestimated you.\n\nBut that was just a prototype and I have plenty time to tinker",
		},
		{			
			"action":"clear_portraits",
		},
		{
			"character":"rex",
			"position" :"right",
			"text":"\nWhy won't you face me? Strong is fighting. It's hard and it's painful, but if you're too much of a coward for that, then just die"
		},	
		{			
			"action":"clear_portraits",
		},
		{
			"character":"squidgy",
			"position" :"left",
			"text":"\n\n[color=#C000C0][wave amp=30 freq=10]You're such a fiesty thing[/wave][/color]\n\nAll in good time my little friend, all in good time...",
		},
		{
			"character":"squidgy",
			"position" :"left",
			"text":"But where are my manners: Welcome to the upper-basement of Zenith tower, I hope you like what I've done with the place.\n[color=#C000C0][wave amp=30 freq=10]Remember to wipe your feet, everything is still shiny and new[/wave][/color]",
		},
		{			
			"action":"clear_portraits",
		}
		]
	}
const TUTORIAL_TEXT = {
	"training_deadend_rex": 
		[{
			"character": "rex",
			"position":"right",
			"text":"This tunnel is blocked,There must be another way. I will keep trying for hidden paths...[color=#00C0C0][wave amp=30 freq=10]\n\nHave I missed any already![/wave][/color]"
		}
		],
	"tutorial_whichtutorial_rex":
		[{
			"character":"rex",
			"position":"right",
			"text" : "I sense going left will help me acclimatise after this long journey to Zenith\n\nBut right will lead me into into a battle I am not prepared for..."
		}],
	"tutorial_full_welcome_rex":
		[{
			"character":"rex",
			"position":"right",
			"text":"This display of weaponry humbles me - [color=#00c0c0]a fitting welcome to the Hellmouth![/color]\n\nHowever,I alone will stand against the humans. [color=#c000c0][shake rate=5 level=10]Invincibility force field on![/shake][/color]"
		}],
	"tutorial_welcome":
		[{
			"text": "[color=#00c000]You are about to play Rex. A remake of the 1989 debut game by[/color] [color=#c0c0c0][center]The Light[/center][/color]\n\n[color=#c0c0c0]Comprising,cough,John Anderson,Neil Harris and Richard Allan.[/color]\n\n[color=#00c000]Apologies for ruining such a beautiful game[/color]\n\nCTRL/Fire continues..."
		},
		{
				"text": "When the game starts...\n\nMove left/right:\n[color=#00c000] cursor keys/Joypad D-pad[/color]\n\nJump:\n[color=#00c000] cursor up/Joypad D-pad[/color]\n\nAll other controls, including redefining inputs are explained in the tutorial"
		}		
		],
	"tutorial_movement_basic1":
		[
		{
			"text": "\nApart from this screen, every level in this tutorial, and most of the words, are from the original demo cassette.\n\nThis originally played as a non-interactive screen capture."
		}
		],
	"tutor_beam":
		[
		{
			"text": "Use the beam pads you have just landed on to record position,shown by spinning cap. In event of imminent threat to life, Rex will teleport to last recorded beam pad\n\n[color=#c000c0]Top Tip![/color]\nVisit every beam pad as this is a hard game.\n\n[wave amp=50 freq=3][color=#c000c0]Welcome to the 80's[/color][/wave]",
		},
		{
			"text": "The yellow gauge in your HUD shows how much oxygen is left in the room. After 60 seconds in the main game you will die.\n\n[color=#c0c000]all humans who entered the room also die and no more will return[/color]\n\n[color=#c000c0]If only there was a way round this...[/color]",
			"tag" : "tutor_beam"
		},
		],
	"tutor_basic_gauge":
		[
		{
			"text": "You hear a voice in your head whispering...\n\n[color=#c000c0][shake rate=5 level=10]No one ever made the first jump...[/shake][/color]"
		}],
	"tutor_weapons_colours":
		[{
			"text" : "Each weapon you collect has five energy levels from pink - low, up to white - high.\n\n[color=#00c000]The energy level affects each weapon in a different way[/color]\n\nNote. Blue denotes disabled or not found",
			"tag" : "tutor_weapons_colours"
		},
		{
			"text": "Fire Current weapon:\n[color=#00c000] CTRL\n Joypad button 2\n left mouse[/color]\n\nWeapons are explained later"
		}],
	"tutor_shield":
		[{
			"text" : "Shields:\n[color=#00c000] Cursor down\n Joypad Bumpers[/color]\n\nYour Shield Can destroy the enemy,deflect bullets and lasts 30 seconds [color=#c000c0][shake rate=5 level=10]\n\nbut will deplete rapidly on being hit[/shake][/color]\n\nStand on disc to charge your shield and recharge shields whenever you can!",
			"tag" : "tutor_shield"
		}
		],
	"tutor_charge":
		[		
		{
			"text": "\n[color=#c000c0]A few guidelines on how rex can make the best of his surroundings:[/color]\n\nCollect the bubbles dropped by the enemy to increase your weapons energy as every use depletes the energy level"
		},
		{
			"text" : "\nArrows suggest exits from screen but look for unmarked exits\n\n[color=#c000c0]However If rex falls down a screen without an exit arrow he is forced to beam out[/color]\n\nAlso look out for secret passages that can be shot through.You may have already missed some!"
		},
		{
			"text": "\n\nYou start with 4 lives in the actual game and extra lives are gained every 10,000 points.",
			"tag" : "tutor_lives"
		}
		],
	"tutor_smart":
		[{
			"text": "Smart bomb:\n[color=#00c000] Keyboard C\n Joypad Button 3\n Right Mouse button[/color]\n\nKills most enemies, extra ones are sometimes left by the enemy.\n\nIn training you start with zero",
			"tag" : "tutor_smart"
		}],
	"tutorial_gfx":
		[{
			"text": "\nYou are playing in the new graphics mode.\n\n[color=#00C0C0]Press F7 to toggle modes between this and the original ZX Spectrum game[/color]\n\nGameplay remains the same"
		}],
	"tutor_weapons_full":
		[{
			"text" : "[color=#c000c0]Next an explanation of the five weapon systems rex can use[/color]\n\nRex starts with the standard issue apa150 machine gun. as with the mkii version,power makes the gun more rapid\n\nVisit each drop ship to collect a new weapon.",
			"tag" : "tutor_weapons_colours"
		}],
	"tutorial_tv80":
		[{
		"text":"TV80:\n[color=#00c000]ESC key[/color]\n\nYou can view collectibles and achievements, the high-scores, configure or reset the game, go back to the last check point, and more apparently...\n\n[color=#00c0c0]The Sinclair TV80, flatscreen TV or FTV1 was cutting edge tech in the 80's..."
		}],
	"training_rex2":
		[{
			"text" : "This screen is a sample of of what lies ahead in part 2\n\n[color=#00c0c0]The Great Tower, testament to the ability of mankind to destroy nature in the name of progress.\n\nIt has become overrun by a living organism that is weaved into the very fabric of the building.[/color]"
		}],
	"tutor_rapid1":
		[{
			"text" : "\n\nYou have heard about the hardware.\n\n[color=#00c0c0]Now the effect it has on the enemy\n\nWhere you are now is just the right position![/color]"
		},
		{
			"text" : "[color=#00c000]\n\nAn extra bonus is awarded for rapid hits on the soldiers from the single APA 150 machine gun[/color]\n\nGet that multiplier!"
		}],
	"tutor_rapid2":
		[
		{
			"text" : "[color=#00c000]\n\nAn extra bonus is awarded for rapid hits on the soldiers from the double APA 150 machine gun[/color]\n\nGet that multiplier!"
		}],
	"tutor_laser":
		[{
			"text" : "\n[color=#00c000]The laser can kill a row of the enemy[/color]\n\nMore power gives more follow through. Kill them all!"
		},
		{
			"text" :"In the original game each power level increased laser distance\n\n[color=#00c000]In this game the distance is the same,instead each power level allows it to continue further after each hit.\n\nHope you do not mind![/color]"
		}],
	"tutor_drone":
		[{
			"text" : "\n[color=#00c000]With the drone bi-drectional weapon,More power adds more drones[/color]\n\nGet them in one shot!"
		}],
	"tutor_scatter":
		[{
			"text" : "\n[color=#00c000]Each level of multi-gun adds more bullets, new directions and even enemy aerial homing missiles at yellow level 4.[/color]\n\nTry it out!"
		}],
	"tutorial_bunny_rex":
		[{
			"character":"rex",
			"position":"right",
			"text":"What were those red bouncing things, and why could I never reach them?"
		},
		{
			"character":"rex",
			"position":"right",
			"text":"Is it a demon? No something isn't right there...\n\n[color=#c000c0][shake rate=5 level=10]I've got a theory it could be bunnies![/shake][/color]"
		}
		],
	"tutorial_full_gone":
		[{
			"character":"rex",
			"position":"right",
			"text":"Just what I need, My force field to break! Invincibility or not,I am ready humans.\n\nyou think know who you are,what is to come? [color=#c000c0][shake rate=5 level=10]You have not even begun...[/shake][/color]"
		}],
	"tutorial_bunny":
		[
			{
				"character":"rex",
				"position":"right",
				"text":"What was that bunny...\n\nHow does it keep disappearing...[color=#c000c0][shake rate=5 level=10]I will kill it and find its secret[/shake][/color]"
			}
		],
	"train_game_over": 
		[
			{"text":"You Died.\n\nGood news, game over is disable during training and you did start with less lives.\n\nBad news, the real game does not bode well for you!"}
		],
	"password4":
		[{
			"text": "\n\nYou search the human remains and find the following:\n\n[color=#00c000]  Password: 6031769[/color]"
		}],
	"password2":
		[{
			"text": "\n\nYou search the human remains and find the following:\n\n[color=#00c000]Password: 9985998207709208[/color]"
		}],
	"password3":
		[{
			"text": "\n\nYou search the enemy dome and find the following:\n\n[color=#00c0c0]  Root access[/color]\n    [color=#00c000]POKE 56287,58\n    POKE 56405,201[/color]"
		}],
	}
#0 shield loss
#1 1000 bonus
#2 energy loss
#3 100 rapid
#4 speedup
#if change order, update signals
const DIALOGUE_RANDOM = [
	{"text":"[color=#FF0000][center]\n\nShield Loss[/center][/color]"}, 
	{"text":"[color=#FF0000][center]\n\n1000 Bonus[/center][/color]"},
	{"text":"[color=#FF0000][center]\n\nEnergy Loss[/center][/color]"},
	{"text":"[color=#FFFF00][center]\n\n100 Rapid Shots[/center][/color]"},
	{"text":"[color=#FFFF00][center]\n\nSpeed up[/center][/color]"}
]

const COLOURS = { 
	"black" 		: [0.0,0.0,0.0],
	"blue" 			: [0.0,0.0,192.0/256.0],
	"red"   		: [192.0/256.0,0.0,0.0],
	"magenta"  		: [192.0/256.0,0.0,192.0/256.0],
	"green"   		: [0.0,192.0/256.0,0.0],
	"cyan"   		: [0.0,192.0/256.0,192.0/256.0],
	"yellow"   		: [192.0/256.0,192.0/256.0,0.0],
	"white"   		: [192.0/256.0,192.0/256.0,192.0/256.0],
	"blue_bright"  	: [0.0,0.0,1.0],
	"red_bright"   	: [1.0,0.0,0.0],
	"magenta_bright": [1.0,0.0,1.0],
	"green_bright"  : [0.0,1.0,0.0],
	"cyan_bright"   : [0.0,1.0,1.0],
	"yellow_bright" : [1.0,1.0,0.0],
	"white_bright"  : [1.0,1.0,1.0]
 }

onready var explosion_environment=load("res://entities/particles/Explosion_Miss.tscn")
onready var explosion_standard=load("res://entities/particles/Explosion_Main.tscn")
onready var explosion_shard=load("res://entities/particles/Explosion_Shard.tscn")
onready var bubble=load("res://entities/environment/DeadPickup.tscn")
onready var ingame_dialog=load("res://dialogue/Dialog_rex_ingame.tscn")
onready var ingame_dialogs=load("res://dialogue/Dialog_rex_story.tscn")
onready var ingame_dialogt=load("res://dialogue/Dialog_rex_tutorial.tscn")
onready var missilelock=load("res://entities/environment/MissileLock.tscn")


func _init() -> void:
	init_once()
	
func init_once():
	debug=true
	dialog_parent=null
	in_menu=false
	init()
	
func init():
	game_scene=null
	bullet_time = 1.0
	is_game_over=false
	temp_location=null
	lives=START_LIVES
	training_mode=false
	quick_spawn=false
	birth_invisible_override=false
	invincible=false
	ingame_time_start=0
	highscore_allowed=false
	popup_allowed=true
	humans_killed=0
	biogrowths_killed=0
	game_ended=false
	infiltrate_seen=false
	player_being_born=false

	used_shield_this_game=false
	disallow_options=false

func set_bullet_time(value):
	bullet_time=value
	AudioManager.sfx_mute_override(value!=1.0)	#mute if not 1
