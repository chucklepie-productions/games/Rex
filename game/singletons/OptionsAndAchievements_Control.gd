extends Node2D
signal intro_finished

const INTERFACE_OPENCLOSE_ACTION = "achievement_interface_open_close";
const STD_TIME=10.0
const STD_TEXT=7.0

var tab_container:TabContainer
var bunny_container 
var config_section 
var is_in_intro:=false
var is_in_outro:=false
var hiscoretimer
var anim_next_intro=[9,14,20.5,31,41,47.5,54,59,66,74,84.5,89.5,96,102]
var anim_next_outro=[6,12,19,22.5,31,34.5,41.5,46,51,58.5,64.5,68.5,71.5,80.5,83,94,98.5,102.5,106,116,126]
var anim_next_index=0
var playing_outro_music=false

#const SLOC=Vector2(280,350)
const SLOC=Vector2(480,100)
var audio_changed:=false

onready var game_button_left:Button
onready var game_button_right:Button

onready var crt=$ControlCRT/ColorRect
onready var taba=$ControlTabs/VBoxContainer/TabContainer/TabsAchievement
onready var tabl=$ControlTabs/VBoxContainer/TabContainer/TabsLog
onready var tabc=$ControlTabs/VBoxContainer/TabContainer/TabConfig
onready var tabh=$ControlTabs/VBoxContainer/TabContainer/TabsHighScore
onready var tabb=$ControlTabs/VBoxContainer/TabContainer/TabsBunny
onready var tabg=$ControlTabs/VBoxContainer/TabContainer/TabGame
onready var tabd=$ControlTabs/VBoxContainer/TabContainer/TabDemo
onready var ui_override_sound=$AudioStreamPlayerKeyboard

onready var gametime
onready var menu_buttons_container=$ControlTabs/VBoxContainer/Panel/MarginContainer/CenterContainer/HBoxContainer
onready var tooltip=preload("res://scenes/Tooltip.tscn")
var current_tooltip=null

#core events
func _ready():
	game_button_left=find_node("ButtonGameLeft")
	game_button_right=find_node("ButtonGameRight")
	gametime=find_node("GameTime")
	controller_buttons(false,false,false)
	tab_container=find_node("TabContainer")
	bunny_container=find_node("BunnyPanel")
	config_section=find_node("VBoxContainerConfig")
	visible=false

#high level input
func _gui_input(event):
	if event is InputEventMouseButton:
		get_tree().set_input_as_handled()
		
func _process(delta: float) -> void:
	pass
	
func start_click(scale_multiplier):
	#called by animation to start clicking sound
	if scale_multiplier:
		ui_override_sound.pitch_scale=scale_multiplier
	ui_override_sound.play()
	pass
	
func stop_click():
	#called by animation to start clicking sound
	ui_override_sound.stop()
	
func _input(event: InputEvent) -> void:
	#cannot be in parent as does not work
	var is_toggle:=event.is_action_pressed(INTERFACE_OPENCLOSE_ACTION) && event.is_pressed()

	if is_in_intro || is_in_outro:
		#if in intro but timer might be running
		#then abort here and let the intro do the exit
		if event.is_action_pressed("fire") || event.is_action_pressed("ui_accept"):
			_skip_next_animation()
			return
		
	#has escape been pressed
	if !is_toggle:
		return
		
	#cannot press escape in menu to open, only close
	if Globals.in_menu && !visible:
		return
		
	#is abort allowed
	if !Globals.disallow_options:
		if audio_changed:
			_save_changes()
		_abort_options()
	else:
		pass

func _skip_next_animation():
	stop_click()
	if !is_in_intro && !is_in_outro:
		return

	var time=$AnimationPlayer.current_animation_position
	
	var next
	if is_in_intro:
		next=_get_next_anim_up(anim_next_intro,time)
	else:
		next=_get_next_anim_up(anim_next_outro,time)
		
	if next>0:
		$AnimationPlayer.seek(next,true)
		
func _get_next_anim_up(times:Array,time:float):
	for i in times:
		if i>time:
			return i
	
	return -1.0
	
func _abort_options():
	if !visible && !Globals.popup_allowed:
		#if not allowed and it's closed then do not allow open
		return
		
	set_mode(!visible)
	get_tree().set_input_as_handled()
	return

#gui events
func simulate(button):
	var b:TextureButton=menu_buttons_container.find_node(button)
	if b:
		b.pressed=true
		b.emit_signal("pressed")
		
func _on_TextureButton_pressed(nosound:=false) -> void:
	#achievements
	if !nosound: 
		_play_white_noise(true)
	switch_tab(0)

func _on_TextureButton2_pressed(nosound:=false) -> void:
	#collections
	if !nosound: 
		_play_white_noise(true)
	switch_tab(1)

func _on_TextureButton3_pressed(nosound:=false) -> void:
	#config
	if !nosound: 
		_play_white_noise(true)
	switch_tab(2)

func _on_TextureButton4_pressed(nosound:=false) -> void:
	#bunny game
	if !nosound: 
		_play_white_noise(true)
	switch_tab(4)
	if is_in_intro || is_in_outro:
		return

	bunny_container.propagate_call("set_process",[true])
	bunny_container.propagate_call("set_process_unhandled_input",[true])

func _on_TextureButton5_pressed(nosound:=false) -> void:
	#exit options
	if is_in_intro || is_in_outro:
		#not allowed
		return
		
	if audio_changed:
		_save_changes()
		
	set_mode(false)

func _on_TextureButton6_pressed(nosound:=false) -> void:
	#game config
	if !nosound: 
		_play_white_noise(true)
	switch_tab(5)

func _on_TextureButton7_pressed(nosound:=false) -> void:
	#highscore
	if !nosound: 
		_play_white_noise(true)
	switch_tab(3)
#game buttons quit/checkpoint are handled by gui


func _on_TextureButtonIntro_pressed(nosound:=false) -> void:
	if !nosound: 
		_play_white_noise(true)
	if is_in_intro || is_in_outro:
		return
		
	SceneManager.game_scene_sequence=SceneManager.SCENE_GAME_MODE.JUST_INTRO
	do_intro(true)

func _on_TextureButtonOutro_pressed(nosound:=false) -> void:
	if !nosound: 
		_play_white_noise(true)
	if is_in_intro || is_in_outro:
		return
	SceneManager.game_scene_sequence=SceneManager.SCENE_GAME_MODE.JUST_OUTRO
	do_intro(false)
	
func _on_TabContainer_tab_changed(tab: int) -> void:
	#remove bunny in case it is there
	for i in bunny_container.get_children():
		i.queue_free()
	if tab==2:	#config
		_update_config_view()
	if tab==4:	#bunny
		if GlobalPersist.bunny_unlocked:
			bunny_container.visible=true
			var b=load("res://Bunny/Bunny.tscn")
			var bb=b.instance()
			bb.connect("bunny_quit",self,"_bunny_quit")
			bunny_container.add_child(bb)
		else:
			bunny_container.visible=false
			
	if audio_changed:
		audio_changed=false
		_save_changes()		
			
func _save_changes() -> void:
	#save options then apply
	var wm=config_section.get_node("WindowMode").selected
	var mm=config_section.get_node("MouseButtons").selected
	var r2=config_section.get_node("Rex2Backdrop").pressed
	var tt=config_section.get_node("ToolTips").pressed
	var vm=config_section.get_node("MusicVolume").value
	var vs=config_section.get_node("SoundVolume").value
	var crt_flag=config_section.get_node("CRT").pressed
	
	var graphics=GlobalPersist.windowed_mode!=wm
	var music=GlobalPersist.volume_music!=vm
	
	#save and apply
	if GlobalPersist.windowed_mode!=wm || GlobalPersist.mouse_mode!=mm || GlobalPersist.volume_music!=vm || GlobalPersist.volume_sound!=vs ||  GlobalPersist.rex2_backdrop!=r2 || GlobalPersist.crt_enabled!=crt_flag || GlobalPersist.tooltips!=tt:
		if GlobalPersist.crt_enabled!=crt_flag:
			GlobalPersist.crt_enabled=crt_flag
			_set_crt()

		if GlobalPersist.rex2_backdrop!=r2:
			GlobalTileMap.set_rex2_backdrop(r2)
		GlobalPersist.windowed_mode=wm
		GlobalPersist.mouse_mode=mm
		GlobalPersist.volume_music=vm
		GlobalPersist.volume_sound=vs
		GlobalPersist.rex2_backdrop=r2
		GlobalPersist.tooltips=tt
		GlobalPersist.save_config_world_data(false)
			
		if music:
			pass	#when ready

		if graphics:
			GlobalMethods.screen_change()

func _set_crt():
	var curve:=0.02
	var colour:=0.023
	var grill:=0.53
	var rf:=0.0
	if !GlobalPersist.crt_enabled:
		curve=0.0
		colour=0.0
		grill=1.0
		rf=0.0
	
	crt.material.set_shader_param("crt_curve",curve)
	crt.material.set_shader_param("crt_scan_line_color",colour)
	crt.material.set_shader_param("aperture_grille_rate",grill)
	crt.material.set_shader_param("rf_switch_esque_blur",rf)
	crt.material.set_shader_param("white_noise_rate",0.0)
	
#public
func controller_buttons(_intro,_outro,menu_buttons):
	#text for labels
	var l=game_button_left.find_node("Label")
	var r=game_button_right.find_node("Label")
	if Globals.in_menu:
		l.text="Watch Prologue"
		r.text="Watch Epilogue"
		find_node("TextureRectMenu").visible=true
		find_node("TextureRectInGame").visible=false
		game_button_left.disabled=!GlobalPersist.has_continue
		game_button_right.disabled=!GlobalPersist.has_finished
	else:
		l.text="Go To Last Checkpoint"
		r.text="Quick Quit\n(No prompt)"
		find_node("TextureRectMenu").visible=false
		find_node("TextureRectInGame").visible=true
		#what is available
		#game_button_left.disabled=!GlobalPersist.has_continue
		#game_button_right.disabled=!GlobalPersist.has_finished
		game_button_left.disabled=false
		game_button_right.disabled=false

	#tv80 butons
	var b=[
	find_node("TextureButton6"),
	find_node("TextureButton1"),
	find_node("TextureButton2"),
	find_node("TextureButton7"),
	find_node("TextureButton3"),
	find_node("TextureButton4"),
	find_node("TextureButton5")
	]
	for item in b:
		if item!=null:
			#if item.has_node("Label"):
				#var but=item.get_node("Label")
				#but.visible=menu_buttons
			item.disabled=!menu_buttons
	
func switch_tab(tab):
	_tooltip_hide()

	#images are persisting for some reason so blank them
	$AnimationPlayer.stop()
	
	if tab==5:
		$AnimationPlayer.play("cutscene_default")
		yield($AnimationPlayer,"animation_finished")
		
	#_reset_tabs()
	if tab>=0:
		AudioManager.play_sfx(Globals.SFX_TV80_CLICK)
		$AnimationPlayer.play("change_channel")
		tab_container.current_tab=tab
	else:
		$AnimationPlayer.play("no_channel")
		crt.material.set_shader_param("white_noise_rate",1.0)
		
	bunny_container.propagate_call("set_process",[false])
	bunny_container.propagate_call("set_process_unhandled_input",[false])

func _reset_tabs():
	for t in tab_container.get_children():
		t.visible=false

func _play_white_noise(shortest:=false):
	#done via animation player during opening ov tv80
	#yield(get_tree().create_timer(0.2), "timeout")
	if shortest:
		AudioManager.play_sfx(Globals.SFX_TV80_NOISE2)
	else:
		AudioManager.play_sfx(Globals.SFX_TV80_NOISE)
	
func set_mode(visibility, immediate:=false, screen="6", do_button:=true):
	#main entry point
	#screen (corresponds to buttons): 
	#1 Achievements
	#2 Log
	#3 Options
	#4 Bunny
	#5 ON OFF
	#6 Game (intro/outro or checkpoint/quit)
	#7 High score
	if visibility:
		AudioManager.play_sfx(Globals.SFX_TV80_OPEN)

	else:
		AudioManager.play_sfx(Globals.SFX_TV80_CLOSE)
	_set_crt()
	crt.material.set_shader_param("white_noise_rate",1.0)

	for i in bunny_container.get_children():
		i.queue_free()
		
	if visibility:
		controller_buttons(GlobalPersist.has_continue,GlobalPersist.has_finished,true)

	var q=find_node("ButtonQuit")
	_reset_tabs()
	
	if q:
		if Globals.training_mode:
			q.text="Skip Training (no confirm)"
		else:
			q.text="Quit (no confirmation)"
			
	if visibility:
		hiscoretimer=Time.get_ticks_msec()
		var gt=GlobalMethods.get_game_time_as_string()
		if GlobalPersist.game_mode!=GlobalPersist.GAMEMODE.NORMAL:
			gt+="\nNo High-score: Not Normal game"
		else:
			if !Globals.highscore_allowed:
				gt+="\nNo High-score: Continue mode"
			else:
				gt+="\nHigh-score entry available!"
		if Globals.in_menu:
			gametime.bbcode_text="You can rewatch if you have finished the tutorial or game"
		else:
			gametime.bbcode_text="Game Time: " + gt
				
		visible=true
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_viewport().warp_mouse(Vector2(340,500))
		get_tree().paused=true
		
		if !immediate:
			#ensure caller knows this will yield so may have to yield itself
			$AnimationPlayer.play("reveal")
			yield($AnimationPlayer,"animation_finished")

			
		if do_button:
			find_node("TextureButton"+(screen)).pressed=true
			find_node("TextureButton"+(screen)).emit_signal("pressed",true)
			
		Physics2DServer.set_active(true)	#allow bunny. maybe change this to a flag so is only on when bunny is active
	else:
		#get time in settings then offset it from start time
		var elapsed=Time.get_ticks_msec()-hiscoretimer
		Globals.ingame_time_start+=elapsed
		switch_tab(-1)
		if !immediate:
			$AnimationPlayer.play_backwards("reveal no noise (for backward)")
			#yield($AnimationPlayer,"animation_finished")
			yield(get_tree().create_timer(1), "timeout")
		visible=false
		get_tree().paused=false
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		

#internal
func _update_config_view():
	config_section.get_node("WindowMode").selected=GlobalPersist.windowed_mode
	config_section.get_node("MouseButtons").selected=GlobalPersist.mouse_mode
	config_section.get_node("Rex2Backdrop").pressed=GlobalPersist.rex2_backdrop
	config_section.get_node("ToolTips").pressed=GlobalPersist.tooltips
	config_section.get_node("CRT").pressed=GlobalPersist.crt_enabled
	config_section.get_node("MusicVolume").value=GlobalPersist.volume_music
	config_section.get_node("SoundVolume").value=GlobalPersist.volume_sound

func _bunny_quit():
	set_mode(false)

func do_intro(show_intro:bool):
	#true = show intro
	#false = show outro
	playing_outro_music=false
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	is_in_intro=show_intro
	is_in_outro=!show_intro
	Globals.disallow_options=true
	
	controller_buttons(false,false,false)
	#crt.material.set_shader_param("white_noise_rate",1.0)
	#yield(get_tree().create_timer(6), "timeout")
	game_button_left.pressed=show_intro
	game_button_right.pressed=!show_intro
	
	var demotext=find_node("DemoText",true,true)
	demotext.text=""
	$AnimationPlayer.seek(0.0)
	$AnimationPlayer.play("cutscene_default")
	
	switch_tab(6)
	
	#wait for channel to switch so all effects are normalised
	yield(get_tree().create_timer(2.5), "timeout")
	
	#var demoimg=find_node("CenterContainerDemo")
	
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	#temp
	#show_intro=false
	#is_in_intro=false
	#is_in_outro=true
	#end temp
	
	if show_intro:
		$AnimationPlayer.play("Prologue")
	else:
		AudioManager.stop_music()
		$AnimationPlayer.play("Epilogue")
	
	yield($AnimationPlayer,"animation_finished")
	controller_buttons(GlobalPersist.has_continue,GlobalPersist.has_finished,true)
	is_in_intro=false
	is_in_outro=false
	Globals.disallow_options=false
	
	#if new game then switch to next sequence
	#if not just go back to start of menu
	if !GlobalPersist.has_continue:
		get_tree().paused=false
		SceneManager.sequence_shift()
	else:
		switch_tab(5)

func _play_music_intro():
	AudioManager.play_music(Globals.MUSIC_INTRO,true, 6.0)

func _stop_intro_menu_music():
	AudioManager.stop_music(5.0)
	
func _play_music_outro():
	#we have it multiple times to ensure it starts so only play once
	if !playing_outro_music:
		AudioManager.play_music(Globals.MUSIC_OUTRO,true, 8.0)
		playing_outro_music=true
	
func _tooltip_hide():
	if current_tooltip:
		current_tooltip.close()
		current_tooltip=null
	
func _tooltip_show(text:String,pos_offset:Vector2=Vector2.ZERO, pos_offset_absolute=false):
	#pause mode is on so tooltips do not work in 3.x
	if is_in_intro || is_in_outro || !GlobalPersist.tooltips:
		return
		
	var loc=get_viewport().get_mouse_position()-pos_offset
	if pos_offset_absolute:
		loc=pos_offset
	if current_tooltip!=null:
		current_tooltip.close()
		
	current_tooltip=tooltip.instance()
	add_child(current_tooltip)
	
	current_tooltip.open(text,loc)
	


func _on_TextureButtonOutro_mouse_exited() -> void:
	_tooltip_hide()

func _on_TextureButtonOutro_mouse_entered() -> void:
	_tooltip_show("Watch the epilogue.\n\nESCape to exit, but there will be a delay until the current page is finished.", Vector2(0,200))

func _on_TextureButtonIntro_mouse_entered() -> void:
	_tooltip_show("Watch the prologue.\n\nESCape to exit, but there will be a delay until the current page is finished.", Vector2(0,200))

func _on_TextureButton6_mouse_entered() -> void:
	#Game actions and timer
	_tooltip_show("Perform in-game actions and view current game play time.\n\nPlay time only stops when in the TV80.", Vector2(0,200))

func _on_TextureButton1_mouse_entered() -> void:
	_tooltip_show("Check your achievement progress, learn about the game and earn Speccy badges.", Vector2(0,200))

func _on_TextureButton2_mouse_entered() -> void:
	_tooltip_show("Check your collectibles progress and learn about the game mechanics.", Vector2(0,200))

func _on_TextureButton7_mouse_entered() -> void:
	_tooltip_show("View the online hiscore table.\n\nHosted at:  retrospec.sgn.net\n\nComplete the game on Normal mode to enter it", Vector2(50,200))

func _on_TextureButton3_mouse_entered() -> void:
	_tooltip_show("That CRT filter annoying? Music too loud? The screen shaders not working?\n\nFix them here!", Vector2(100,200))

func _on_TextureButton4_mouse_entered() -> void:
	_tooltip_show("'B' is for? If you see a test card, clearly you don't know. Maybe check the achievements or find something that is not fake.", Vector2(150,200))

func _on_TextureButton5_mouse_entered() -> void:
	_tooltip_show("Show and hide this TV80. TV80 was a miniature 'flatscreen' TV from the great inventor and home computer pioneer, Sir Clive Sinclair (1940-2021)", Vector2(200,200))
	
func _on_TextureButton1_mouse_exited() -> void:
	_tooltip_hide()
func _on_TextureButton3_mouse_exited() -> void:
	_tooltip_hide()
func _on_TextureButton4_mouse_exited() -> void:
	_tooltip_hide()
func _on_TextureButton5_mouse_exited() -> void:
	_tooltip_hide()
func _on_TextureButton7_mouse_exited() -> void:
	_tooltip_hide()
func _on_TextureButton2_mouse_exited() -> void:
	_tooltip_hide()
func _on_TextureButtonIntro_mouse_exited() -> void:
	_tooltip_hide()
func _on_TextureButton6_mouse_exited() -> void:
	_tooltip_hide()


func _on_ToolTips_pressed() -> void:
	AudioManager.play_sfx(Globals.SFX_TV80_CLICK)
	_save_changes()
func _on_WindowMode_item_selected(_index: int) -> void:
	AudioManager.play_sfx(Globals.SFX_TV80_CLICK)
	_save_changes()
func _on_MouseButtons_item_selected(_index: int) -> void:
	AudioManager.play_sfx(Globals.SFX_TV80_CLICK)
	_save_changes()
func _on_CRT_pressed() -> void:
	AudioManager.play_sfx(Globals.SFX_TV80_CLICK)
	_save_changes()
func _on_Rex2Backdrop_pressed() -> void:
	AudioManager.play_sfx(Globals.SFX_TV80_CLICK)
	_save_changes()


func _on_OptionsController_intro_finished():
	pass # Replace with function body.


func _on_Rex2Backdrop_mouse_entered():
		_tooltip_show("If you don't like the blue background in the Tower levels turn it off here",SLOC+Vector2(0,35),true)
func _on_CRT_mouse_entered():
		_tooltip_show("Whether to apply CRT effect in this TV80",SLOC+Vector2(0,80),true)
func _on_ToolTips_mouse_entered():
	_tooltip_show("Turn the menu tips and the popup tips on the TV buttons below off if they are annoying you?",SLOC+Vector2(0,105),true)

func _on_CRT_mouse_exited():
	_tooltip_hide()
func _on_Rex2Backdrop_mouse_exited():
	_tooltip_hide()
func _on_ToolTips_mouse_exited():
	_tooltip_hide()

func _on_MusicVolume_value_changed(value: float) -> void:
	audio_changed=true
	AudioManager.set_music_volume(value/100.0)

func _on_SoundVolume_value_changed(value: float) -> void:
	audio_changed=true
	AudioManager.set_sfx_volume(value/100.0)
