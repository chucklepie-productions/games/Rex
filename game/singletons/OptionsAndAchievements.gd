extends CanvasLayer
const ACHIEVEMENT_DATA_SCRIPT = "res://Achievements/Scripts/AchievementData.gd";

signal reset_to_checkpoint
signal quit_active_game

onready var _controller=$OptionsController

var achievement_data = null;
var achievement_interface
var log_interface 

func _ready():
	achievement_interface=find_node("Achievements")
	log_interface=find_node("Logs")
	achievement_data = load(ACHIEVEMENT_DATA_SCRIPT).new()
	achievement_interface.init(achievement_data.get_achievements());
	log_interface.init(achievement_data.get_achievements());

func show_checkpoint_saved(message="",x_offset:=0):
	achievement_interface.show_checkpoint_saved(message,x_offset)
	
func increment_achievement(achievement_name, amount, amount_string:="", x_offset:=0, sfx_if_already_got:=""):
	achievement_interface.increment_achievement(achievement_data,achievement_name,amount,amount_string,false,x_offset,null,sfx_if_already_got)

func increment_log(achievement_name, amount, amount_string:=""):
	#this will not update the "rex" achievement coded in increment
	#if from log as that code only searches the interface, i.e. log
	log_interface.increment_achievement(achievement_data,achievement_name,amount,amount_string,true,0,achievement_interface)
	
func reset_achievement(achievement_name,reset_if_complete:=false):
	achievement_interface.reset_achievement(achievement_data,achievement_name,reset_if_complete)

func do_intro(show_intro):
	#intro (true) or outro (false)
	_controller.is_in_intro=show_intro
	_controller.is_in_outro=!show_intro
	_controller.set_mode(true,false,"-1",false)
	yield(get_tree().create_timer(3.0), "timeout")
	_controller.do_intro(show_intro)
	
func show_options():
	_controller.set_mode(true,false,"3")

func _on_ButtonCheckpoint_pressed() -> void:
	#doubles as ingame and menu
	if Globals.in_menu:
		#play intro
		AudioManager.play_sfx(Globals.SFX_TV80_CLICK)
		SceneManager.game_scene_sequence=SceneManager.SCENE_GAME_MODE.JUST_INTRO
		do_intro(true)
		return
		
	#go back to checkpoint
	emit_signal("reset_to_checkpoint")
	_controller.set_mode(false)

func set_controller_mode(show:bool, immediate:bool):
	_controller.set_mode(show,immediate)
	AudioManager.play_sfx(Globals.SFX_TV80_CLICK)	
	
func _on_ButtonQuit_pressed() -> void:
	#doubles as ingame and menu
	if Globals.in_menu:
		#play epilogue
		SceneManager.game_scene_sequence=SceneManager.SCENE_GAME_MODE.JUST_OUTRO
		do_intro(false)
		return
		
	#quit game or move to next stage of training
	if Globals.training_mode:
		_controller.set_mode(false)
		yield(get_tree().create_timer(1.0), "timeout")
		_controller.is_in_intro=false
		_controller.is_in_outro=false
		var g=get_tree().root.get_node("GameWithHUD")
		g.free()
		SceneManager.sequence_shift()
		return
		
	emit_signal("quit_active_game")
	_controller.set_mode(false)

func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		if _controller.is_in_intro or _controller.is_in_outro:
			return
		#if _controller.visible or Globals.in_menu:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		return
	elif event is InputEventJoypadButton || event is InputEventKey:
		if !_controller.visible:
			Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _unhandled_input(event: InputEvent) -> void:
	#button from gui handler and mouse detection
	if !_controller.visible:
		return
		
	if !event.is_pressed():
		return
		
	if event.is_action_pressed("options_achievements"):
		_controller.simulate("TextureButton1")
		get_tree().set_input_as_handled()
	elif event.is_action_pressed("options_bunny"):
		_controller.simulate("TextureButton4")
		get_tree().set_input_as_handled()
	elif event.is_action_pressed("options_game"):
		_controller.simulate("TextureButton6")
		get_tree().set_input_as_handled()
	elif event.is_action_pressed("options_highscore"):
		_controller.simulate("TextureButton7")
		get_tree().set_input_as_handled()
	elif event.is_action_pressed("options_log"):
		_controller.simulate("TextureButton2")
		get_tree().set_input_as_handled()
	elif event.is_action_pressed("options_options"):
		_controller.simulate("TextureButton3")
		get_tree().set_input_as_handled()

