extends Node

signal dialog_finished(title)
signal dialog_started(title)
signal dialog_tag(tag)
signal hint_read(meta)

func get_random_spectrum_colour(exclude_list):
	var colours = Globals.COLOURS.keys()
	for c in exclude_list:
		colours.erase(c)
	
	var r = randi()%colours.size()
	return Globals.COLOURS[colours[r]]

func get_as_colour(t):
	#pass in COLORS.magenta, etc.
	var c=Color(t[0],t[1],t[2])
	return c
	
func missile_lock(e_position):
	var cont = assign_temp_location()
	var m = Globals.missilelock.instance()
	cont.add_child(m)
	m.global_position =  e_position
	
func get_spawn_point_node(default_position):
	var location=GlobalPersist.player_spawn_point if GlobalPersist.player_spawn_point!="" else "SpawnStart"
	var node=get_tree().get_root().find_node(location,true,false)
	return node
	
func get_spawn_point(default_position):
	var location=GlobalPersist.player_spawn_point if GlobalPersist.player_spawn_point!="" else "SpawnStart"
	var node=get_tree().get_root().find_node(location,true,false)
	
	if node!=null:
		return node.global_position
	else:
		return default_position

func assign_temp_location():
	if Globals.temp_location==null:
		var root=get_tree().get_root()
		Globals.temp_location=root.find_node("Container_Level_Transient",true,false)
		if Globals.temp_location==null:
			print("ERROR. container_level_transient not found for temp location. If tutorial, this is fine.")
	return Globals.temp_location

func _dropped_hint_pickup(_pickup_type,location, meta):
	do_hint(location,meta)
	
func do_hint(location, meta:String,sfx_open:=true,sfx_close:=true):
	if meta=="" || Globals.TUTORIAL_TEXT.get(meta)==null:
		print("hint meta not found: " + meta)
	else:
		if meta.ends_with("_rex") or meta.ends_with("_queen"):
			sfx_open=false
			#need to do similar for queen
		var loc=location
		if loc!=null:
			loc=GlobalTileMap.get_local_from_global(location)
		var dtype=Globals.DIALOGUE_SIZE.TUTORIAL
		if meta.begins_with("full_") or meta.begins_with("tutorial_full_") or meta.ends_with("_rex"):
			dtype=Globals.DIALOGUE_SIZE.DIALOG
		dialogue_box(dtype,meta,
			 Globals.TUTORIAL_TEXT[meta],
			true,loc,true,0.01,false,true,false,sfx_open,sfx_close)
		emit_signal("hint_read",meta)
			
func do_drop_hint(global_position,bubble_death_drop_tip):
	var node=assign_temp_location()
	var energy=Globals.bubble.instance()
	energy.metadata=bubble_death_drop_tip
	if Globals.game_scene!=null:
		energy.connect("picked_up",Globals.game_scene,"_on_pickup")
	node.call_deferred("add_child",energy)
	energy.call_deferred("start_bouncing",global_position,Globals.PICKUP.HINT,0,0,0)
	
func create_energy_bubble(global_pos:Vector2, ptype:=Globals.PICKUP.LARGE_ENERGY, 
	start_delay:=0.0, bounce_wait:=0.0, life_time:=10, appear_delay:=Globals.BUBBLE_DISPLAY_DELAY):

	if appear_delay>0:
		yield(get_tree().create_timer(appear_delay), "timeout")
		
	var node=assign_temp_location()
	var energy=Globals.bubble.instance()
	randomize()
	node.call_deferred("add_child",energy)
	#random rare choice for pickup
	var rando_picked=false
	if GlobalTileMap.rex_room_manager.can_choose_random():
		#about 40 rooms
		#we want about 6 smart bomb pickups (1 every 7 rooms)
		#we want about 3 random pickups (1 every 13 screens)
		#assume 7 kills per screen on average. 24 is just a random number
		if randi()%40 == 24:
			energy.call_deferred("start_bouncing",global_pos,Globals.PICKUP.SMARTBOMB,start_delay,5,0)
			energy.play_bounce_audio = true
			rando_picked=true
		elif randi()%90 == 24:
			energy.call_deferred("start_bouncing",global_pos,Globals.PICKUP.RANDOMITEM,start_delay,5,0)
			energy.play_bounce_audio = true
			rando_picked=true
	if rando_picked:
		GlobalTileMap.rex_room_manager.can_choose_random_picked()
	else:
		Globals.game_scene.bubble_achievement_start()
		OptionsAndAchievements.increment_log("energy",1)
		energy.call_deferred("start_bouncing",global_pos,ptype,start_delay, bounce_wait,life_time)
		
	#this seems evil and horrible
	if Globals.game_scene!=null:
		energy.connect("picked_up",Globals.game_scene,"_on_pickup")
	
func enemy_explosion(global_pos:Vector2,is_human,shard_type, max_trauma:float, via_smart:bool, tex=null, sfx=""):
	#essentially a standard explosion with or without shards
	#enemy type determines explosion type
	#explosion type:
	#	0 none, 1 single sprite, 2 human. Think that's all we want

	var trauma=max_trauma
	var explosion_type=1	

	if Globals.game_scene!=null && Globals.game_scene.player!=null && Globals.game_scene.player.weapons!=null && Globals.game_scene.player.weapons.current_weapon!=null:
		#weapon determines type - currently not used
		#var wtype=Globals.game_scene.player.weapons.current_weapon.weapon_type

		#override type based on enemy - currently not used
		if via_smart && !is_human:
			explosion_type=1
		elif is_human:
			explosion_type=2
		else:
			trauma*=1.2
						
	var has_trauma=true if trauma>0.0 else false
	
	explosion_standard(global_pos,true,has_trauma,trauma,0,sfx)
	if explosion_type!=0 && shard_type!="":
		explosion_shards(global_pos,shard_type,tex)
	
func explosion_standard(global_pos:Vector2, with_particles=true,shake=false, trauma:float=0.2, zindex_override=0,sfx=""):
	#basic original rex explosion
	var node=assign_temp_location()
	var explosion=Globals.explosion_standard.instance()
	explosion.z_index=50
	node.add_child(explosion)
	explosion.global_position=global_pos
	if zindex_override!=0:
		explosion.z_index=zindex_override
	
	if sfx!="":
		AudioManager.play_sfx(sfx)
	if with_particles:
		explosion.initialise("original")
	else:
		explosion.initialise("original_noparticles")
	explosion.play()
	if shake:
		screen_shake(trauma)
	
#immediately drawn shards, no smoke or anything
func explosion_shards(global_pos:Vector2,
	shard_type,
	tex=null,
	start_delay=0.0,
	shard_time=1.5,
	shard_count=4,
	speed=22,
	gravity=35
):
	#speed=1			#test
	#shard_time=5	#test
	var node=assign_temp_location()
	var explosion=Globals.explosion_shard.instance()
	explosion.z_index=50
	node.add_child(explosion)
	explosion.global_position=global_pos
	explosion.initialise(shard_type, tex,
		start_delay, 
		shard_count,
		speed,
		gravity,
		shard_time
		)
	explosion.play()
		
func explosion_miss(global_pos:Vector2, colour1_array=null, colour2_array=null):
	#self managing
	#default colour is white for colour 1 and 2
	#colour1 sets start colour, colour2 sets end colour
	var node=assign_temp_location()
	var explosion=Globals.explosion_environment.instance()
	
	var particle=explosion.get_node("Particles2D")
	if colour1_array!=null:
		particle.process_material.color_ramp.gradient.colors[0]=Color(colour1_array[0],colour1_array[1],colour1_array[2])
		pass
	if colour2_array!=null:
		particle.process_material.color_ramp.gradient.colors[0]=Color(colour2_array[0],colour2_array[1],colour2_array[2])
		pass
		
	explosion.z_index=50
	node.add_child(explosion)
	explosion.global_position=global_pos
	explosion.play()

func dialogue_box_opinionated(dialog_size,text_dict,open_sfx:=true, close_sfx:=true,pause_game:=false,title:=""):
	dialogue_box(dialog_size,title,text_dict,pause_game,null,true,0.03,false,true,false,open_sfx,close_sfx)
	
func dialogue_box(dialog_size,text_name,text_dict,pause_game:=false,location=null, show_if_original:=true, speed:=0.03, lottery:=false, fade_background:=true, location_force:=false, open_sfx:=true, close_sfx:=true):
	#name and text_dict should always be the same
	#use DIALOGUE_ICON constants for icon as part of string for character
	#if graphics_type==0 && !show_if_original:
	if !show_if_original:
		return
		
	if Globals.dialog_parent==null:
		push_error("dialog overlay not found")
		return
		
	Globals.popup_allowed=false		#no options dialog if dialog showing
	var text=JSON.print(text_dict)
	var dialog
	if dialog_size==Globals.DIALOGUE_SIZE.PICKUP:
		dialog=Globals.ingame_dialog.instance()
	elif dialog_size==Globals.DIALOGUE_SIZE.TUTORIAL:
		dialog=Globals.ingame_dialogt.instance()
	else:
		dialog=Globals.ingame_dialogs.instance()
		location=null
	
	dialog.connect("dialogue_closed",self,"_on_dialogue_closed",[text_name,close_sfx,lottery])
	dialog.connect("dialogue_opened",self,"_on_dialogue_opened",[text_name,open_sfx,lottery])
	dialog.connect("dialogue_tag",self,"_on_dialogue_tag")
	dialog.connect("dialogue_lottery",self,"_on_dialogue_lottery")
	if fade_background && Globals.game_scene!=null :
		#game_scene.fullscreen_shader_grey_advanced(true,true)
		Globals.game_scene.fullscreen_grey_fade(0.01)
	dialog.dialog_script=parse_json(text)
	dialog.pause_mode(true)
	dialog.text_speed=speed
	dialog.lottery_mode=lottery
	Globals.dialog_parent.add_child(dialog)
	var onleft:=false
	var s=GlobalTileMap.found_screen_size/2
	if location!=null:
		#assumes screen co-ords and adds an offset
		#offset right/above player
		var loc
		if !location_force:
			loc=location+Vector2(64,-64)
			#switch positions over halfway points
			if loc.x>s.x:
				loc.x-=144
				onleft=true
			if loc.y<s.y:
				loc.y+=128
		else:
			loc=location
		
		dialog.set_textbubble_location(loc,onleft)
	else:
		dialog.set_textbubble_location(location,false)
		
	get_tree().paused=pause_game
	GlobalTileMap.set_slomo(pause_game,0)

	
func _on_dialogue_closed(title,sfx,lottery):
	if sfx && !lottery:
		AudioManager.play_sfx(Globals.SFX_TV80_CLOSE)
	Globals.popup_allowed=true
	emit_signal("dialog_finished",title)
	if Globals.game_scene!=null:
		Globals.game_scene.fullscreen_shader_grey_advanced(false)
	
	#specific dialogue actions
	if title=="tutorial_full_welcome_rex":
		yield(get_tree().create_timer(0.5), "timeout")
		AudioManager.play_sfx(Globals.SFX_PLAYER_INVINCIBLE)
		var x=GlobalTileMap.get_local_from_global(Globals.game_scene.player.global_position)/GlobalTileMap.found_screen_size
		Globals.game_scene.fullscreen_shock(true,Vector2(x.x,1.0-x.y))
		Globals.invincible=true
		Globals.game_scene.player.update_player_invincible()


func _on_dialogue_opened(title,sfx,lottery):
	if sfx && !lottery:
		AudioManager.play_sfx(Globals.SFX_TV80_OPEN)
	if sfx && lottery:
		AudioManager.play_sfx(Globals.SFX_MENU_NAVIGATE)
	emit_signal("dialog_started",title)
	
func _on_dialogue_tag(tag):
	emit_signal("dialog_tag",tag)
	
func _on_dialogue_lottery(item):
#0 shield loss
#1 1000 bonus
#2 energy loss
#3 100 rapid
#4 speedup
	Globals.game_scene.fullscreen_shader_grey_advanced(false)
	#print("Lottery: " + str(item))
	match item:
		0:
			if Globals.game_scene!=null && Globals.game_scene.player!=null && Globals.game_scene.player.defence!=null:
				var h=Globals.game_scene.player.defence.get_health()
				Globals.game_scene.player.defence.set_new_health(h-20)
		1:
			GlobalPersist.score+=1000
		2:
			if Globals.game_scene!=null && Globals.game_scene.player!=null && Globals.game_scene.hud_ui!=null:
				Globals.game_scene.player.weapons.decrease_current_weapon_energy(4)
				Globals.game_scene.hud_ui.on_energy_pickup()	#just flash the screen
		3:
			if Globals.game_scene!=null && Globals.game_scene.player!=null && Globals.game_scene.player.weapons!=null:
				Globals.game_scene.player.weapons.gain_rapid_fire()
		4:
			if Globals.game_scene!=null && Globals.game_scene.player!=null:
				Globals.game_scene.player.gain_speedup()

func register_game_owner(game:Node):
	Globals.game_scene=game
	
func screen_shake(trauma:float):
	Globals.game_scene.screen_shake(trauma)

func screen_change():
	
	match GlobalPersist.windowed_mode:
		0:
			OS.window_fullscreen=false
			OS.window_maximized=false
			OS.window_size=IntegerResolutionHandler.base_resolution
		1:
			OS.window_fullscreen=false
			OS.window_maximized=true
		2:
			OS.window_fullscreen=true

func _ready() -> void:
	Globals.dialog_parent=get_tree().root.find_node("DialogueOverlay",true,false)

func find_nodes_like(name_start,node_container):
	var items=[]
	var c:Node=Globals.game_scene.find_node(node_container,true,true)
	if c!=null:
		for item in c.get_children():
			if item.name.begins_with(name_start):
				items.append(item)
				
	return items

func take_screenshot(file):
	# Retrieve the captured image
	var image = get_tree().get_root().get_texture().get_data()

	# Flip it on the y-axis (because it's flipped)
	image.flip_y()

	image.save_png(file)

func get_game_mode_colour(mode_override:=-1):
	var val=192.0/255.0
	var mode=GlobalPersist.game_mode
	
	if mode_override!=-1:
		mode=mode_override
		
	match mode:
		GlobalPersist.GAMEMODE.EASY:
			return Color(val,0,val,1)
		GlobalPersist.GAMEMODE.SIMPLE:
			return  Color(0,val,val,1)
		_:
			return  Color(val,0,0,1)

func get_game_time_as_seconds() -> int:
	var elapsed=Time.get_ticks_msec()-Globals.ingame_time_start
	return int(elapsed/1000)
	
func get_game_time_as_string() -> String:
	var gsecs=get_game_time_as_seconds()
	var mins=int(gsecs/60)
	var secs=gsecs%60
	
	return str(mins)+" mins, " + str(secs) + " secs"
	
func get_highscore_code_string():
	var gt="Time: [color=#C0C0C0]"+get_game_time_as_string()+"[/color]"
	if !Globals.game_ended:
		return gt+"\n\n[color=#C000C0]No high-score allowed, you died[/color]"
		
	if GlobalPersist.game_mode!=GlobalPersist.GAMEMODE.NORMAL:
		return gt+"\n\n[color=#C000C0]No high-score allowed, you played on easy[/color]"
		
	if !Globals.highscore_allowed:
		return gt+"\n\n[color=#C000C0]No high-score allowed, you played a continue[/color]"
		
	return gt+"\n\n[color=#C000C0]High-score table success![/color]"
	
func get_highscore_code_string2():
	var string=""
	
	if Globals.game_ended && Globals.highscore_allowed:
		var code=_get_highscore_code()
		OS.clipboard=code
		string="[color=#C0C0C0]Your code is: [/color]\n[color=#00C0C0]"+code+"[/color]\n\nVisit [color=#00C0C0]http://retrospec.sgn.net/[/color] to submit this code, it is also in the clipboard.\n\nYou can view the high score table top 20 in the TV80"
	else:
		string="\nYou can view the high score table in your TV80 in the game, or at the website\n\n[color=#00C0C0]http://retrospec.sgn.net/[/color]"
	return string
	
func _get_highscore_code():
	#temp until done
	return "ABCDEFGH123"

func get_texture_from_region(tex:Texture, region:Rect2) -> Texture:
	if tex==null:
		return null
		
	var img_tex = ImageTexture.new();
	img_tex.create_from_image(tex.get_data().get_rect(region),0);
	return img_tex
	
