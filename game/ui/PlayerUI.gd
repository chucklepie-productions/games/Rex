extends CanvasLayer

signal ui_shown(status,immediate)

#this is minimal code to display and inform children
#nodes are duplicated in partial ui so all code should be in actual weapon boxes not here
#full UI
onready var hud_weapons=$MarginContainer/HBoxContainer/HBoxContainerWeapons
onready var weapon_single:=$MarginContainer/HBoxContainer/HBoxContainerWeapons/Single
onready var weapon_double:=$MarginContainer/HBoxContainer/HBoxContainerWeapons/Double
onready var weapon_laser:=$MarginContainer/HBoxContainer/HBoxContainerWeapons/Laser
onready var weapon_drone:=$MarginContainer/HBoxContainer/HBoxContainerWeapons/Drone
onready var weapon_spray:=$MarginContainer/HBoxContainer/HBoxContainerWeapons/Spray
onready var main_score:=$MarginContainer/HBoxContainer/HBoxContainerMain/VBoxContainer/HBoxContainer/LabelScoreValue
onready var main_lives:=$MarginContainer/HBoxContainer/HBoxContainerMain/VBoxContainer/HBoxContainer/LabelLives
onready var main_shield:Label=$MarginContainer/HBoxContainer/HBoxContainerMain/VBoxContainer/HBoxContainer/LabelShield
onready var hud_smarts=$MarginContainer/HBoxContainer/HBoxContainerSmart
onready var smart_1:=$MarginContainer/HBoxContainer/HBoxContainerSmart/Smart1
onready var smart_2:=$MarginContainer/HBoxContainer/HBoxContainerSmart/Smart2
onready var smart_3:=$MarginContainer/HBoxContainer/HBoxContainerSmart/Smart3
onready var hud_levels:=$MarginContainer/HBoxContainer/HBoxContainerMain/VBoxContainer/HBoxContainer
onready var oxygen:=$MarginContainer/HBoxContainer/HBoxContainerMain/VBoxContainer/HBoxContainer/TextureRectOxygen
onready var gauge:=$MarginContainer/HBoxContainer/HBoxContainerMain/VBoxContainer/Gauge
onready var warning:=$ColorRect
onready var lives_icon=$MarginContainer/HBoxContainer/HBoxContainerMain/VBoxContainer/HBoxContainer/TextureRectLives

var current_weapon=""
var current_flash=0
var current_time_limit=-1
var reset_loc:Vector2
var hud_animating:=0

func _ready() -> void:
	#stopped because affects rooms showing/hiding hud in 'before room' 
	#with immediate mode. if affects game then in before room event
	#call this if not set explicitly
	#show_hud(true)	
	
	warning.visible=false
	if false:
		_test()
	pass
	$AnimationPlayer.play("RESET")
	reset_loc=$MarginContainer.rect_position

func _test():
	var list=[
		["Weapon_Single",false,3],
		["Weapon_Double",false,2],
		["Weapon_Laser",true,1],
		["Weapon_Drone",false,2],
		["Weapon_Spray",false,3]
	]
	if true:
		new_smarts_left(2)
		
	if true:
		_on_new_defence_level(25)
		update_weapons_display(list)
		
	if false:
		for x in range(4):
			_on_new_defence_level(x)
			yield(get_tree().create_timer(1), "timeout")
		for x in range(4,-1,-1):
			_on_new_defence_level(x)
			yield(get_tree().create_timer(1), "timeout")
	if false:
		for x in range(1,6):
			list[0][2]=x
			update_weapons_display(list)
			yield(get_tree().create_timer(1), "timeout")
	if false:
		for x in range(10):
			new_score(x*1000)
			yield(get_tree().create_timer(1), "timeout")
	if false:
		for x in range(5):
			new_smarts_left(x)
			yield(get_tree().create_timer(1), "timeout")
		for x in range(5,0,-1):
			new_smarts_left(x)
			yield(get_tree().create_timer(1), "timeout")

func _on_new_weapon_found(weapons):
	update_weapons_display(weapons)
	
#bubble defence
func _on_new_defence_level(new_level):
	new_level=min(99.0,new_level)
	main_shield.text=str(new_level).pad_zeros(2)
	var colour=Globals.COLOURS.white
	if new_level<16:
		colour=Globals.COLOURS.blue
	elif new_level<32:
		colour=Globals.COLOURS.red
	elif new_level<48:
		colour=Globals.COLOURS.magenta
	elif new_level<64:
		colour=Globals.COLOURS.green
	elif new_level<80:
		colour=Globals.COLOURS.cyan
	elif new_level<96:
		colour=Globals.COLOURS.yellow
	main_shield.add_color_override("font_color",GlobalMethods.get_as_colour(colour))
	
	if new_level==16:
		tutorial("shield")
		yield(get_tree().create_timer(2), "timeout")
		tutorial("off")
	
func new_lives_left(new_life,is_extra:=false):
	main_lives.text=str(new_life).pad_zeros(2)
	if is_extra:
		tutorial("lives")
		yield(get_tree().create_timer(2), "timeout")
		tutorial("off")
		
func new_smarts_left(new_smart):
	#only ever 3 smarts, but if more we show 3
	var s1=1 if new_smart>0 else 0
	var s2=1 if new_smart>1 else 0
	var s3=1 if new_smart>2 else 0
	smart_1.item_change(s1,true)
	smart_2.item_change(s2,true)
	smart_3.item_change(s3,true)
	
func new_score(score):
	main_score.text=str(score).pad_zeros(6)

func flash(speccy_colour=Globals.COLOURS.red):
	if !$MarginContainer.visible:
		return
		
	var colour=GlobalMethods.get_as_colour(speccy_colour)
	for x in range(6):
		if x%2==0:
			warning.color=colour
			warning.color.a=1.0
			warning.visible=true
		else:
			warning.color.a=0.0
		yield(get_tree().create_timer(0.25), "timeout")
	warning.visible=false
		
func dialog_started():
	gauge.dialog_started()

func dialog_ended():
	gauge.dialog_ended()
	
func tutorial(item):
	var audio=true
	match item:
		"weapons":
			#weapons in hud
			$AnimationPlayer.play("tutorial_weapons")
		"shield","lives":
			#shield, lives, score in hud
			$AnimationPlayer.play("tutorial_levels")
		"smart":
			#smart bomb in hud
			$AnimationPlayer.play("tutorial_smart")
		"meter":
			#room meter in hud
			$AnimationPlayer.play("tutorial_gauge")
			pass
		"off":
			audio=false
			$AnimationPlayer.play("tutorial_off")
			dialog_ended()
		_:
			$AnimationPlayer.play("tutorial_off")
			print("ERROR. Not valid tutorial hud item " + item)
			audio=false
	if audio:
		AudioManager.play_sfx(Globals.SFX_UI_HIGHLIGHT)
			
func new_room(time_limit):
	$AnimationPlayer.play("tutorial_off")
	current_time_limit = time_limit
	gauge.start(time_limit)
	
func show_oxygen(is_available):
	oxygen.visible=is_available
	gauge.set_colour_from_oxygen(is_available)

#ui
#if acquired show weapon, if not, no weapon
#higher levels turn blue, i.e. they are not active
func update_weapons_display(weapons_list):
	#0 is name
	#1 is firing (Active weapon - all after this are blue)
	#2 is current level
	#3 is its index in the list
	var found_firing:=false
	var active_weapon_index=-1
	for weapon in weapons_list:
		var current=null
		var level=weapon[2]
		var index=weapon[3]
		found_firing=false
		match weapon[0]:
			"Weapon_Single":
				current=weapon_single
			"Weapon_Double":
				current=weapon_double
			"Weapon_Laser":
				current=weapon_laser
			"Weapon_Drone":
				current=weapon_drone
			"Weapon_Spray":
				current=weapon_spray
			_:
				print("ERROR ui unknown weapon " + weapons_list[0])
		
		#set active weapon for if showing 'current'
		if current.is_active:
			active_weapon_index=index
			current_weapon=weapon[0]
			
		#level 0 is not found
		#level 1 to 5 is the level
		#for ui we want to show as inactive (blue) items after the active weapon
		if weapon[1]:
			found_firing=true

		if current!=null:
			current.item_change(level,found_firing,(index<=active_weapon_index || active_weapon_index==-1))

func show_hud(status,immediate:=false,speed:=1.0):
	#if !status && $MarginContainer.rect_position == reset_loc:
	if !status and hud_animating==-1 && !immediate:
		#want off but already off
		return
	#if status && $MarginContainer.rect_position != reset_loc:
	if status and hud_animating==1  && !immediate:
		#want on but already on
		return
		
	lives_icon.modulate=GlobalMethods.get_game_mode_colour()
	emit_signal("ui_shown",status,immediate)
	$AnimationPlayer.play("tutorial_off")	#reset modulate to normal
	yield($AnimationPlayer,"animation_finished")
	
	$AnimationPlayer.playback_speed=speed
	if status:
		hud_animating=1
		if immediate:
			$AnimationPlayer.play("ShowIconsImmediateOn")
		else:
			AudioManager.play_sfx(Globals.SFX_UI_HUDOPEN)
			$AnimationPlayer.play("ShowIcons")
	else:
		hud_animating=-1
		if immediate:
			$AnimationPlayer.play("ShowIconsImmediateOff")
		else:
			AudioManager.play_sfx(Globals.SFX_UI_HUDCLOSE)
			$AnimationPlayer.play_backwards("ShowIcons")
	
func on_energy_pickup():
	#signal that got energy
	AudioManager.play_sfx(Globals.SFX_UI_FLASH)
	
	if $AnimationPlayer.is_playing():
		return
	$AnimationPlayer.play("Flash")
	
func flash_display():
	#called by game on pickup of energy
	#done via animation player, don't call direct
	var current=null
	current_flash+=1
	if current_flash==1:
		current=weapon_single
	elif current_flash==2:
		current=weapon_double
	elif current_flash==3:
		current=weapon_laser
	elif current_flash==4:
		current=weapon_drone
	elif current_flash==5:
		current=weapon_spray
	elif current_flash==6:
		current=smart_1
	elif current_flash==7:
		current=smart_2
	elif current_flash==8:
		current=smart_3
	if current!=null:
		current.flash(current_flash/15.0)

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	hud_animating=0
	if anim_name=="Flash":
		current_flash=0

