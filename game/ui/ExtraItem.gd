extends Node2D

var speed=5#50
var target=Vector2.ZERO
var life=preload("res://assets/graphics/sheet_ui.sprites/extra_original1.tres")
var smart=preload("res://assets/graphics/sheet_ui.sprites/extra_original3.tres")
var oxy=preload("res://assets/graphics/sheet_ui.sprites/extra_original4.tres")
func _ready() -> void:
	set_process(false)

func start_life(end_position:Vector2):
	target=end_position
	$Sprite.texture=life
	AudioManager.play_sfx(Globals.SFX_PLAYER_GOTUP)
	set_process(true)

func start_smart(end_position:Vector2):
	$Sprite.texture=smart
	target=end_position
	AudioManager.play_sfx(Globals.SFX_PLAYER_GOTUP)
	set_process(true)
	
func start_oxygen(end_position:Vector2):
	$Sprite.texture=oxy
	target=end_position
	AudioManager.play_sfx(Globals.SFX_PLAYER_GOTUP)
	set_process(true)

func _process(delta):
	position=position.move_toward(target,speed*delta)
	speed+=20
	if position.is_equal_approx(target):
		queue_free()
