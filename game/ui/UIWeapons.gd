extends Control

enum ICON_TYPE {SINGLE, DOUBLE, LASER, DRONE, SPRAY,SMART}

export(ICON_TYPE) var current_type:=ICON_TYPE.SINGLE
export(int) var current_level=0		#not acquired
export var is_smart=false
export (Texture) var texture_active
export (Texture) var texture_inactive

var is_active=false
var flash_colours=[
	Globals.COLOURS.yellow,
	Globals.COLOURS.magenta,
	Globals.COLOURS.magenta,
	Globals.COLOURS.red,
	Globals.COLOURS.green,
	Globals.COLOURS.cyan,
	Globals.COLOURS.blue,
]

onready var background:=$ColorRectBackground
onready var icon:=$TextureRectIcon
onready var label:=$LabelValue
onready var weapon_material:Material=$TextureRectIcon.material

var level_colours:=[	Globals.COLOURS.blue,		#not found or cannot be used
						Globals.COLOURS.magenta,
						Globals.COLOURS.green,
						Globals.COLOURS.cyan,
						Globals.COLOURS.yellow,
						Globals.COLOURS.white,
						Globals.COLOURS.red]	#smart

var flash_times
func _ready() -> void:
	item_change(current_level,false)

func flash(delay):
	#called by parent
	flash_times=flash_colours.size()
	if delay>0:
		yield(get_tree().create_timer(.1), "timeout")
	$Timer.start()
	
func item_change(level, show_active, fire_latch_active:=false):
	#main entry point. always update from here
	#show texture if weapon is a lesser one
	current_level=level
	is_active=show_active
	_update(show_active,fire_latch_active)

func _update(_is_active:=false,fire_latch_active:=false):
	#show colour if a level and fire_latch is false
	if is_smart:
		#always same background
		#level 0 is off, 1 is on
		label.visible=false
		_set_background_specific_colour(GlobalMethods.get_as_colour(Globals.COLOURS.red),is_active,fire_latch_active)
		if current_level==0:
			$BackOverlay.visible=true
			icon.visible=false	#doesn't look good disabled icon
		else:
			$BackOverlay.visible=false
			icon.texture=texture_active
			icon.visible=true
			
	else:
		#weapon
		#label
		if GlobalPersist.graphics_type==0:
			label.visible=false
		else:
			label.text=str(current_level)
			
		#icon
		if current_level>0:
			icon.texture=texture_active
			icon.visible=true
			if GlobalPersist.graphics_type!=0:
				label.visible=true
		else:
			icon.visible=false
			label.visible=false
			
		#active status, only if active or early weapon to show what we'd downgrade to
		if !fire_latch_active && !is_active:
			#icon.modulate.a=0.4
			#label.modulate.a=0.4
			label.visible=false
			icon.texture=texture_inactive
		
		_set_background_colour_from_current_level(fire_latch_active)
	
func _set_background_colour_from_current_level(fire_latch_active:=false):
	var colour
	#weapon
	if is_active || (current_level>0 && fire_latch_active):
		colour=level_colours[current_level]
	else:
		colour=level_colours[0]
	
	_set_background_specific_colour(GlobalMethods.get_as_colour(colour),1.0,is_active,fire_latch_active)
	
func _set_background_specific_colour(colour:Color, alpha:=1.0,_is_active:=false,latch_override:=false):
	#and shader for infill
	background.color=colour
	background.color.a=alpha
	if GlobalPersist.graphics_type==0:
		weapon_material.set_shader_param("new_colour",colour)
	else:
		if is_smart:
			weapon_material.set_shader_param("new_colour",GlobalMethods.get_as_colour(Globals.COLOURS.red_bright))
		else:
			if !latch_override && !is_active:
				weapon_material.set_shader_param("new_colour",colour)
			else:
				weapon_material.set_shader_param("new_colour",GlobalMethods.get_as_colour(Globals.COLOURS.yellow))
	


func _on_Timer_timeout() -> void:
	flash_times-=1
	if flash_times<=0:
		item_change(current_level,is_active)
	else:
		background.color=GlobalMethods.get_as_colour(flash_colours[flash_times])
		$Timer.start()
