extends Control

onready var progress_bar = $ProgressGauge
var size
#gauge is self-managing, it is started with room timer
#but then has no part of the room life

const NUM_BLOCKS:=12.0
var block_time:float

func _ready() -> void:
	size=progress_bar.rect_size.x/NUM_BLOCKS
	progress_bar.max_value=NUM_BLOCKS
	set_process(false)
	
func start(room_time:int=60):
	progress_bar.value=100
	if room_time == -1:
		$BlockTimer.stop()
		return
		
	set_process(true)
	block_time=room_time/NUM_BLOCKS
	$BlockTimer.wait_time=block_time
	$BlockTimer.start()
	
func dialog_started():
	$BlockTimer.paused=true
	
func dialog_ended():
	$BlockTimer.paused=false

func stop():
	set_process(false)
	$BlockTimer.stop()

func _on_PulseTimer_timeout() -> void:
	#check value and set new idle time
	#progress bar value is percentage
	#each pulse is progress bar percentage of 1 second
	#each pulse has forward and backward so time is halved
	progress_bar.value-=1
	if progress_bar.value<1:
		stop()
	else:
		$BlockTimer.wait_time=block_time
		$BlockTimer.start()

func set_colour_from_oxygen(has_oxygen):
	if !has_oxygen:
		progress_bar.material.set_shader_param("new_colour",GlobalMethods.get_as_colour(Globals.COLOURS.red))
	else:
		progress_bar.material.set_shader_param("new_colour",GlobalMethods.get_as_colour(Globals.COLOURS.green))
