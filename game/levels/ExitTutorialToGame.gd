extends Area2D

onready var mines=$Mines
onready var mine_real=$Mines/DeadPickup_Pretend_Trigger

var trigger1:=false
func _on_ExitTutorialToGame_body_entered(_body: Node) -> void:
	#go to main game or exit tutorial
	if !GlobalPersist.has_continue:
		GlobalPersist.player_spawn_point="SpawnStart3"
		Globals.quick_spawn=true
	SceneManager.sequence_shift()


func _on_ExitTrigger_body_shape_entered(_body_id: RID, _body: Node, _body_shape: int, local_shape: int) -> void:
	if local_shape==0:
		mine_real.visible=true
		mine_real.enable(true)
		trigger1=true
		#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"training_end1",Globals.DIALOGUE_TEXT["training_end1"],true)
		GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["training_end1"],false, true,true)
		var c=get_node("ExitTrigger/CollisionShape2D")
		c.set_deferred("monitoring",true)
		return
		
	if local_shape==1 && trigger1:
		#make explosion
		GlobalTileMap.current_player.double_lock(true)
		for m in mines.get_children():
			m.visible=true
			m.enable(true)
		trigger1=false
		#GlobalMethods.dialogue_box(Globals.DIALOGUE_SIZE.DIALOG,"training_end2",Globals.DIALOGUE_TEXT["training_end2"],true)
		GlobalMethods.dialogue_box_opinionated(Globals.DIALOGUE_SIZE.DIALOG,Globals.MINIBOSS_TEXT["training_end2"],false, true,true)
		yield(GlobalMethods,"dialog_finished")
		yield(get_tree().create_timer(1.0), "timeout")
		OptionsAndAchievements.increment_achievement("first",1,"")

		GlobalTileMap.rex_room_manager.room_147_auto_trigger()
		yield(get_tree().create_timer(4.5), "timeout")
		
		GlobalTileMap.current_player.double_lock(false)
		mines.visible=false
		for m in mines.get_children():
			m.visible=false
			m.disable(false)
		
