extends Control
class_name DialogNode_Rex, "res://addons/dialogic/Images/icon.svg"
signal dialogue_closed(title)
signal dialogue_opened(title)
signal dialogue_tag(tag)
signal dialogue_lottery(item_index)

var input_next = 'fire'
var dialog_index = 0
var finished = false
var text_speed = 0.02 # Higher = lower speed
var waiting_for_answer = false
var waiting_for_input = false
var lottery_mode=false
var lottery_index=0
var lottery_stop=-1
var lottery_next=false
var lottery_timeout=false
var lottery_exiting=false
var current_page=0

var lottery_speeds=[0.7,0.5,0.25,0.1,0.1,0.1,0.05]
onready var dg_background=$Background
onready var dg_text=$TextBubble
onready var dg_name=$TextBubble/NameLabel
onready var dg_next=$TextBubble/NextIndicator
onready var dg_options=$Options
onready var dg_bubble_text=$TextBubble/RichTextLabel
onready var dg_portraits=$Portraits
onready var dg_input=$TextInputDialog
onready var dg_fx_fadein=$FX/FadeInNode
onready var original_position:Vector2=dg_text.rect_position
onready var portrait_offset=dg_portraits.rect_position-dg_text.rect_position

onready var tween_textbubble:SceneTreeTween
onready var tween_FX:SceneTreeTween

export(String, "Option", "Another one", "Last one") var character_name

export(Resource) var dialog_resource = load("res://addons/dialogic/Resources/DefaultDialogResource.tres")
export(Array, Resource) var dialog_characters
var dialog_script=[]

var unpause:=true

onready var Portrait = load("res://dialogue/Nodes/Portrait.tscn")

func set_textbubble_location(new_location,onleft:bool):
	#set position of box but ensure not off the screen
	#if position is on left of player then adjust by width for true location
	#as position we send is is where we want box beside player
	var new_position=original_position
	if onleft:
		new_location.x-=dg_text.rect_size.x
	var size=get_tree().root.get_visible_rect().size
	if new_location!=null:
		if new_location.x<0:
			new_location.x=0
		if new_location.y<0:
			new_location.y=0
		if new_location.x+new_position.x>size.x:
			new_location.x=size.x-dg_text.rect_size.x
		if new_location.y+dg_text.rect_size.y>size.y:
			new_location.y=size.y-dg_text.rect_size.y
		dg_text.rect_position=new_location
	#portraits are also relative so need to adjust those
	dg_portraits.rect_position=dg_text.rect_position+portrait_offset
	
func pause_mode(keep_running:=true):
	if keep_running:
		pause_mode=Node.PAUSE_MODE_PROCESS
	else:
		pause_mode=Node.PAUSE_MODE_STOP
		
func fade_background(fade_level:=0.5, time:=0.5):
	dg_fx_fadein.modulate = Color(0,0,0,1)
	tween_FX = create_tween()
	tween_FX.tween_property(dg_fx_fadein, "modulate",Color(0,0,0,0),time)
	tween_FX.set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)

	
func file(file_path):
	# Reading a json file to use as a dialog.
	var file = File.new()
	var fileExists = file.file_exists(file_path)
	var dict = []
	if fileExists:
		file.open(file_path, File.READ)
		var content = file.get_as_text()
		file.close()
		
		var p = JSON.parse(content)
		if typeof(p.result) == TYPE_ARRAY:
			return p.result
		else:
			push_error("Invalid json file: " + file_path)
			return null
	else:
		push_error("File " + file_path  + " doesn't exists. ")
	return dict

func parse_text(text):
	# This will parse the text and automatically format some of your available variables
	var end_text = text
	
	# for character variables
	if '{' and '}' in end_text:
		for c in dialog_characters: #dialog_resource.characters:
			if c.name in end_text:
				end_text = end_text.replace('{' + c.name + '}',
					'[color=#' + c.color.to_html() + ']' + c.name + '[/color]'
				)
		
	var c_variable
	for key in dialog_resource.custom_variables.keys():
		c_variable = dialog_resource.custom_variables[key]
		# If it is a dictionary, get the label key
		if typeof(c_variable) == TYPE_DICTIONARY:
			if c_variable.has('label'):
				if '.value' in end_text:
					end_text = end_text.replace(key + '.value', c_variable['value'])
				end_text = end_text.replace('[' + key + ']', c_variable['label'])
		# Otherwise, just replace the value
		else:
			end_text = end_text.replace('[' + key + ']', c_variable)
	return end_text

func _ready():
	randomize()
	current_page=0
	# Checking if the dialog should read the code from a external file
	if dialog_resource.dialog_json != '':
		dialog_script = file(dialog_resource.dialog_json)
	
	# Check if dialog has a valid resource file
	#if not dialog_resource or not dialog_resource.characters:
	#	print("You must provide a valid DialogResource")
	#	return
	
	# Setting everything up for the node to be default
	dg_name.text = ''
	dg_background.visible = false
	load_dialog()

func _input(event: InputEvent) -> void:
	#hack to get mouse button working for dialogue boxes
	if event is InputEventMouseButton:
		_unhandled_input(event)
	
func _unhandled_input(event: InputEvent) -> void:
	if !event.is_pressed():
		return
		
	if lottery_mode:
		if event.is_action_pressed(input_next) || event.is_action_pressed("ui_accept") || lottery_timeout:
			get_tree().set_input_as_handled()
			lottery_stop=dialog_index-1
			_exit_lottery()
	else:
		if event.is_action_pressed(input_next) || event.is_action_pressed("ui_accept"):
			get_tree().set_input_as_handled()
			if tween_textbubble and tween_textbubble.is_running():
				# Skip to end if key is pressed during the text animation
				tween_textbubble.set_speed_scale(10.0)
				finished = true
			else:
				if waiting_for_answer == false and waiting_for_input == false:
					load_dialog()

func _exit_lottery():
	if lottery_exiting:	#we re-enter on auto select
		return
	lottery_exiting=true
	AudioManager.play_sfx(Globals.SFX_GAME_GOTSOMETHING)
	yield(get_tree().create_timer(2.0), "timeout")
	emit_signal("dialogue_lottery",lottery_stop)
	if unpause:
		get_tree().paused=false
		GlobalTileMap.set_slomo(false)
	queue_free()

func _process(_delta):
	dg_next.visible = finished
	if lottery_mode:
		dg_next.visible=true
	# Multiple choices
	if waiting_for_answer:
		dg_options.visible = finished
	else:
		dg_options.visible = false
	
	if !lottery_mode:
		pass
	else:
		var time=lottery_speeds[lottery_index]
		if lottery_next:
			$LotteryTimer.stop()
			$LotteryTimer.wait_time=time
			$LotteryTimer.start()
			lottery_next=false
			load_dialog()
			
func hide_dialog():
	visible = false

func show_dialog():
	visible = true
	emit_signal("dialogue_opened")

func start_text_tween():
	# This will start the animation that makes the text appear letter by letter
	var tween_duration = text_speed * dg_bubble_text.get_total_character_count()

	tween_textbubble  = create_tween()
	
	dg_bubble_text.percent_visible=0.0
	tween_textbubble.tween_property(dg_bubble_text, "percent_visible",1.0, tween_duration)
	tween_textbubble.set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)
	tween_textbubble.tween_callback(self,"_tween_finished")


func update_name(name_string, color='FFFFFF'):
	var parsed_name = parse_text(name_string)
	dg_name.bbcode_text = '[color=#' + color + ']' + parsed_name + '[/color]'
	return true

func update_text(text):
	# Updating the text and starting the animation from 0
	dg_bubble_text.bbcode_text = parse_text(text)
	
	# The call to this function needs to be deferred. 
	# More info: https://github.com/godotengine/godot/issues/36381
	dg_bubble_text.percent_visible = 0
	call_deferred("start_text_tween")
	return true

func load_dialog(skip_add = false):
	# This will load the next entry in the dialog_script array.
	if dialog_index < dialog_script.size():
		event_handler(dialog_script[dialog_index])
	else:
		current_page=0	#tell it not to make intermediate sound as easiest way
		if lottery_mode:
			lottery_index+=1
			dialog_index=0
			if lottery_index>=lottery_speeds.size():
				lottery_index=lottery_speeds.size()-1
				yield(get_tree().create_timer(rand_range(1.0,1.5)), "timeout")
				lottery_timeout=true	#stop after a few seconds
				_exit_lottery()
			else:
				event_handler(dialog_script[dialog_index])
		else:
			if unpause:
				get_tree().paused=false
				GlobalTileMap.set_slomo(false)
			emit_signal("dialogue_closed")
			queue_free()
	if skip_add == false:
		dialog_index += 1

func get_character_variable(name):
	if name=="":
		return
	for c in dialog_characters:#dialog_resource.characters:
		if c.name == name:
			return c
	push_error('DialogCharacterResource [' + name + '] does not exists. Make sure the name field is not empty.')
	return false

func reset_dialog_extras():
	dg_name.bbcode_text = ''

func event_handler(event):
	# Handling an event and updating the available nodes accordingly. 
	current_page+=1
	if lottery_stop>=0:
		return
	reset_dialog_extras()
	match event:
		{'show-name'}:
			var name=dg_name
			name.visible=event['show-name']
			
		{'text'}, {'text', 'name'}, {'text','tag'}:
			show_dialog()
			finished = false
			update_text(event['text'])
			if event.has('name'):
				update_name(event['name'])
			if event.has('tag'):
				emit_signal("dialogue_tag",event['tag'])
				
			if current_page>1:
				if !lottery_mode:	#lottery plays its own sound
					AudioManager.play_sfx(Globals.SFX_MENU_NAVIGATE)	#play sound intermediate text
				
		{'text', 'character'}, {'text', 'character', ..}:
			show_dialog()
			finished = false
			var character_data = get_character_variable(event['character'])
			
			if character_data:
				if character_data.intro_ditty:
					AudioManager.play_sfx(character_data.intro_ditty)
				dg_portraits.visible=true
				update_name(character_data.name, character_data.color.to_html())
				var exists = false
				var existing
				for portrait in dg_portraits.get_children():
					if portrait.character_data == character_data:
						exists = true
						existing = portrait
					else:
						portrait.focusout()
						
				if exists:
					existing.focus()
				if exists == false:
					var p = Portrait.instance()
					p.character_data = character_data
					#p.debug = true
					if event.has('position'):
						p.init(event['position'])
					else:
						p.init('left') # Default character position
					dg_portraits.add_child(p)
					p.fade_in()
					#no sound, presume all characters have a sound
			else:
				AudioManager.play_sfx(Globals.SFX_MENU_NAVIGATE)	#play sound intermediate text
				var name=dg_name
				for portrait in dg_portraits.get_children():
					portrait.fade_out()
				name.visible=false
			
			update_text(event['text'])
		{'question', ..}:
			show_dialog()
			finished = false
			waiting_for_answer = true
			if event.has('name'):
				update_name(event['name'])			
			update_text(event['question'])
			for o in event['options']:
				var button = Button.new()
				button.text = o['label']
				if event.has('variable'):
					button.connect("pressed", self, "_on_option_selected", [button, event['variable'], o])
				else:
					# Checking for checkpoints
					if o['value'] == '0':
						button.connect("pressed", self, "change_position", [button, int(event['checkpoint'])])
					else:
						# Continue
						button.connect("pressed", self, "change_position", [button, 0])
				dg_options.add_child(button)
		{'input', ..}:
			show_dialog()
			finished = false
			waiting_for_input = true
			update_text(event['input'])
			dg_input.window_title = event['window_title']
			dg_input.popup_centered()
			dg_input.connect("confirmed", self, "_on_input_set", [event['variable']])
		{'action'}:
			if event['action'] == 'game_end':
				get_tree().quit()
			if event['action'] == 'clear_portraits':
				for p in dg_portraits.get_children():
					p.fade_out()
				
				dialog_index += 1
				load_dialog(true)
			if event['action'] == 'focusout_portraits':
				for p in dg_portraits.get_children():
					p.focusout()
				dialog_index += 1
				load_dialog(true)
		{'scene'}:
			get_tree().change_scene(event['scene'])
		{'background'}:
			dg_background.visible = true
			dg_background.texture = load(event['background'])
			dialog_index += 1
			load_dialog(true)
		{'fade-in'}:
			dg_fx_fadein.modulate = Color(0,0,0,1)
			tween_FX = create_tween()
			tween_FX.tween_property($FX/FadeInNode, "modulate",Color(0,0,0,0),event['fade-in'])
			tween_FX.set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)
			dialog_index += 1
			load_dialog(true)
			
		{'sound'}:
			print('Play sound here: ', event)
			dialog_index += 1
			load_dialog()
		_:
			hide_dialog()
			print('Other event. ', event)

func _on_input_set(variable):
	var input_value = dg_input.get_node("LineEdit").text
	if input_value == '':
		dg_input.popup_centered()
	else:
		dialog_resource.custom_variables[variable] = input_value
		waiting_for_input = false
		dg_input.get_node("LineEdit").text = ''
		dg_input.disconnect("confirmed", self, '_on_input_set')
		dg_input.visible = false
		load_dialog()
		print('[!] Input selected: ', input_value)
		print('[!] dialog variables: ', dialog_resource.custom_variables)

func reset_options():
	# Clearing out the options after one was selected.
	for option in dg_options.get_children():
		option.queue_free()

func change_position(i, checkpoint):
	print('[!] Going back ', checkpoint, i)
	print('    From ', dialog_index, ' to ', dialog_index - checkpoint)
	waiting_for_answer = false
	dialog_index += checkpoint
	print('    dialog_index = ', dialog_index)
	reset_options()
	load_dialog()

func _on_option_selected(option, variable, value):
	dialog_resource.custom_variables[variable] = value
	waiting_for_answer = false
	reset_options()
	load_dialog()
	print('[!] Option selected: ', option.text, ' value= ' , value)
	#print(dialog_resource.custom_variables)

func _on_Tween_tween_completed(_object, _key):
	finished = true

func _on_TextInputDialog_confirmed():
	pass # Replace with function body.


func _on_LotteryTimer_timeout() -> void:
	if !lottery_timeout:
		lottery_next=true

func _tween_finished():
	finished = true
