shader_type canvas_item;
uniform vec4 new_colour:hint_color;

void fragment() {
	COLOR = texture(TEXTURE, UV);
	
	if(COLOR.a>0.0) {
	//if(COLOR.r==1.0) {
		COLOR.r=new_colour.r;
		COLOR.g=new_colour.g;
		COLOR.b=new_colour.b;
	}
}
