shader_type canvas_item;

//for flashing and colour change
uniform bool just_black=false;
uniform bool flash_enabled=false;
uniform bool all_white=false;

//white params
uniform vec4 all_white_turn_black:hint_color=vec4(1.0);

//flash params
uniform bool black_is_transparent=false;
uniform bool keep_original_alpha=true;
uniform vec4 flash_colour:hint_color=vec4(1.0,0.0,0.0,1.0);


void fragment() {
	//flash and colour change
	vec4 colour = texture(TEXTURE, UV);
	float d=0.75;	//dull	- or use 0.75
	float b=1.0;	//bright
	if(just_black) {
			if(colour.a>0.1) {
				colour=vec4(0.0,0.0,0.0,1.0);
			}
	} else 
	{
		if(flash_enabled) {
			vec4 flash = flash_colour;
			flash.a=colour.a;
			if(!keep_original_alpha && colour.a>0.0) {
				flash.a=flash_colour.a;
			}
			if(black_is_transparent && colour.r==0.0 && colour.g==0.0 && colour.b==0.0) {
				flash.a=0.0;
			}
			colour=flash;
		} else {
			//if there's a colour turn it white if flag enabled
			//but if it is black then leave it
			//also if all_white_turn_black is not white then turn this colour black
			//this is for things like rex's eye, space ship glass
			if(all_white && colour.a>0.0 && (colour.r!=0.0 || colour.g!=0.0 || colour.b!=0.0)) {
				if(all_white_turn_black!=vec4(1.0)) {
					//exact check not working, must be doing stuff with the floats
					if(abs(colour.r-all_white_turn_black.r)+abs(colour.g-all_white_turn_black.g)+abs(colour.b-all_white_turn_black.b)<0.2) {
						colour.rgb=vec3(1.0,0.0,0.0);
					}
					else {
						colour.rgb=vec3(1.0,1.0,1.0);
					}
				} else {
					colour.rgb=vec3(1.0,1.0,1.0);
				}
			}
		}
	}
	
	COLOR=colour;
}

