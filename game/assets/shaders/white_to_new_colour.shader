shader_type canvas_item;
uniform vec4 new_colour:hint_color;

void fragment() {
	COLOR = texture(TEXTURE, UV);
	
	if(COLOR.r>0.9 && COLOR.g>0.9 && COLOR.b>0.9) {
		COLOR.r=new_colour.r;
		COLOR.g=new_colour.g;
		COLOR.b=new_colour.b;
	}
}