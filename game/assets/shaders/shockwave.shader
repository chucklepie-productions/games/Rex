shader_type canvas_item;

//https://www.youtube.com/watch?v=SCHdglr35pk&t=5s
uniform vec2 centre=vec2(0.5,0.5);
uniform float force=0.1;
uniform float size=0.5;
uniform float thickness=0.1;

void fragment() {
	float ratio=SCREEN_PIXEL_SIZE.x/SCREEN_PIXEL_SIZE.y;
	vec2 scaledUV=(SCREEN_UV-vec2(0.5,0.0))/vec2(ratio,1.0)+vec2(0.5,0.0);
	float mask = (1.0-smoothstep(size-0.1,size,length(scaledUV-centre))) *
			smoothstep(size-0.2,size-thickness,length(scaledUV-centre));
	vec2 disp=normalize(scaledUV-centre)*force*mask;
	COLOR=texture(SCREEN_TEXTURE,SCREEN_UV-disp);
	//COLOR.rgb=vec3(mask);
}