shader_type canvas_item;

uniform bool enabled=false;
uniform bool black_is_transparent=false;
uniform bool keep_original_alpha=true;
uniform vec4 flash_colour:hint_color=vec4(1.0,0.0,0.0,1.0);
uniform bool all_white=false;

//only one shader allowed so adding all white option here for the 'original' graphics mode

void fragment() {
	vec4 colour = texture(TEXTURE, UV);
	if(enabled) {
		vec4 flash = flash_colour;
		flash.a=colour.a;
		if(!keep_original_alpha && colour.a>0.0) {
			flash.a=flash_colour.a;
		}
		if(black_is_transparent && colour.r==0.0 && colour.g==0.0 && colour.b==0.0) {
			flash.a=0.0;
		}
		colour=flash;
	} else {
		if(all_white && colour.a>0.0) {
			colour.rgb=vec3(1.0,1.0,1.0);
		}
	}
	
	COLOR=colour;
}