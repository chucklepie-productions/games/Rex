shader_type canvas_item;

vec4 colour_change(vec4 colour,float timef) {
	if((colour.r>0.2 || colour.g>0.2 || colour.b>=0.2)) {
		colour.r+=clamp(sin(timef),0,0.8);
	}
	return colour;
}
void fragment() {
	vec4 original=texture(TEXTURE,UV);
	COLOR=colour_change(original,TIME);
}
